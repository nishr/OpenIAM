.. include:: ../../common/replace_math.rst

NRAP-Open-IAM Quality Assurance Documentation
=============================================================

This is the documentation of the Quality Assurance (QA) for the NRAP-Open-IAM.
The NRAP-Open-IAM is an open-source framework for assessing risks associated
with geologic carbon storage.
NRAP-Open-IAM evaluates GCS risk using an integrated assessment modeling
approach, where models representing GCS components (e.g., reservoir, wellbore,
shallow aquifer, atmosphere) can be linked together into a complete GCS system model.
This document provides details of QA for individual components,
coupled components, benchmark tests, and describes the process whereby
NRAP-Open-IAM maintains QA during development.

:download:`NRAP-Open-IAM Quality Assurance Plan <docs/NRAP_Software_Quality_Assurance_Plan_revision_20180306.pdf>`.

.. toctree::
   :maxdepth: 1

   qaqc_dev

   components

   benchmarks

   testsuite

Contributors
============
    Dylan Harp, Los Alamos National Laboratory

    Veronika Vasylkivska, National Energy Technology Laboratory

    Diana Bacon, Pacific Northwest National Laboratory

    Yingqi Zhang, Lawrence Berkeley National Laboratory

    Kayyum Mansoor, Lawrence Livermoore National Laboratory

    Jaisree Iyer, Lawrence Livermoore National Laboratory

    Ernest Lindner, National Energy Technology Laboratory

.. Indices and tables
.. ==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
