.. seal_horizon:
.. include:: ../../common/replace_math.rst

Seal Horizon Model
==================

Class documentation
--------------------
.. autoclass:: openiam.SealHorizon

Unittests
----------
.. autoclass:: iam_test.Tests
   :members: test_seal_horizon
   :noindex:

Additional QA documentation
---------------------------
Additional information about the Seal Flux model behind the Seal Horizon component
can be found in the model user guide :cite:`Lindner2022` available for download
:download:`here <docs/seal_flux_user_manual.pdf>`.

.. bibliography:: ../../bibliography/project.bib
    :filter: docname in docnames
