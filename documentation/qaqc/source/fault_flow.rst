.. fault_flow:
.. include:: ../../common/replace_math.rst

Fault Flow Model
================

Class documentation
--------------------
.. autoclass:: openiam.FaultFlow

Unittests
----------
.. autoclass:: iam_test.Tests
   :members: test_fault_flow
   :noindex:

.. bibliography:: ../../bibliography/project.bib
    :filter: docname in docnames
