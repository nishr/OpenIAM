.. toctree::
    :maxdepth: 2

Use Cases
=========

.. toctree::

In the folder *examples/Control_Files* there are a number of example control files
distributed with the NRAP-Open-IAM tool.
The description of the files is provided in the :num:`Table #cfitab`.

    .. tabularcolumns:: |p{4cm}|p{4cm}|p{8cm}|

    .. csv-table:: Control file examples distributed with NRAP-Open-IAM
       :name: cfitab
       :file: control_file_examples.csv
       :header-rows: 1
       :class: longtable

Beyond control files the described scenarios can be implemented with a help of a Python script.
Example scripts can be found in the *examples/scripts* folder.
