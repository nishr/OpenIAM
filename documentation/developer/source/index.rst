.. |10^-12| replace:: 10\ :sup:`-12`\
.. |cdot| unicode:: U+000B7
.. |CO2| replace:: CO\ :sub:`2`\
.. |C| replace:: :math:`^{\circ}C`
.. |t_i| replace:: t\ :sub:`i`\
.. |m| replace:: :math:`m`
.. |m/s| replace:: :math:`m/s`
.. |m^2| replace:: :math:`m^2`
.. |m^3| replace:: :math:`m^3`
.. |m^4| replace:: :math:`m^4`
.. |m^2/g| replace:: :math:`m^2/g`
.. |m^3/s| replace:: :math:`m^2/s`
.. |kg| replace:: :math:`kg`
.. |kg/s| replace:: :math:`kg/s`
.. |kg/m^3| replace:: :math:`kg/m^3`
.. |L/kg| replace:: :math:`L/kg`
.. |log10| replace:: :math:`\log_{10}`
.. |1/s| replace:: :math:`1/s`
.. |1/s^2| replace:: :math:`1/s^2`
.. |1/m| replace:: :math:`1/m`
.. |mol/L| replace:: :math:`mol/L`
.. |Pa| replace:: :math:`Pa`
.. |MPa| replace:: :math:`MPa`
.. |Pa^-1| replace:: :math:`Pa^{-1}`
.. |Pa/s| replace:: :math:`Pa/s`
.. |Pa/s^2| replace:: :math:`Pa/s^2`
.. |Pa*s| replace:: :math:`Pa{\cdot}s`
.. |microg/L| replace:: :math:`{\mu}g/L`
.. |millig/L| replace:: :math:`mg/L`
.. |times| unicode:: U+000D7
.. |y = ax^2 + bx + c| replace:: *y* = *ax* \ :sup:`2`\ + *bx* + *c*

Welcome to OpenIAM Developer's Guide!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. include:: introduction.rst

.. include:: ../../user/source/installation.rst

.. include:: coding_logistics.rst

.. include:: code_documentation.rst

.. include:: git_introduction.rst

.. include:: tests_and_examples.rst

.. include:: bibliography.rst
