.. NRAPOpenIAM_web documentation master file, created by
   sphinx-quickstart on Thu May 14 11:06:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NRAP-Open-IAM
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

`User's Guide website <user_html/index.html>`_

`Developer's Guide website <dev_html/index.html>`_

`Software Quality Assurrance website <qaqc_html/index.html>`_

Links to PDF versions of documents
----------------------------------

:download:`User's Guide <doc/OpenIAM_user.pdf>`

:download:`Developer's Guide <doc/OpenIAM_dev.pdf>`

:download:`Software Quality Assurrance <doc/OpenIAM_QAQC.pdf>`


.. Indices and tables
.. ==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
