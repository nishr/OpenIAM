# OpenIAM Control File Example 1
# To run this file, use the command (\ for Windows / for Mac or Linux):
#  python ../../source/openiam/openiam_cf.py --file ControlFile_ex1.yaml
# This example links an analytical reservoir model (Nordbotten et al., 2011)
# with a cemented wellbore model.
# The leakage from 4 leaky wellbores connected to the simple reservoir model
# is computed.
# This example produces three plots, the first two plots is of the CO2 leakage
# into the intermediate aquifer (aquifer1) and the shallow aquifer (aquifer2).
# The third plot is of the pressure in the reservoir at the wellbore locations.
# Last Modified: May 21, 2020
#-------------------------------------------------
ModelParams:
    # Time is in years
    EndTime: 50    # EndTime and TimeStep - 1st option to enter time
    TimeStep: 1.0
    # TimePoints: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] # 2nd option to enter time
    # TimePoints: 'examples/Control_Files/time_points.csv' # 3rd option to enter time
    Analysis: forward
    Components: [SimpleReservoir1,
                 CementedWellbore1]
    OutputDirectory: output/output_ex1_{datetime}
    Logging: Debug
Stratigraphy:
    numberOfShaleLayers:
        vary: False
        value: 3
    # Thickness is in meters
    shale1Thickness:
        min: 500.0
        max: 550.0
        value: 525.0
    shale2Thickness:
        min: 450.0
        max: 500.0
        value: 475.0
    shale3Thickness:
        vary: False
        value: 11.2
    aquifer1Thickness:
        vary: False
        value: 22.4
    aquifer2Thickness:
        vary: False
        value: 19.2
    reservoirThickness:
        vary: False
        value: 51.2
# All coordinates are in meters.
# With the SimpleReservoir the default injection location is at (0, 0)
#-------------------------------------------------
# SimpleReservoir1 is a user defined name for component
# the type SimpleReservoir is the ROM model name
#-------------------------------------------------
SimpleReservoir1:
    Type: SimpleReservoir
    InjectionWell:
        coordx: 10
        coordy: 20
    Parameters:
        injRate: 0.1
    Outputs: [pressure,
              CO2saturation]
#-------------------------------------------------
# Here, 4 leaky wellbores are added using the Cemented wellbore model.
# Two of the wellbores have predetermined locations; locations of another two
# wellbores will be generated randomly within the specified domain.
#-------------------------------------------------
CementedWellbore1:
    Type: CementedWellbore
    Connection: SimpleReservoir1
    Number: 4
    Locations:
        coordx: [100, 540]
        coordy: [100, 630]
    RandomLocDomain:
        xmin: 150
        xmax: 250
        ymin: 200
        ymax: 300
    Parameters:
        logWellPerm:
            min: -14.0
            max: -12.0
            value: -13.0
    Outputs: [CO2_aquifer1,
              CO2_aquifer2,
              CO2_atm,
              brine_aquifer1,
              brine_aquifer2]
#-------------------------------------------------
# Time-series plots of the output observations are
# specified in the next section.
#-------------------------------------------------
Plots:
    #------------------------------------------------------
    # The following commands will plot the CO2 leakage
    # rates to both aquifers.  The plots will be given
    # the names 'CO2_Leakage1' and 'CO2_Leakage2' and saved as 'CO2_Leakage1.png'
    # and 'CO2_Leakage2.png' respectively in the output directory.
    #------------------------------------------------------
    CO2_Leakage1:
        TimeSeries: [CO2_aquifer1]
        subplot:
            ncols: 2
            use: True
    CO2_Leakage2:
        TimeSeries: [CO2_aquifer2]
        subplot:
            ncols: 2
            use: True
    #------------------------------------------------------
    # Next do the same thing for the pressure at the base
    # of the leaky wellbores.
    # A custom title and subtitles for each subplot are added to the produced figure.
    #------------------------------------------------------
    Pressure_plot:
        TimeSeries: [pressure]
        subplot:
            ncols: 2
            use: True
            SimpleReservoir1_000.pressure: 'Pressure at well #1'
            SimpleReservoir1_001.pressure: 'Pressure at well #2'
            SimpleReservoir1_002.pressure: 'Pressure at well #3'
            SimpleReservoir1_003.pressure: 'Pressure at well #4'
        Title: Reservoir Pressure at Wellbore Location
