# -*- coding: utf-8 -*-
import copy
import os
import sys
import logging
import numpy as np
import matplotlib.pyplot as plt

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel, ComponentModel, IAM_DIR, LookupTableReservoir
except ImportError as err:
    print('Unable to load IAM class module: '+ str(err))
import components.seal.seal_model as srom
try:

    import components.seal.seal_units as sunit
    import components.seal.seal_intro as intro
    from components.seal.seal_setup import SETUP_DICT
except ImportError:
    print('\nERROR: Unable to load additional modules for Seal Horizon component\n')
    sys.exit()


SH_SCALAR_OBSERVATIONS = ['CO2_aquifer_total', 'brine_aquifer_total',
                          'mass_CO2_aquifer_total', 'mass_brine_aquifer_total']
SH_GRID_OBSERVATIONS = ['CO2_aquifer', 'brine_aquifer',
                        'mass_CO2_aquifer', 'mass_brine_aquifer']

class SealHorizon(ComponentModel):
    """
    The Seal Horizon component model simulates the flow of |CO2| through a low
    permeability but fractured rock horizon (a "seal" formation) overlying the
    storage reservoir into which |CO2| is injected.

    The rock horizon is represented by a number of "cells" arranged
    (conceptually) in an arbitrary shape grid. A two-phase, relative
    permeability approach is used with Darcy’s law for one-dimensional (1D)
    flow computations of |CO2| through the horizon in the vertical
    direction. The code also allows the simulation of time-dependent processes
    that can influence such flow.

    The model is based on an earlier code, NSealR, created with GoldSim,
    and described in :cite:`Lindner2015`. A stand-alone version of this code in Python
    is also available on the NETL EDX system, described as seal_flux.

    In the NRAP-Open-IAM control file, the type name for the component is SealHorizon.
    The following is a list of the component variables, including the
    variable's title, unit, accepted value range and the default value.

    Reference parameters for each cell:

    * **thickness** [|m|] (10 to 1000) - thickness of the cell (vertically)
      (default: 100)

    * **permeability** [|m^2|] (1.0e-22 to 1.0e-16) - cell equivalent initial
      permeability (default: 1.0e-20)

    * **depth** [|m|] (800 to 9500) - depth to top of seal
      (reservoir top) (default: 1007)

    * **influence** [-] (0 to 1) - permeability influence factor
      (default: 1)

    * **entryPressure** [|Pa|] (100 to 2.0e+6) - entry threshold pressure
      that controls flow into rock (default: 5000)

    Distribution parameters for all cells: for thickness of the seal layer

    * **aveThickness** [|m|] (10 to 1000) - mean of the truncated normal
      distribution for thickness (default: 100)

    * **stdDevThickness** [|m|] (0 to 500) - standard deviation
      of the thickness distribution (default: 20)

    * **minThickness** [|m|] (5 to 100) - minimum thickness; this value
      truncates the distribution and limits lower values (default: 75)

    * **maxThickness** [|m|] (10 to 1000) - maximum thickness; this value
      truncates the distribution and limits higher values (default: 125)

    Note: The setup of the four distribution parameters above is not yet implemented
    in NRAP-Open-IAM, and available only in the stand-alone version
    of the seal horizon model.

    For permeability of the seal layer:

    * **avePermeability** [|m^2|] (1.0e-22 to 1.0e-16) - mean total vertical
      permeability of a lognormal distribution; equivalent value
      for fractured rock (default: 2.5e-20)

    * **stdDevPermeability** [|m^2|] (0 to 1.0e-17) - standard deviation
      of the total vertical permeability distribution (default: 0)

    * **minPermeability** [|m^2|] (1.0e-24 to 1.0e-17) - minimum total vertical
      permeability; this value truncates (censors) the vertical random
      distribution and limits lower values (default: 1.0e-23)

    * **maxPermeability** [|m^2|] (1.0e-20 to 1.0e-16) - maximum total vertical
      permeability; this value truncates (censors) the random distribution and
      limits higher values (default: 1.0e-18)

    Note: The setup of the four distribution parameters above is not yet implemented
    in NRAP-Open-IAM,, and available only in the stand-alone version
    of the seal horizon model.

    * **heterFactor** [-] (1.0e-2 to 100) - increase factor of the permeability
      of cells selected for heterogeneity, if the heterogeneity approach is used
      (default: 0.5).

    Reference parameters for all cells:

    * **aveBaseDepth** [|m|] (800 to 9500) - average depth to base of
      cell/reservoir top (default: 1130)

    * **aveBasePressure** [|Pa|] (1.0e+6 to 6.0e+7) - average pressure
      at seal base (default: 3.2e+7)

    * **aveTemperature** [|C|] (31 to 180) - average temperature of seal
      (default: 50)

    * **salinity** [|ppm|] (0 to 80000) - average salinity of seal
      (default: 2.0e+4)

    * **staticDepth** [|m|] (80 to 9500) - reference depth for computing
      static pressure at top of seal (default: 1000)

    * **staticPressure** [|Pa|] (1.0e+6 to 6.0e+7) - pressure at static
      reference depth for computing pressure at the cell top (default: 1.0e+7).

    Fluid Parameters:

    * **brineDensity** [|kg/m^3|] (880 to 1080) - density of brine phase
      (default: 1007)

    * **CO2Density** [|kg/m^3|] (93 to 1050) - density of |CO2| phase
      (default:  583)

    * **brineViscosity** [|Pa*s|] (1.5e-4 to 1.6e-3) - viscosity of brine phase
      (default: 2.535e-4)

    * **CO2Viscosity** [|Pa*s|] (1.8e-5 to 1.4e-4) - viscosity of |CO2| phase
      (default: 4.387e-5)

    * **CO2Solubility** [|mol/kg|] (0 to 2.04) - solubility of |CO2| phase in brine
      (default: 1.0).

    Two-phase model parameters for LET model:

    * **wetting1** [-] (0.5 to 5) - wetting phase parameter |L| (default: 1)

    * **wetting2** [-] (1 to 30) - wetting phase parameter |E| (default: 10)

    * **wetting3** [-] (0.2 to 3) - wetting phase parameter |T| (default: 1.25)

    * **nonwet1** [-] (0.5 to 5) - nonwetting phase parameter |L| (default: 1.05)

    * **nonwet2** [-] (1 to 30) - nonwetting phase parameter |E| (default: 10)

    * **nonwet3** [-] (0 to 3) - nonwetting phase parameter |T| (default: 1.25)

    * **capillary1** [-] (0.01 to 5) - LET-model parameter |L| for capillary
      pressure (default: 0.2)

    * **capillary2** [-] (0.01 to 30) - LET-model parameter |E| for capillary
      pressure (default: 2.8)

    * **capillary3** [-] (0.01 to 3) - LET-model parameter |T| for capillary
      pressure (default: 0.43)

    * **maxCapillary** [|Pa|] (100 to 2.0e+8) - maximum capillary pressure
      for model (default: 1.0e+7)

    Parameters for BC model:

    * **lambda** [-] (0 to 5) - lambda parameter in Brooks-Corey model (default: 2.5)

    Additional parameters for two-phase flow:

    * **brineResSaturation** [-] (0.01 to 0.35) - residual brine saturation
      (default: 0.2)

    * **CO2ResSaturation** [-] (0.01 to 0.35) - residual |CO2| saturation
      (default: 0.01)

    * **relativeModel** [-] (LET or BC) - relative permeability model (default: LET)

    * **permRatio** [-] (0.1 to 1.5) - ratio of nonwetting to wetting permeability
      (default: 1.0).

    Time-model parameters:

    * **influenceModel** [-] (integer: 0, 1, 2) - time-dependent permeability model
      (default: 0)

    * **totalEffect** [-] (0.01 to 200) - total change in permeability factor
      (default: 0.1)

    * **rateEffect** [-] (0.01 to 0.65) - rate factor for permeability change
      (default: 0.1)

    * **reactivity** [-] (0 to 10) - reactivity of time model (default: 0)

    Rock-type parameters:

    * **clayType** [-] (one of smectite, illite, or chlorite) - clay type for swell
      (default: smectite)

    * **carbonateContent** [%] (0 to 100) - carbonate content of rock matrix
      (default: 8)

    * **clayContent** [%] (0 to 100) - clay mineral content of rock
      (default: 60).

    The possible outputs from the Seal Horizon component are
    leakage rates of |CO2| and brine to aquifer through seal layer. The names
    of the observations are of the form:

    * **CO2_aquifer**, **brine_aquifer** [|kg/s|] - |CO2| and brine leakage rates to
      aquifer through seal layer (individual cells) into overlying aquifer

    * **mass_CO2_aquifer**, **mass_brine_aquifer** [|kg|] - mass of the |CO2|
      and brine leaked through seal layer (individual cells) into overlying aquifer

    * **CO2_aquifer_total**, **brine_aquifer_total** [|kg/s|] - cumulative
      (for all cells) |CO2| and brine leakage rates to aquifer through
      seal layer into overlying aquifer

    * **mass_CO2_aquifer_total**, **mass_brine_aquifer_total** [|kg|] - cumulative
      (for all cells) mass of the |CO2| and brine leaked through seal layer
      into overlying aquifer.
    """

    def __init__(self, name, parent, locX=None, locY=None,
                 area=10000.0, influence_model=0, status=1):
        """
        Constructor method of SealHorizon class

        :param name: name of component model
        :type name: [str]

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel [object]

        :param locX: x-coordinates of each individual cell center
        :type locX: [float]

        :param locY: y-coordinates of each individual cell center
        :type locY: [float]

        :param area: area of each individual cell of interest;
            area can be a scalar if it is the same for all cells
        :type area: [float] or float

        :param influence_model: time model type; can be one of 0, 1, 2
        :type influence_model: [int]

        :param status: status of each individual cell; by default, status is 1,
            i.e. each cell is active.
        :type status: int or [int]

        :returns: SealHorizon class object
        """
        # >TODO: do we need influence_model to be different for each cell?

        model_kwargs = {'time_point': 365.25, 'time_step': 365.25}

        super().__init__(name, parent, model=self.model, model_kwargs=model_kwargs)

        # Check provided cell coordinates; if conditions are satisfied then save.
        if locX and locY:
            if len(locX) == len(locY):  # should have the same length
                # Create array with cell centers of shape (N,2)
                self.cell_xy_centers = np.array([locX, locY]).T
            else:
                msg = ''.join(['Length of array locX is not equal ',
                               'to the length of array locY.'])
                logging.error(msg)
                raise ValueError(msg)
        else:
            self.cell_xy_centers = None

        # Copy default setup
        self.seal_controls = {}
        self.seal_controls = intro.convert_yaml_parameters(
            SETUP_DICT, self.seal_controls)

        # Define dictionary of input parameters and temporal input boundaries
        self.define_pars_bounds()

        if self.cell_xy_centers is not None:
            self.setup_remaining_attributes(area, influence_model, status)

        # Set default parameters
        self.setup_default_pars()

        # Define individual cell parameters
        self.grid_pars_keys = ['depth', 'thickness', 'permeability',
                               'influence', 'entryPressure']
        # Define gridded observations names
        self.grid_obs_keys = SH_GRID_OBSERVATIONS

    def setup_remaining_attributes(self, area=10000.0, influenceModel=0, status=1):
        """
        If locations are provided, setup dependent attributes
        """
        # Determine number of cells
        self.num_cells = self.cell_xy_centers.shape[0]
        self.seal_controls['num_cells'] = self.num_cells

        # Check whether the area argument is a scalar
        if isinstance(area, (int, float)):
            self.area = area*np.ones(self.num_cells)
        else:
            if len(area) == self.num_cells:
                self.area = np.array(area)
            else:
                msg = ''.join(['Length of area array is not equal ',
                               'to the number of cells.'])
                logging.error(msg)
                raise ValueError(msg)

        # Check whether area satisfies the model limits
        if np.any(self.area < self.pars_bounds['area'][0]) or (
                np.any(self.area > self.pars_bounds['area'][1])):
            msg = 'Cell areas do not satisfy model limits [{}, {}].'.format(
                self.pars_bounds['area'][0], self.pars_bounds['area'][1])
            logging.error(msg)
            raise ValueError(msg)

        # Check whether the status argument is a scalar
        if isinstance(status, int):
            self.cell_status = status*np.ones(self.num_cells)
        else:
            if len(status) == self.num_cells:
                self.cell_status = np.array(status)
            else:
                msg = ''.join(['Length of status array is not equal ',
                               'to the number of cells.'])
                logging.error(msg)
                raise ValueError(msg)

        # Save time model choice
        if influenceModel in self.pars_bounds['influenceModel']:
            # self.pars_bounds['influenceModel'] is list of possible values [0, 1, 2]
            self.influence_model = int(influenceModel)
        else:
            msg = 'Invalid value of influence_model argument: {}.'.format(
                influenceModel)
            logging.error(msg)
            raise ValueError(msg)

        # Create grid of cells
        self.grid = []
        for ind in range(self.num_cells):
            # Create a cell
            self.grid.append(
                srom.Cell(x_center=self.cell_xy_centers[ind, 0],
                          y_center=self.cell_xy_centers[ind, 1]))
            # Set area
            self.grid[-1].area = self.area[ind]
            # Set model and cell_status
            self.grid[-1].influenceModel = self.influence_model
            self.grid[-1].status = self.cell_status[ind]

    def setup_default_pars(self):
        """
        Add parameters of the component with default values.
        """
        # Conditions: all cells
        self.add_default_par('salinity',
                             value=SETUP_DICT['Description']['salinity'])
        self.add_default_par('aveTemperature',
                             value=SETUP_DICT['Description']['aveTemperature'])
        self.add_default_par('depth',
                             value=SETUP_DICT['Description']['aveBaseDepth'])
        self.add_default_par('aveBaseDepth',
                             value=SETUP_DICT['Description']['aveBaseDepth'])
        self.add_default_par('aveBasePressure',
                             value=SETUP_DICT['Description']['aveBasePressure'])
        self.add_default_par('staticDepth',
                             value=SETUP_DICT['Conditions']['staticDepth'])
        self.add_default_par('staticPressure',
                             value=SETUP_DICT['Conditions']['staticPressure'])

        # Fluid parameters: all cells
        self.add_default_par('brineDensity',
                             value=SETUP_DICT['Conditions']['brineDensity'])
        self.add_default_par('CO2Density',
                             value=SETUP_DICT['Conditions']['CO2Density'])
        self.add_default_par('brineViscosity',
                             value=SETUP_DICT['Conditions']['brineViscosity'])
        self.add_default_par('CO2Viscosity',
                             value=SETUP_DICT['Conditions']['CO2Viscosity'])
        self.add_default_par('CO2Solubility',
                             value=SETUP_DICT['Conditions']['CO2Solubility'])

        # Permeability: all cells
        scalar = sunit.microd_to_metersq()
        self.add_default_par('permeability',
                             value=scalar*SETUP_DICT['Permeability']['avePermeability'])
        self.add_default_par('avePermeability',
                             value=scalar*SETUP_DICT['Permeability']['avePermeability'])
        self.add_default_par('stdDevPermeability',
                             value=scalar*SETUP_DICT['Permeability']['stdDevPermeability'])
        self.add_default_par('minPermeability',
                             value=scalar*SETUP_DICT['Permeability']['minPermeability'])
        self.add_default_par('maxPermeability',
                             value=scalar*SETUP_DICT['Permeability']['maxPermeability'])
        self.add_default_par('heterFactor',
                             value=SETUP_DICT['Permeability']['heterFactor'])

        # Thickness: all cells
        self.add_default_par('thickness',
                             value=SETUP_DICT['Thickness']['aveThickness'])
        self.add_default_par('aveThickness',
                             value=SETUP_DICT['Thickness']['aveThickness'])
        self.add_default_par('stdDevThickness',
                             value=SETUP_DICT['Thickness']['stdDevThickness'])
        self.add_default_par('minThickness',
                             value=SETUP_DICT['Thickness']['minThickness'])
        self.add_default_par('maxThickness',
                             value=SETUP_DICT['Thickness']['maxThickness'])

        # Relative flow: all cells
        self.add_default_par('brineResSaturation',
                             value=SETUP_DICT['RelativeFlowLimits']['brineResSaturation'])
        self.add_default_par('CO2ResSaturation',
                             value=SETUP_DICT['RelativeFlowLimits']['CO2ResSaturation'])
        self.add_default_par('permRatio',
                             value=SETUP_DICT['RelativeFlowLimits']['permRatio'])
        self.add_default_par('entryPressure',
                             value=SETUP_DICT['CapillaryPressure']['entryPressure'])
        # String variable cannot be parameter so the only way to pass
        # string type parameters is through keyword arguments to the model method
        # LET, default value
        self.model_kwargs['relativeModel'] = SETUP_DICT['RelativeFlowLimits']['relativeModel']

        # BC model: all cells
        self.add_default_par('lambda', value=SETUP_DICT['BrooksCoreyModel']['lambda'])

        # Two-phase model parameters for L-E-T model: all cells
        self.add_default_par('wetting1',
                             value=SETUP_DICT['LETModel']['wetting1'])
        self.add_default_par('wetting2',
                             value=SETUP_DICT['LETModel']['wetting2'])
        self.add_default_par('wetting3',
                             value=SETUP_DICT['LETModel']['wetting3'])
        self.add_default_par('nonwet1',
                             value=SETUP_DICT['LETModel']['nonwet1'])
        self.add_default_par('nonwet2',
                             value=SETUP_DICT['LETModel']['nonwet2'])
        self.add_default_par('nonwet3',
                             value=SETUP_DICT['LETModel']['nonwet3'])
        self.add_default_par('capillary1',
                             value=SETUP_DICT['LETCapillaryModel']['capillary1'])
        self.add_default_par('capillary2',
                             value=SETUP_DICT['LETCapillaryModel']['capillary2'])
        self.add_default_par('capillary3',
                             value=SETUP_DICT['LETCapillaryModel']['capillary3'])
        self.add_default_par('maxCapillary',
                             value=SETUP_DICT['LETCapillaryModel']['maxCapillary'])

        # Time-model parameters: all cells
        self.add_default_par('influenceModel',
                             value=SETUP_DICT['TimeModel']['influenceModel'])
        self.add_default_par('influence',
                             value=SETUP_DICT['TimeModel']['influence'])
        self.add_default_par('totalEffect',
                             value=SETUP_DICT['TimeModel']['totalEffect'])
        self.add_default_par('rateEffect',
                             value=SETUP_DICT['TimeModel']['rateEffect'])
        self.add_default_par('reactivity',
                             value=SETUP_DICT['TimeModel']['reactivity'])
        self.add_default_par('carbonateContent',
                             value=SETUP_DICT['TimeModel']['carbonateContent'])
        self.add_default_par('clayContent',
                             value=SETUP_DICT['TimeModel']['clayContent'])
        # String variable cannot be parameter so the only way to pass
        # string type parameters is through keyword arguments to the model method
        self.model_kwargs['clayType'] = SETUP_DICT['TimeModel'][
            'clayType']  # 'smectite', default value

    def define_pars_bounds(self):
        """ Define dictionaries for limits of mode parameters and temporal inputs."""
        param_bounds = intro.define_input_limits()

        # Define dictionary of boundaries
        self.pars_bounds = dict()

        # Controls
        # For now we don't check these limits: as they relate to the grid/model setup
        self.pars_bounds['startTime'] = param_bounds['start_time']
        self.pars_bounds['endTime'] = param_bounds['end_time']
        self.pars_bounds['timePoints'] = param_bounds['time_points']
        self.pars_bounds['realizations'] = param_bounds['realizations']

        # Geometry parameters: grid parameters
        # For now we don't check these limits: as they relate to the grid/model setup
        self.pars_bounds['gridRows'] = param_bounds['rows']
        self.pars_bounds['gridColumns'] = param_bounds['cols']
        self.pars_bounds['numCells'] = param_bounds['num_cells']
        self.pars_bounds['cellHeight'] = param_bounds['cell_height']
        self.pars_bounds['cellWidth'] = param_bounds['cell_width']
        self.pars_bounds['area'] = param_bounds['area']
        self.pars_bounds['depth'] = param_bounds['base_depth']

        # Conditions: all cells
        self.pars_bounds['salinity'] = param_bounds['salinity']
        self.pars_bounds['aveTemperature'] = param_bounds['temperature']
        self.pars_bounds['aveBaseDepth'] = param_bounds['base_depth']
        self.pars_bounds['aveBasePressure'] = param_bounds['base_pressure']
        self.pars_bounds['staticDepth'] = param_bounds['ref_depth']
        self.pars_bounds['staticPressure'] = param_bounds['ref_pressure']

        # Fluid parameters: all cells
        self.pars_bounds['brineDensity'] = param_bounds['brine_density']
        self.pars_bounds['CO2Density'] = param_bounds['co2_density']
        self.pars_bounds['brineViscosity'] = param_bounds['brine_viscosity']
        self.pars_bounds['CO2Viscosity'] = param_bounds['co2_viscosity']
        self.pars_bounds['CO2Solubility'] = param_bounds['co2_solubility']

        # Permeability: all cells
        scalar = sunit.microd_to_metersq()
        self.pars_bounds['permeability'] = [
            scalar*param_bounds['perm_mean'][0], scalar*param_bounds['perm_mean'][1]]
        self.pars_bounds['avePermeability'] = [
            scalar*param_bounds['perm_mean'][0], scalar*param_bounds['perm_mean'][1]]
        self.pars_bounds['stdDevPermeability'] = [
            scalar*param_bounds['perm_std'][0], scalar*param_bounds['perm_std'][1]]
        self.pars_bounds['minPermeability'] = [
            scalar*param_bounds['perm_min'][0], scalar*param_bounds['perm_min'][1]]
        self.pars_bounds['maxPermeability'] = [
            scalar*param_bounds['perm_max'][0], scalar*param_bounds['perm_max'][1]]
        self.pars_bounds['heterFactor'] = param_bounds['perm_heter_factor']

        # Thickness: all cells
        self.pars_bounds['thickness'] = param_bounds['thickness_ave']
        self.pars_bounds['aveThickness'] = param_bounds['thickness_ave']
        self.pars_bounds['stdDevThickness'] = param_bounds['thickness_std']
        self.pars_bounds['minThickness'] = param_bounds['thickness_min']
        self.pars_bounds['maxThickness'] = param_bounds['thickness_max']

        # Relative flow: all cells
        self.pars_bounds['brineResSaturation'] = param_bounds['resid_brine']
        self.pars_bounds['CO2ResSaturation'] = param_bounds['resid_co2']
        self.pars_bounds['permRatio'] = param_bounds['perm_ratio']
        self.pars_bounds['relativeModel'] = ['BC', 'LET']
        self.pars_bounds['entryPressure'] = param_bounds['entry_pressure']

        # Time-model parameters:all cells
        self.pars_bounds['influenceModel'] = [0, 1, 2]
        self.pars_bounds['influence'] = param_bounds['influence']
        self.pars_bounds['totalEffect'] = param_bounds['total_effect']
        self.pars_bounds['rateEffect'] = param_bounds['rate_effect']
        self.pars_bounds['reactivity'] = param_bounds['reactivity']
        self.pars_bounds['carbonateContent'] = param_bounds['carbonate_content']
        self.pars_bounds['clayContent'] = param_bounds['clay_content']
        self.pars_bounds['clayType'] = ["smectite", "illite", "chlorite"]

        # Two-phase model parameters for L-E-T model: all cells
        self.pars_bounds['wetting1'] = param_bounds['l_wetting']
        self.pars_bounds['wetting2'] = param_bounds['e_wetting']
        self.pars_bounds['wetting3'] = param_bounds['t_wetting']
        self.pars_bounds['nonwet1'] = param_bounds['l_nonwet']
        self.pars_bounds['nonwet2'] = param_bounds['e_nonwet']
        self.pars_bounds['nonwet3'] = param_bounds['t_nonwet']
        self.pars_bounds['capillary1'] = param_bounds['l_capillary']
        self.pars_bounds['capillary2'] = param_bounds['e_capillary']
        self.pars_bounds['capillary3'] = param_bounds['t_capillary']
        self.pars_bounds['maxCapillary'] = param_bounds['max_capillary']

        # BC model: all cells
        self.pars_bounds['lambda'] = param_bounds['zeta']

        # Define dictionary of temporal data limits
        self.temp_data_bounds = dict()
        self.temp_data_bounds['pressure'] = ['Pressure']+param_bounds['reservoir_pressure']
        self.temp_data_bounds['CO2saturation'] = (
            ['CO2 saturation']+param_bounds['reservoir_saturation'])

    def update_seal_controls(self, p):
        """ Update seal_controls with values provided by user."""

        # Model parameters
        if self._parent.time_array is not None:
            self.seal_controls['start_time'] = self._parent.time_array[0] # first time point
            self.seal_controls['end_time'] = self._parent.time_array[-1]  # last time point
            self.seal_controls['time_points'] = len(self._parent.time_array)
        else:
            err_msg = 'Argument time_array has to be defined for system model.'
            logging.error(err_msg)

        # Conditions
        self.seal_controls['salinity'] = p['salinity']
        self.seal_controls['temperature'] = p['aveTemperature']
        self.seal_controls['base_depth'] = p['aveBaseDepth']
        self.seal_controls['base_pressure'] = p['aveBasePressure']
        self.seal_controls['ref_depth'] = p['staticDepth']
        self.seal_controls['ref_pressure'] = p['staticPressure']

        # Fluid properties
        self.seal_controls['co2_density'] = p['CO2Density']
        self.seal_controls['co2_viscosity'] = p['CO2Viscosity']
        self.seal_controls['brine_density'] = p['brineDensity']
        self.seal_controls['brine_viscosity'] = p['brineViscosity']
        self.seal_controls['co2_solubility'] = p['CO2Solubility']

        # Permeability parameters
        scalar = sunit.microd_to_metersq()
        self.seal_controls['perm_mean'] = scalar*p['avePermeability']
        self.seal_controls['perm_std'] = scalar*p['stdDevPermeability']
        self.seal_controls['perm_min'] = scalar*p['minPermeability']
        self.seal_controls['perm_max'] = scalar*p['maxPermeability']
        self.seal_controls['perm_heter_factor'] = p['heterFactor']

        # Thickness parameters
        self.seal_controls['thickness_ave'] = p['aveThickness']
        self.seal_controls['thickness_std'] = p['stdDevThickness']
        self.seal_controls['thickness_min'] = p['minThickness']
        self.seal_controls['thickness_max'] = p['maxThickness']

        # Check type of model for relative permeability
        if p['relativeModel'] == 'BC':
            # BrooksCoreyModel:
            self.seal_controls['zeta'] = p['lambda']
        else:
            # LETModel parameters
            self.seal_controls['l_wetting'] = p['wetting1']
            self.seal_controls['e_wetting'] = p['wetting2']
            self.seal_controls['t_wetting'] = p['wetting3']
            self.seal_controls['l_nonwet'] = p['nonwet1']
            self.seal_controls['e_nonwet'] = p['nonwet2']
            self.seal_controls['t_nonwet'] = p['nonwet3']

            # LET CapillaryPressure:
            self.seal_controls['l_capillary'] = p['capillary1']
            self.seal_controls['e_capillary'] = p['capillary2']
            self.seal_controls['t_capillary'] = p['capillary3']
            self.seal_controls['max_capillary'] = p['maxCapillary']

        # Additional parameters for two-phase flow
        self.seal_controls['resid_brine'] = p['brineResSaturation']
        self.seal_controls['resid_co2'] = p['CO2ResSaturation']
        self.seal_controls['relative_model'] = p['relativeModel']
        self.seal_controls['perm_ratio'] = p['permRatio']

        # Time parameters
        self.seal_controls['rate_effect'] = p['rateEffect']
        self.seal_controls['total_effect'] = p['totalEffect']
        self.seal_controls['reactivity'] = p['reactivity']
        self.seal_controls['clay_content'] = p['clayContent']
        self.seal_controls['carbonate_content'] = p['carbonateContent']
        self.seal_controls['clay_type'] = p['clayType']

    def update_all_parameters(self, scalar_p, **kwarg_p):
        """
        Transform all model parameters into array versions of themselves.
        """
        # Update major arrays
        combined_p = {}
        for key in self.grid_pars_keys:
            if key in kwarg_p:
                combined_p[key] = np.array(kwarg_p[key])
            elif key in scalar_p:
                combined_p[key] = scalar_p[key]*np.ones(self.num_cells)

        for key in ['clayType', 'relativeModel']:
            combined_p[key] = kwarg_p[key]

        for key in scalar_p:
            if key not in self.grid_pars_keys:
                combined_p[key] = scalar_p[key]

        return combined_p

    def update_cell_attributes(self, p):
        """
        Update parameters of all cells in the grid.
        """
        # Loop over each cell.
        for ind in range(self.num_cells):
            self.grid[ind].thickness = p['thickness'][ind]
            self.grid[ind].permeability = p['permeability'][ind]*sunit.metersq_to_microd()
            self.grid[ind].depth = p['depth'][ind]
            self.grid[ind].influence = p['influence'][ind]
            self.grid[ind].entryPressure = p['entryPressure'][ind]

        # Update attributes common to all cells
        srom.Cell.assign_controls(self.seal_controls)

    def check_temporal_inputs(self, time, temp_inputs):
        """
        Check whether temporal data fall within specified boundaries.

        :param time: time point at which temporal input is compared with the
            minimum and maximum allowed values.
        :type time: float

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        """
        debug_msg = 'Temporal inputs of component {} are {}'.format(
            self.name, temp_inputs)
        logging.debug(debug_msg)

        for key, val in temp_inputs.items():
            if (val < self.temp_data_bounds[key][1]) or (
                    val > self.temp_data_bounds[key][2]):
                err_msg = ''.join([
                    'Temporal input {} ({}) of SealHorizon component {} ',
                    'is outside the model range [{}, {}] at time t = {} days']).format(
                        self.temp_data_bounds[key][0].lower(), val, self.name,
                        self.temp_data_bounds[key][1],
                        self.temp_data_bounds[key][2], time)
                logging.error(err_msg)
                raise ValueError('Temporal inputs are outside the ROM limits.')

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        if self.cell_xy_centers is None:
            msg = ''.join(['x- and y-coordinates of cell centers are not '
                           'provided.'])
            logging.error(msg)
            raise ValueError(msg)

        msg = ('Input parameters of {name} component are {p}'.
               format(name=self.name, p=p))
        logging.debug(msg)

        for key, val in p.items():
            if key not in self.pars_bounds:
                msg = ('Parameter {key} not recognized as a SealHorizon input ' +
                       'parameter.')
                msg = msg.format(key=key)
                logging.warning(msg)
            else:
                if ((val < self.pars_bounds[key][0]) or
                        (val > self.pars_bounds[key][1])):
                    msg = 'Parameter {} is out of bounds.'.format(key)
                    logging.warning(msg)

    def check_keyword_parameters(self, **kw_pars):
        """
        Check cell parameters provided as keyword arguments.

        :param kw_pars: input parameters of component model
        :type :
        """
        # Check the values of the gridded parameters for initial time point
        for key in self.grid_pars_keys:
            if key in kw_pars:
                if len(kw_pars[key]) == self.num_cells:

                    # Check that parameters satisfy boundaries
                    par_array = np.array(kw_pars[key])
                    if ((np.any(par_array < self.pars_bounds[key][0])) or
                            (np.any(par_array > self.pars_bounds[key][1]))):
                        msg = ('For some cells parameter ' +
                               '{} is out of boundaries.'.format(key))
                        logging.warning(msg)
                else:
                    msg = ('Length of keyword argument {} '.format(key) +
                           'is not equal to the number of cells.')
                    logging.error(msg)
                    raise ValueError(msg)

        # Check for (string) "clayType"
        if 'clayType' in kw_pars:
            if kw_pars['clayType'] not in ['smectite', 'illite', 'chlorite']:
                msg = "Argument clayType has an unrecognized value: {}.".format(
                    kw_pars['clayType'])
                logging.warning(msg)

                msg = ("Argument clayType will be set to 'smectite'.")
                logging.warning(msg)
                kw_pars['clayType'] = 'smectite'
        else:
            kw_pars['clayType'] = 'smectite'

        if 'relativeModel' in kw_pars:
            if kw_pars['relativeModel'] not in ['LET', 'BC']:
                msg = "Argument relativeModel has an unrecognized value: {}.".format(
                    kw_pars['relativeModel'])
                logging.warning(msg)

                msg = ("Argument relativeModel will be set to 'LET'.")
                logging.warning(msg)
                kw_pars['relativeModel'] = 'LET'
        else:
            kw_pars['relativeModel'] = 'LET'

        return kw_pars

    def connect_with_system(self, component_data, name2obj_dict,
                            locations, system_adapters, **kwargs):
        """
        Code to add SealHorizon to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :returns: None
        """
        # Check cells setup data
        cell_data = component_data['Cells']

        # Get cell centers coordinates
        locX = locations[self.name]['coordx']
        locY = locations[self.name]['coordy']

        # Setup cell centers
        self.cell_xy_centers = np.array([locX, locY]).T

        # Read other parameters
        default_vals = {'area': 10000.0, 'influenceModel': 0, 'status': 1}
        for key in default_vals:
            if key in cell_data:
                if isinstance(cell_data[key], str):
                    filename = os.path.join(IAM_DIR, cell_data[key])
                    data = np.genfromtxt(filename)
                    default_vals[key] = data
                else:
                    default_vals[key] = cell_data[key]

        # Update attributes of the component
        self.setup_remaining_attributes(**default_vals)

        # Add parameters
        if ('Parameters' in component_data) and (component_data['Parameters']):
            # Check for presence of keyword type parameters
            for kwarg_nm in ['relativeModel', 'clayType']:
                if kwarg_nm in component_data['Parameters']:
                    kw_data = component_data['Parameters'][kwarg_nm]
                    # Get the value and remove it from the dictionary
                    if not isinstance(kw_data, dict):
                        self.model_kwargs[kwarg_nm] = component_data[
                            'Parameters'].pop(kwarg_nm)
                    else:
                        self.model_kwargs[kwarg_nm] = component_data[
                            'Parameters'].pop(kwarg_nm)['value']

            for key in component_data['Parameters']:
                if not isinstance(component_data['Parameters'][key], dict):
                    component_data['Parameters'][key] = {
                        'value': component_data['Parameters'][key],
                        'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        # Check whether dynamic kwargs are provided
        if ('DynamicParameters' in component_data) and (
                component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                inp_data = component_data['DynamicParameters'][key]
                if isinstance(inp_data, str):  # if filename is provided
                    inp_data_file_path = os.path.join(IAM_DIR, inp_data)
                    if os.path.isfile(inp_data_file_path):
                        data = np.genfromtxt(inp_data_file_path,
                                             delimiter=",", dtype='f8')
                        # Reshape data to fit the requirements
                        data = data.reshape(data.shape[0], -1)
                        self.add_dynamic_kwarg(key, data)
                    else:
                        err_msg = '{} is not a valid file name.'.format(inp_data)
                        logging.error(err_msg)
                else:
                    # If numerical data is provided
                    # check that the shape of the data is right at least partially
                    data = np.array(inp_data)
                    data = data.reshape(data.shape[0], -1)
                    self.add_dynamic_kwarg(key, inp_data)
        elif 'Connection' in component_data:
            # collectors is a dictionary containing inputs collected into arrays
            collectors = self._parent.collectors
            # Get references to the needed collectors
            cdict1 = collectors['pressure'][self.name]
            cdict2 = collectors['CO2saturation'][self.name]

            connections = np.array([component_data['Connection']]).flatten()
            for connect_nm in connections:
                try:
                    connection = name2obj_dict[connect_nm]
                except KeyError:
                    err_msg = ''.join([
                        'Component {} is not setup in the yaml ',
                        'dict data']).format(connect_nm)
                    logging.error(err_msg)
                    raise KeyError(err_msg)

                for obs_nm in ['pressure', 'CO2saturation']:
                    # Add to be lonked observation of the reservoir component
                    connection.add_obs_to_be_linked(obs_nm)

                # Update list of links to the observations
                cdict1['data'].append(connection.linkobs['pressure'])
                cdict2['data'].append(connection.linkobs['CO2saturation'])

            # Add link to observations to the data of collectors
            self.add_kwarg_linked_to_collection('pressure', cdict1['data'])
            self.add_kwarg_linked_to_collection('CO2saturation', cdict2['data'])

        if 'Outputs' in component_data:
            comp_outputs = component_data['Outputs']
            new_comp_outputs = []
            for obs_nm in comp_outputs:
                if obs_nm in SH_SCALAR_OBSERVATIONS:
                    new_comp_outputs.append(obs_nm)
                    self.add_obs(obs_nm)
                elif obs_nm in SH_GRID_OBSERVATIONS:
                    # Get number of segments
                    num_locs = len(locX)
                    for ind in range(num_locs):
                        augm_obs_nm = obs_nm+'_loc{}'.format(ind+1)
                        self.add_local_obs(augm_obs_nm, obs_nm, 'array', 0)
                        new_comp_outputs.append(augm_obs_nm)
                else:
                    warn_msg = ''.join([
                        '{} is not recognised as observation name ',
                        'of Fault Flow component {}.']).format(obs_nm, self.name)
                    logging.warning(warn_msg)

            component_data['Outputs'] = new_comp_outputs


    def model(self, p, time_point=365.25, time_step=365.25,
              pressure=None, CO2saturation=None, **kwargs):
        """
        :param p: input parameters of SealHorizon component model
        :type p: dict

        param time_point: time point in days at which the leakage rates are
            to be calculated; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param pressure: pressure at the bottom of cells in the reservoir, in Pa;
            by default, its value is None
        :type pressure: [float]

        :param CO2saturation: saturation of |CO2| phase at the bottom
            of cells in the reservoir; by default, its value is None
        :type CO2saturation: [float]

        :Returns: out - dictionary of observations of Seal Horizon model;
        outputs with keys ['CO2_aquifer', 'brine_aquifer',
        'mass_CO2_aquifer', 'mass_brine_aquifer'] are arrays;
        outputs with keys ['CO2_aquifer_total', 'brine_aquifer_total',
        'mass_CO2_aquifer_total', 'mass_brine_aquifer_total'] are scalars.
        """
        # Obtain the default values of the parameters from dictionary of
        # default parameters.
        actual_p = dict([(k, v.value) for k, v in self.default_pars.items()])

        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # Check parameters provided by keyword arguments for initial time.
        msg = 'Provided keyword arguments: {}'.format(kwargs)
        kwarg_p = copy.deepcopy(kwargs)
        if time_point == 0:
            kwarg_p = self.check_keyword_parameters(**kwargs)

            combined_p = self.update_all_parameters(actual_p, **kwarg_p)

            # Update seal_controls attribute with values provided by user
            self.update_seal_controls(combined_p)

            # Update cell properties.
            self.update_cell_attributes(combined_p)

            # Use one of the cells to calculate pressure at the top of cells
            self.press_top = np.zeros(self.num_cells)
            for ind in range(self.num_cells):
                self.press_top[ind] = self.grid[ind].compute_static_pressure(
                    self.seal_controls['ref_pressure'],
                    self.seal_controls['ref_depth'])
                if ind == 0:
                    msg = 'Top pressure: {}'.format(self.press_top[ind])
                    logging.debug(msg)

        # Initialize outputs.
        out = {}
        out['CO2_aquifer'] = np.zeros(self.num_cells)
        out['brine_aquifer'] = np.zeros(self.num_cells)
        out['mass_CO2_aquifer'] = np.zeros(self.num_cells)
        out['mass_brine_aquifer'] = np.zeros(self.num_cells)
        out['CO2_aquifer_total'] = 0.0
        out['brine_aquifer_total'] = 0.0
        out['mass_CO2_aquifer_total'] = 0.0
        out['mass_brine_aquifer_total'] = 0.0

        if time_point == 0.0:
            return out

        for ind in range(self.num_cells):

            # Compute flow rates for each cell - vol./sec.
            flow_rate_co2, flow_rate_brine = self.grid[ind].compute_flow(
                pressure[ind], CO2saturation[ind],
                self.press_top[ind], self.seal_controls)

            if ind == 0:
                msg = ''.join([
                    'Volume flow rates:\n',
                    'CO2 rates: {}\n'.format(flow_rate_co2),
                    'Brine rates: {}\n'.format(flow_rate_brine)])
                logging.debug(msg)

            # Update history of cell
            self.grid[ind].track_history(pressure[ind], flow_rate_co2, time_point)

            # Update influence factors for cell
            self.grid[ind].compute_model_influence(flow_rate_co2)

            # Convert flows from vol/sec to kg/sec
            out['CO2_aquifer'][ind] = flow_rate_co2 * self.grid[ind].CO2Density
            out['brine_aquifer'][ind] = flow_rate_brine * self.grid[ind].brineDensity

            out['mass_CO2_aquifer'][ind] = time_step*out['CO2_aquifer'][ind]
            out['mass_brine_aquifer'][ind] = time_step*out['brine_aquifer'][ind]

        # Calculate cumulative flow rate for all cells
        out['CO2_aquifer_total'] = np.sum(out['CO2_aquifer'])
        out['brine_aquifer_total'] = np.sum(out['brine_aquifer'])

        msg = ''.join(['Outputs at time{}:\n CO2 rate: {}\n Brine rate: {}\n',
                       'Total CO2 rate: {}\n Total brine rate: {}']).format(
                           time_point, out['CO2_aquifer'], out['brine_aquifer'],
                           out['CO2_aquifer_total'], out['brine_aquifer_total'])
        logging.debug(msg)

        return out


if __name__ == "__main__":

    # Setup logging and constants.
    logging.basicConfig(level=logging.DEBUG)
    test_case = 1  # maximum number of test cases

    if test_case == 1:
        # Setup location of lookup table data set
        file_directory = os.sep.join(['..', 'components', 'reservoir',
                                      'lookuptables', 'Kimb_54_sims'])

        if not os.path.exists(os.sep.join([file_directory, 'Reservoir_data_sim01.csv'])):
            msg = ''.join(['\nKimberlina data set can be downloaded ',
                           'from one of the following places:\n',
                           '1. https://edx.netl.doe.gov/dataset/nrap-open-source-iam \n',
                           '2. https://gitlab.com/NRAP/Kimberlina_data  \n',
                           'Check this example description for more information.'])
            logging.error(msg)

        # Define keyword arguments of the system model.
        num_years = 5
        time_array = 365.25 * np.arange(0.0, num_years+1)
        seal_model_kwargs = {'time_array': time_array} # time is given in days

        # Create system model.
        sm = SystemModel(model_kwargs=seal_model_kwargs)

        # Read file with signatures of interpolators and names of files
        # with the corresponding data.
        signature_data = np.genfromtxt(
            os.path.join(file_directory, 'parameters_and_filenames.csv'),
            delimiter=",", dtype='str')

        # Add reservoir component.
        loc_X = [35315.8, 36036.4, 36757.1, 37477.7, 38198.4, 38919, 39639.7,
                 40360.3, 41081, 41801.6, 42522.3, 43242.9, 43963.6, 44684.2, 45404.8]

        loc_Y = 15*[51110.2]

        ltres = sm.add_component_model_object(
            LookupTableReservoir(name='ltres', parent=sm, locX=loc_X, locY=loc_Y))

        ltres.build_and_link_interpolators(file_directory=file_directory,
                                           intr_family='reservoir',
                                           default_values={'salinity': 0.1,
                                                           'temperature': 50.0},
                                           recompute_triangulation=False,
                                           build_on_the_fly=False)

        ltres.add_par('index', value=5, vary=False)

        # Add observations of reservoir component model to be used as input for
        # SealHorizon component
        ltres.add_obs_to_be_linked('pressure', obs_type='grid',
                                   constr_type='array')
        ltres.add_obs_to_be_linked('CO2saturation', obs_type='grid',
                                   constr_type='array')
        # Add local observation: pressure at one of the cells
        ltres.add_local_obs('loc_pressure', grid_obs_name='pressure',
                            constr_type='array', loc_ind=3)

        cell_coord_x = loc_X
        cell_coord_y = loc_Y

        shc = sm.add_component_model_object(
            SealHorizon(name='shc', parent=sm,
                        locX=cell_coord_x, locY=cell_coord_y, area=6.3e+4,
                        influence_model=0, status=1))

        # Add time varying keyword arguments of SealHorizon component
        shc.add_kwarg_linked_to_obs('pressure', ltres.linkobs['pressure'],
                                    obs_type='grid', constr_type='array')
        shc.add_kwarg_linked_to_obs('CO2saturation',
                                    ltres.linkobs['CO2saturation'],
                                    obs_type='grid', constr_type='array')
        # Add local observation of the component
        shc.add_local_obs('loc_CO2_aquifer', grid_obs_name='CO2_aquifer',
                          constr_type='array', loc_ind=3)
        # Add scalar observations
        shc.add_obs('CO2_aquifer_total')
        shc.add_obs('brine_aquifer_total')

        print('Starting simulation...')
        sm.forward()
        print('Simulation is finished.')

        # Collect observations
        pressure = sm.collect_observations_as_time_series(ltres, 'loc_pressure')
        print('Pressure at location 3:', pressure, sep='\n')
        plt.semilogy(time_array/365.25, pressure/1.0e+6, label='Pressure')

        CO2_rate = sm.collect_observations_as_time_series(shc, 'loc_CO2_aquifer')
        print('Rate at location 3:', CO2_rate, sep='\n')
        plt.semilogy(time_array/365.25, CO2_rate, label='Local CO2 rate')

        total_CO2_rate = sm.collect_observations_as_time_series(
            shc, 'CO2_aquifer_total')
        print('Cumulative CO2 rate at all 15 locations:', total_CO2_rate, sep='\n')
        plt.semilogy(time_array/365.25, total_CO2_rate, label='Total CO2 rate')

        total_brine_rate = sm.collect_observations_as_time_series(
            shc, 'brine_aquifer_total')
        print('Cumulative brine rate at all 15 locations:', total_brine_rate, sep='\n')
        plt.semilogy(time_array/365.25, total_brine_rate, label='Total brine rate')
        plt.legend()

    else: # Simple example for one cell.

        # Time constants.
        num_years = 1
        time_array = 365.25 * np.arange(0.0, num_years+1)
        seal_model_kwargs = {'time_array': time_array} # time is given in days

        # Create system model.
        sm = SystemModel(model_kwargs=seal_model_kwargs)
        shc = sm.add_component_model_object(
            SealHorizon(name='shc', parent=sm,
                        locX=[50.0], locY=[550.0], area=[100.0],
                        influence_model=0, status=1))

        # Parameters to vary. Arrays size of keyword arguments coincide with the size
        # of arrays containing coordinates of the cell coordinates
        shc.model_kwargs['thickness'] = [92.89171013411794]
        shc.model_kwargs['permeability'] = [2.5e-2 * sunit.microd_to_metersq()]
        shc.model_kwargs['depth'] = [1037.108289865882]
        shc.model_kwargs['aveBaseDepth'] = [1130.0]
        shc.model_kwargs['aveBasePressure'] = [32000000.0]

        # 2.09157562673603e-09 7.22994065417135e-11 for 92.89171013411794, 1037.108289865882
        # Top pressure 10365363.642856505

        shc.model_kwargs['aveTemperature'] = [50.0]
        shc.model_kwargs['salinity'] = [15000.0]
        shc.model_kwargs['staticDepth'] = [1000.0]
        shc.model_kwargs['staticPressure'] = [10000000.0,]

        # Fluid parameters
        shc.add_par('brineDensity', value=1004.0, vary=False)
        shc.add_par('CO2Density', value=597.8, vary=False)
        shc.add_par('brineViscosity', value=5.634e-4, vary=False)
        shc.add_par('CO2Viscosity', value=4.52e-5, vary=False)
        shc.add_par('CO2Solubility', value=0.0, vary=False)

        # Two-phase model parameters for L-E-T model
        shc.add_par('wetting1', value=1.0, vary=False)
        shc.add_par('wetting2', value=10.0, vary=False)
        shc.add_par('wetting3', value=1.25, vary=False)
        shc.add_par('nonwet1', value=1.05, vary=False)
        shc.add_par('nonwet2', value=3.5, vary=False)
        shc.add_par('nonwet3', value=1.25, vary=False)
        shc.add_par('capillary1', value=0.2, vary=False)
        shc.add_par('capillary2', value=2.8, vary=False)
        shc.add_par('capillary3', value=0.43, vary=False)
        shc.add_par('maxCapillary', value=10000000.0, vary=False)

        # Additional parameters for two-phase
        shc.add_par('permRatio', value=0.6, vary=False)
        shc.add_par('entryPressure', value=5.0e+3, vary=False)
        shc.add_par('brineResSaturation', value=0.15)
        shc.add_par('CO2ResSaturation', value=0.0)

        # Time-model parameters
        shc.add_par('totalEffect', value=0.5, vary=False)
        shc.add_par('rateEffect', value=0.1, vary=False)
        shc.add_par('reactivity', value=8.0, vary=False)
        shc.add_par('carbonateContent', value=5.0, vary=False)
        shc.add_par('clayContent', value=60.0, vary=False)
        shc.model_kwargs['clayType'] = 'smectite'

        shc.add_dynamic_kwarg('CO2saturation', [[0.5], [0.5]])
        shc.add_dynamic_kwarg('pressure', [[3.20E+07], [3.20E+07]])

        shc.add_obs('CO2_aquifer_total')
        shc.add_obs('brine_aquifer_total')

        sm.forward()

        print('Cumulative CO2 flow rate:',
              sm.collect_observations_as_time_series(shc, 'CO2_aquifer_total'))
        print('Cumulative brine flow rate:',
              sm.collect_observations_as_time_series(shc, 'brine_aquifer_total'))

        # The results should be
        #        Cumulative CO2 flow rate: [0.00000000e+00 1.25034391e-06]
        #        Cumulative brine flow rate: [0.00000000e+00 7.25886042e-08]
