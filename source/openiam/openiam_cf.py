# -*- coding: utf-8 -*-
"""
NRAP OpenIAM Control File reader code
Reads YAML formatted control file and runs IAM analysis.
Use: python openiam_cf.py --file path_to_control_file/ControlFileName.yaml

Created: October 11, 2017
Last modified: March 9, 2020

Authors: Seth King, Veronika Vasylkivska
"""
import argparse
import collections
from datetime import datetime
import logging
import math
import os
import pickle
import random
import shutil
import sys

import matplotlib.pyplot as plt
import numpy as np
import yaml

SOURCE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(SOURCE_DIR)

import openiam as iam
import openiam.visualize as iam_vis
from openiam import IAM_DIR


# The following line creates a parser to parse arguments from the command line to the code.
parser = argparse.ArgumentParser(description='YAML control file IAM reader')

# Create a group of mutually exclusive command line arguments.
# parser will make sure that only one of the arguments in the mutually
# exclusive group is present on the command line.
group = parser.add_mutually_exclusive_group(required=False)

# lhs_sample_size and parstudy_divisions are mutually exclusive.
# --lhs and --parstudy are the keys used to indicate which of the parameters
# are given in the command line.
group.add_argument('--lhs', type=int,
                   dest='lhs_sample_size', default=20,
                   help='Latin Hypercube Sampling mode: enter number of samples')
group.add_argument('--parstudy', type=int,
                   dest='parstudy_divisions', default=3,
                   help='Parameter study mode: enter number of parameter partitions')

# Parameter ncpus has default value 1, i.e. by default all simulations are run
# sequentially. If a user wants to run simulations in parallel, he/she has to
# specify the number of cpus to use.
parser.add_argument('--ncpus', type=int, default=1,
                    help='Number of processors to use to run concurrent simulations')

# Parameter file sets the control file to read in.
parser.add_argument('--file', type=str, dest='yaml_cf_name',
                    # default = 'test_CFI',  # to test all control files examples
#                    default = 'test_GUI',  # to test all GUI examples
                    default='../../test/test_control_file.yaml',
                    # default='../../examples/Control_Files/ControlFile_ex18.yaml',
#                    default='../../examples/GUI_Files/01_Forward_SR_CW.OpenIAM',
#                    default='../../examples/GUI_Files/03_LHS_LUT_MSW.yaml',
                    help='NRAP-Open-IAM Control File Name')
parser.add_argument('--binary', type=bool, dest='binary_file',
                    default=False, help='Set to true for binary control file')
args = parser.parse_args()

output_header = "".join(["\nNRAPOpenIAM version: {iam_version}",
                         "\nRuntime: {now} \n"])

pathway_components = ['LookupTableReservoir',
                      'SimpleReservoir',
                      'AnalyticalReservoir',
                      'MultisegmentedWellbore',
                      'CementedWellbore',
                      'OpenWellbore',
                      'GeneralizedFlowRate',
                      'AlluviumAquifer',
                      'AlluviumAquiferLF',
                      'DeepAlluviumAquifer',
                      'DeepAlluviumAquiferML',
                      'FutureGen2Aquifer',
                      'FutureGen2AZMI']

reservoir_components = ['LookupTableReservoir',
                        'SimpleReservoir',
                        'AnalyticalReservoir']

wellbore_components = ['MultisegmentedWellbore',
                       'CementedWellbore',
                       'OpenWellbore',
                       'GeneralizedFlowRate']

single_input_aquifer_components = ['AlluviumAquifer',
                                   'AlluviumAquiferLF',
                                   'DeepAlluviumAquifer',
                                   'DeepAlluviumAquiferML',
                                   'FutureGen2Aquifer',
                                   'FutureGen2AZMI']

multi_input_aquifer_components = ['CarbonateAquifer']

alluvium_aquifer_components = ['AlluviumAquifer', 'DeepAlluviumAquifer',
                               'AlluviumAquiferLF', 'DeepAlluviumAquiferML']


def main(yaml_filename):
    """
    Reads in yaml data control file to create OpenIAM model and run it.

    :param yaml_filename: yaml formatted OpenIAM control file name
    :type filename: str

    :returns: None
    """
    start_time = datetime.now()
    now = start_time.strftime('%Y-%m-%d_%H.%M.%S')
    # Load yaml file data
    if args.binary_file:
        with open(yaml_filename, 'rb') as cf:
            yaml_data = pickle.load(cf)
    else:
        with open(yaml_filename, 'r') as yaml_cf:
            yaml_data = yaml.load(yaml_cf, Loader=yaml.SafeLoader)

    model_data = yaml_data['ModelParams']

    if 'Analysis' not in model_data:
        model_data['Analysis'] = 'forward'
    if isinstance(model_data['Analysis'], str):
        analysis = model_data['Analysis'].lower()
        analysis_dict = {}
    elif isinstance(model_data['Analysis'], dict):
        analysis_dict = model_data['Analysis']
        analysis = analysis_dict.pop('type').lower()

    if 'OutputDirectory' in model_data:
        out_dir = os.path.join(IAM_DIR, model_data['OutputDirectory'])
        if not os.path.exists(os.path.dirname(out_dir)):
            os.mkdir(os.path.dirname(out_dir))
        out_dir = out_dir.format(datetime=now)
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        model_data['OutputDirectory'] = out_dir
        logging_file_name = os.path.join(out_dir, 'IAM_log.log')
        analysis_log = os.path.join(out_dir, 'Analysis.log')
        outfile = os.path.join(out_dir, '{analysis}_results.txt'.format(analysis=analysis))
        # Copy input YAML file
        shutil.copy2(yaml_filename, out_dir)
    else:
        logging_file_name = 'IAM_log.log'
        analysis_log = 'analysis.log'
        outfile = '{analysis}_results.txt'.format(analysis=analysis)

    # If logging level not specified, set default
    if 'Logging' not in model_data:
        model_data['Logging'] = 'Info'
    log_dict = {'All': logging.NOTSET,
                'Debug': logging.DEBUG,
                'Info': logging.INFO,
                'Warning': logging.WARNING,
                'Error': logging.ERROR,
                'Critical': logging.CRITICAL}

    log_level = log_dict[model_data['Logging']]

    logging.basicConfig(
        level=log_level,
        format='from %(module)s %(funcName)s - %(levelname)s %(message)s',
        datefmt='%m-%d %H:%M', filename=logging_file_name, filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # Tell the handler to use this format
    console.setFormatter(formatter)
    # Add the handler to the root logger
    logging.getLogger('').addHandler(console)

    info_msg = output_header.format(iam_version=iam.__version__, now=now)
    logging.info(info_msg)

    # Check whether user input time points through input file or list
    if 'TimePoints' in model_data:
        time_data = model_data['TimePoints']
        if isinstance(time_data, str):  # if filename is provided
            time_data_file_path = os.path.join(IAM_DIR, time_data)
            if os.path.isfile(time_data_file_path):
                time_array = np.genfromtxt(
                    time_data_file_path, delimiter=",", dtype='f8')
            else:
                logging.debug('Wrong path is provided for time points')
        else: # list is provided
            time_array = np.array(time_data)

        # Convert time points data to days
        time_array = 365.25*time_array
    else:
        if 'EndTime' in model_data:
            num_years = model_data['EndTime']
        else:
            num_years = 1
        if 'TimeStep' in model_data:
            time_step = model_data['TimeStep']
        else:
            time_step = 1
        time_array = 365.25*np.arange(0.0, num_years+time_step, time_step)

    sm_model_kwargs = {'time_array': time_array}  # time is given in days

    # Create system model
    sm = iam.SystemModel(model_kwargs=sm_model_kwargs)

    # Add stratigraphy component
    strata = sm.add_component_model_object(iam.Stratigraphy(name='strata', parent=sm))

    if 'Stratigraphy' in yaml_data:
        strat_params = yaml_data['Stratigraphy']
        for parameter in strat_params:
            if not isinstance(strat_params[parameter], dict):
                strat_params[parameter] = {'value': strat_params[parameter],
                                           'vary': False}
            strata.add_par(parameter, **(strat_params[parameter]))

    # Perform extra needed actions to connect stratigraphy to the system model
    strata.connect_with_system()

    # Initialize component data that would keep the information
    # for which components the given component provides an output
    for comp_model in model_data['Components']:
        yaml_data[comp_model]['DistributedTo'] = []

    output_prov_cmpnts = []
    for comp_model in model_data['Components']:
        comp_data = yaml_data[comp_model]
        if 'connection' in comp_data:
            comp_data['Connection'] = comp_data['connection']
            comp_data.pop('connection', None)

        if 'Connection' in comp_data:
            if (comp_data['Connection'] == 'none') or (
                    comp_data['Connection'] == 'Dynamic Parameters'):
                comp_data.pop('Connection', None)
                continue
            else:
                # Works for lists and single components
                connections = np.array([comp_data['Connection']]).flatten()
                for connect in connections:
                    if connect not in output_prov_cmpnts:
                        output_prov_cmpnts.append(connect)
                    yaml_data[connect]['DistributedTo'].append(comp_model)

    debug_msg = ''.join([
        'List of components providing output to other components {} and ',
        'to which they provide an output {}']).format(
            output_prov_cmpnts,
            {comp_model: yaml_data[comp_model][
                'DistributedTo'] for comp_model in model_data['Components']})
    logging.debug(debug_msg)

    debug_msg = 'Initial list of components from control file {}'.format(
        model_data['Components'])
    logging.debug(debug_msg)

    # Sort components according to the order of connections
    comp_list = []
    for comp_model in model_data['Components']:
        comp_data = yaml_data[comp_model]
        if comp_model in comp_list:
            continue
        if 'Connection' in comp_data:
            # Works for lists and single components
            connections = np.array([comp_data['Connection']]).flatten()
            for connect in connections:
                if connect not in comp_list:
                    comp_list.append(connect)
            # Add component after adding its connections
            comp_list.append(comp_model)
        elif comp_model in output_prov_cmpnts:
            # If component does not have connections but provides outputs
            # for other components it has to be moved to the
            # beginning of the list to avoid being end up at the end of the list
            comp_list.insert(0, comp_model)
        else:
            # If the component does not have any connections and does not provide
            # outputs for other components
            comp_list.append(comp_model)

    debug_msg = 'List of components sorted according to connections {}'.format(comp_list)
    logging.debug(debug_msg)

    # List of components in the system from pathway_components group
    path_list = []

    # List of important locations for each component
    locations = {}
    # List of injection well locations
    inj_well_locations = {}

    # ad_connect is a list of pairs
    # where 1st element is names of wellbores (to be linked to adapters),
    # and 2nd element is name of aquifer to which the leakage rates should be output
    # adapters are components needed to link aquifer components to wellbores
    ad_connect = []
    # List of all the final components of the system model
    comp_list2 = []

    # Dictionary of system model collectors
    # collectors is a dictionary with keys being names of inputs coming from
    # observations of 'connection' components (mainly wellbore) and having
    # information about to what aquifer leakage rates are input
    sm.collectors = {}
    debug_msg = 'System component list {}'.format(comp_list)
    logging.debug(debug_msg)

    for comp_model in comp_list:
        comp_data = yaml_data[comp_model]

        if 'type' in comp_data:
            comp_data['Type'] = comp_data['type']

        if comp_data['Type'] in pathway_components:
            path_list.append(comp_model)

        if comp_data['Type'] in wellbore_components:
            locations = process_wellbore_locations(
                comp_data, comp_model, locations)

        if comp_data['Type'] in reservoir_components:
            locations, inj_well_locations = process_reservoir_locations(
                comp_data, comp_model,
                locations, inj_well_locations, comp_data['Type'])

        if comp_data['Type'] == 'SealHorizon':
            locations = process_cell_centers(
                comp_data, comp_model, locations)

        if comp_data['Type'] == 'FaultFlow':
            locations = process_fault_segment_centers(
                comp_data, comp_model, locations)

        comp_type = getattr(iam, comp_data['Type'])
        if hasattr(comp_type, 'adapters') and comp_type.adapters:
            path_list.append('adapter')

            if 'Connection' in comp_data:
                # Works for lists and single components
                connections = np.array([comp_data['Connection']]).flatten()
                ad_connect.append([connections, yaml_data[comp_model]['AquiferName']])
                comp_list2.append('adapter')

        if hasattr(comp_type, 'system_collected_inputs') and comp_type.system_collected_inputs:
            # sci dictionary with keys names of model method keyword argument and values
            # names of observations needed to be linked to the arguments
            sci = comp_type.system_collected_inputs
            if 'Connection' in comp_data:
                # For each keyword argument of model method
                for sinput in sci:
                    if not sinput in sm.collectors:
                        sm.collectors[sinput] = {}

                    sm.collectors[sinput][comp_model] = {
                        'Connection': np.array([comp_data['Connection']]).flatten(),
                        'argument': sci[sinput],
                        'data': []}
        comp_list2.append(comp_model)

    debug_msg = ''.join(["Updated component list with adapters {}.\n",
                         "List of adapters {}.\nPath list {}."]).format(
                             comp_list2, ad_connect, path_list)
    logging.debug(debug_msg)

    # Create component list with all pathways
    comps = comp_list2
    comp_list = []
    num_adapters = 0
    adapters = {}
    for comp_model in comps:  # go over all components in the components list
        if comp_model in path_list:  # reservoir, wellbore or aquifer (except carbonate)
            if comp_model == 'adapter':
                num_adapters = process_adapter(
                    yaml_data, comp_list, adapters, ad_connect,
                    locations, num_adapters)
                continue  # proceed to the next component in "comps" list

            else:  # if comp_model is not an adapter
                comp_data = yaml_data[comp_model]
                # For reservoir components providing output for other components

                if comp_data['Type'] in reservoir_components:
                    process_reservoir(
                        yaml_data, comp_list, comp_data, comp_model,
                        output_prov_cmpnts, locations, inj_well_locations)
                    continue

                if comp_data['Type'] in wellbore_components:
                    process_wellbore(yaml_data, comp_list, comp_data,
                                     comp_model, locations)
                    continue

                if comp_data['Type'] in single_input_aquifer_components:
                    process_aquifer(yaml_data, comp_list, comp_data,
                                    comp_model, locations)
                    continue


        else:  # if comp_model is not in the path_list
            # For components not in the path_list
            # As of now these are carbonate aquifer, atmospheric ROM,
            # seal horizon, fault flow component, plume stability
            # Carbonate aquifer and atmospheric ROM would not belong to the path_list
            # but they need locations to be collected from their connections
            # Seal horizon and fault don't need locations collected from their connection
            # since they themselves require locations in their setup but they fit this flow
            comp_data = yaml_data[comp_model]

            if comp_data['Type'] in ['SealHorizon', 'FaultFlow']:
                process_connections_to_reservoir(yaml_data, comp_list, comp_data,
                                                 comp_model, locations, sm)

            if comp_data['Type'] == 'CarbonateAquifer':
                process_carb_aquifer(yaml_data, comp_data, comp_model, locations)

            if comp_data['Type'] == 'AtmosphericROM':
                process_atm_rom(yaml_data, comp_data, comp_model, locations)

            comp_list.append(comp_model)

    debug_msg = "Updated component list {}".format(comp_list)
    logging.debug(debug_msg)
    # Create dictionary to map component names to objects
    name2obj_dict = {'strata': strata}
    # Create dictionary of component models with outputs and lists of output names
    output_list = {}
    # Create list of component models
    components = []

    for i, component_name in enumerate(comp_list):
        component_data = yaml_data[component_name]
        comp_type = getattr(iam, component_data['Type'])
        components.append(sm.add_component_model_object(
            comp_type(name=component_name, parent=sm)))
        name2obj_dict[component_name] = components[-1]

        if hasattr(components[-1], 'connect_with_system'):
            components[-1].connect_with_system(component_data,
                                               name2obj_dict,
                                               locations,
                                               adapters)
            # Outputs
            if 'Outputs' in component_data:
                # Due to the specific types of observations for Fault Flow
                # and Seal Horizon component they are added inside the connect_with_system
                # method and not here
                if component_data['Type'] not in [
                        'FaultFlow', 'SealHorizon', 'ChemicalWellSealing']:
                    comp_outputs = component_data['Outputs']
                    for output in comp_outputs:
                        components[-1].add_obs(output)
                    output_list[components[-1]] = comp_outputs
                else:
                    output_list[components[-1]] = component_data['Outputs']

            continue

        # Parameters
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if not isinstance(component_data['Parameters'][key], dict):
                    component_data['Parameters'][key] = {
                        'value': component_data['Parameters'][key],
                        'vary': False}
                components[-1].add_par(key, **component_data['Parameters'][key])

        # Dynamic keyword arguments of component model method
        if ('DynamicParameters' in component_data) and (
                component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                if key != 'structure':
                    inp_data = component_data['DynamicParameters'][key]
                    if isinstance(inp_data, str):  # if filename is provided
                        inp_data_file_path = os.path.join(IAM_DIR, inp_data)
                        if os.path.isfile(inp_data_file_path):
                            data = np.genfromtxt(inp_data_file_path,
                                                 delimiter=",", dtype='f8')
                            components[-1].add_dynamic_kwarg(key, data)
                        else:
                            err_msg = '{} is not a valid file name.'.format(inp_data)
                            logging.error(err_msg)

                    else:  # if numerical data is provided
                        components[-1].add_dynamic_kwarg(key, inp_data)

        # Handle pressure/saturation locations for wells or reservoir
        if hasattr(components[-1], 'needs_locXY') and components[-1].needs_locXY:
            try:
                components[-1].locX = component_data['locX']
                components[-1].locY = component_data['locY']
            except IndexError:
                logging.info('Well location unknown, placing at (100, 100)')
                components[-1].locX = 100
                components[-1].locY = 100

        # Handle injection well locations
        if hasattr(components[-1], 'needs_injXY') and components[-1].needs_injXY:
            try:
                components[-1].injX = component_data['injX']
                components[-1].injY = component_data['injY']
            except KeyError:
                pass

        if hasattr(components[-1], 'needsXY'):
            if components[-1].needsXY:
                components[-1].model_kwargs['x'] = component_data['locX']
                components[-1].model_kwargs['y'] = component_data['locY']

        # Outputs
        if 'Outputs' in component_data:
            comp_outputs = component_data['Outputs']
            output_list[components[-1]] = comp_outputs
            for output in comp_outputs:
                components[-1].add_obs(output)

        # Make model connections
        if 'Connection' in component_data:
            connection = None
            try:
                connection = name2obj_dict[component_data['Connection']]
            except KeyError:
                pass

            if hasattr(components[-1], 'system_inputs'):
                for sinput in components[-1].system_inputs:
                    # TODO add adapter linked obs for system_inputs
                    # if adapters different from RateToMassAdapter
                    # will be ever created
                    connection.add_obs_to_be_linked(sinput)
                    components[-1].add_kwarg_linked_to_obs(
                        sinput, connection.linkobs[sinput])

            if (hasattr(components[-1], 'system_collected_inputs') and
                    components[-1].system_collected_inputs):
                collectors = sm.collectors
                for collect, cdict in collectors.items():
                    argument = cdict['argument'].format(
                        aquifer_name=component_data['AquiferName'])
                    connections = cdict['Connection']

                    for connect in connections:
                        for ind in range(locations[connect]['number']):
                            cname = connect + '_{0:03}'.format(ind)
                            for ad_nm, adptr_item in adapters.items():
                                if (adptr_item['Connection'] == cname) and (
                                        adptr_item['AquiferName'] == component_data[
                                            'AquiferName']):
                                    aname = ad_nm

                            adapter = name2obj_dict[aname]
                            connector = name2obj_dict[cname]
                            if argument in adapter.linkobs:
                                cdict['data'].append(adapter.linkobs[argument])
                            else:
                                cdict['data'].append(connector.linkobs[argument])

                sci = components[-1].system_collected_inputs
                for sinput in sci:
                    components[-1].add_kwarg_linked_to_collection(
                        sinput, collectors[sinput][component_name]['data'])

            if hasattr(components[-1], 'composite_inputs'):
                for key in components[-1].composite_inputs:
                    components[-1].add_composite_par(
                        key, components[-1].composite_inputs[key].format(
                            driver=connection.name,
                            selfm=components[-1].name,
                            strata='strata'))
        # End of "if Connection in ..." statement

        if hasattr(components[-1], 'system_params'):
            if yaml_data[component_name]['Type'] in [
                    'SimpleReservoir', 'AnalyticalReservoir', 'MultisegmentedWellbore']:
                if 'numberOfShaleLayers' in strata.pars:
                    connect = strata.pars
                elif 'numberOfShaleLayers' in strata.deterministic_pars:
                    connect = strata.deterministic_pars
                else:
                    connect = strata.default_pars
                components[-1].add_par_linked_to_par(
                    'numberOfShaleLayers', connect['numberOfShaleLayers'])

                nSL = connect['numberOfShaleLayers'].value
                components[-1].system_params = [
                    'shale{}Thickness'.format(ind) for ind in range(1, nSL+1)] + [
                        'aquifer{}Thickness'.format(ind) for ind in range(1, nSL)] + [
                            'reservoirThickness', 'datumPressure']

            for sparam in components[-1].system_params:
                connect = None
                if sparam in strata.pars:
                    connect = strata.pars
                elif sparam in strata.deterministic_pars:
                    connect = strata.deterministic_pars
                elif sparam in strata.default_pars:
                    connect = strata.default_pars
                else:
                    info_msg = 'Unable to find parameter {}.'.format(sparam)
                    logging.info(info_msg)

                components[-1].add_par_linked_to_par(sparam, connect[sparam])
    # End Component model loop

    # Setup Analysis
    if analysis == 'lhs':
        if 'siz' not in analysis_dict:
            analysis_dict['size'] = args.lhs_sample_size
        if 'seed_size' in analysis_dict:
            seed_size = [int(ss) for ss in analysis_dict.pop('seed_size').strip('()').split(',')]
            analysis_dict['seed'] = random.randint(*seed_size)
        if 'seed' not in analysis_dict:
            analysis_dict['seed'] = random.randint(500, 1100)
    elif analysis == 'parstudy':
        if 'nvals' not in analysis_dict:
            analysis_dict['nvals'] = args.parstudy_divisions
    model_data['Analysis_type'] = analysis
    model_data['Analysis_dict'] = analysis_dict
    # End setup

    calc_start_time = datetime.now()
    setup_time = calc_start_time - start_time
    debug_msg = 'Model setup time: {}'.format(setup_time)
    logging.debug(debug_msg)

    # Run Analysis
    if analysis == 'forward':
        results = sm.forward()
        s = None
    elif analysis == 'lhs':
        s = sm.lhs(**analysis_dict)
        results = s.run(cpus=args.ncpus, verbose='progress', logfile=analysis_log)
        # New line in console
        print("")
    elif analysis == 'parstudy':
        s = sm.parstudy(**analysis_dict)
        results = s.run(cpus=args.ncpus, verbose='progress', logfile=analysis_log)
        # New line in console
        print("")
    else:
        raise ValueError('Analysis type {atype} not found.\nNo analysis ran'.format(atype=analysis))

    yaml_data['Results'] = results
    yaml_data['s'] = s
    yaml_data['sm'] = sm
    yaml_data['output_list'] = output_list
    yaml_data['components'] = components
    yaml_data['time_array'] = time_array
    # End Analysis

    calc_end_time = datetime.now()
    calc_time = calc_end_time - calc_start_time
    debug_msg = 'Model analysis time: {}'.format(calc_time)
    logging.debug(debug_msg)

    if 'OutputDirectory' in model_data:
        if analysis == 'forward':
            for output_component in list(output_list.keys()):
                for output_name in output_list[output_component]:
                    outfile = os.path.join(
                        out_dir, output_component.name + '.' + output_name + '.txt')
                    indices = None
                    if output_name in ['seal_flag', 'seal_time']:
                        indices = [0]
                    np.savetxt(outfile, sm.collect_observations_as_time_series(
                        output_component, output_name, indices=indices), fmt='%1.12e')

        elif analysis == 'lhs' or analysis == 'parstudy':
            outfile = os.path.join(
                out_dir, '{analysis}_results.txt'.format(analysis=analysis))
            analysisfile = os.path.join(
                out_dir, '{analysis}_statistics.txt'.format(analysis=analysis))
            s.savetxt(outfile)
            s.savestats(analysisfile)
        else:
            pass
        outfile = os.path.join(out_dir, 'output_dump.pkl')
        with open(outfile, 'wb') as output_dump:
            pickle.dump(yaml_data, output_dump)
        timefile = os.path.join(out_dir, 'time_series.csv')
        np.savetxt(timefile, time_array/365.25, fmt='%.12e',
                   delimiter=',', header='Time (in years)')

    if 'Analysis' in yaml_data:
        process_analysis(yaml_data, model_data, sm, s, output_list, analysis,
                     time_array, components)

    if 'Plots' in yaml_data:
        process_plots(yaml_data, model_data, sm, s, output_list, analysis,
                      time_array, components)

    total_time = datetime.now() - start_time
    info_msg = '\nAnalysis completed at {}.\nTotal run time: {} \n'.format(
        datetime.now().strftime('%Y-%m-%d_%H.%M.%S'), total_time)
    logging.info(info_msg)

    if args.binary_file:
        # For GUI produced binary control files
        info_msg = ''.join([
            '\nSimulation results can be found in the output folder: \n{}.',
            '\nProceed to the Post Processing tab to access plotting options.']).format(
                model_data['OutputDirectory'])
    else:
        info_msg = ''.join([
            '\nSimulation results and plots can be found ',
            'in the output folder: \n{}']).format(model_data['OutputDirectory'])
    logging.info(info_msg)

    # Remove all handlers from the logger for proper work in the consecutive runs
    while logging.getLogger('').handlers:
        logging.getLogger('').handlers.pop()
    return True


def process_adapter(yaml_data, comp_list, adapters, ad_connect, locations,
                     prev_num_adapters):
    """ """
    # ad_connect is a list of pairs
    # where 1st element is names of wellbores (to be linked to adapters),
    # and 2nd element is name of aquifer to which the leakage rates should be output
    # adapters are components needed to link aquifer components to wellbores
    # ad_connect_name_base is possibly a list of connections
    # Remove the first element from the list and return it
    ad_connect_name_base = ad_connect.pop(0)
    # Index to keep track of the total number of adapters
    ad_ind = prev_num_adapters
    # Since adapter through aquifer can be connected to
    # several groups of wells
    # connect_cmpnt represents a particular group of wells
    # ad_connect_name_base[0] is a list of wellbore components
    # which provide input for adapters
    # ad_connect_name_base[1] is a name of aquifer to which
    # leakage rates are of interest
    for connect_cmpnt in ad_connect_name_base[0]:
        # Depending on the number of wells in the given group
        for ind in range(locations[connect_cmpnt]['number']):
            ad_name = 'adapter_{0:03}'.format(ad_ind)
            # ad_connect_name is name of (wellbore) component
            # from which output will be requested
            ad_connect_name = connect_cmpnt + '_{0:03}'.format(ind)
            # Save information about adapter component in the yaml_data dictionary
            # where information about other components is saved as well
            yaml_data[ad_name] = {'Type': 'RateToMassAdapter',
                                  'Connection': ad_connect_name,
                                  'AquiferName': ad_connect_name_base[1]}
            # Save information about adapter component in adapters dictionary
            adapters[ad_name] = yaml_data[ad_name]
            # Add adapters to the component list
            comp_list.append(ad_name)
            # Increase count index for adapters
            ad_ind = ad_ind + 1

    return ad_ind


def process_reservoir(yaml_data, comp_list, comp_data, comp_model,
                      output_prov_cmpnts, locations, inj_well_locations):
    """ """
    # Check whether location of injection well is provided
    if comp_model in inj_well_locations:
        comp_data_copy = comp_data.copy()
        comp_data_copy['injX'] = inj_well_locations[comp_model][0]
        comp_data_copy['injY'] = inj_well_locations[comp_model][1]
        yaml_data[comp_model] = comp_data_copy
        comp_data = yaml_data[comp_model]

    # Check if reservoir provides output for other components
    res_ind = 0
    if comp_model in output_prov_cmpnts:
        # comp_data['DistributedToIndices'] is a dictionary with keys
        # being names of components to which reservoir data is provided
        # and values being tuples of the first and the last indices
        # used to create corresponding reservoir components from which
        # the corresponding reservoir data will be used
        comp_data['DistributedToIndices'] = {}

        # Here, comp_data['DistributedTo'] is a list of well groups
        # connected to the given reservoir component
        for cmpnt in comp_data['DistributedTo']:
            # Reservoir component can provide input for
            # several groups of wells. To make sure each group
            # is connected to the right reservoir component
            # we save the reservoir component indices
            # corresponding to the well group
            comp_data['DistributedToIndices'][cmpnt] = (
                res_ind, res_ind + locations[cmpnt]['number'])
            for ind in range(locations[cmpnt]['number']):
                comp_name = comp_model + '_{0:03}'.format(res_ind)
                comp_data_copy = comp_data.copy()
                comp_data_copy['locX'] = locations[cmpnt]['coordx'][ind]
                comp_data_copy['locY'] = locations[cmpnt]['coordy'][ind]
                yaml_data[comp_name] = comp_data_copy
                comp_list.append(comp_name)
                res_ind = res_ind + 1

    # If separate (from wellbore) locations are specified for a reservoir component
    if comp_model in locations:
        for ind in range(locations[comp_model]['number']):
            comp_name = comp_model + '_{0:03}'.format(res_ind)
            comp_data_copy = comp_data.copy()
            comp_data_copy['locX'] = locations[comp_model]['coordx'][ind]
            comp_data_copy['locY'] = locations[comp_model]['coordy'][ind]
            yaml_data[comp_name] = comp_data_copy
            comp_list.append(comp_name)
            res_ind = res_ind + 1


def process_wellbore(yaml_data, comp_list, comp_data, comp_model, locations):
    for ind in range(locations[comp_model]['number']):
        comp_name = comp_model + '_{0:03}'.format(ind)
        comp_data_copy = comp_data.copy()

        # If the well locations are provided
        if locations[comp_model]['coordx']:
            comp_data_copy['locX'] = locations[comp_model]['coordx'][ind]
            comp_data_copy['locY'] = locations[comp_model]['coordy'][ind]
        yaml_data[comp_name] = comp_data_copy
        comp_list.append(comp_name)

    # For wells the possible connection is reservoir component
    if 'Connection' in comp_data:
        connect = comp_data['Connection']
        # This is where we use the indices of the corresponding
        # reservoir components to link well to the right reservoir
        # component
        ind_offset = yaml_data[connect]['DistributedToIndices'][comp_model][0]

        for ind in range(locations[comp_model]['number']):
            comp_name = comp_model + '_{0:03}'.format(ind)
            yaml_data[comp_name]['Connection'] = (
                connect + '_{0:03}'.format(ind+ind_offset))


def process_aquifer(yaml_data, comp_list, comp_data, comp_model, locations):
    if 'Connection' in comp_data:
        connect = comp_data['Connection']
        if connect in locations:  # connection is a wellbore component
            for ind in range(locations[connect]['number']):
                comp_name = comp_model + '_{0:03}'.format(ind)
                comp_data_copy = yaml_data[comp_model].copy()
                comp_data_copy['Connection'] = connect + '_{0:03}'.format(ind)
                comp_data_copy['locX'] = locations[connect]['coordx'][ind]
                comp_data_copy['locY'] = locations[connect]['coordy'][ind]
                yaml_data[comp_name] = comp_data_copy
                comp_list.append(comp_name)
        else:
            err_msg = ''.join([
                'Connection {} does not exist and is not possible for component {}. ',
                'Please check and update the control file.']).format(
                    connect, comp_model)
            logging.error(err_msg)
    else:
        # No connections. Possible situations: testing of single input
        # aquifer components as single components
        if 'Locations' in comp_data:
            num_pathways = len(comp_data['Locations']['coordx'])
            well_data_x = comp_data['Locations']['coordx']
            well_data_y = comp_data['Locations']['coordy']

        else:
            num_pathways = 1
            info_msg = ''.join(
                ['Locations are not specified for {} component. '.format(comp_model),
                 'Default location (100, 100) will be used.'])
            logging.info(info_msg)
            well_data_x = [100.0]
            well_data_y = [100.0]

        for ind in range(num_pathways):
            comp_name = comp_model + '_{0:03}'.format(ind)
            comp_data_copy = yaml_data[comp_model].copy()
            comp_data_copy['locX'] = well_data_x[ind]
            comp_data_copy['locY'] = well_data_y[ind]
            yaml_data[comp_name] = comp_data_copy
            comp_list.append(comp_name)

def process_carb_aquifer(yaml_data, comp_data, comp_model, locations):

    if 'Connection' in comp_data:
        connections = np.array([comp_data['Connection']]).flatten()

        yaml_data[comp_model]['locX'] = []
        yaml_data[comp_model]['locY'] = []

        for connect in connections:
            if connect in locations:
                yaml_data[comp_model]['locX'] = (
                    yaml_data[comp_model]['locX'] + locations[connect]['coordx'])
                yaml_data[comp_model]['locY'] = (
                    yaml_data[comp_model]['locY'] + locations[connect]['coordy'])
    else:
        if 'Locations' in comp_data:
            yaml_data[comp_model]['locX'] = comp_data['Locations']['coordx']
            yaml_data[comp_model]['locY'] = comp_data['Locations']['coordy']

        else:
            info_msg = ''.join([
                'Locations are not specified for {} component.',
                'Default location (100, 100) will be used.']).format(comp_model)
            logging.info(info_msg)
            yaml_data[comp_model]['locX'] = [100.0]
            yaml_data[comp_model]['locY'] = [100.0]


def process_atm_rom(yaml_data, comp_data, comp_model, locations):
    if 'Connection' in comp_data:
        connections = np.array([comp_data['Connection']]).flatten()

        yaml_data[comp_model]['locX'] = []
        yaml_data[comp_model]['locY'] = []

        for connect in connections:
            if connect in locations:
                yaml_data[comp_model]['locX'] = (
                    yaml_data[comp_model]['locX'] + locations[connect]['coordx'])
                yaml_data[comp_model]['locY'] = (
                    yaml_data[comp_model]['locY'] + locations[connect]['coordy'])
    else:
        if 'Locations' in comp_data:
            yaml_data[comp_model]['locX'] = comp_data['Locations']['coordx']
            yaml_data[comp_model]['locY'] = comp_data['Locations']['coordy']

        else:
            info_msg = ''.join([
                'Locations are not specified for {} component.',
                'Default location (100, 100) will be used.']).format(comp_model)
            logging.info(info_msg)
            yaml_data[comp_model]['locX'] = [100.0]
            yaml_data[comp_model]['locY'] = [100.0]


def process_connections_to_reservoir(yaml_data, comp_list, comp_data,
                                     comp_model, locations, sm):
    if 'Connection' in comp_data:
        # Get name of reservoir component that whose output will be used
        # by Seal or Fault component
        connect = comp_data['Connection']

        # We use the indices of the corresponding reservoir components
        # to link Seal/Fault to the right reservoir components
        ind_offset = yaml_data[connect]['DistributedToIndices'][comp_model][0]

        # Get number of reservoir components that will be linked
        num = locations[comp_model]['number']

        # We need to update comp_data['Connection'] with a list
        # of reservoir components that will be linked to the Seal or Fault
        # component
        comp_data['Connection'] = [connect + '_{0:03}'.format(
            ind+ind_offset) for ind in range(num)]

        for obs_nm in ['pressure', 'CO2saturation']:
            if obs_nm not in sm.collectors:
                sm.collectors[obs_nm] = {}
            sm.collectors[obs_nm][comp_model] = {
                'Connection': comp_data['Connection'],
                'data': []}


def process_cell_centers(comp_data, comp_model, locations):
    try:
        cell_data = comp_data['Cells']
    except KeyError:
        msg = "".join(["Required keyword 'Cells' is missing in the setup ",
                       "of the SealHorizon component {}."]).format(comp_model)
        logging.error(msg)
        raise KeyError(msg)

    # Determine number of cells
    try:
        num_cells = cell_data['Number']
    except KeyError:
        msg = "".join(["Keyword 'Number' is missing in the setup of ",
                       "'Cells' for SealHorizon component {}."]).format(
            comp_model)
        logging.error(msg)
        raise KeyError(msg)

    # Determine locations of cells
    try:
        loc_data = cell_data['Locations']
    except KeyError:
        msg = "".join(["Keyword 'Locations' is missing in the setup of ",
                       "'Cells' for SealHorizon component {}."]).format(
            comp_model)
        logging.error(msg)
        raise KeyError(msg)
    else:
        if 'file' in loc_data:
            filename = os.path.join(IAM_DIR, loc_data['file'])
            data = np.genfromtxt(filename)
            locX = data[:, 0].tolist()
            locY = data[:, 1].tolist()
        elif ('coordx' in loc_data) and ('coordy' in loc_data):
            locX = loc_data['coordx']
            locY = loc_data['coordy']
            if len(locX) != len(locY):
                msg = ''.join(['Length of array coordx is not equal ',
                           'to the length of array coordy.'])
                logging.error(msg)
                raise ValueError(msg)
        else:
            msg = "".join(["Cell centers coordinates are not provided ",
                           "in the setup of SealHorizon component {}.",
                           "Keywords 'coordx'/'coordy' or 'file' ",
                           "should be provided"]).format(comp_model)
            logging.error(msg)
            raise KeyError(msg)

    # Setup cell centers
    if num_cells == len(locX):
        locations[comp_model] = {}
        locations[comp_model]['number'] = len(locX)
        locations[comp_model]['coordx'] = locX
        locations[comp_model]['coordy'] = locY
        # Indicate whether component is a wellbore component
        locations[comp_model]['well'] = False
    else:
        msg = "".join(["Length of array with cell centers coordinates is not equal ",
                       "to the value provided in the keyword 'Number'."])
        logging.error(msg)
        raise ValueError(msg)

    return locations


def process_fault_segment_centers(comp_data, comp_model, locations):
    """ Obtain information about coordinates of the fault segment centers."""
    try:
        segm_data = comp_data['Segments']
    except KeyError:
        msg = "".join(["Required keyword 'Segments' is missing in the setup ",
                       "of the FaultFlow component {}."]).format(comp_model)
        logging.error(msg)
        raise KeyError(msg)

    # Determine number of segments
    try:
        num_segments = segm_data['Number']
    except KeyError:
        msg = "".join(["Keyword 'Number' is missing in the setup of ",
                       "'Segments' for FaultFlow component {}."]).format(
            comp_model)
        logging.error(msg)
        raise KeyError(msg)

    # Determine locations of fault segments centers
    try:
        loc_data = segm_data['Locations']
    except KeyError:
        if 'DynamicParameters' not in comp_data:
            msg = "".join(["Keyword 'Locations' is missing in the setup of ",
                           "'Segments' for FaultFlow component {}.",
                           "Locations will be calculated based on values ",
                           "of strike and dip."]).format(comp_model)
            logging.info(msg)
        # Get parameters data
        par_data = comp_data['Parameters']
        # Get strike, dip and length
        par_vals = {'strike': 30.0, 'length': 1000.0, 'xStart': 500.0, 'yStart': 300.0}
        for nm in par_vals:
            if nm in par_data:
                if not isinstance(par_data[nm], dict):
                    par_vals[nm] = par_data[nm]
                else:
                    par_vals[nm] = par_data[nm]['value']

        # Initialize list of locations
        locX = [par_vals['xStart']]
        locY = [par_vals['yStart']]
        if num_segments > 1:
            seg_length = par_vals['length']/num_segments
            for ind in range(1, num_segments):
                new_x = locX[-1] + seg_length*math.sin(math.radians(par_vals['strike']))
                new_y = locY[-1] + seg_length*math.cos(math.radians(par_vals['strike']))
                locX.append(new_x)
                locY.append(new_y)
    else:
        if 'file' in loc_data:
            filename = os.path.join(IAM_DIR, loc_data['file'])
            data = np.genfromtxt(filename)
            locX = data[:, 0].tolist()
            locY = data[:, 1].tolist()
        elif ('coordx' in loc_data) and ('coordy' in loc_data):
            locX = loc_data['coordx']
            locY = loc_data['coordy']
            if len(locX) != len(locY):
                msg = ''.join(['Length of array coordx is not equal ',
                           'to the length of array coordy.'])
                logging.error(msg)
                raise ValueError(msg)
        else:
            msg = "".join(["Coordinates of the fault segment centers are not provided ",
                           "in the setup of FaultFlow component {}.",
                           "Keywords 'coordx'/'coordy' or 'file' ",
                           "should be provided"]).format(comp_model)
            logging.error(msg)
            raise KeyError(msg)

    # Setup fault segments centers
    if num_segments == len(locX):
        locations[comp_model] = {}
        locations[comp_model]['number'] = len(locX)
        locations[comp_model]['coordx'] = locX
        locations[comp_model]['coordy'] = locY
        # Indicate whether component is a wellbore component
        locations[comp_model]['well'] = False
    else:
        msg = "".join(["Length of array with fault segment centers coordinates ",
                       "is not equal to the value provided in the keyword 'Number'."])
        logging.error(msg)
        raise ValueError(msg)

    return locations

def process_reservoir_locations(comp_data, comp_model,
                                locations, inj_well_locations, comp_type):
    """
    Analyze and process reservoir location data.
    """
    # Сheck if locations are provided
    if 'Locations' in comp_data:
        if comp_data['Locations']:
            locations[comp_model] = {}
            locations[comp_model]['number'] = len(
                comp_data['Locations']['coordx'])
            locations[comp_model]['coordx'] = comp_data['Locations']['coordx']
            locations[comp_model]['coordy'] = comp_data['Locations']['coordy']
            # Indicate whether component is a wellbore component
            locations[comp_model]['well'] = False
            try:
                locations[comp_model]['coordz'] = comp_data['Locations']['coordz']
            except:
                pass
        else:
            err_msg = "".join([
                "Parameter 'Locations' is used but no information ",
                "about coordinates is provided."])
            logging.error(err_msg)
            raise KeyError(err_msg)

    if comp_type in ['SimpleReservoir', 'AnalyticalReservoir']:
        if 'InjectionWell' in comp_data:
            if comp_data['InjectionWell']:
                inj_well_locations[comp_model] = [
                    comp_data['InjectionWell']['coordx'],
                    comp_data['InjectionWell']['coordy']]
            else:
                warn_msg = "".join([
                "Parameter 'InjectionWell' is used but no information ",
                "about coordinates is provided. Default location of injection ",
                "well at (0, 0) will be used."])
                inj_well_locations[comp_model] = [0.0, 0.0]
                logging.warning(warn_msg)
                raise Warning(warn_msg)

    return locations, inj_well_locations


def process_wellbore_locations(comp_data, comp_model, locations):
    """
    Analyze and process wellbore location data.
    """
    # Initialize dictionary key corresponding to the given component
    locations[comp_model] = {}
    locations[comp_model]['coordx'] = []
    locations[comp_model]['coordy'] = []
    # Indicate whether component is a wellbore component
    locations[comp_model]['well'] = True

    # Check if number of locations is provided
    if 'number' in comp_data:
        locations[comp_model]['number'] = comp_data['number']
    elif 'Number' in comp_data:
        locations[comp_model]['number'] = comp_data['Number']
    else:
        if 'Locations' in comp_data and not 'RandomLocDomain' in comp_data:
            locations[comp_model]['number'] = len(
                comp_data['Locations']['coordx'])
        else:
            if not 'Locations' in comp_data:
                err_msg = ''.join([
                    "It is not possible to determine number of random ",
                    "locations to generate: parameter ",
                    "'Number' is not provided for component {}."]).format(
                        comp_model)
                logging.error(err_msg)
                raise KeyError(err_msg)
            else:
                err_msg = ''.join([
                    "It is not possible to determine total number of ",
                    "locations (including random ones): parameter ",
                    "'Number' is not provided for component {}."]).format(
                        comp_model)
                logging.error(err_msg)
                raise KeyError(err_msg)

    # Save number of locations
    num_locs = locations[comp_model]['number']

    if 'Locations' in comp_data:
        well_loc = comp_data['Locations']
        locations[comp_model]['coordx'] = well_loc['coordx']
        locations[comp_model]['coordy'] = well_loc['coordy']
        if len(well_loc['coordx']) != len(well_loc['coordy']):
            err_msg = ''.join([
                'Lengths of the provided lists of x-coordinates and ',
                'y-coordinates are not the same.'])
            logging.error(err_msg)
            raise ValueError(err_msg)
    else:
        locations[comp_model]['coordx'] = []
        locations[comp_model]['coordy'] = []

    if num_locs < len(locations[comp_model]['coordx']):
        warn_msg = ''.join([
            "Number of provided locations (through x- and y-coordinates) ",
            "exceeds value specified in parameter 'number'. Only the ",
            "first {} locations will be used for simulation."]).format(num_locs)
        logging.warning(warn_msg)
        locations[comp_model]['coordx'] = locations[comp_model][
            'coordx'][0:num_locs]
        locations[comp_model]['coordy'] = locations[comp_model][
            'coordy'][0:num_locs]
    elif num_locs > len(locations[comp_model]['coordx']):
        if 'RandomLocDomain' in comp_data:
            if 'seed' in comp_data['RandomLocDomain']:
                loc_seed = comp_data['RandomLocDomain']['seed']
            elif 'Seed' in comp_data['RandomLocDomain']:
                loc_seed = comp_data['RandomLocDomain']['Seed']
            else:
                loc_seed = random.randint(1, 10000)

            # Get minimums and maximums of the uniform distribution
            xmin = comp_data['RandomLocDomain']['xmin']
            xmax = comp_data['RandomLocDomain']['xmax']
            ymin = comp_data['RandomLocDomain']['ymin']
            ymax = comp_data['RandomLocDomain']['ymax']

            # Generate additional random locations
            random.seed(loc_seed)
            for ind in range(num_locs-len(locations[comp_model]['coordx'])):
                xy = (random.uniform(xmin, xmax),
                      random.uniform(ymin, ymax))
                locations[comp_model]['coordx'].append(xy[0])
                locations[comp_model]['coordy'].append(xy[1])
                debug_msg = 'Placing random well/source at ({}, {}).'.format(
                    xy[0], xy[1])
                logging.debug(debug_msg)
            random.seed()
        elif locations[comp_model]['coordx']:
            warn_msg = ''.join([
                "Number of provided locations (through x- and ",
                "y-coordinates) is less than the specified value ",
                "in parameter 'Number'. Only provided locations ",
                "will be used."])
            locations[comp_model]['number'] = len(
                locations[comp_model]['coordx'])
        else:
            err_msg = 'No locations data is provided for component {}'.format(
                comp_model)
            logging.error(err_msg)

    return locations

def process_analysis(yaml_data, model_data, sm, s, output_list, analysis,
                     time_array, components):
    if analysis != 'lhs':
            logging.warning('Sensitivity analysis only available for lhs sampling')
    else:
        analysis_dict = yaml_data['Analysis']
        if 'CorrelationCoeff' in analysis_dict:
            corrcoeff_dict = {'capture_point': len(time_array)-1,
                              'excludes': [],
                              'ctype': 'pearson',
                              'plot': True,
                              'printout': False,
                              'plotvals': True,
                              'figsize': (15, 15),
                              'title': 'Pearson Correlation Coefficients at time {cp}',
                              'xrotation': 90,
                              'savefig': 'correlation_coefficients.png',
                              'outfile': 'correlation_coefficients.csv'}
            if isinstance(analysis_dict['CorrelationCoeff'], dict):
                corrcoeff_dict.update(analysis_dict['CorrelationCoeff'])
            if 'OutputDirectory' in model_data:
                corrcoeff_dict['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                         corrcoeff_dict['savefig'])
                corrcoeff_dict['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                         corrcoeff_dict['outfile'])
            iam_vis.correlations_at_time(s, **corrcoeff_dict)
        if 'SensitivityCoeff' in analysis_dict:
            if isinstance(analysis_dict['SensitivityCoeff'], dict):
                sens_dict = analysis_dict['SensitivityCoeff']
                obs = sens_dict.pop('Outputs')
            else:
                obs = analysis_dict['SensitivityCoeff']
                sens_dict = {}
            if isinstance(obs, str):
                obs = [obs]
            res_obs = resolve_obs_names(obs, output_list)
            if 'capture_point' in sens_dict:
                capture_point = sens_dict.pop('capture_point')
            else:
                capture_point = len(time_array)-1
            if not isinstance(capture_point, collections.Iterable):
                capture_points = [capture_point]
            else:
                capture_points = capture_point
            for capture_point in capture_points:
                cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                          for ob in res_obs]
                sens_dict_full = {'title': '{ob} Sensitivity Coefficients',
                                  'ylabel': None,
                                  'savefig': '{ob}_sensitivity.png',
                                  'outfile': '{ob}_sensitivity.txt'}
                sens_dict_full.update(sens_dict)
                for ob in cp_obs:
                    if 'OutputDirectory' in model_data:
                        sens_dict_full['savefig'] = os.path.join(
                            model_data['OutputDirectory'], sens_dict_full['savefig'])
                        sens_dict_full['outfile'] = os.path.join(
                            model_data['OutputDirectory'], sens_dict_full['outfile'])
                    sens_dict2 = {}
                    for key, value in list(sens_dict_full.items()):
                        if isinstance(value, str):
                            v = value.format(ob=ob, cp=capture_point)
                        else:
                            v = value
                        sens_dict2[key] = v
                    sensitivities = s.rbd_fast(obsname=ob, print_to_console=False)
                    iam_vis.simple_sensitivities_barplot(sensitivities, sm,
                                                         **sens_dict2)
        if 'MultiSensitivities' in analysis_dict:
            if isinstance(analysis_dict['MultiSensitivities'], dict):
                sens_dict = analysis_dict['MultiSensitivities']
                obs = sens_dict.pop('Outputs')
            else:
                obs = analysis_dict['MultiSensitivities']
                sens_dict = {}
            if isinstance(obs, str):
                obs = [obs]
            res_obs = resolve_obs_names(obs, output_list)
            if 'capture_point' in sens_dict:
                capture_point = sens_dict.pop('capture_point')
            else:
                capture_point = len(time_array)-1
            if not isinstance(capture_point, collections.Iterable):
                capture_points = [capture_point]
            else:
                capture_points = capture_point
            for capture_point in capture_points:

                cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                          for ob in res_obs]
                sens_dict_full = {'title': 'Sensitivity Coefficients',
                                  'ylabel': None,
                                  'savefig': 'Sensitivity_Analysis.png',
                                  'outfile': 'Sensitivity_Analysis.csv'}
                sens_dict_full.update(sens_dict)
                if 'OutputDirectory' in model_data:
                    ob = '_'.join(obs)
                    sens_dict_full['savefig'] = os.path.join(
                        model_data['OutputDirectory'],
                        sens_dict_full['savefig'].format(
                            ob=ob, cp=capture_point))
                    sens_dict_full['outfile'] = os.path.join(
                        model_data['OutputDirectory'],
                        sens_dict_full['outfile'].format(
                            ob=ob, cp=capture_point))
                iam_vis.multi_sensitivities_barplot(cp_obs, sm, s,
                                                    **sens_dict_full)
        if 'TimeSeriesSensitivity' in analysis_dict:
            if isinstance(analysis_dict['TimeSeriesSensitivity'], dict):
                sens_dict = analysis_dict['TimeSeriesSensitivity']
                obs = sens_dict.pop('Outputs')
            else:
                obs = analysis_dict['TimeSeriesSensitivity']
                sens_dict = {}
            if isinstance(obs, str):
                obs = [obs]
            res_obs = resolve_obs_names(obs, output_list)
            if 'capture_point' in sens_dict:
                capture_point = sens_dict.pop('capture_point')
            else:
                capture_point = len(time_array)-1
            sens_dict_full = {'title': '{ob} Time Sensitivity Coefficients',
                              'ylabel': None,
                              'num_sensitivities': 5,
                              'savefig': '{ob}_Sensitivity_Analysis.png',
                              'outfile': '{ob}_Sensitivity_Analysis.csv'}
            sens_dict_full.update(sens_dict)
            for ob in res_obs:
                if 'OutputDirectory' in model_data:
                    sens_dict_full['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                             sens_dict_full['savefig'])
                    sens_dict_full['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                             sens_dict_full['outfile'])
                sens_dict = {}
                for key, value in list(sens_dict_full.items()):
                    if isinstance(value, str):
                        v = value.format(ob=ob)
                    else:
                        v = value
                    sens_dict[key] = v
                iam_vis.time_series_sensitivities(
                    ob, sm, s, time_array,
                    capture_point=capture_point, **sens_dict)


def process_plots(yaml_data, model_data, sm, s, output_list, analysis,
                  time_array, components):
    plots = yaml_data['Plots']
    for p in plots:
        savefig = None
        title = None
        if 'Title' in plots[p]:
            title = plots[p]['Title']
        subplot = {'use': False}
        if 'subplot' in plots[p]:
            subplot = plots[p]['subplot']
            if 'use' not in subplot:
                subplot['use'] = True
        if 'OutputDirectory' in model_data:
            savefig = os.path.join(model_data['OutputDirectory'], p)
        if 'Data' in plots[p]:
            # Keep Data keyword for backward compatibility
            plots[p]['TimeSeries'] = plots[p]['Data']

        if 'TimeSeries' in plots[p]:
            iam_vis.time_series_plot(
                plots[p]['TimeSeries'], sm, s, output_list,
                name=p, analysis=analysis, savefig=savefig,
                title=title, subplot=subplot,
                plot_type=['real'], figsize=(13, 8))

        if 'TimeSeriesStats' in plots[p]:
            iam_vis.time_series_plot(
                plots[p]['TimeSeriesStats'], sm, s, output_list,
                name=p, analysis=analysis, savefig=savefig,
                title=title, subplot=subplot,
                plot_type=['stats'], figsize=(13, 8))

        if 'TimeSeriesAndStats' in plots[p]:
            iam_vis.time_series_plot(
                plots[p]['TimeSeriesAndStats'], sm, s, output_list,
                name=p, analysis=analysis, savefig=savefig,
                title=title, subplot=subplot,
                plot_type=['real', 'stats'], figsize=(13, 8))

        if 'AtmPlumeSingle' in plots[p]:
            satm = find_atm_comp(components)
            if 'savefig' in plots[p]['AtmPlumeSingle']:
                plots[p]['AtmPlumeSingle']['savefig'] = os.path.join(
                    model_data['OutputDirectory'],
                    plots[p]['AtmPlumeSingle']['savefig'])

            if not os.path.exists(os.path.dirname(plots[p]['AtmPlumeSingle']['savefig'])):
                os.mkdir(os.path.dirname(plots[p]['AtmPlumeSingle']['savefig']))
            iam_vis.map_plume_plot_single(s, satm, time_array,
                                          **plots[p]['AtmPlumeSingle'])

        if 'AtmPlumeEnsemble' in plots[p]:
            satm = find_atm_comp(components)
            if 'savefig' in plots[p]['AtmPlumeEnsemble']:
                plots[p]['AtmPlumeEnsemble']['savefig'] = os.path.join(
                    model_data['OutputDirectory'],
                    plots[p]['AtmPlumeEnsemble']['savefig'])

            if not os.path.exists(os.path.dirname(plots[p]['AtmPlumeEnsemble']['savefig'])):
                os.mkdir(os.path.dirname(plots[p]['AtmPlumeEnsemble']['savefig']))
            iam_vis.map_plume_plot_ensemble(s, satm, time_array,
                                            **plots[p]['AtmPlumeEnsemble'])


def find_atm_comp(components):
    """ Return AtmosphericROM component if it was added to the system model."""
    for comp in components[::-1]:
        if isinstance(comp, iam.AtmosphericROM):
            return comp
    else:
        logging.warning('Unable to find Atmospheric ROM for plume plots')
    return


def resolve_obs_names(obs, output_list):
    """
    Takes in base observation name and returns list of appended cm.obs for
    all component model names that have observations matching the base name.
    """
    resolved_names = []
    for ob in obs:
        for output_component in list(output_list.keys()):
            if ob in output_list[output_component]:
                resolved_names.append('.'.join([output_component.name, ob]))
    return resolved_names

if __name__ == "__main__":

    __spec__ = None

    if args.yaml_cf_name == 'test_CFI':
        examples_description = {
            '1':  'Simple reservoir, Cemented wellbore, forward',
            '2':  'Simple reservoir, Multisegmented wellbore, LHS',
            '3':  'Simple reservoir, Multisegmented wellbore, Carbonate aquifer, LHS',
            '4':  'Simple reservoir, Open wellbore, Carbonate aquifer, LHS',
            '5':  'Simple reservoir, Cemented wellbore, parstudy',
            '6':  'LUT reservoir, Multisegmented wellbore, LHS',
            '7a': 'Multisegmented wellbore, LHS, dyn_pars: arrays',
            '7b': 'Multisegmented wellbore, LHS, dyn_pars: filenames',
            '8':  'Simple reservoir, Open wellbore, Carbonate aquifer, LHS',
            '9':  'Simple reservoir, Open wellbore, Atmospheric ROM, LHS',
            '10': 'LUT reservoir, Cemented wellbore, Deep alluvium aquifer, LHS',
            '11': 'Simple reservoir, Multisegmented wellbore (2), Carbonate aquifer (2), forward',
            '12': 'Generalized flow rate, forward',
            '13': 'Generalized flow rate (2), Carbonate aquifer (2), forward',
            '14': 'LUT reservoir (2), Multisegmented wellbore (2), FutureGen2 aquifer (2), LHS',
            '15': 'LUT reservoir (2), Multisegmented wellbore (2), FutureGen2 AZMI (2), LHS',
            '16': 'Plume stability analysis, parstudy',
            '17': 'Fault flow, forward',
            '18': 'LUT reservoir, fault flow, LHS',
            '19': 'LUT reservoir, seal horizon, LHS',
            '20': 'Analytical reservoir, LHS',
            '21': 'LUT reservoir, Multisegmented wellbore, Deep alluvium aquifer (ML), forward',
            '22': 'Chemical well sealing, forward'}

        for key in examples_description:
            print('Testing file ControlFile_ex{}.yaml:'.format(key))
            print('Example features: {}'.format(examples_description[key]))
            main('../../examples/Control_Files/ControlFile_ex{}.yaml'.format(key))
            plt.close('all')

        print('Test of all control file interface examples is done.')

    elif args.yaml_cf_name == 'test_GUI':
        examples_description = [
            '01_Forward_AR_CW.OpenIAM',
            '02_LHS_AR_MSW.OpenIAM',
            '03_LHS_LUT_MSW.OpenIAM',
            '04_LHS_DP_MSW.OpenIAM',
            '05_LHS_AR_OW_CA.OpenIAM',
            '06_LHS_AR_OW_ATM.OpenIAM',
            '07_LHS_LUT_MSW_FG2AZ.OpenIAM',
            '08_PARSTUDY_PSA.OpenIAM']

        args.binary_file = True

        for key in examples_description:
            print('Testing file {}:'.format(key))
            main('../../examples/GUI_Files/{}'.format(key))
            plt.close('all')

        print('Test of all GUI examples is done.')

    else:
        main(args.yaml_cf_name)
