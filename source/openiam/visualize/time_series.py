# -*- coding: utf-8 -*-
"""
Code to create different time-series visualizations for the OpenIAM.
Created on Wed Feb 14 15:01:05 2018

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import warnings
import math

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook

from matk.sampleset import percentile, mean

# Ignore futurewarning from numpy about record array subarray copies vs views.
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=matplotlib.cbook.mplDeprecation)

# Dictionary with the y-label names for different component output for time series type of plot
Y_LABEL_DICT = {
    # Reservoir components
    'pressure': 'Reservoir pressure at location, $[Pa]$',
    'CO2saturation': 'Reservoir $CO_2$ saturation at location, [-]',
    'mass_CO2_reservoir': 'Mass of $CO_2$ in the reservoir, $[kg]$',
    # Wellbore and adapter components
    'CO2_aquifer': 'Leakage rate of $CO_2$ to aquifer, $[kg/s]$',
    'CO2_aquifer1': 'Leakage rate of $CO_2$ to aquifer 1, $[kg/s]$',
    'CO2_aquifer2': 'Leakage rate of $CO_2$ to aquifer 2, $[kg/s]$',
    'CO2_atm': 'Leakage rate of $CO_2$ to atmosphere, $[kg/s]$',
    'brine_aquifer': 'Leakage rate of brine to aquifer, $[kg/s]$',
    'brine_aquifer1': 'Leakage rate of brine to aquifer 1, $[kg/s]$',
    'brine_aquifer2': 'Leakage rate of brine to aquifer 2, $[kg/s]$',
    'brine_atm': 'Leakage rate of brine to atmosphere, $[kg/s]$',
    'mass_CO2_aquifer': 'Mass of $CO_2$ leaked to aquifer, $[kg]$',
    'mass_CO2_aquifer1': 'Mass of $CO_2$ leaked to aquifer 1, $[kg]$',
    'mass_CO2_aquifer2': 'Mass of $CO_2$ leaked to aquifer 2, $[kg]$',
    'mass_brine_aquifer': 'Mass of brine leaked to aquifer, $[kg]$',
    'mass_brine_aquifer1': 'Mass of brine leaked to aquifer 1, $[kg]$',
    'mass_brine_aquifer2': 'Mass of brine leaked to aquifer 2, $[kg]$',
    # Fault flow and Seal horizon components
    'CO2_aquifer_total': 'Cumulative leakage rate of $CO_2$ to aquifer, $[kg/s]$',
    'brine_aquifer_total': 'Cumulative leakage rate of brine to aquifer, $[kg/s]$',
    'mass_CO2_aquifer_total': 'Cumulative mass of $CO_2$ leaked to aquifer, $[kg]$',
    'mass_brine_aquifer_total': 'Cumulative mass of brine leaked to aquifer, $[kg]$',
    # Aquifer components
    'Benzene_volume': 'Volume of plume above benzene threshold, $[m^3]$',
    'Naphthalene_volume': 'Volume of plume above naphthalene threshold, $[m^3]$',
    'Phenol_volume': 'Volume of plume above phenol threshold, $[m^3]$',
    'As_volume': 'Volume of plume above arsenic threshold, $[m^3]$',
    'Pb_volume': 'Volume of plume above lead threshold, $[m^3]$',
    'Cd_volume': 'Volume of plume above cadmium threshold, $[m^3]$',
    'Ba_volume': 'Volume of plume above barium threshold, $[m^3]$',
    'Flux': '$CO_2$ leakage rate to atmosphere, $[kg/s]$',
    # FutureGen2 aquifer components
    'TDS_volume': 'Volume of plume above TDS threshold, $[m^3]$',
    'TDS_dx': 'Length of plume above TDS threshold, $[m]$',
    'TDS_dy': 'Width of plume above TDS threshold, $[m]$',
    'TDS_dz': 'Height of plume above TDS threshold, $[m]$',
    'pH_volume': 'Volume of plume below pH threshold, $[m^3]$',
    'pH_dx': 'Length of plume below pH threshold, $[m]$',
    'pH_dy': 'Width of plume below pH threshold, $[m]$',
    'pH_dz': 'Height of plume below pH threshold, $[m]$',
    'Pressure_volume': 'Volume of plume above baseline pressure change, $[m^3]$',
    'Pressure_dx': 'Length of plume above baseline pressure change, $[m]$',
    'Pressure_dy': 'Width of plume above baseline pressure change, $[m]$',
    'Pressure_dz': 'Height of plume above baseline pressure change, $[m]$',
    'Dissolved_CO2_volume': 'Volume of plume above baseline dissolved $CO_2$, $[m^3]$',
    'Dissolved_CO2_dx': 'Length of plume above baseline dissolved $CO_2$, $[m]$',
    'Dissolved_CO2_dy': 'Width of plume above baseline dissolved $CO_2$, $[m]$',
    'Dissolved_CO2_dz': 'Height of plume above baseline dissolved $CO_2$, $[m]$',
    'Temperature_volume': 'Volume of plume above baseline temperature change, $[m^3]$',
    'Temperature_dx': 'Length of plume above baseline temperature change, $[m]$',
    'Temperature_dy': 'Width of plume above baseline temperature change, $[m]$',
    'Temperature_dz': 'Height of plume above baseline temperature change, $[m]$',
    # Plume stability component
    'pressure_areas': 'Pressure plume area, $[m^2]$',
    'pressure_areas_dt': 'Change in pressure plume area, $[m^2/year]$',
    'pressure_mobility': 'Velocity of pressure plume centroid, $[m/year]$',
    'pressure_mobility_angles': 'Direction of pressure plume centroid, [-]',
    'pressure_spreading': 'Dispersion of pressure plume, $[m^2/year]$',
    'pressure_spreading_angles': 'Direction of pressure plume dispersion, [-]',
    'CO2saturation_areas': '$CO_2$ saturation plume area, $[m^2]$',
    'CO2saturation_areas_dt': 'Change in $CO_2$ saturation plume area, $[m^2/year]$',
    'CO2saturation_mobility': 'Velocity of $CO_2$ saturation plume centroid, $[m/year]$',
    'CO2saturation_mobility_angles': 'Direction of $CO_2$ saturation plume centroid, [-]',
    'CO2saturation_spreading': 'Dispersion of $CO_2$ saturation plume, $[m^2/year]$',
    'CO2saturation_spreading_angles': 'Direction of $CO_2$ saturation plume dispersion, [-]',
    }


def time_series_plot(output_names, sm, s, output_list, name='Figure1',
                     analysis='forward', savefig=None, title=None,
                     subplot=None, plot_type=None, figsize=None):
    """
    Makes a time series plots of statistics of data in output_names.
    Highlights percentiles, mean, and median values.

    :param output_names: List of observation names to match with component models and plot.
    :type output_names: list

    :param sm: OpenIAM System model for which plots are created
    :type sm: openiam.SystemModel object

    :param s: SampleSet with results of analysis performed on the system model.
        None for forward analysis.
    :type s: matk.SampleSet object

    :param output_list: Dictionary mapping observations to component models
    :type output_list: dict

    :param name: Figure Name to be used/created.
    :type name: str

    :param analysis: Type of OpenIAM system analysis performed ('forward',
        'lhs', or 'parstudy')
    :type analysis: str

    :param savefig: Filename to save resulting figure to. No file saved if None.
    :type savefig: str

    :param title: Optional Title for figure
    :type title: str

    :param subplot: Dictionary for subplot controls, use=True will use
        subplotting (boolean default=False), ncols=n will use n columns
        of subplots (positive integer default 1); comp.var_name = sub_title
        will title the subplots of comp.var_name subtitle (string default=comp.var_name)
    :type subplot: dict

    :param plot_type: List of 'real' and/or 'stats' plot types to produce.
        'real' plots realization values
        'stats' plots quartiles, mean, and median values
    :type plot_type: list of 'real' and/or 'stats'

    :return: None
    """
    if figsize is None:
        figsize = (9, 6)
    fig = plt.figure(num=name, figsize=figsize)
    if subplot is None:
        subplot = {'use': False}
    if plot_type is None:
        plot_type = ['real']
    time = sm.time_array/365.25
    default_title = ' '
    # fig = plt.gcf()
    # Find number of subplots
    nplots = 0
    for pt_value in output_names:
        for output_component in list(output_list.keys()):
            if pt_value in output_list[output_component]:
                nplots += 1
    single_plot = False
    if not subplot['use']:
        ncols = 1
        single_plot = True
    elif 'ncols' in subplot:
        ncols = subplot['ncols']
    else:
        ncols = 1
    nrows = math.ceil(float(nplots)/ncols)
    if single_plot:
        nrows = 1
    subplot_n = 1
    color = 1
    for pt_value in output_names:
        default_title += pt_value + ' '
        for output_component in list(output_list.keys()):
            if pt_value in output_list[output_component]:
                full_obs_nm = '.'.join([output_component.name, pt_value])
                ax = plt.subplot(nrows, ncols, subplot_n, title=full_obs_nm)
                if not single_plot:
                    subplot_n += 1

                if analysis == 'forward':
                    values = sm.collect_observations_as_time_series(
                        output_component, pt_value)
                    label = output_component.name + '.' + pt_value
                    line_style = '-'
                    # raise IOError('Stats not available for forward model')
                elif analysis in ['lhs', 'parstudy']:
                    ind_list = list(range(len(time)))
                    obs_names = [full_obs_nm + '_{0}'.format(indd) for indd in ind_list]
                    obs_percentiles = percentile(s.recarray[obs_names],
                                                 [0, 25, 50, 75, 100])
                    obs_means = mean(s.recarray[obs_names])
                    values = np.array(
                        [s.recarray[full_obs_nm+'_'+str(indd)] for indd in ind_list])
                    label = 'Simulated Values'
                    line_style = 'C0'
                else:
                    pass
                if 'real' in plot_type:
                    plt.plot(time, values, line_style, label=label)
                if 'stats' in plot_type:
                    plt.fill_between(time, obs_percentiles[3, :],
                                     obs_percentiles[4, :], label='Upper quartile',
                                     color='#1f77b4', alpha=0.1)
                    plt.fill_between(time, obs_percentiles[1, :],
                                     obs_percentiles[3, :],
                                     label='Middle quartile',
                                     color='#ff7f0e', alpha=0.2)
                    plt.fill_between(time, obs_percentiles[0, :],
                                     obs_percentiles[1, :],
                                     label='Lower quartile',
                                     color='#2ca02c', alpha=0.1)
                    plt.plot(time, obs_percentiles[2, :],
                             color='C{0}'.format(color % 10), label='Median Value')
                    plt.plot(time, obs_means,
                             color='C{0}'.format((color+1) % 10), label='Mean Value')

                plt.xlabel('Time, t $[years]$')
                plt.grid(alpha=0.15)
                if pt_value in Y_LABEL_DICT:
                    plt.ylabel(Y_LABEL_DICT[pt_value])
                sub_title = full_obs_nm
                if full_obs_nm in subplot:
                    sub_title = subplot[full_obs_nm]
                if not single_plot:
                    ax.set_title(sub_title)
                else:
                    color += 2

    if analysis == 'forward':
        fig.subplots_adjust(left=0.10, right=0.95, hspace=0.3)
        ax_list = fig.axes
        for ax in ax_list:
            handle_list = []
            label_list = []
            handles, labels = ax.get_legend_handles_labels()

            for handle, label in zip(handles, labels):
                if label not in label_list:
                    handle_list.append(handle)
                    label_list.append(label)

                ax.legend(handle_list, label_list)

    elif analysis in ['lhs', 'parstudy']:
        fig.subplots_adjust(left=0.10, right=0.85, hspace=0.3)
        ax = plt.gca()
        handles, labels = ax.get_legend_handles_labels()
        handle_list, label_list = [], []
        for handle, label in zip(handles, labels):
            if label not in label_list:
                handle_list.append(handle)
                label_list.append(label)
        fig.legend(handle_list, label_list, loc=7)
    else:
        pass
    # ax.ticklabel_format(axis='y', style='sci')
    if title:
        plt.suptitle(title)
    else:
        plt.suptitle(default_title)
    if savefig:
        try:
            plt.savefig(savefig)
        except ValueError:
            # User has specified plot with a '.' in name but no extension.
            # Add .png as output format.
            savefig += '.png'
            plt.savefig(savefig)
    else:
        plt.show()
