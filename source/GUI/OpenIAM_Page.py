import os
import Pmw

import tkinter as tk
from tkinter import ttk
from tkinter import StringVar
from tkinter import DoubleVar
from tkinter import BooleanVar
from tkinter import messagebox

from dictionarydata import componentVars, APP_SIZE, TAB_SIZE
from dictionarydata import aquifers
from dictionarydata import connections
from dictionarydata import ANALYSIS_TYPES
from dictionarydata import LOGGING_TYPES
from dictionarydata import COMPONENT_TYPES
from dictionarydata import savedDictionary
from dictionarydata import LABEL_FONT
from dictionarydata import INSTRUCTIONS_FONT
from dictionarydata import BUTTON_WIDTH, FILE_ENTRY_WIDTH
from dictionarydata import (MODEL_TAB_LABEL_WIDTH1, MODEL_TAB_LABEL_WIDTH2,
                            MODEL_TAB_LARGE_LABEL_WIDTH,
                            MODEL_TAB_ENTRY_WIDTH, MODEL_TAB_MENU_WIDTH)
from dictionarydata import (SETUP_LABEL_WIDTH, SETUP_ENTRY_WIDTH, SETUP_MENU_WIDTH)

import cmpnts_tabs.strata_tab as strata_tab


# Define default values for the dynamic kwargs for different components
well_dyn_data_defaults = [1.0e+6, 0.0]
aquifer_dyn_data_defaults = [1.0e-7, 1.0e-7, 100.0, 100.0]
atm_dyn_data_defaults = [1.0e-5]


class OpenIAM_Page(tk.Frame):
    def __init__(self, parent, controller):
        """
        Constructor method for OpenIAMPage.
        """
        tk.Frame.__init__(self, parent)
        self.toolTip = Pmw.Balloon(parent)

        # Setup all variables needed for this frame
        self.controller = controller
        self.controller.connection = StringVar()
        self.controller.connection.set(connections[0])

        # dyn_data_vars are used to pass
        # pressure/saturation or brine/CO2 leakage rates, masses data
        self.dyn_data_vars = []

        componentVars['analysis'] = {}
        componentVars['strata'] = {}

        def show_dashboard():
            """
            Create and show dashboard page.
            """
            currentDictionary = {}

            for key in componentVars:
                try:
                    currentDictionary[key] = componentVars[key].get()
                except:
                    currentDictionary[key] = {}

                    for object in componentVars[key]:
                        try:
                            currentDictionary[key][object] = (
                                componentVars[key][object].get())
                        except:
                            currentDictionary[key][object] = (
                                componentVars[key][object])

            if savedDictionary != currentDictionary:
                MsgBox = messagebox.askquestion(
                    'Save data', ''.join(['Would you like to save the data ',
                                          'before switching to the Dashboard?']),
                    default='yes', icon='question', type='yesnocancel')

                if MsgBox == 'cancel':
                    pass
                else:
                    if MsgBox == 'yes':
                        self.controller.populate_dictionary()
                    self.controller.show_dashboard()
            else:
                self.controller.show_dashboard()

        def add_widgets_for_dyn_pars(cmpnt_type):
            if cmpnt_type == 'well_fault_seal': # works for wells, fault and seal components
                pressure_label = ttk.Label(
                    componentsSetupFrame, width=SETUP_LABEL_WIDTH, text="Pressure [Pa]:")
                pressure_textField = tk.Entry(
                    componentsSetupFrame, width=SETUP_ENTRY_WIDTH,
                    textvariable=self.dyn_data_vars[0])

                pressure_label.grid(
                    row=2, column=0, pady=5, padx=5, sticky='w')
                pressure_textField.grid(
                    row=2, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(pressure_textField, ''.join(
                    ['Enter pressure data manually or provide path ',
                     'to the file containing data.']))

                pressure_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[0]), width=BUTTON_WIDTH)
                pressure_button.grid(row=2, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(pressure_button,
                                  'Select file containing pressure data.')

                saturation_label = ttk.Label(
                    componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                    text="CO{} saturation [-]:".format(u'\u2082'))
                saturation_textField = tk.Entry(
                    componentsSetupFrame, width=SETUP_ENTRY_WIDTH,
                    textvariable=self.dyn_data_vars[1])

                saturation_label.grid(row=3, column=0, pady=5, padx=5, sticky='w')
                saturation_textField.grid(row=3, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(saturation_textField, ''.join(
                    ['Enter CO{} saturation data manually or provide path ',
                     'to the file containing data.']).format(u'\u2082'))

                saturation_button = ttk.Button(
                    componentsSetupFrame, text="Browse", width=BUTTON_WIDTH,
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[1]))
                saturation_button.grid(row=3, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(saturation_button,
                                  ''.join(['Select file containing CO{} ',
                                           'saturation data.']).format(u'\u2082'))

            elif cmpnt_type == 'aquifer':
                brine_rate_label = ttk.Label(componentsSetupFrame,
                                             width=SETUP_LABEL_WIDTH,
                                             text="Brine flow rate [kg/s]:")
                brine_rate_textField = tk.Entry(
                    componentsSetupFrame, width=SETUP_ENTRY_WIDTH,
                    textvariable=self.dyn_data_vars[0])

                brine_rate_label.grid(row=2, column=0, pady=5, padx=5, sticky='w')
                brine_rate_textField.grid(row=2, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(brine_rate_textField, ''.join(
                    ['Enter brine flow rate data manually or ',
                     'provide path to the file containing data.']))

                brine_rate_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[0]), width=BUTTON_WIDTH)
                brine_rate_button.grid(row=2, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(brine_rate_button,
                                  'Select file containing brine flow rate data.')

                co2_rate_label = ttk.Label(
                    componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                    text="CO{} flow rate [kg/s]:".format(u'\u2082'))
                co2_rate_textField = tk.Entry(
                    componentsSetupFrame, width=SETUP_ENTRY_WIDTH,
                    textvariable=self.dyn_data_vars[1])

                co2_rate_label.grid(
                    row=3, column=0, pady=5, padx=5, sticky='w')
                co2_rate_textField.grid(
                    row=3, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(co2_rate_textField, ''.join(
                    ['Enter CO{} flow rate data manually or provide path to ',
                     'the file containing data.']).format(u'\u2082'))

                co2_rate_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[1]), width=BUTTON_WIDTH)
                co2_rate_button.grid(row=3, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(co2_rate_button, ''.join([
                    'Select file containing CO{} flow rate data.']).format(u'\u2082'))

                brine_mass_label = ttk.Label(componentsSetupFrame,
                                             width=SETUP_LABEL_WIDTH,
                                             text="Brine mass [kg]:")
                brine_mass_textField = tk.Entry(componentsSetupFrame,
                                                textvariable=self.dyn_data_vars[2],
                                                width=SETUP_ENTRY_WIDTH)
                brine_mass_label.grid(
                    row=4, column=0, pady=5, padx=5, sticky='w')
                brine_mass_textField.grid(
                    row=4, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(brine_mass_textField, ''.join(
                    ['Enter mass of brine leaked to the aquifer manually ',
                     'or provide path to the file containing data.']))

                brine_mass_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[2]), width=BUTTON_WIDTH)
                brine_mass_button.grid(
                    row=4, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(brine_mass_button,
                                  'Select file containing brine mass data.')

                co2_mass_label = ttk.Label(
                    componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                    text="CO{} mass [kg]:".format(u'\u2082'))
                co2_mass_textField = tk.Entry(componentsSetupFrame,
                                              textvariable=self.dyn_data_vars[3],
                                              width=SETUP_ENTRY_WIDTH)
                co2_mass_label.grid(row=5, column=0, pady=5, padx=5, sticky='w')
                co2_mass_textField.grid(row=5, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(co2_mass_textField, ''.join(
                    ['Enter mass of CO{} leaked to the aquifer manually or ',
                     'provide path to the file containing data.']).format(u'\u2082'))

                co2_mass_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[3]), width=BUTTON_WIDTH)
                co2_mass_button.grid(row=5, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(
                    co2_mass_button,
                    'Select file containing CO{} mass data.'.format(u'\u2082'))

            else:
                co2_rate_label = ttk.Label(
                    componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                    text="CO{} flow rate [kg/s]:".format(u'\u2082'))
                co2_rate_textField = tk.Entry(componentsSetupFrame,
                                              textvariable=self.dyn_data_vars[0],
                                              width=SETUP_ENTRY_WIDTH)

                co2_rate_label.grid(
                    row=2, column=0, pady=5, padx=5, sticky='w')
                co2_rate_textField.grid(
                    row=2, column=1, pady=5, padx=5, sticky='w')
                self.toolTip.bind(co2_rate_textField, ''.join(
                    ['Enter flow rate of CO{} leaked to the atmosphere ',
                     'manually or provide path to the file containing ',
                     'data.']).format(u'\u2082'))

                co2_rate_button = ttk.Button(
                    componentsSetupFrame, text="Browse",
                    command=lambda: controller.choose_dynamic_parameters_file(
                        self.dyn_data_vars[0]), width=BUTTON_WIDTH)
                co2_rate_button.grid(
                    row=2, column=2, pady=5, padx=25, sticky='w')
                self.toolTip.bind(
                    co2_rate_button,
                    'Select file containing CO{} flow rate data.'.format(u'\u2082'))

        def change_connection(conn):
            """
            Set the component model that the new component model will
            have a connection to.
            """
            for widget in componentsSetupFrame.winfo_children():
                widget.destroy()

            connection_label = ttk.Label(
                componentsSetupFrame, width=SETUP_LABEL_WIDTH, text="Connection:")
            self.controller.connection.set(connections[0])
            connection_menu = tk.OptionMenu(
                componentsSetupFrame, self.controller.connection,
                *connections, command=change_connection)

            connection_menu.config(width=SETUP_MENU_WIDTH)
            connection_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
            connection_menu.grid(row=0, column=1, pady=5, padx=5, sticky='w')

            if (self.controller.componentType.get().find('Aquifer') != -1) or (
                    self.controller.componentType.get().find('AZMI') != -1):
                aquiferName.set(aquifers[0])
                label = ttk.Label(componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                                  text="Aquifer name:")
                menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

                menu.config(width=SETUP_MENU_WIDTH)
                label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
                menu.grid(row=1, column=1, pady=5, padx=5, sticky='w')

            # # TODO This part will replace lines above 271-280 above if we decide that
            # # fault flow component can provide input for any of the aquifer components
            # if (self.controller.componentType.get().find('Aquifer') != -1) or (
            #         self.controller.componentType.get().find('AZMI') != -1) or (
            #             self.controller.componentType.get().find('Fault') != -1):
            #     aquiferName.set(aquifers[0])
            #     label = ttk.Label(componentsSetupFrame, width=SETUP_LABEL_WIDTH,
            #                       text="Aquifer name:")
            #     menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

            #     menu.config(width=SETUP_MENU_WIDTH)
            #     label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
            #     menu.grid(row=1, column=1, pady=5, padx=5, sticky='w')

            if conn != 'Dynamic Parameters':
                self.controller.connection.set(conn)

            if conn == 'Dynamic Parameters':
                if (self.controller.componentType.get().find('Wellbore') != -1) or (
                        self.controller.componentType.get().find('Fault') != -1) or (
                            self.controller.componentType.get().find('Seal') != -1):
                    self.dyn_data_vars = [StringVar(), StringVar()]
                    for ind in range(2):
                        self.dyn_data_vars[ind].set(well_dyn_data_defaults[ind])

                    add_widgets_for_dyn_pars('well_fault_seal')

                if (self.controller.componentType.get().find('Aquifer') != -1) or (
                        self.controller.componentType.get().find('AZMI') != -1):
                    self.dyn_data_vars = [StringVar(), StringVar(), StringVar(), StringVar()]
                    for ind in range(4):
                        self.dyn_data_vars[ind].set(aquifer_dyn_data_defaults[ind])

                    add_widgets_for_dyn_pars('aquifer')

                if self.controller.componentType.get().find('Atmo') != -1:
                    self.dyn_data_vars = [StringVar()]
                    for ind in range(1):
                        self.dyn_data_vars[ind].set(atm_dyn_data_defaults[ind])

                    add_widgets_for_dyn_pars('atmosphere')

        def change_component_type_setup(componentType):
            """
            Update the add component model frame.

            Update the add component model frame to match what is needed
            for each different type of component model.
            """
            global aquifers

            for widget in componentsSetupFrame.winfo_children():
                widget.destroy()

            aquifers = ['aquifer{}'.format(ind) for ind in range(
                1, componentVars['strata']['Params']['numberOfShaleLayers'].get())]

            if componentType.find('Reservoir') != -1:
                return

            if componentType.find('Plume') != -1:
                return

            if componentType.find('Chemical') != -1:
                return

            if (componentType.find('Generalized') != -1) or (
                    componentType.find('Aquifer') != -1) or (
                        componentType.find('AZMI') != -1):
                aquiferName.set(aquifers[0])
                label = ttk.Label(componentsSetupFrame, width=SETUP_LABEL_WIDTH,
                                  text="Aquifer name:")
                menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

                menu.config(width=SETUP_MENU_WIDTH)
                self.toolTip.bind(menu,
                                  ''.join(['Select which of the aquifer layers ',
                                           'this component model will represent.']))
                label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
                menu.grid(row=1, column=1, pady=5, padx=5, sticky='w')
                if (componentType.find('Generalized') != -1):
                    # No further processing is required for this type of components
                    # since generalized flow rate component does not need connections
                    return

            # # TODO This part will replace lines above 345-363 above if we decide that
            # # fault flow component can provide input for any of the aquifer components
            # if (componentType.find('Generalized') != -1) or (
            #         componentType.find('Fault') != -1) or (
            #             componentType.find('Aquifer') != -1) or (
            #                 componentType.find('AZMI') != -1):
            #     aquiferName.set(aquifers[0])
            #     label = ttk.Label(componentsSetupFrame, width=SETUP_LABEL_WIDTH,
            #                       text="Aquifer name:")
            #     menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

            #     menu.config(width=SETUP_MENU_WIDTH)
            #     self.toolTip.bind(menu,
            #                       ''.join(['Select which of the aquifer layers ',
            #                                'this component model will represent.']))
            #     label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
            #     menu.grid(row=1, column=1, pady=5, padx=5, sticky='w')
            #     if (componentType.find('Generalized') != -1):
            #         # No further processing is required for this type of components
            #         # since generalized flow rate component does not need connections
            #         return

            connection_label = ttk.Label(
                componentsSetupFrame, width=SETUP_LABEL_WIDTH, text="Connection:")
            self.controller.connection.set(connections[0])
            connection_menu = tk.OptionMenu(
                componentsSetupFrame, self.controller.connection,
                *connections, command=change_connection)

            connection_menu.config(width=SETUP_MENU_WIDTH)
            self.toolTip.bind(connection_menu,
                              'Select the connection for your component model.')
            connection_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
            connection_menu.grid(row=0, column=1, pady=5, padx=5, sticky='w')

            if (componentType.find('Wellbore') != -1) or (
                    componentType.find('Seal') != -1) or (
                        componentType.find('Fault') != -1):
                self.dyn_data_vars = [StringVar(), StringVar()]
                for ind in range(2):
                    self.dyn_data_vars[ind].set(well_dyn_data_defaults[ind])

                add_widgets_for_dyn_pars('well_fault_seal')

            if (componentType.find('Aquifer') != -1) or (
                    componentType.find('AZMI') != -1):
                self.dyn_data_vars = [StringVar(), StringVar(), StringVar(), StringVar()]
                for ind in range(4):
                    self.dyn_data_vars[ind].set(aquifer_dyn_data_defaults[ind])

                add_widgets_for_dyn_pars('aquifer')

            if componentType.find('Atmo') != -1:
                self.dyn_data_vars = [StringVar()]
                for ind in range(1):
                    self.dyn_data_vars[ind].set(atm_dyn_data_defaults[ind])

                add_widgets_for_dyn_pars('atmosphere')

        tabControl = ttk.Notebook(self, width=APP_SIZE[0]-70)
        style = ttk.Style()
        current_theme = style.theme_use()
        style.theme_settings(
            current_theme, {"TNotebook.Tab": {"configure": {"padding": [20, 5]}}})

        self.controller.tabControl = tabControl
        modelTab = ttk.Frame(tabControl, padding=10)
        modelFrame = ttk.Frame(modelTab, padding=10)
        tabControl.add(modelTab, text="Model")
        tabControl.pack(expand=1, fill="both", padx=10, pady=5)

        if componentVars['simName'].get() == '':
            componentVars['simName'].set('Default')

        def test_val(inStr, acttyp):
            if acttyp == '1': # insert
                if not list(inStr)[-1].isalnum():
                    return False
            return True

        # Setup model frame and content of Model tab
        modelFrame.grid(row=0, column=0, columnspan=4)

        nameFrame = ttk.Frame(modelFrame)
        nameFrame.grid(row=0, column=0, columnspan=6, sticky='w')

        simName_label = ttk.Label(nameFrame, text='Simulation name:',
                                  width=MODEL_TAB_LABEL_WIDTH2)
        simName_label.grid(row=0, column=0, padx=5, pady=(5, 10), sticky='w')

        simName_txtField = tk.Entry(nameFrame, textvariable=componentVars['simName'],
                                    validate="key", width=FILE_ENTRY_WIDTH)
        simName_txtField.grid(row=0, column=1, padx=5, pady=5, sticky='w')
        simName_txtField['validatecommand'] = (
            simName_txtField.register(test_val), '%P', '%d')
        self.toolTip.bind(simName_txtField,
                          'Enter a unique name for the simulation.')

        modelParams_label = ttk.Label(
            modelFrame, text="Model Parameters", font=LABEL_FONT,
            width=MODEL_TAB_LARGE_LABEL_WIDTH)
        modelParams_label.grid(row=1, column=0, columnspan=2, sticky='w')

        timeFrame = ttk.Frame(modelFrame)
        timeFrame.grid(row=2, column=0, columnspan=6, sticky='w')

        componentVars['endTime'] = DoubleVar()
        componentVars['endTime'].set(50.0)
        endTime_label = ttk.Label(timeFrame, text="End time [years]:",
                                  width=MODEL_TAB_LABEL_WIDTH2)
        endTime_txtField = tk.Entry(timeFrame, width=MODEL_TAB_ENTRY_WIDTH,
                                    textvariable=componentVars['endTime'])
        endTime_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
        endTime_txtField.grid(row=0, column=1, padx=5, pady=5, sticky='w')
        self.toolTip.bind(endTime_txtField, "Enter simulation time.")

        componentVars['timeStep'] = DoubleVar()
        componentVars['timeStep'].set(1.0)
        timeStep_label = ttk.Label(timeFrame, text="Time step [years]:",
                                   width=MODEL_TAB_LABEL_WIDTH2)
        timeStep_txtField = tk.Entry(timeFrame, width=MODEL_TAB_ENTRY_WIDTH,
                                     textvariable=componentVars['timeStep'])
        timeStep_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
        timeStep_txtField.grid(row=1, column=1, padx=5, pady=5, sticky='w')
        self.toolTip.bind(timeStep_txtField, "Enter time step.")

        self.controller.analysisFrame = tk.Frame(modelFrame)
        self.controller.analysisFrame.grid(row=3, column=0, columnspan=6, sticky='we')

        componentVars['analysis']['type'] = StringVar()
        componentVars['analysis']['type'].set(ANALYSIS_TYPES[0])
        analysis_label = ttk.Label(
            self.controller.analysisFrame, text="Analysis:", width=MODEL_TAB_LABEL_WIDTH2)
        analysis_menu = tk.OptionMenu(
            self.controller.analysisFrame,
            componentVars['analysis']['type'], *ANALYSIS_TYPES,
            command=lambda _: self.controller.set_analysis_type(
                componentVars['analysis']['type'].get()))
        analysis_menu.config(width=MODEL_TAB_MENU_WIDTH)
        analysis_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
        analysis_menu.grid(row=0, column=1, pady=5, padx=5, sticky='w')
        self.toolTip.bind(analysis_menu,
                          'Select type of analysis to be used for simulation.')

        loggingFrame = ttk.Frame(modelFrame)
        loggingFrame.grid(row=4, column=0, columnspan=6, sticky='w')

        componentVars['logging'] = StringVar()
        componentVars['logging'].set(LOGGING_TYPES[1])
        logging_label = ttk.Label(
            loggingFrame, text="Logging:", width=MODEL_TAB_LABEL_WIDTH2)
        logging_Menu = tk.OptionMenu(
            loggingFrame, componentVars['logging'], *LOGGING_TYPES)
        logging_Menu.config(width=MODEL_TAB_MENU_WIDTH)
        logging_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
        logging_Menu.grid(row=0, column=1, pady=5, padx=5, sticky='w')
        self.toolTip.bind(logging_Menu,
                          'Select type of logging for simulation.')

        componentVars['outputDirectory'] = StringVar()
        try:
            componentVars['outputDirectory'].set(os.path.join(
                os.path.dirname(os.path.dirname(
                    os.path.dirname(os.path.abspath(__file__)))), 'Output'))
        except:
            componentVars['outputDirectory'].set('~Documents')

        outputFrame1 = ttk.Frame(modelFrame)
        outputFrame1.grid(row=5, column=0, columnspan=6, sticky='w')

        outputDirectory_label = ttk.Label(
            outputFrame1, text="Output directory:", width=MODEL_TAB_LABEL_WIDTH2)
        outputDirectory_txtField = tk.Entry(outputFrame1, width=FILE_ENTRY_WIDTH,
                                            textvariable=componentVars['outputDirectory'])
        outputDirectory_browse = ttk.Button(
            outputFrame1, width=BUTTON_WIDTH, text="Browse",
            command=lambda: self.controller.choose_output_dir(
                componentVars['outputDirectory']))
        outputDirectory_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
        outputDirectory_txtField.grid(
            row=1, column=1, columnspan=3, pady=5, padx=5, sticky='w')
        outputDirectory_browse.grid(
            row=1, column=4, pady=5, padx=5, sticky='w')
        self.toolTip.bind(outputDirectory_txtField,
                          'Enter or select a location to save simulation outputs.')
        self.toolTip.bind(outputDirectory_browse,
                          'Open file browser and select output directory.')

        outputFrame2 = ttk.Frame(modelFrame)
        outputFrame2.grid(row=6, column=0, columnspan=6, sticky='w')

        componentVars['outputDirectoryGenerate'] = BooleanVar()
        componentVars['outputDirectoryGenerate'].set(0)
        outputDirectoryGenerate_label = ttk.Label(
            outputFrame2, text="Generate output directory:",
            width=MODEL_TAB_LABEL_WIDTH1)
        outputDirectoryGenerate_checkbox = tk.Checkbutton(
            outputFrame2, variable=componentVars['outputDirectoryGenerate'])
        outputDirectoryGenerate_label.grid(
            row=2, column=0, pady=5, padx=5, sticky='w')
        outputDirectoryGenerate_checkbox.grid(
            row=2, column=1, pady=5, padx=5, sticky='w')
        self.toolTip.bind(outputDirectoryGenerate_checkbox,
                          ''.join(['Check to generate a directory name augmented with',
                                   ' timestamp for outputs to be saved.']))

        descriptionFrame = ttk.Frame(modelFrame)
        descriptionFrame.grid(row=9, column=0, columnspan=7,
                              sticky='w', pady=(200, 5))

        descriptionLabel = ttk.Label(
            descriptionFrame,
            text=''.join(['After entering system model parameters',
                          ' proceed to Stratigraphy.']),
            font=INSTRUCTIONS_FONT)
        descriptionLabel.grid(row=0, column=0, columnspan=6,
                              padx=5, pady=15, sticky='w')

        nextButton = ttk.Button(
            descriptionFrame, text='Stratigraphy',
            width=BUTTON_WIDTH, command=lambda: tabControl.select(
                '.!frame.!openiam_page.!notebook.!frame2'))
        nextButton.grid(row=0, column=6, padx=5, pady=15, sticky='w')
        self.toolTip.bind(
            nextButton,
            'Switch to the Stratigraphy tab, the second step of the setup.')

        # Setup stratigraphy tab
        new_tab = ttk.Frame(tabControl, padding=10)

        self.controller.strata_scanv = tk.Canvas(new_tab, relief=tk.SUNKEN)
        self.controller.strata_scanv.config(width=TAB_SIZE[0], height=TAB_SIZE[1])
        self.controller.strata_scanv.config(scrollregion=(0, 0, 0, 0))
        self.controller.strata_scanv.config(highlightthickness=0)

        sybar = tk.Scrollbar(new_tab, orient='vertical')
        sybar.config(command=self.controller.strata_scanv.yview)

        self.controller.strata_scanv.config(yscrollcommand=sybar.set)
        sybar.pack(side=tk.RIGHT, fill=tk.Y)
        self.controller.strata_scanv.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        stratigraphyTab = tk.Frame(self.controller.strata_scanv)
        stratigraphyTab.grid(row=0, column=0, columnspan=10)

        self.controller.strata_scanv.create_window((10, 0), window=stratigraphyTab, anchor='nw')

        tabControl.add(new_tab, text="Stratigraphy")
        tabControl.pack(expand=1, fill="both")

        strata_tab.add_widgets(self.controller, stratigraphyTab, self.toolTip)

        addComponentTab = ttk.Frame(tabControl, padding=10)
        tabControl.add(addComponentTab, text="Add Components")
        tabControl.pack(expand=1, fill="both")
        addComponentFrame = ttk.Frame(addComponentTab, padding=10)
        addComponentFrame.pack(expand=1, fill="both", anchor=tk.NW)

        addComponent_label = ttk.Label(
            addComponentFrame, text="Add Component", font=LABEL_FONT)
        addComponent_label.grid(row=0, column=0, sticky='w')

        componentName = StringVar()
        aquiferName = StringVar()
        componentName.set('')

        componentNameFrame = ttk.Frame(addComponentFrame)
        componentNameFrame.grid(row=1, column=0, columnspan=2, sticky='w')
        componentName_label = ttk.Label(
            componentNameFrame, width=SETUP_LABEL_WIDTH, text="Component name:")
        self.controller.componentName_textField = tk.Entry(
            componentNameFrame, textvariable=componentName,
            width=SETUP_ENTRY_WIDTH, validate="key")
        self.controller.componentName_textField['validatecommand'] = (
            self.controller.componentName_textField.register(test_val), '%P', '%d')

        componentName_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
        self.controller.componentName_textField.grid(
            row=1, column=1, pady=5, padx=5, sticky='w')
        self.toolTip.bind(self.controller.componentName_textField,
                          'Assure that the component has a unique name.')

        self.controller.componentType = StringVar()
        self.controller.componentType.set(COMPONENT_TYPES[0])
        componentType_label = ttk.Label(
            componentNameFrame, width=SETUP_LABEL_WIDTH, text="Model type:")
        componentType_Menu = tk.OptionMenu(
            componentNameFrame, self.controller.componentType,
            *COMPONENT_TYPES, command=change_component_type_setup)

        componentType_Menu.config(width=SETUP_MENU_WIDTH)
        self.toolTip.bind(
            componentType_Menu,
            'Select the type of component model to be added to the simulation.')
        componentType_label.grid(row=2, column=0, pady=5, padx=5, sticky='w')
        componentType_Menu.grid(row=2, column=1, pady=5, padx=5, sticky='w')

        connection_menu = tk.OptionMenu(
            addComponentFrame, self.controller.connection, *connections)
        tabControl.connection_menu = connection_menu

        componentsSetupFrame = ttk.Frame(addComponentFrame)
        tabControl.componentsSetupFrame = componentsSetupFrame
        componentsSetupFrame.grid(
            row=3, column=0, columnspan=3, sticky='w')

        addComponentButton = ttk.Button(
            addComponentFrame, text="Add Component", width=BUTTON_WIDTH,
            command=lambda: self.controller.add_component(
                self.controller.connection, aquiferName, tabControl,
                componentName, self.controller.componentType, connection_menu,
                componentsSetupFrame, self.controller, self.dyn_data_vars))
        addComponentButton.grid(row=1, column=2, pady=2, padx=25, sticky='nw')
        self.toolTip.bind(
            addComponentButton,
            ''.join(['After configuration is finished click Add Component',
                     ' and switch to the corresponding tab for its setup.']))

        textDescription = ttk.Label(
            addComponentTab, font=INSTRUCTIONS_FONT,
            text=''.join([
                'Add each component model for the system to be ',
                'simulated. After specifying all component models,',
                '\nsave the model and return to Dashboard to run the ',
                'simulation. ']))
        textDescription.pack(anchor=tk.SW, pady=10, padx=5)
#        textDescription.grid(row=6, column=0, pady=(50,5),
#                             padx=5, columnspan=10, sticky='w')

        saveButton = ttk.Button(
            self, text="Save", width=BUTTON_WIDTH,
            command=lambda: self.controller.populate_dictionary())
        saveButton.pack(side='left', padx=10, pady=5)

        cancelButton = ttk.Button(self, text="Return to Dashboard",
                                  command=show_dashboard, width=BUTTON_WIDTH)
        cancelButton.pack(side='right', padx=10, pady=5)

        for key in componentVars:
            if key != 'strata':
                try:
                    savedDictionary[key] = componentVars[key].get()
                except:
                    savedDictionary[key] = {}

                    for object in componentVars[key]:
                        savedDictionary[key][object] = componentVars[
                            key][object].get()
