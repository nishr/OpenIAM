# -*- coding: utf-8 -*-
"""
Module contains methods needed to read data from wellbore type component tabs.
"""
import os
import sys

import tkinter as tk
from tkinter import ttk
from tkinter import StringVar, IntVar, DoubleVar
from tkinter import messagebox

# Save location of GUI folder
GUI_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(GUI_DIR)

from dictionarydata import componentVars
from dictionarydata import (PARAMETER_LABEL_WIDTH,
                            DISTRIBUTION_ARG_TEXTFIELD_WIDTH)

LABEL_WIDTH = 17


def read_locations_data(data, cmpnt_nm):
    """ Read data located in the well/source location frames."""
    # Get total number of locations
    num_locs = componentVars[cmpnt_nm]['number'].get()
    data['number'] = num_locs

    # Check if the known well locations are provided
    if componentVars[cmpnt_nm]['xCoordinates'].get() and (
            componentVars[cmpnt_nm]['yCoordinates'].get()):
        data['Locations'] = {}
        data['Locations']['coordx'] = []
        data['Locations']['coordy'] = []
        for numx in componentVars[cmpnt_nm]['xCoordinates'].get().split(','):
            data['Locations']['coordx'].append(float(numx.strip()))

        for numy in componentVars[cmpnt_nm]['yCoordinates'].get().split(','):
            data['Locations']['coordy'].append(float(numy.strip()))

    elif componentVars[cmpnt_nm]['xCoordinates'].get():
        messagebox.showerror('Error', ''.join([
            'The y-coordinates of the locations for component {} ',
            'are not provided.']).format(cmpnt_nm))
        return

    elif componentVars[cmpnt_nm]['yCoordinates'].get():
        messagebox.showerror('Error', ''.join([
            'The x-coordinates of the locations for component {} ',
            'are not provided.']).format(cmpnt_nm))
        return

    if len(data['Locations']['coordx']) != len(data['Locations']['coordy']):
        messagebox.showerror('Error', ''.join([
            'Lengths of the provided lists of x-coordinates and y-coordinates ',
            'are not the same.']))
        return

    if num_locs < len(data['Locations']['coordx']):
        messagebox.showwarning('Warning', ''.join([
            'Number of provided locations (through x- and y-coordinates) exceeds ',
            'specified value in parameter Number. Only the first {} locations ',
            'will be used for simulation.']).format(num_locs))
        data['Locations']['coordx'] = data['Locations']['coordx'][0:num_locs]
        data['Locations']['coordy'] = data['Locations']['coordy'][0:num_locs]
        return

    if num_locs > len(data['Locations']['coordx']):
        # Check whether random locations domain is involved
        if componentVars[cmpnt_nm]['useRandomLocDomain'].get():
            data['RandomLocDomain'] = {}
            for key in ['xmin', 'ymin', 'xmax', 'ymax', 'seed']:
                data['RandomLocDomain'][key] = (
                    componentVars[cmpnt_nm]['RandomLocDomain'][key].get())
        else:
            messagebox.showwarning('Warning', ''.join([
                'Number of provided locations (through x- and y-coordinates) ',
                'is less than the specified value in parameter Number. Only provided ',
                'locations will be used. Consider using random locations domain ',
                'to generate additional locations.']))
            data['number'] = len(data['Locations']['coordx'])

    return


def load_locations_data(controller, comp_data, cmpnt_nm):
    componentVars[cmpnt_nm]['number'].set(comp_data['number'])
    # if known locations were provided
    if 'Locations' in comp_data:
        # TODO update values with what is inside
        xcoords = ", ".join(
            str(item) for item in comp_data['Locations']['coordx'])
        ycoords = ", ".join(
            str(item) for item in comp_data['Locations']['coordy'])
        componentVars[cmpnt_nm]['xCoordinates'].set(xcoords)
        componentVars[cmpnt_nm]['yCoordinates'].set(ycoords)
    else:
        componentVars[cmpnt_nm]['xCoordinates'].set('')
        componentVars[cmpnt_nm]['yCoordinates'].set('')

    if 'RandomLocDomain' in comp_data:
        for key_arg in ['xmin', 'ymin', 'xmax', 'ymax', 'seed']:
            componentVars[cmpnt_nm]['RandomLocDomain'][key_arg].set(
                str(comp_data['RandomLocDomain'][key_arg]))
            componentVars[cmpnt_nm]['useRandomLocDomain'].set(1)
    else:
        componentVars[cmpnt_nm]['useRandomLocDomain'].set(0)

    # Get name of frame containing random locations
    parent_frame = controller.nametowidget(controller.getvar(cmpnt_nm+'_locs'))
    randloc_frame = parent_frame.rand_locs_subframe
    disable_rand_locs_frame_widgets(componentVars[cmpnt_nm], randloc_frame)

    if comp_data['type'] in ['OpenWellbore', 'GeneralizedFlowRate']:
        componentVars[cmpnt_nm]['LeakTo'].set(comp_data['LeakTo'])


def add_wellbore_frame_widgets(controller, cmpnt_nm, frame,
                               tool_tip, gfr_cmpnt=False):
    """ Add frame allowing to setup locations for wellbore-type of components."""
    if not gfr_cmpnt:
        tip_pieces = ['Wellbore', 'well', 'wells']
    else:
        tip_pieces = ['Source', 'source', 'sources']

    well_loc_label = ttk.Label(
        frame, text="{} locations:".format(tip_pieces[0]),
        width=PARAMETER_LABEL_WIDTH)
    well_loc_label.grid(row=0, column=0, columnspan=2, sticky='w', padx=5)

    known_well_frame = ttk.Frame(frame)
    known_well_frame.grid(row=1, column=0, sticky='w', padx=20)

    componentVars[cmpnt_nm]['xCoordinates'] = StringVar()
    componentVars[cmpnt_nm]['xCoordinates'].set("100")
    x_coords_label = ttk.Label(known_well_frame, text="x-coordinates [m]:",
                               width=LABEL_WIDTH)
    x_coords_field = tk.Entry(
        known_well_frame, width=2*DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['xCoordinates'])
    x_coords_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
    x_coords_field.grid(row=0, column=1, pady=5, padx=10, sticky='w')
    tool_tip.bind(x_coords_field, ''.join([
        'Enter x-coordinates of known {} locations, ',
        'separated by comma.']).format(tip_pieces[1]))

    componentVars[cmpnt_nm]['yCoordinates'] = StringVar()
    componentVars[cmpnt_nm]['yCoordinates'].set("100")
    y_coords_label = ttk.Label(known_well_frame, text="y-coordinates [m]:",
                               width=LABEL_WIDTH)
    y_coords_field = tk.Entry(
        known_well_frame, width=2*DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['yCoordinates'])
    y_coords_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
    y_coords_field.grid(row=1, column=1,
                        pady=5, padx=10, sticky='w')
    tool_tip.bind(y_coords_field, ''.join([
        'Enter y-coordinates of known {} locations, ',
        'separated by comma.']).format(tip_pieces[1]))

    rand_well_label_frame = ttk.Frame(frame)
    rand_well_label_frame.grid(row=2, column=0, sticky='w', padx=0)

    rand_well_domain_label = ttk.Label(
        rand_well_label_frame,
        text="Use random {} domain:".format(tip_pieces[2]),
        width=PARAMETER_LABEL_WIDTH)
    rand_well_domain_label.grid(row=0, column=0, sticky='w', padx=5)

    frame.rand_locs_subframe = ttk.Frame(frame)
    frame.rand_locs_subframe.grid(row=3, column=0, sticky='w', padx=20)

    domain_checkbox = tk.Checkbutton(
        rand_well_label_frame,
        variable=componentVars[cmpnt_nm]['useRandomLocDomain'],
        command=lambda: disable_rand_locs_frame_widgets(
            componentVars[cmpnt_nm], frame.rand_locs_subframe))
    domain_checkbox.grid(row=0, column=1, sticky='w', padx=5)
    tool_tip.bind(domain_checkbox,
                  ''.join(['Check to generate additional random {} ',
                           'locations over the domain.']).format(tip_pieces[1]))

    # produces a bounding box for all random wellbores to be placed within
    componentVars[cmpnt_nm]['RandomLocDomain'] = {}
    setup_dict = {'xmin': 40, 'ymin': 50, 'xmax': 100, 'ymax': 140, 'seed': 345}
    for key in setup_dict:
        if key != 'seed':
            componentVars[cmpnt_nm]['RandomLocDomain'][key] = DoubleVar()
        else:
            componentVars[cmpnt_nm]['RandomLocDomain'][key] = IntVar()
        componentVars[cmpnt_nm]['RandomLocDomain'][key].set(setup_dict[key])

    seed_label = ttk.Label(
        frame.rand_locs_subframe, text="Seed:", width=LABEL_WIDTH)
    seed_txtField = tk.Entry(
        frame.rand_locs_subframe, width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['RandomLocDomain']['seed'])

    randomWellXMin_label = ttk.Label(
        frame.rand_locs_subframe, text="x-minimum [m]:", width=LABEL_WIDTH)
    randomWellXMin_txtField = tk.Entry(
        frame.rand_locs_subframe, width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['RandomLocDomain']['xmin'])
    randomWellYMin_label = ttk.Label(
        frame.rand_locs_subframe, text="y-minimum [m]:", width=LABEL_WIDTH)
    randomWellYMin_txtField = tk.Entry(
        frame.rand_locs_subframe, width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['RandomLocDomain']['ymin'])

    randomWellXMax_label = ttk.Label(
        frame.rand_locs_subframe, text="x-maximum [m]:", width=LABEL_WIDTH)
    randomWellXMax_txtField = tk.Entry(
        frame.rand_locs_subframe, width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['RandomLocDomain']['xmax'])
    randomWellYMax_label = ttk.Label(
        frame.rand_locs_subframe, text="y-maximum [m]:", width=LABEL_WIDTH)
    randomWellYMax_txtField = tk.Entry(
        frame.rand_locs_subframe, width=DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
        textvariable=componentVars[cmpnt_nm]['RandomLocDomain']['ymax'])

    seed_label.grid(row=0, column=0, pady=5, padx=5, sticky='w')
    seed_txtField.grid(row=0, column=1, pady=5, padx=5, sticky='w')
    tool_tip.bind(seed_txtField,
                  ''.join(['Enter seed (starting point) for generation ',
                           'of random {} locations.']).format(tip_pieces[1]))

    randomWellXMin_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
    randomWellXMin_txtField.grid(row=1, column=1, pady=5, padx=5, sticky='w')
    tool_tip.bind(randomWellXMin_txtField,
                  'Enter minimum x-value for random {} domain.'.format(
                      tip_pieces[2]))

    randomWellYMin_label.grid(row=1, column=2, pady=5, padx=5, sticky='w')
    randomWellYMin_txtField.grid(row=1, column=3, pady=5, padx=5, sticky='w')
    tool_tip.bind(randomWellYMin_txtField,
                  'Enter minimum y-value for random {} domain.'.format(
                      tip_pieces[2]))

    randomWellXMax_label.grid(row=2, column=0, pady=5, padx=5, sticky='w')
    randomWellXMax_txtField.grid(row=2, column=1, pady=5, padx=5, sticky='w')
    tool_tip.bind(randomWellXMax_txtField,
                  'Enter maximum x-value for random {} domain.'.format(
                      tip_pieces[2]))

    randomWellYMax_label.grid(row=2, column=2, pady=5, padx=5, sticky='w')
    randomWellYMax_txtField.grid(row=2, column=3, pady=5, padx=5, sticky='w')
    tool_tip.bind(randomWellYMax_txtField,
                  'Enter maximum y-value for random {} domain.'.format(
                      tip_pieces[2]))
    disable_rand_locs_frame_widgets(
            componentVars[cmpnt_nm], frame.rand_locs_subframe)

    # Add reference to the locations frame to the controller to use
    # when the saved simulation is loaded
    controller.setvar(name=cmpnt_nm+'_locs', value=frame)


def disable_rand_locs_frame_widgets(variables, frame):
    """ Disable/enable widgets located on random sources/wells frame."""
    par_frames_state = {0: 'normal', 1: 'disabled'}

    check_button_state = variables['useRandomLocDomain'].get()

    for widget in frame.winfo_children():
        widget.configure(state=par_frames_state[1-check_button_state])


def add_obs_locs_frame_widgets(controller, cmpnt_nm, frame, tool_tip):
    obs_loc_label = ttk.Label(
        frame, text="Observation locations:", width=PARAMETER_LABEL_WIDTH)
    obs_loc_label.grid(row=0, column=0, columnspan=2, sticky='w', padx=5)

    coords_frame = ttk.Frame(frame)
    coords_frame.grid(row=1, column=0, sticky='w', padx=20)

    coords = ['x', 'y']
    arg_names = ['xCoordinates', 'yCoordinates']
    arg_labels = ['x-coordinates [m]:', 'y-coordinates [m]:']

    coords_labels = []
    coords_fields = []
    # Create and place label and entry widgets
    for ind in range(2):
        # Create variable to keep value of the coordinate
        componentVars[cmpnt_nm][arg_names[ind]] = StringVar()
        componentVars[cmpnt_nm][arg_names[ind]].set("")

        coords_labels.append(ttk.Label(
            coords_frame, text=arg_labels[ind], width=LABEL_WIDTH))

        coords_fields.append(tk.Entry(
            coords_frame,
            width=2*DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
            textvariable=componentVars[cmpnt_nm][arg_names[ind]]))
        coords_labels[-1].grid(row=ind, column=0, pady=5, padx=5, sticky='w')
        coords_fields[-1].grid(row=ind, column=1, pady=5, padx=10, sticky='w')
        tool_tip.bind(coords_fields[-1], ''.join([
            'Enter {}-coordinates of observation locations, ',
            'separated by comma.\nIf the field is left empty no additional ',
            'observations will be generated.']).format(coords[ind]))


def add_inj_well_frame_widgets(controller, cmpnt_nm, frame, tool_tip):
    inj_well_label = ttk.Label(frame, text="Injection well location:")
    inj_well_label.grid(row=0, column=0, columnspan=2, sticky='w', padx=5)

    inj_well_coords_frame = ttk.Frame(frame)
    inj_well_coords_frame.grid(row=1, column=0, sticky='w', padx=20)

    coords = ['x', 'y']
    arg_names = ['injX', 'injY']
    arg_labels = ['x-coordinate [m]:', 'y-coordinate [m]:']

    coords_labels = []
    coords_fields = []
    # Create and place label and entry widgets
    for ind in range(2):
        # Create variable to keep value of the coordinate
        componentVars[cmpnt_nm][arg_names[ind]] = StringVar()
        componentVars[cmpnt_nm][arg_names[ind]].set("0")

        coords_labels.append(ttk.Label(
            inj_well_coords_frame, text=arg_labels[ind], width=LABEL_WIDTH))

        coords_fields.append(tk.Entry(
            inj_well_coords_frame,
            width=2*DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
            textvariable=componentVars[cmpnt_nm][arg_names[ind]]))
        coords_labels[-1].grid(row=ind, column=0, pady=5, padx=5, sticky='w')
        coords_fields[-1].grid(row=ind, column=1, pady=5, padx=10, sticky='w')
        tool_tip.bind(
            coords_fields[-1],
            'Enter {}-coordinate of injection well'.format(coords[ind]))


def read_obs_locations_data(data, cmpnt_nm):
    """ Read data located in the observation location and injection well
    (if applicable) frames."""
    # Check if the observation locations are provided
    if componentVars[cmpnt_nm]['xCoordinates'].get():
        if componentVars[cmpnt_nm]['yCoordinates'].get():

            data['Locations'] = {}
            data['Locations']['coordx'] = []
            data['Locations']['coordy'] = []
            for numx in componentVars[cmpnt_nm]['xCoordinates'].get().split(','):
                data['Locations']['coordx'].append(float(numx.strip()))

            for numy in componentVars[cmpnt_nm]['yCoordinates'].get().split(','):
                data['Locations']['coordy'].append(float(numy.strip()))

            if len(data['Locations']['coordx']) != len(data['Locations']['coordy']):
                messagebox.showerror('Error', ''.join([
                    'Lengths of the provided lists of x-coordinates and y-coordinates ',
                    'are not the same.']))
                return
        else:
            messagebox.showerror('Error', ''.join([
                'The y-coordinates of the locations for component {} ',
                'are not provided.']).format(cmpnt_nm))
            return
    else:
        if componentVars[cmpnt_nm]['yCoordinates'].get():
            messagebox.showerror('Error', ''.join([
            'The x-coordinates of the locations for component {} ',
            'are not provided.']).format(cmpnt_nm))
            return

    # Check the type of the reservoir component
    if data['type'] in ['SimpleReservoir', 'AnalyticalReservoir']:
        if componentVars[cmpnt_nm]['injX'].get():
            if componentVars[cmpnt_nm]['injY'].get():
                data['InjectionWell'] = {}
                data['InjectionWell']['coordx'] = float(
                    componentVars[cmpnt_nm]['injX'].get())
                data['InjectionWell']['coordy'] = float(
                    componentVars[cmpnt_nm]['injY'].get())
            else:
                messagebox.showerror('Error', ''.join([
                    'The y-coordinate of the injection well location for component {} ',
                    'is not provided.']).format(cmpnt_nm))
                return
        else:
            if componentVars[cmpnt_nm]['yCoordinates'].get():
                messagebox.showerror('Error', ''.join([
                    'The x-coordinate of the injection well location for component {} ',
                    'is not provided.']).format(cmpnt_nm))
                return
            else:
                messagebox.showinfo('Information', ''.join([
                    "Default location of injection well at (0, 0) ",
                    "for component {} will be used."]).format(cmpnt_nm))
                data['InjectionWell'] = {}
                componentVars[cmpnt_nm]['injX'].set("0")
                componentVars[cmpnt_nm]['injY'].set("0")
                data['InjectionWell']['coordx'] = 0.0
                data['InjectionWell']['coordy'] = 0.0

    return


def load_obs_locations_data(comp_data, cmpnt_nm):
    # If locations are provided
    if 'Locations' in comp_data:
        xcoords = ", ".join(
            str(item) for item in comp_data['Locations']['coordx'])
        ycoords = ", ".join(
            str(item) for item in comp_data['Locations']['coordy'])
        componentVars[cmpnt_nm]['xCoordinates'].set(xcoords)
        componentVars[cmpnt_nm]['yCoordinates'].set(ycoords)
    else:
        componentVars[cmpnt_nm]['xCoordinates'].set('')
        componentVars[cmpnt_nm]['yCoordinates'].set('')

    if comp_data['type'] in ['SimpleReservoir', 'AnalyticalReservoir']:
        if 'InjectionWell' in comp_data:
            componentVars[cmpnt_nm]['injX'].set(str(
                comp_data['InjectionWell']['coordx']))
            componentVars[cmpnt_nm]['injY'].set(str(
                comp_data['InjectionWell']['coordy']))
        else:
            componentVars[cmpnt_nm]['injX'].set('0')
            componentVars[cmpnt_nm]['injY'].set('0')

def add_cell_locs_frame_widgets(controller, cmpnt_nm, frame, tool_tip):
    obs_loc_label = ttk.Label(
        frame, text="Cell locations:", width=PARAMETER_LABEL_WIDTH)
    obs_loc_label.grid(row=0, column=0, columnspan=2, sticky='w', padx=5)

    coords_frame = ttk.Frame(frame)
    coords_frame.grid(row=1, column=0, sticky='w', padx=20)

    coords = ['x', 'y']
    arg_names = ['coordx', 'coordy']
    arg_labels = ['x-coordinates [m]:', 'y-coordinates [m]:']

    coords_labels = []
    coords_fields = []

    # Create and place label and entry widgets
    for ind in range(2):
        coords_labels.append(ttk.Label(
            coords_frame, text=arg_labels[ind], width=LABEL_WIDTH))

        coords_fields.append(tk.Entry(
            coords_frame,
            width=2*DISTRIBUTION_ARG_TEXTFIELD_WIDTH,
            textvariable=componentVars[cmpnt_nm]['Cells']['Locations'][arg_names[ind]]))
        coords_labels[-1].grid(row=ind, column=0, pady=5, padx=5, sticky='w')
        coords_fields[-1].grid(row=ind, column=1, pady=5, padx=10, sticky='w')
        tool_tip.bind(
            coords_fields[-1],
            'Enter {}-coordinates of cell locations separated by comma.'.format(
                coords[ind]))
