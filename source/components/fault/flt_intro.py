#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions for checking and interpreting YAML file.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_intro.py

Contents (12)
    check_python(op_major, op_minor, version_name)
    convert_aperture(docs, fault_controls)
    convert_two_phase(docs, fault_controls)
    convert_yaml_parameters(docs, fault_controls)
    define_aperture_limits(param_bounds)
    define_let_limits(param_bounds)
    define_input_limits()
    check_blanks(fault_params)
    check_input_parameters(fault_params, param_bounds)
    cross_check_input(fault_params)
    acquire_control_parameters(yaml_file_selected, fault_controls)
    start(alone, yaml_file)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import sys                      # For program exit
import logging                  # For reporting errors
import time                     # For calculating runtime
from datetime import datetime

import flt_file as fileop       # For file operations
import flt_reveal as revl       # Save summary data and create plots
import flt_warranty as war      # Print warranty

# Text constants
LEAD_IN = "\n  --> "            # Message indent
ERR_IN = "\n    > "             # Secondary Error messages indent
FINAL = "  >>> "                # Last line indent

# Other constants
ECHO = False                    # Control to print for debugging
SMALL_DEV = 1.0e-6              # Minimum std deviation for aperture


def check_python(op_major, op_minor, version_name):
    """Check that user is using proper Python version.

    Parameters
    ----------
    op_major = (int) Python version for code - major number (e.g., 3)
    op_minor = (int) Python version for code - minor number (e.g., 7)
    version_name = (str) Code version in string format (e.g, "3.7")

    Returns
    -------
    N/A
    """
    # Get python version of system.
    logging.info('Checking Python Version')
    py_major, py_minor = sys.version_info[0:2]

    # Check if OK - they must use Version 3.7
    # -> New versions are assumed backwards compatible
    if (py_major != op_major) or (py_minor < op_minor):
        msg = ('Python ' + version_name + ' is required for this Code!'
               + ERR_IN + 'Version Python {maj}.{mnr} was Detected'.
               format(maj=py_major, mnr=py_minor))
        fileop.opx_problem(msg)

    # return None


def convert_aperture(docs, fault_controls):
    """Convert YAML aperture parameters into fault controls.

    Parameters
    ----------
    docs = (str) YAML array input
    fault_controls = (dict) control parameter dictionary

    Returns
    -------
    fault_controls = (dict) control parameters (modified)
    """
    # Set aperture parameters if aperture <not> from file.
    if not fault_controls['aperture_approach']:
        fault_controls['aperture_mean'] = docs['Aperture']['aveAperture']
        fault_controls['aperture_std'] = docs['Aperture']['stdDevAperture']
        fault_controls['aperture_min'] = docs['Aperture']['minAperture']
        fault_controls['aperture_max'] = docs['Aperture']['maxAperture']

        # Internal control on stochastic aperture - False = no variability.
        if ((fault_controls['aperture_std'] < SMALL_DEV) or
                (fault_controls['aperture_approach'])):
            fault_controls['vary_aperture'] = False
        else:
            fault_controls['vary_aperture'] = True

    # For no variability, use mean only.
    else:
        fault_controls['aperture_mean'] = docs['Aperture']['aveAperture']

    return fault_controls


def convert_two_phase(docs, fault_controls):
    """Convert YAML two_phase parameters into seal controls.

    Parameters
    ----------
    docs = (stream) YAML array input
    seal_controls = (dict) seal control parameters

    Returns
    -------
    seal_controls = (dict) seal control parameters (modified)
    """
    # Set relative flow limits and entry pressure.
    fault_controls['resid_brine'] = \
        docs['RelativeFlowLimits']['brineResSaturation']
    fault_controls['resid_co2'] = \
        docs['RelativeFlowLimits']['CO2ResSaturation']
    fault_controls['relative_model'] = \
        docs['RelativeFlowLimits']['relativeModel']
    fault_controls['perm_ratio'] = \
        docs['RelativeFlowLimits']['permRatio']
    fault_controls['entry_pressure'] = \
        docs['RelativeFlowLimits']['entryPressure']

    # Set two_phase parameters depending on model.
    model_selected = fault_controls['relative_model'].upper()
    if 'BC' in model_selected:
        # Brooks Corey model,
        #   -> Avoid using lambda in Python source -> use "zeta".'
        #   -> Clean model string.
        fault_controls['relative_model'] = 'BC'
        fault_controls['zeta'] = docs['BrooksCoreyModel']['lambda']

    else:
        # Clean model string and Set LET model parameters.
        fault_controls['relative_model'] = 'LET'
        fault_controls['l_wetting'] = docs['LETModel']['wetting1']
        fault_controls['e_wetting'] = docs['LETModel']['wetting2']
        fault_controls['t_wetting'] = docs['LETModel']['wetting3']
        fault_controls['l_nonwet'] = docs['LETModel']['nonwet1']
        fault_controls['e_nonwet'] = docs['LETModel']['nonwet2']
        fault_controls['t_nonwet'] = docs['LETModel']['nonwet3']

        # Set LET Capillary pressure.
        fault_controls['l_capillary'] = docs['LETCapillaryModel']['capillary1']
        fault_controls['e_capillary'] = docs['LETCapillaryModel']['capillary2']
        fault_controls['t_capillary'] = docs['LETCapillaryModel']['capillary3']
        fault_controls['max_capillary'] = \
            docs['LETCapillaryModel']['maxCapillary']

    return fault_controls


def convert_yaml_parameters(docs, fault_controls):
    """Convert YAML parameters into the fault controls dictionary.

    Parameters
    ----------
    docs = YAML array input
    fault_controls = (dict) control parameter dictionary

    Returns
    -------
    fault_controls = (dict) control parameters (modified)

    Notes
    -----
    Parameters that not needed are not set and therefore are not checked.
    """
    # ModelParams:
    fault_controls['title'] = docs['ModelParams']['runTitle']
    fault_controls['start_time'] = docs['ModelParams']['startTime']
    fault_controls['end_time'] = docs['ModelParams']['endTime']
    fault_controls['inject_end'] = docs['ModelParams']['injectEndTime']
    fault_controls['time_points'] = int(docs['ModelParams']['timePoints'])
    fault_controls['realizations'] = int(docs['ModelParams']['realizations'])
    fault_controls['time_input'] = docs['ModelParams']['timeInput']

    # Controls:
    fault_controls['aperture_approach'] = docs['Controls']['apertureApproach']
    fault_controls['interpolate_approach'] = \
        docs['Controls']['interpolateApproach']
    fault_controls['strike_approach'] = docs['Controls']['strikeApproach']

    # FaultCore:
    fault_controls['strike_mean'] = docs['FaultCore']['aveStrike']
    if fault_controls['strike_approach']:
        fault_controls['strike_dev'] = docs['FaultCore']['spreadStrike']
    fault_controls['dip'] = docs['FaultCore']['dip']
    fault_controls['length'] = docs['FaultCore']['length']
    fault_controls['x_start'] = docs['FaultCore']['xStart']
    fault_controls['y_start'] = docs['FaultCore']['yStart']
    fault_controls['n_plates'] = int(docs['FaultCore']['nSegments'])
    fault_controls['sgr'] = docs['FaultCore']['SGR']
    fault_controls['state_variable'] = docs['FaultCore']['stateVariable']

    # Aperture variability parameters:
    convert_aperture(docs, fault_controls)  # Optional parameters.

    # Field:
    fault_controls['aquifer_depth'] = docs['Field']['aquiferDepth']
    fault_controls['inject_depth'] = docs['Field']['injectDepth']
    fault_controls['aquifer_pressure'] = docs['Field']['aquiferPressure']
    fault_controls['field_pressure'] = docs['Field']['fieldPressure']
    fault_controls['final_pressure'] = docs['Field']['finalPressure']
    fault_controls['inject_pressure'] = docs['Field']['injectPressure']
    fault_controls['aquifer_temperature'] = docs['Field']['aquiferTemperature']
    fault_controls['inject_temperature'] = docs['Field']['injectTemperature']
    fault_controls['inject_x'] = docs['Field']['injectX']
    fault_controls['inject_y'] = docs['Field']['injectY']

    # Conditions:
    fault_controls['salinity'] = docs['Conditions']['salinity']
    fault_controls['co2_density'] = docs['Conditions']['CO2Density']
    fault_controls['co2_viscosity'] = docs['Conditions']['CO2Viscosity']
    fault_controls['brine_density'] = docs['Conditions']['brineDensity']
    fault_controls['brine_viscosity'] = docs['Conditions']['brineViscosity']
    fault_controls['co2_solubility'] = docs['Conditions']['CO2Solubility']

    # RelativeFlow:
    fault_controls = convert_two_phase(docs, fault_controls)

    # Stress:
    fault_controls['max_horizontal'] = docs['Stress']['maxHorizontal']
    fault_controls['min_horizontal'] = docs['Stress']['minHorizontal']
    fault_controls['max_trend'] = docs['Stress']['maxTrend']

    # FaultPlot:
    fault_controls['plot_time_history'] = docs['FlowPlot']['timeSeriesPlot']
    fault_controls['skip_output'] = docs['FlowPlot']['skipOutput']

    return fault_controls


def define_aperture_limits(param_bounds):
    """Define numeric limits/ranges of aperture parameters.

    Parameters
    ----------
    pars_bounds = (dict) dictionary with min/max indices

    Returns
    -------
    param_bounds = (dict) updated dictionary
    """
    # Aperture variability parameters.
    param_bounds['aperture_mean'] = [0.0, 25.0]
    param_bounds['aperture_std'] = [0.0, 20.0]
    param_bounds['aperture_min'] = [0.0, 1.0]
    param_bounds['aperture_max'] = [1.0E-03, 50.0]
    param_bounds['aperture_limits'] = [0.0, 50.0]

    return param_bounds


def define_let_limits(param_bounds):
    """Define numeric limits/ranges of LET model parameters.

    Parameters
    ----------
    pars_bounds = (dict) dictionary with min/max indices

    Returns
    -------
    param_bounds = (dict) updated dictionary
    """
    # Two-phase model parameters for L-E-T model.
    param_bounds['l_wetting'] = [0.5, 5.0]
    param_bounds['e_wetting'] = [0.1, 30.0]
    param_bounds['t_wetting'] = [0.0, 3.0]
    param_bounds['l_nonwet'] = [0.5, 5.0]
    param_bounds['e_nonwet'] = [0.1, 30.0]
    param_bounds['t_nonwet'] = [0.0, 3.0]

    # Other parameters for two-phase L-E-T model.
    param_bounds['l_capillary'] = [0.01, 5.0]
    param_bounds['e_capillary'] = [0.01, 30.0]
    param_bounds['t_capillary'] = [0.01, 3.0]
    param_bounds['max_capillary'] = [1.0E+02, 2.0E+08]

    return param_bounds


def define_input_limits():
    """Define the numeric limits/ranges of fault input parameters.

    Parameters
    ----------
    N/A

    Returns
    -------
    param_bounds = (dict) dictionary with min/max indices
    """
    # Define dictionary of input bounds.
    param_bounds = dict()

    # ModelParams:
    param_bounds['start_time'] = [0.0, 5000.0]
    param_bounds['end_time'] = [0.0, 10000.0]
    param_bounds['inject_end'] = [0.0, 10000.0]
    param_bounds['time_points'] = [0, 50]
    param_bounds['realizations'] = [0, 100000]

    # FaultCore:
    param_bounds['strike_mean'] = [0.0, 360.0]
    param_bounds['strike_dev'] = [0.0, 180.0]
    param_bounds['dip'] = [10.0, 90.0]
    param_bounds['length'] = [0.0, 1.0e+04]
    param_bounds['x_start'] = [-5.0e+07, 5.0e+07]
    param_bounds['y_start'] = [-5.0e+07, 5.0e+07]
    param_bounds['n_plates'] = [0, 100]
    param_bounds['sgr'] = [0, 100]
    param_bounds['state_variable'] = [0, 1.0]

    # Aperture variability parameters:
    param_bounds = define_aperture_limits(param_bounds)

    # Field:
    param_bounds['aquifer_depth'] = [0.0, 2.0e+03]
    param_bounds['inject_depth'] = [8.0e+02, 1.0e+04]
    param_bounds['aquifer_pressure'] = [0.0, 1.0e+07]
    param_bounds['field_pressure'] = [1.0e+05, 6.0e+07]
    param_bounds['final_pressure'] = [1.0e+05, 6.0e+07]
    param_bounds['inject_pressure'] = [7.0e+06, 6.0e+08]
    param_bounds['aquifer_temperature'] = [5.0, 180.0]
    param_bounds['inject_temperature'] = [31.0, 180.0]
    param_bounds['inject_x'] = [-5.0e+07, 5.0e+07]
    param_bounds['inject_y'] = [-5.0e+07, 5.0e+07]

    # Conditions:
    param_bounds['salinity'] = [0.0, 8.0e+04]
    param_bounds['co2_density'] = [9.3E+01, 1.05e+03]
    param_bounds['co2_viscosity'] = [1.8E-05, 1.4e-04]
    param_bounds['brine_density'] = [8.8e+02, 1.08e+03]
    param_bounds['brine_viscosity'] = [1.5e-04, 1.6e-03]
    param_bounds['co2_solubility'] = [0.0, 2.0]

    # Relative permeability limits.
    param_bounds['resid_brine'] = [0.01, 0.35]
    param_bounds['resid_co2'] = [0.00, 0.35]
    param_bounds['perm_ratio'] = [0.00, 1.5]
    param_bounds['entry_pressure'] = [1.0E+02, 2.0E+06]
    param_bounds['zeta'] = [0.00, 5.0]

    # Stress:
    param_bounds['max_horizontal'] = [0.0, 5.0e+07]
    param_bounds['min_horizontal'] = [0.0, 5.0E+07]
    param_bounds['max_trend'] = [0.0, 180.0]

    # Define LET Parameters.
    param_bounds = define_let_limits(param_bounds)

    # Repository Input.
    param_bounds['reservoir_pressure'] = [1.0E+00, 1.0E+08]
    param_bounds['reservoir_saturation'] = [0.0, 1.0]
    param_bounds['coordinates'] = [-1.0E+05, 1.0E+05]

    return param_bounds


def check_blanks(fault_params):
    """Check whether if any input parameter is blank (= None).

    Parameters
    ----------
    fault_params = (dict) input parameters of fault component model

    Returns
    -------
    N/A

    Notes
    -----
    1. "logging" module still uses string format - added statements
        to format string messages prior to logging.
    2. Error messages -> explicitly format string outside of <logging>
        as <logging> still uses string format!
    """
    # Define text and error flag.
    txt_blank = '> Parameter <{}> is None!'
    err_flag = 0

    # Check all values for None.
    for key, val in fault_params.items():
        if val is None:
            msg = txt_blank.format(key)
            logging.error(msg)
            err_flag = -1

    # Report error if found. Missing data is Fatal!
    if err_flag == -1:
        fileop.opx_problem("Input Parameter(s) are Missing!")

    # return None


def check_input_parameters(fault_params, param_bounds):
    """Check whether input parameters fall within specified limits.

    Parameters
    ----------
    fault_params = (dict) input parameters of fault component model
    param_bounds = (dict) bounds on fault parameters (w. 2 values)

    Returns
    -------
    stats = (int) error code, default = 0, if warning = -1

    Notes
    -----
    1. "logging" module still uses string format - add statements
      to format string messages prior to logging.
    2. Some parameters are checked using a defined list.
    """
    # Define parameters for checking.
    stats = 0
    true_false_parameters = ['approach', 'plot', 'time_input',
                             'vary_aperture', 'skip_output']
    truth_list = [True, False]
    relative_list = ["LET", "BC"]

    # Check if values for various conditions.
    for key, val in fault_params.items():

        # Check if title present.
        if 'title' in key:
            if val == '':
                txt_lost = '> Parameter <{}> not found!'
                msg = txt_lost.format(key)
                logging.error(msg)
                stats = -1

        # Check relative permeability type name if OK.
        elif 'relative' in key:
            if val not in relative_list:
                txt_not_model = ('> Parameter <{}> not recognized as a '
                                 + 'relative permeability model type.')
                msg = txt_not_model.format(key)
                logging.error(msg)
                stats = -1

        # Check all True/False parameters for Yes/No.
        elif [ele for ele in true_false_parameters if ele in key]:
            if val not in truth_list:
                txt_wrong = ('Parameter <{}> is not defined correctly '
                             + 'and has a value of <{}>!')
                msg = txt_wrong.format(key, val)
                logging.error(msg)
                stats = -1

        # Check other numeric values to be within bounds.
        elif key in param_bounds:
            if (val < param_bounds[key][0]) \
                    or (val > param_bounds[key][1]):
                txt_outside = ('> Parameter <{}> is outside of the '
                               + 'recommended bounds with a value of {}!')
                msg = txt_outside.format(key, val)
                logging.error(msg)
                stats = -1

        else:
            # Otherwise - missing parameter in fault_controls.
            txt_not_fault = ('> Parameter <{}> not recognized as a '
                             + 'fault parameter.')
            msg = txt_not_fault.format(key)
            logging.error(msg)
            stats = -1

    return stats


def cross_check_input(fault_params):
    """Check if input is numerically logical for variability.

    Parameters
    ----------
    fault_params = (dict) input parameters of fault component

    Returns
    -------
    statx = (int) error code, default = 0, if warning = -1

    Notes
    -----
    "logging" module still uses string format -
        format string messages prior to calling <logging>.
    """
    # Define message strings.
    txt_cross = '> Minimum {} exceeds maximum {}! '
    txt_outside = '> Mean {} is outside min./max. bounds! '

    # Set default return.
    statx = 0

    # Run check only if no file input for permeability.
    if not fault_params['aperture_approach']:

        # Run checks on min and max values and mean.
        if fault_params['aperture_min'] > fault_params['aperture_max']:
            causitive = "aperture"
            msg = txt_cross.format(causitive, causitive)
            logging.error(msg)
            statx = -1
        if (fault_params['aperture_mean'] > fault_params['aperture_max'] or
                fault_params['aperture_mean'] < fault_params['aperture_min']):
            causitive = "aperture"
            msg = txt_outside.format(causitive)
            logging.error(msg)
            statx = -1

    return statx


def acquire_control_parameters(yaml_file_selected, fault_controls):
    """Obtain the fault parameters from a formatted YAML file.

    Parameters
    ----------
    yaml_file_selected = (str) file name of YAML file in same directory
    fault_controls = (dict) fault controls (empty)

    Returns
    -------
    fault_controls = (dict) fault parameters - with values
    """
    # Get YAML data from file.
    docs = fileop.acquire_yaml_file(yaml_file_selected)

    # Load parameters into fault_controls dictionary.
    try:
        convert_yaml_parameters(docs, fault_controls)
    except KeyError as errd:
        cause = errd.args[0]
        msg = ("Parameter <{}> is missing from YAML file!".format(cause))
        fileop.opx_problem(msg + "\n      " +
                           "- KeyError in defining fault_controls Dictionary!")

    return fault_controls


def start(alone, yaml_file, op_major, op_minor, version_name):
    """Open the YAML control file, and establishes controls and checks.

    Parameters
    ----------
    alone = (bool) status of code operation; True = if stand-alone
    yaml_file = (str) name of input file
    op_major = (int) Python version for code - major number (e.g., 3)
    op_minor = (int) Python version for code - minor number (e.g., 7)
    version_name = (str) Code version in string format (e.g, "1.4")

    Returns
    -------
    fault_controls = (dict) input parameters (dictionary)
    param_bounds = (dict) parameter bounds (dictionary)
    """
    # Set duration timer and set date.
    timer = time.monotonic()
    flag = datetime.now()

    # Show header statements on console - construct Python version.
    if alone:
        print("\n\n  *** FAULT_FLO - Version: " + version_name +
              " ***" + "                  Python "
              + str(op_major) + "." + str(op_minor))
        war.show_warranty()
        revl.echo_status(alone, "BEGIN")

    # Check Python
    check_python(op_major, op_minor, version_name)

    # Initiate input control parameters from text file.
    fault_controls = {}
    fault_controls = acquire_control_parameters(yaml_file, fault_controls)

    if alone:
        revl.echo_status(alone, "CLEAN")

    # Wipe output directory to remove old output files.
    fileop.clean_output()

    # Check for any missing data.
    check_blanks(fault_controls)

    # Identify input bounds and check values.
    param_bounds = define_input_limits()
    status = check_input_parameters(fault_controls, param_bounds)
    status2 = cross_check_input(fault_controls)

    # Consider all warnings fatal! - and exit program.
    if status < 0 or status2 < 0:
        fileop.opx_problem("Input Parameter(s) Outside of Range or Wrong!")

    # Save time values in fault_controls for later use.
    fault_controls['code_start'] = timer
    fault_controls['record_date'] = flag
    fault_controls['seg_length'] = (fault_controls['length']
                                    / fault_controls['n_plates'])

    return fault_controls, param_bounds


#
# -----------------------------------------------------------------------------
# End of module
