# Define default setup of the Fault Flow Component
SETUP_DICT = {
    'ModelParams': {
        'runTitle': 'Example_setup',
        'startTime': 0.0,                # years
        'endTime': 200.0,                # years
        'injectEndTime': 50.0,           # years; Time when injection stops
        'timePoints': 46,                # -; Including start point
        'realizations': 10,              # -; Simulation cycles
        'timeInput': False},             # True/False; True = Time steps from File
    'Controls': {
        'apertureApproach': False,       # True/False; True = Get Apertures from file
        'interpolateApproach': False,     # True/False; True = Interpolate fluid constants
        'strikeApproach': False},         # True/False; True = Vary Fault Strike
    'FaultCore': {
        'aveStrike': 95.4,               # deg; from North
        'spreadStrike': 10.0,            # deg; spread in orientation
        'dip': 90.0,                     # deg; right hand rule from strike
        'length': 100.0,                 # m; length at surface from start point
        'xStart': 388618.7,              # m; X-coordinate, left start
        'yStart': 3436341.1,             # m; Y-coordinate, left start
        'nSegments': 1,                  # -; Number of fault divisions / plates
        'SGR': 0.0,                      # %; SGR = shale gouge ratio
        'stateVariable': 1.0},           # -; Non-Isothermal correction factor
    'Aperture': {
        'aveAperture': 1.0e-1,           # mm
        'stdDevAperture': 5.0e-02,       # mm
        'minAperture': 1.0e-4,           # mm
        'maxAperture': 5.0e-1},          # mm
    'Field': {
        'aquiferDepth': 500.0,           # m; Depth to base of deepest aquifer
        'injectDepth': 2884.31,          # m; Depth to top of injection horizon
        'aquiferPressure': 4.9e+6,       # Pa; Pressure at aquifer base depth
        'fieldPressure': 2.85e+7,        # Pa; Initial Pressure at injection depth
        'injectPressure': 3.0778e+7,     # Pa; Ave. pressure at depth during injection
        'finalPressure': 3.9e+7,         # Pa; Ave. pressure at depth after injection
        'aquiferTemperature': 30.0,      # oC; Temperature at aquifer depth
        'injectTemperature': 98.9,       # oC; Temperature at injection depth
        'injectX': 388505.9,             # m; X coordinate location of injection well
        'injectY': 3434629.9},           # m; Y coordinate location of injection well
    'Conditions': {
        'salinity': 50000.0,             # ppm; at injection horizon
        'CO2Density': 430.0,             # kg/m^3; average
        'CO2Viscosity': 3.72e-5,         # Pa*s
        'brineDensity': 988.00,          # kg/m^3
        'brineViscosity': 4.36e-4,       # Pa*s
        'CO2Solubility': 2.0e-3},        # kg/kg
    'RelativeFlowLimits': {
        'brineResSaturation': 0.15,      # --; Residual wetting
        'CO2ResSaturation': 0.0,         # --; Residual nonwetting; typically = 0.0
        'relativeModel': 'LET',          # --; Model type; options: BC
        'permRatio': 0.6,                # --; Ratio of nonwetting/wetting
        'entryPressure': 5000.0},        # Pa; threshold value
    'BrooksCoreyModel': {
        'lambda': 2.5},                  # --; Brooks Corey Model lamda parameter
    'LETModel': {
        'wetting1': 1.0,                 # -; Used only if relativeModel = LET
        'wetting2': 10.0,                # -; Used only if relativeModel = LET
        'wetting3': 1.25,                # -; Used only if relativeModel = LET
        'nonwet1': 1.05,                 # -; Used only if relativeModel = LET
        'nonwet2': 3.50,                 # -; Used only if relativeModel = LET
        'nonwet3': 1.25},                # -; Used only if relativeModel = LET
    'LETCapillaryModel': {
        'capillary1': 0.2,               # -; Used only if relativeModel = LET
        'capillary2': 2.8,               # -; Used only if relativeModel = LET
        'capillary3': 0.43,              # -; Used only if relativeModel = LET
        'maxCapillary': 1.0e+7},         # Pa; Used only if relativeModel = LET
    'Stress': {
        'maxHorizontal': 3.0e+07,        # Pa; secondary at depth of top of injection
        'minHorizontal': 2.0e+07,        # Pa; secondary at depth of top of injection
        'maxTrend': 55.0},               # deg; from North
    'FlowPlot': {
        'timeSeriesPlot': False,         # True/False; True = plot time history
        'skipOutput': True}}
