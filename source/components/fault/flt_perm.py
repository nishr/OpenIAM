#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions to compute fault permeability.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_perm

Contents (7)
    compute_effective_saturation(co2_saturation, brine_residual, co2_residual)
    compute_brine_pressure(base_co2_pressure, capillary_pressure)
    compute_capillary_pressure(zeta, beta, normal_saturation,
        bubbling_pressure)
    brine_relative_perm(rel_model, effective_saturation, fault_controls)
    co2_relative_perm(rel_model, effective_saturation, fault_controls)
    current_perm(rel_factor, sgr_effect, state_effect, permeability,
                         viscosity)
    soluble_co2(self, fault_controls, brine_flow)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import flt_units as fit         # For unit conversion

# Permeability and heterogeneity constants.
MIN_SATURATION = 0.001              # Numerical minimum limit
MAX_SATURATION = 0.999              # Numerical maximum limit
V_SMALL_VARIANCE = 1.0e-05          # Small variability of log normal

# Other constants
BOZO = False                        # Control to print permeability each loop


def compute_effective_saturation(co2_saturation, brine_residual, co2_residual):
    """Compute the effective saturation value for two-phase flow.

    Parameters
    ----------
    co2_saturation = (float) current CO2 saturation (decimal)
    brine_residual = (float) brine residual saturation (decimal)
    co2_residual = (float) CO2 residual saturation (decimal)

    Returns
    -------
    effective_saturation => normalized wet (brine) saturation
    """
    # Compute current brine saturation.
    wet_saturation = 1.0 - co2_saturation

    #  Compute effective saturation within residual limits.
    if wet_saturation <= brine_residual:
        effective_saturation = 0.0
    elif wet_saturation > 1.0 - co2_residual:
        effective_saturation = 1.0
    else:
        effective_saturation = (wet_saturation - brine_residual) \
                                / (1.0 - brine_residual - co2_residual)
    return effective_saturation


def compute_brine_pressure(fluid_pressure, capillary_pressure):
    """Compute the base brine pressures on fault horizon.

    Parameters
    ----------
    fluid_pressure = (float) pressure from reservoir data
    capillary_pressure = (float) capillary pressure

    Returns
    -------
    base_pressure = brine pressure at base of fault (MPa)
    """
    # Compute brine pressure from co2 pressure.
    new_pressure = fluid_pressure - capillary_pressure

    return new_pressure


def compute_capillary_pressure(rel_model, normal_saturation, fault_controls,
                               entry_pressure):
    """Compute the capillary pressure based on effective saturation.

    Parameters
    ----------
    rel_model = (str) relative permeability model = BC/LET
    normal_saturation = (float) normalized saturation between residual
        values = effective wetting saturation
    fault_controls = (dict) dictionary of fault parameters
    entry_pressure = (float) limit pressure (Pa) for capillary pressure (Pa)

    Returns
    -------
    capillary_pressure = (float) capillary pressure (Pa)

    Notes
    -----
    1. See Equation B-15a/b in seal manual for Brooks-Corey model.
    2. See Equations B-18 and B-19 in seal manual for LET model.
    """
    # Define nonwetting saturation.
    non_saturation = 1.0 - normal_saturation

    # Define capillary pressure based on model type.

    if 'LET' in rel_model:
        # Compute based on LET capillary terms.
        term_1 = pow(non_saturation, fault_controls['l_capillary'])
        term_2 = (fault_controls['e_capillary'] *
                  pow(normal_saturation, fault_controls['t_capillary']))
        fcap = term_1 / (term_1 + term_2)
        change = fault_controls['max_capillary'] - entry_pressure

        capillary_pressure = fcap * change + entry_pressure
    else:
        # Compute based on modified Brooks-Corey model.
        exponent = (1.0 / fault_controls['zeta'])
        if normal_saturation >= MAX_SATURATION:
            divisor = 1.0
        elif normal_saturation <= MIN_SATURATION:
            divisor = pow(MIN_SATURATION, exponent)
        else:
            divisor = pow(normal_saturation, exponent)

        capillary_pressure = (entry_pressure / divisor)

    return capillary_pressure  # in Pascals


def brine_relative_perm(rel_model, effective_saturation, fault_controls):
    """Compute the wetting relative permeabilities.

    Parameters
    ----------
    rel_model = (str) relative permeability model - either"LET" or "BC"
    effective_saturation = (float) effective saturation
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    wet_perm = (float) wetting relative permeability (brine)

    Notes
    -----
    1. See Equation B-8 in seal manual for Brooks-Corey model.
    2. See Equation B-16 in seal manual for LET model.
    """
    # Define relative permeability based on model type.

    if 'LET' in rel_model:
        # Define wetting relative permeability using LET model.
        term_1 = pow(effective_saturation, fault_controls['l_wetting'])
        non_sat = 1.0 - effective_saturation
        term_2 = (fault_controls['e_wetting'] *
                  pow(non_sat, fault_controls['t_wetting']))
        wet_perm = term_1 / (term_1 + term_2)
    else:
        # Define nonwetting permeability using B_C, with zeta = lambda.
        term_1 = (2.0 + 3.0 * fault_controls['zeta']) / fault_controls['zeta']
        wet_perm = pow(effective_saturation, term_1)

    return wet_perm


def co2_relative_perm(rel_model, effective_saturation, fault_controls):
    """Compute the nonwetting relative permeability.

    Parameters
    ----------
    rel_model = (str) relative permeability model - either"LET" or "BC"
    effective_saturation = (float) effective wetting saturation
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    nonwet_perm = (float) nonwetting relative permeability (co2)

    Notes
    -----
    1. See Equation B-9 in seal manual for Brooks-Corey model.
    2. See Equation B-17 in seal manual for LET model.
    """
    # Define nonwetting saturation and ratio.
    non_saturation = 1.0 - effective_saturation
    p_ratio = fault_controls['perm_ratio']

    # Define relative permeability based on model type.

    if 'LET' in rel_model:
        # Define nonwetting permeability using LET model.
        term_1 = pow(non_saturation, fault_controls['l_nonwet'])
        term_2 = (fault_controls['e_nonwet'] *
                  pow(effective_saturation, fault_controls['t_nonwet']))
        fcap = term_1 / (term_1 + term_2)
        nonwet_perm = p_ratio * fcap
    else:
        # Define nonwetting permeability using B_C, with zeta = lambda.
        term_1 = pow(non_saturation, 2.0)
        term_2 = (2.0 + fault_controls['zeta']) / fault_controls['zeta']
        term_3 = pow(effective_saturation, term_2)
        nonwet_perm = p_ratio * term_1 * (1.0 - term_3)

    return nonwet_perm


def current_perm(rel_factor, sgr_effect, state_effect, permeability,
                 viscosity):
    """Compute the current permeability for fault / viscosity.

    Parameters
    ----------
    rel_factor = r(float) relative permeability factor (decimal)
    sgr_ affect = (float) SGR correction factor
    state_effect = (float) state variable correction factor
    permeability = (float) single fracture (m^2)
    viscosity = (float) viscosity (Pa-s)

    Returns
    -------
    effective_permeability = (float) permeability of fault (m2) / viscosity

    """
    # Compute effective permeability - based on relative perm and factors.
    effective_perm = sgr_effect * state_effect * rel_factor * permeability
    effective_perm /= viscosity

    return effective_perm


def soluble_co2(fault_controls, brine_flow):
    """Compute amount of CO2 dissolved in brine (in m3/s).

    Parameters
    ----------
    fault_controls = (dict) dictionary of parameters
    brine_flow = (float) amount of brine (mˆ3/s)

    Returns
    -------
    result = soluble CO2 (mˆ3/s)

    Note
    ----
    1. Assumes brine is 100% saturated with CO2.
    """
    # Define variables for clarity.
    soluble = fault_controls['co2_solubility']
    brine_dense = fault_controls['brine_density']
    co2_dense = fault_controls['co2_density']

    # Define soluble amount per time.
    # --> Define weight (kg) )of brine per sec.
    brine_kg = brine_dense * brine_flow   # kg - amount of brine^per sec.

    # --> Define solubility = mol/kg-brine * g/mol / g/kg  = kg-CO2/kg-brine.
    co2_dissolved = soluble * fit.co2_molar_mass() / fit.kilogram_to_gram()

    # --> Compute volume of CO2 - in mˆ3/s.
    result = co2_dissolved * brine_kg / co2_dense

    return result

#
# -----------------------------------------------------------------------------
# End of module
