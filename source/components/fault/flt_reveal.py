#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions to write data to summary file and console.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_reveal

Contents (15)
    write_iam_parameters(fault_controls, zum)
    write_description_parameters(fault_controls, zum)
    write_aperture_parameters(fault_controls, zum)
    write_fluid_values(fault_controls, zum)
    write_relative_perm(fault_controls, zum)
    write_stress_parameters(fault_controls, zum)
    write_cutoff_parameters(fault_controls, zum)
    write_fault_parameters(fault, zum)
    write_co2_results(sim_flux_list, zum)
    write_last(zum)
    write_summary(fault_controls, sim_flux_list, fault)
    echo_status(alive, phase):
    echo_time_step(desired, alive, current)
    echo_simulation_step(alive, sim_step)
    closeout_time(alive, fault_controls)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import time                     # For calculating time
from datetime import timedelta  # For calculating runtime
import numpy as np              # For array operations

import flt_file as fileop       # For paths and error reports
import flt_global as glo        # IO directory and file names
import flt_units as fit         # For unit conversion

# Constants for file handling & writing to Summary File
LEAD_IN = "\n  --> "            # Message start
ZIPX_IN = "  --> "              # Parameter start
INFO_IN = "    > "              # Other messages start
SPEC_IN = "    >>> "            # Message start
ECHO = False                    # Additional printout control


def write_iam_parameters(fault_controls, zum):
    """Write time model info to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Write time stamp for start of the run.
    print('\n  Fault_Flo Summary', file=zum)
    now = fault_controls['record_date']
    clump = ZIPX_IN + 'Simulation Run Date: %a %d %b %Y - Time: %H:%M'
    stamp = now.strftime(clump)
    print(stamp, file=zum)

    # Write run details.
    print('\n  > Model Parameters', file=zum)

    print(ZIPX_IN + 'Analysis Title = {:<}.'.
          format(fault_controls.get('title')), file=zum)
    print(ZIPX_IN + 'Start Time of Analysis     = {:<.1f} years'.
          format(fault_controls.get('start_time')), file=zum)
    print(ZIPX_IN + 'End Time of Analysis       = {:<.1f} years'.
          format(fault_controls.get('end_time')), file=zum)
    print(ZIPX_IN + 'End Time of Injection      = {:<.1f} years'.
          format(fault_controls.get('inject_end')), file=zum)
    print(ZIPX_IN + 'Time Steps for Run         = {:<5}'.
          format(fault_controls.get('time_points')), file=zum)
    print(ZIPX_IN + 'Number of Realizations     = {:<5}'.
          format(fault_controls.get('realizations')), file=zum)

    if fault_controls.get('time_input'):
        print(SPEC_IN + 'Time Steps Input From File', file=zum)
    else:
        print(SPEC_IN + 'Uniform Time Steps Assumed', file=zum)

    # return None


def write_description_parameters(fault_controls, zum):
    """Write description data to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Write fault descriptors as input.
    print('\n  > Fault Description and Conditions', file=zum)

    if fault_controls.get('strike_approach'):
        print(SPEC_IN + 'Fault Strike Varied in Analysis', file=zum)
    else:
        print(SPEC_IN + 'Fault Strike Held Constant in Simulations', file=zum)

    print(ZIPX_IN + 'Fault Strike - Mean        = {:.1f} deg'.
          format(fault_controls.get('strike_mean')), file=zum)
    if fault_controls['strike_approach']:
        print(ZIPX_IN + 'Fault Strike - Spread      = {:.1f} deg'.
              format(fault_controls.get('strike_dev')), file=zum)
        print(ZIPX_IN + 'Fault Strike - Kappa       = {:.1f}'.
              format(fault_controls.get('strike_disperse')), file=zum)
    print(ZIPX_IN + 'Fault Dip                  = {:.1f} deg'.
          format(fault_controls.get('dip')), file=zum)
    print(ZIPX_IN + 'Fault Length               = {:.1f} m'.
          format(fault_controls.get('length')), file=zum)
    print(ZIPX_IN + 'At Surface - X Coordinate  = {:.1f} m'.
          format(fault_controls.get('x_start')), file=zum)
    print(ZIPX_IN + 'At Surface - Y Coordinate  = {:.1f} m'.
          format(fault_controls.get('y_start')), file=zum)
    print(ZIPX_IN + 'Number of Segments         = {:<5}'.
          format(fault_controls.get('n_plates')), file=zum)

    # Write correction parameters.
    print('\n  > Correction Factors', file=zum)

    print(ZIPX_IN + 'Non-Isothermal Correction  = {:.2f}'.
          format(fault_controls.get('state_variable')), file=zum)
    print(ZIPX_IN + 'SGR Value                  = {:.0f}%'.
          format(fault_controls.get('sgr')), file=zum)
    print(ZIPX_IN + 'SGR Correction             = {:.5e}'.
          format(fault_controls.get('sgr_perm_correction')), file=zum)
    print(ZIPX_IN + 'Fault Orientation          = ', end='', file=zum)
    print(fault_controls.get('fault_inclined'), file=zum)
    print(ZIPX_IN + 'Flow Correction            = {:.2f}'.
          format(fault_controls.get('fault_orient_effect')), file=zum)
    print(ZIPX_IN + 'Shortest Distance          = {:.5e} m'.
          format(fault_controls.get('shortest_distance')), file=zum)
    print(ZIPX_IN + 'Angle Between Vectors      = {:.1f} deg.'.
          format(fault_controls.get('angle_between_vectors')), file=zum)

    # Write field descriptors.
    print('\n  > Field Conditions', file=zum)

    print(ZIPX_IN + 'Aquifer Depth              = {:.0f} m'.
          format(fault_controls.get('aquifer_depth')), file=zum)

    valu = fault_controls.get('aquifer_pressure') * fit.megapascals()
    print(ZIPX_IN + 'Aquifer Pressure           = {:.4e} MPa'.
          format(valu), file=zum)

    print(ZIPX_IN + 'Aquifer Temperature        = {:.0f} oC'.
          format(fault_controls.get('aquifer_temperature')), file=zum)
    print(ZIPX_IN + 'Injection/Reservoir Depth  = {:.0f} m'.
          format(fault_controls.get('inject_depth')), file=zum)

    valu = fault_controls.get('field_pressure') * fit.megapascals()
    print(ZIPX_IN + 'Initial Reservoir Pressure = {:.4e} MPa'.
          format(valu), file=zum)

    valu = fault_controls.get('inject_pressure') * fit.megapascals()
    print(ZIPX_IN + 'Ave. Injection Pressure    = {:.4e} MPa'.
          format(valu), file=zum)
    print(ZIPX_IN + 'Reservoir Temperature      = {:.0f} oC'.
          format(fault_controls.get('inject_temperature')), file=zum)
    print(ZIPX_IN + 'X Coordinate of Injection  = {:.1f} m'.
          format(fault_controls.get('inject_x')), file=zum)
    print(ZIPX_IN + 'Y Coordinate of Injection  = {:.1f} m'.
          format(fault_controls.get('inject_y')), file=zum)

    # return None


def write_aperture_parameters(fault_controls, zum):
    """Write aperture input data to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # print aperture variability parameters.
    print('\n  > Aperture Parameters', file=zum)

    if fault_controls.get('aperture_approach'):
        print(SPEC_IN + 'Aperture Option: Values Input from File.',
              file=zum)
    elif not fault_controls.get('vary_aperture'):
        print(SPEC_IN + 'Uniform Aperture - All Set to Mean Value.', file=zum)
        print(ZIPX_IN + 'Mean Total Aperture        = {:<.3f} mm'.
              format(fault_controls.get('aperture_mean')), file=zum)
    else:
        print(SPEC_IN + 'Computed with Censored Log-normal Distribution.',
              file=zum)
        print(ZIPX_IN + 'Mean Aperture              = {:.5f} mm'.
              format(fault_controls.get('aperture_mean')), file=zum)
        print(ZIPX_IN + 'Std. Dev. Aperture         = {:.5f} mm'.
              format(fault_controls.get('aperture_std')), file=zum)
        print(ZIPX_IN + 'Minimum Aperture           = {:.5f} mm'.
              format(fault_controls.get('aperture_min')), file=zum)
        print(ZIPX_IN + 'Maximum Aperture           = {:.5f} mm'.
              format(fault_controls.get('aperture_max')), file=zum)

        print(ZIPX_IN + 'LogNorm Aperture Mu        = {:.4f}'.
              format(fault_controls.get('aperture_mu')), file=zum)
        print(ZIPX_IN + 'LogNorm Aperture Scale     = {:.4f}'.
              format(fault_controls.get('aperture_scale')), file=zum)

    # return None


def write_fluid_values(fault_controls, zum):
    """Write controls (boolean parameters) to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Write fluid parameters.
    print('\n  > Fluid Parameters', file=zum)
    if fault_controls['interpolate_approach']:
        print(SPEC_IN + 'Option: Fluid Parameters Interpolated.',
              file=zum)
    else:
        print(SPEC_IN + 'Option: Fluid Parameters From Input.',
              file=zum)
    print(ZIPX_IN + 'Reference Salinity         = {:.0f} ppm'.
          format(fault_controls.get('salinity')), file=zum)
    print(ZIPX_IN + 'CO2 Density                = {:.0f} kg/m3'.
          format(fault_controls.get('co2_density')), file=zum)
    print(ZIPX_IN + 'CO2 Viscosity              = {:.4e} Pa-s'.
          format(fault_controls.get('co2_viscosity')), file=zum)
    print(ZIPX_IN + 'Brine Density              = {:.0f} kg/m3'.
          format(fault_controls.get('brine_density')), file=zum)
    print(ZIPX_IN + 'Brine Viscosity            = {:.4e} Pa-s'.
          format(fault_controls.get('brine_viscosity')), file=zum)
    print(ZIPX_IN + 'CO2 Solubility             = {:.4e} kg/kg'.
          format(fault_controls.get('co2_solubility')), file=zum)

    # return None


def write_relative_perm(fault_controls, zum):
    """Write relative permeability controls to a file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A

    Notes
    -----
    1. Models BC and LET.
    2. LET not implemented in this version.
    """
    # print relative permeability parameters.
    print('\n  > Relative Permeability Parameters', file=zum)

    # Write model type.
    m_type = fault_controls.get('relative_model')
    if 'LET' in m_type:
        print(SPEC_IN + 'Relative Permeability = L-E-T Model',
              file=zum)
    else:
        print(SPEC_IN + 'Relative Permeability = ' +
              'Brooks-Corey Model', file=zum)

    # Write general limits on Relative Permeability Model.
    print(ZIPX_IN + 'Residual Brine Saturation  = {:.2f}'.
          format(fault_controls.get('resid_brine')), file=zum)
    print(ZIPX_IN + 'Residual CO2 Saturation    = {:.2f}'.
          format(fault_controls.get('resid_co2')), file=zum)
    print(ZIPX_IN + 'Nonwetting/Wetting Ratio   = {:.2f}'.
          format(fault_controls.get('perm_ratio')), file=zum)

    # ----------------------------------------
    # LET & BC relative permeability parameters.
    if "LET" in fault_controls.get('relative_model'):
        print(ZIPX_IN + '"L" Wetting Parameter      = {:.2f}'.
              format(fault_controls.get('l_wetting')), file=zum)
        print(ZIPX_IN + '"E" Wetting Parameter      = {:.2f}'.
              format(fault_controls.get('e_wetting')), file=zum)
        print(ZIPX_IN + '"T" Wetting Parameter      = {:.2f}'.
              format(fault_controls.get('t_wetting')), file=zum)
        print(ZIPX_IN + '"L" Nonwetting Parameter   = {:.2f}'.
              format(fault_controls.get('l_nonwet')), file=zum)
        print(ZIPX_IN + '"E" Nonwetting Parameter   = {:.2f}'.
              format(fault_controls.get('e_nonwet')), file=zum)
        print(ZIPX_IN + '"T" Nonwetting Parameter   = {:.2f}'.
              format(fault_controls.get('t_nonwet')), file=zum)
    else:
        print(ZIPX_IN + 'Lambda Parameter for BC    = {:.2f}'.
              format(fault_controls.get('zeta')), file=zum)

    # ----------------------------------------
    # Capillary Pressure
    print('\n  > Capillary Pressure Parameters', file=zum)

    # BC - only stress
    #     Convert capillary pressure to MPa.
    cap_pressure = (fault_controls.get('entry_pressure') *
                    fit.megapascals())
    print(ZIPX_IN + 'Ave. Entry Pressure        = {:.4e} MPa'.
          format(cap_pressure), file=zum)

    # LET model - capillary parameters.
    if 'LET' in fault_controls.get('relative_model'):
        print(ZIPX_IN + '"L" Capillary Parameter    = {:.2f}'.
              format(fault_controls.get('l_capillary')), file=zum)
        print(ZIPX_IN + '"E" Capillary Parameter    = {:.2f}'.
              format(fault_controls.get('e_capillary')), file=zum)
        print(ZIPX_IN + '"T" Capillary Parameter    = {:.2f}'.
              format(fault_controls.get('t_capillary')), file=zum)

        # Convert stress to MPA
        max_press = (fault_controls.get('max_capillary') *
                     fit.megapascals())
        print(ZIPX_IN + 'Maximum Capillary Pressure  = {:.4e} MPa'.
              format(max_press), file=zum)

    # return None


def write_stress_parameters(fault_controls, zum):
    """Write in situ stress parameters to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Convert stresses to MPa.
    maximal = fault_controls.get('max_horizontal') * fit.megapascals()
    minimal = fault_controls.get('min_horizontal') * fit.megapascals()

    # Write values.
    print('\n  > In-Situ, Secondary Principle, Horizontal Stress Parameters',
          file=zum)
    print(ZIPX_IN + 'Maximum Horizontal Stress  = {:.2e} MPa'.
          format(maximal), file=zum)
    print(ZIPX_IN + 'Minimum Horizontal Stress  = {:.2e} MPa'.
          format(minimal), file=zum)
    print(ZIPX_IN + 'Strike of Maximum          = {:.1f} deg'.
          format(fault_controls.get('max_trend')), file=zum)

    # return None


def write_cutoff_parameters(fault_controls, zum):
    """Write CO2 transition / cutoff parameters to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Write values for transition.
    print('\n  > CO2 Transition Parameters', file=zum)

    # Warning flag, if needed!
    if "Full Extent" in fault_controls.get('cutoff_type'):
        print(SPEC_IN + 'Simple Supercritical Flow Model', file=zum)
    else:
        print(SPEC_IN + 'Issue with Near-Surface Flow!!!', file=zum)

    print(ZIPX_IN + 'Cutoff Type                = {} '.
          format(fault_controls.get('cutoff_type')), file=zum)
    print(ZIPX_IN + 'Transition Depth           = {:.1f} m'.
          format(fault_controls.get('cutoff_depth')), file=zum)

    valu = fault_controls.get('cutoff_pressure') * fit.megapascals()
    print(ZIPX_IN + 'Transition Pressure        = {:.4e} MPa'.
          format(valu), file=zum)
    print(ZIPX_IN + 'Transition Temperature     = {:.0f} oC'.
          format(fault_controls.get('cutoff_temperature')), file=zum)

    # return None


def write_fault_parameters(fault, zum):
    """Write fault plate parameters to file named "zum".

    Parameters
    ----------
    zum = (str) file label
    fault = (class) collection of fault section parameters

    Returns
    -------
    N/A
    """
    # Write plate values for last simulation.
    print('\n  > Details for Last Simulation', file=zum)
    for indx, part in enumerate(fault):
        # Write plate values.
        print('\n  > Properties of Fault Section #{}'.format(indx),
              file=zum)
        part.print_plate(zum)

    # return None


def write_co2_results(sim_flux_list, zum):
    """Compute and print results of simulations to file "zum".

    Parameters
    ----------
    zum = (str) file label
    sim_flux_list = (list) CO2 results at the end of simulations
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    N/A
    """
    # Convert list to array.
    simulation_number = len(sim_flux_list)
    flux_data = np.asarray(sim_flux_list)

    # compute statistics of the array.
    ave_flux_data = flux_data.mean()
    std_dev = flux_data.std()
    min_flux_data = flux_data.min()
    max_flux_data = flux_data.max()
    argument_min = flux_data.argmax()
    argument_max = flux_data.argmax()

    # print separator.
    print('\n  ', end='', file=zum)
    print('*' * 60, file=zum)

    # print statistics.
    print('\n  > Summary of Results', file=zum)
    print(ZIPX_IN + 'Number of Simulations               = {:,}'.
          format(simulation_number), file=zum)
    print(ZIPX_IN + 'Average CO2 Flux of All Simulations = {:.3E} tonne'.
          format(ave_flux_data), file=zum)
    print(ZIPX_IN + 'Standard Deviation of CO2 Flux      = {:.3E} tonne'.
          format(std_dev), file=zum)
    print(ZIPX_IN + 'Minimum CO2 Flux of All Simulations = {:<.3E} tonne'.
          format(min_flux_data), file=zum)
    print('       - at Index =      {:}'.format(argument_min),
          file=zum)
    print(ZIPX_IN + 'Maximum CO2 Flux of All Simulations = {:.3E} tonne'.
          format(max_flux_data), file=zum)
    print('       - at Index =      {:}'.format(argument_max),
          file=zum)


def write_last(fault_controls, zum):
    """Write current time value to file named "zum" and end.

    Parameters
    ----------
    fault_controls = (dict) dictionary of fault parameters
    zum = (str) file label

    Returns
    -------
    N/A
    """
    # Print separator.
    print('\n  ', end='', file=zum)
    print('*' * 60, file=zum)

    # Print computation time.
    print('\n  Computation Time = {}  (h.mm.ss.xxxx)'.
          format(fault_controls.get('elapsed')), file=zum)

    # Print last line.
    print('\n  End', file=zum)

    # return None


def write_summary(fault_controls, sim_flux_list, fault):
    """Control the printout of summary data to file.

    Parameters
    ----------
    fault_controls = (dict) dictionary of fault parameters
    sim_flux_list = (list) CO2 flux at end of simulations
    fault = (class) a collection of plates

    Returns
    -------
    N/A

    Notes
    -----
    SUMMARY_FILE = Defined destination file name.
    """
    # Construct full path to results directory and summary file.
    # -- Check for extension "txt".
    sub_path, destination = fileop.get_path_name(glo.OUTPUT_DIR,
                                                 glo.SUMMARY_FILE,
                                                 glo.EXTENSION_TXT)

    # Write information to file on Fault_Flo run.
    try:
        with open(destination, 'w') as zum:
            write_iam_parameters(fault_controls, zum)
            write_description_parameters(fault_controls, zum)
            write_aperture_parameters(fault_controls, zum)
            write_fluid_values(fault_controls, zum)
            write_relative_perm(fault_controls, zum)
            write_stress_parameters(fault_controls, zum)
            write_cutoff_parameters(fault_controls, zum)
            if ECHO:
                write_fault_parameters(fault, zum)
            write_co2_results(sim_flux_list, zum)
            write_last(fault_controls, zum)

    except OSError as err:
        fileop.io_snag(err, sub_path, glo.SUMMARY_FILE)

    # return None


def echo_status(alive, phase):
    """Echo progress to console.

    Parameters
    ----------
    alive = (bool) flag to indicate stand-alone operation
    phase = (str) text string indicating position in program

    Returns
    -------
    N/A
    """
    # Echo status to user.
    if alive:
        if phase == 'BEGIN':
            print(LEAD_IN + 'READING CONTROL FILE.', flush=True)
        elif phase == 'CLEAN':
            print(LEAD_IN + 'DELETING OLD FILES & PERFORMING INPUT CHECK.',
                  flush=True)
        elif phase == 'INTERPOLATE':
            print(LEAD_IN + 'INTERPOLATING FLUID PROPERTIES.', flush=True)
        elif phase == 'RUNNING':
            print(LEAD_IN + 'STARTING FAULT_FLO COMPUTATIONS.', flush=True)
        elif phase == 'SUMMARY':
            print(LEAD_IN + 'CREATED SUMMARY FILE. ', flush=True)
        else:
            print('\n Code Error in Echo!')

    # return None


def echo_time_step(desired, alive, current):
    """Echo time step progress to console.

    Parameters
    ----------
    desired = (bool) flag to indicate header is to be printed
    alive = (bool) stand-alone operation
    current = (float) current time step

    Returns
    -------
    N/A
    """
    # Echo progress line to user on console.
    if desired and alive:
        print('    -->> Time Step = {}'.format(current), flush=True)

    # return None


def echo_simulation_step(alive, sim_step):
    """Echo simulation progress to console.

    Parameters
    ----------
    alive = (bool) stand-alone operation
    sim_step = (int) current simulation step

    Returns
    -------
    N/A
    """
    # Echo progress line to user on console.
    if alive:
        print(ZIPX_IN + 'Realization Loop = {}'.format(sim_step + 1),
              flush=True)

    # return None


def closeout_time(alive, fault_controls):
    """Write analysis time information to console.

    Parameters
    ----------
    alive = (bool) flag to indicate stand-alone operation
    fault_controls = (dict) dictionary of fault parameters

    Returns
    -------
    real_time = simulation time
    """
    # Stop clock on runtime.
    end_code = time.monotonic()

    # Echo end.
    print(LEAD_IN + 'COMPLETED ANALYSIS COMPUTATIONS.', flush=True)

    # Echo execution time.
    start_code = fault_controls['code_start']
    elapsed_time = timedelta(seconds=(end_code - start_code))
    fault_controls['elapsed'] = elapsed_time

    if alive:
        print(INFO_IN + 'Analysis Execution Time = {}'.format(elapsed_time))

    return fault_controls


#
# -----------------------------------------------------------------------------
# End of module
