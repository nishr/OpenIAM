#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Main Module - Code performs a fault analysis for NRAP IAM.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_flo

Contents (1)
    main()

Copyright(c) 2019-2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import logging                       # For reporting errors
# import numpy as np                   # For array operations

import flt_compute as fcp            # For basic calc.s and memory ops
import flt_file as fileop            # For file operations
import flt_fluids as fluid           # For density & viscosity
import flt_inout as fio              # For reservoir input
import flt_intro as intro            # Gets YAML data and checks input
import flt_model as modl              # Fault model functions
import flt_plot as fplot             # For plotting data
import flt_reveal as revl             # Provides summary file and plots
import flt_storage as store          # For defining lists

# Details:
__version__ = "2.0"
__author__ = "Ernest N. Lindner"
__contact__ = "Ernest.Lindner@netl.doe.gov"
__date__ = "2021/04/21"
__deprecated__ = False
__license__ = "Personal"
__maintainer__ = "Ernest N. Lindner"
__status__ = "Development"


"""
-------------------------------------------------------------------------------
Note
----
    This software is provided for use as-is and no representations about the
    suitability or accuracy of this software is made. It is provided without
    warranty.

Warranty Disclaimer
-------------------
    This software development was funded by the United States Department
    of Energy, National Energy Technology Laboratory, in part, through a site
    support contract. Neither the United States Government nor any agency
    thereof, nor any of their employees, nor the support contractor, nor any of
    their employees, makes any warranty, express or implied, or assumes any
    legal liability or responsibility for the accuracy, completeness, or
    usefulness of any information, apparatus, product, or process disclosed,
    or represents that its use would not infringe privately owned rights.
    Reference herein to any specific commercial product, process, or service
    by trade name, trademark, manufacturer, or otherwise does not necessarily
    constitute or imply its endorsement, recommendation, or favoring by the
    United States Government or any agency thereof. The views and opinions of
    authors expressed herein do not necessarily state or reflect those of the
    United States Government or any agency thereof.
-------------------------------------------------------------------------------
"""


# Input control file name
YAML_FILE = "ControlFile_fault.yaml"  # Input file name in source directory

# Python development version constants
PYTHON_MAJOR = 3            # Python major version of code
PYTHON_MINOR = 7            # Python minor version of code

# Debugging controls - also controlled by "RUNNING"
DETAIL_PERM = False        # Save permeability to file
DETAIL_STEP = False         # Save results of time step data to file
DETAIL_PLATE = False        # Save fault plate data to file
NO_OUTPUT = True


RUNNING = (__name__ == "__main__")

logging.basicConfig(format='\n  --> %(levelname)s: %(message)s',
                    level=logging.WARNING)


def main():
    """Control entire operation of code.

    Parameters
    ----------
    See Control File

    Returns
    -------
    N/A
    """
    # -------------------------------------------------------------------------
    # A. SETUP
    # -------------------------------------------------------------------------
    # Read control file and check fault_controls.
    fault_controls, param_bounds = intro.start(RUNNING, YAML_FILE,
                                               PYTHON_MAJOR, PYTHON_MINOR,
                                               __version__)

    # Create additional input values.
    fault_controls = fcp.define_fracto(fault_controls, param_bounds)

    # Initialize: Establish time step array - clock_values.
    clock_values = store.create_time_values(fault_controls)  # In years!!

    # Create reservoir input array and fault dictionary.
    sim_flux = []
    fault = []

    # Create storage arrays for time steps within a realization.
    sim_co2_flow, sim_brine_flow, co2_flow_step, brine_flow_step, \
        simulation_list = store.create_sim_storage(fault_controls['n_plates'])

    # Interpolate fluid properties, if desired; zero sum value.
    fault_controls = fluid.manage_interpolation(RUNNING, fault_controls)
    total_co2 = 0.0

    # -------------------------------------------------------------------------
    # B. COMPUTATION LOOPS
    # -------------------------------------------------------------------------
    revl.echo_status(RUNNING, "RUNNING")

    # >>>>>>> REALIZATION LOOP <<<<<<<<
    for simulation_numbr in range(fault_controls['realizations']):

        # Define strike, apertures and fault plates.
        fault_controls, fault = fcp.fault_create(fault_controls, fault)

        # Save fault plate data to file, if desired.
        if DETAIL_PLATE:
            fio.save_fault_results(simulation_numbr, fault_controls['title'],
                                   fault)

        # Echo simulation number to console.
        revl.echo_simulation_step(RUNNING, simulation_numbr)

        # Update list for simulation output.
        simulation_list = store.create_simulation_list(simulation_numbr,
                                                       simulation_list)

        # Update interpolation values.
        fault_controls = fluid.interpolate_setup(fault_controls)

        #  ******* TIME STEP LOOP *******
        # - start at 1 as 0 = zero accumulation
        for chrono in range(1, fault_controls['time_points']):

            # Provide header, if showing time step data.
            revl.echo_time_step(DETAIL_STEP, RUNNING, chrono)

            # Input reservoir pressure and saturation arrays from files.
            base_co2_pressure, base_co2_saturation = \
                fio.input_reservoir_data(chrono, fault_controls['n_plates'],
                                         param_bounds)

            # Update interpolated fluid values if past injection period.
            fault_controls = fluid.update_interpolate(clock_values[chrono],
                                                      fault_controls)

            # ------ Loop Over Fault Plates ------
            # Compute Darcy flow for each plate over one increment.
            for seg_number in range(fault_controls['n_plates']):

                # Get reservoir data for plate.
                co2_base_pressure = base_co2_pressure[seg_number]
                co2_base_saturation = base_co2_saturation[seg_number]

                # Compute flow rates for each plate - vol./sec.
                # NOTE: Dissolved CO2 is included in CO2 flow calculation as
                #       fluids despite assuming immiscible flow as theory.

                flow_rate_co2, flow_rate_brine = \
                    fault[seg_number].compute_flow(co2_base_pressure,
                                                   co2_base_saturation,
                                                   fault_controls)

                # Accumulate vol. flows of each for this time step.
                brine_flow_step[seg_number] = flow_rate_brine
                co2_flow_step[seg_number] = flow_rate_co2
            # ------- End Fault Plate Loop ------

            # Convert flows into mass flows in tonnes for interval.
            co2_flow_step, brine_flow_step = \
                modl.convert_flows(co2_flow_step, brine_flow_step,
                                   clock_values[chrono],
                                   clock_values[chrono - 1],
                                   fault_controls)

            # Sum time step flows for cumulative flows, and compute totals.
            sim_co2_flow += co2_flow_step
            sim_brine_flow += brine_flow_step
            total_co2 = sim_co2_flow.sum()
            total_brine = sim_brine_flow.sum()

            # Write data to a file for <current> time step, if debug.
            if DETAIL_STEP:
                fio.cache_step_results(simulation_numbr, clock_values[chrono],
                                       fault_controls["title"],
                                       sim_co2_flow, sim_brine_flow)

            # Store results in a list of total simulation results.
            simulation_list = \
                store.update_simulation_list(simulation_numbr,
                                             clock_values[chrono],
                                             total_co2, total_brine,
                                             simulation_list)

            # Zero-out calculation arrays for next cycle.
            co2_flow_step, brine_flow_step = \
                store.clear_cycle_storage(co2_flow_step, brine_flow_step,
                                          fault_controls['n_plates'])
        # ******* END TIME STEP LOOP *******

        # Write the list of results for this simulation.
        fio.cache_sim_results(simulation_numbr, simulation_list,
                              fault_controls['title'],
                              fault_controls['skip_output'])

        # Save simulation CO2 flux results to a list.
        sim_flux.append(total_co2)

        # Write permeability data for simulation, if debugging.
        if DETAIL_PERM:
            fio.cache_perm(fault_controls, simulation_numbr, fault)

        # Zero-out computation arrays for next simulation loop.
        sim_co2_flow, sim_brine_flow, simulation_list = \
            store.clear_storage(sim_co2_flow, sim_brine_flow, simulation_list)

        # Re-set interpolation values
        fault_controls = fluid.refresh_interpolate_controls(fault_controls)

    # >>>>>>> END OF REALIZATION LOOP  <<<<<<<

    # -------------------------------------------------------------------------
    # C. AT END: WRITE STATUS & SUMMARY FILE.
    # -------------------------------------------------------------------------
    # Write run time, if stand-alone.
    fault_controls = revl.closeout_time(RUNNING, fault_controls)

    # Write dictionary values to summary file.
    revl.echo_status(RUNNING, "SUMMARY")
    revl.write_summary(fault_controls, sim_flux, fault)

    # Show plots, if desired.
    fplot.plot_manager(RUNNING, fault_controls)

    # Print Exit statement.
    fileop.end_message(RUNNING)


# -----------------------------------------------------------------------------
# Start MAIN Line of Code.
# -----------------------------------------------------------------------------


if __name__ == "__main__":
    main()


#
# -----------------------------------------------------------------------------
# End of module
# -------1---------2---------3---------4---------5---------6---------7---------8
