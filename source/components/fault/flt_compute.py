#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions for misc. fault setup and computations.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_compute

Contents (15)
    is_almost_equal(x, y, epsilon=1.0e-08)
    compute_transition(fault_controls)
    compute_sgr_influence(fault_controls)
    define_fracto(fault_controls)
    stress_factor(fault_controls, old_strike, new_strike)
    project_to_subsurface(fault_controls)
    create_features(fault_controls, fault, x_valu, y_valu)
    define_aspects(fault_controls, fault)
    define_amplius(fault_controls, fault, param_bounds)
    fault_length(fault_controls)
    fault_plane_eq(fault_controls)
    test_oblique(planar, focus, fault_controls)
    distance_vector(planar)
    check_inclination(fault_controls)
    fault_create(fault_controls, fault, param_bounds)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import sys                      # For stdout
import os
import math                     # For sin and other functions
import numpy as np              # For array of values

sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import flt_inout as fio         # File input/output operations
import flt_file as fileop       # For file operations
import flt_model as model       # For aperture variation
import flt_units as fit         # For unit conversion
import flt_basics as bcs        # For basic math operations

# SGR computation constants
ENTRY_MAXIMUM = 10.0            # Max. entry pressure for SGR model
SGR_SLOPE = 0.02                # Slope of linear pressure relation of SGR
SGR_PRESS = 0.4
SGR_MAXIMUM = 100.0             # Max. value of SGR for test
SGR_MINIMUM = 20.0              # Min. value of SGR for effect
SGR_CONSTANT = -4.0             # Constant for permeability relation

# Default constants
V_SMALL_ANGLE = 11.0            # Dip should be > 10.0
V_SMALL_DIFF = 1.0              # Very small difference in depth
V_SMALL_ZERO = 1.0E-08          # Very small number in division by zero
V_SMALL_ONE = 0.01

# Other constants
ECHO = False                    # Debug - Printout intermediate results
ECHO_2 = False                  # Debug - Printout plate results
EXTRA_D = 200                   # Extra distance on inclusion algorithm


def is_almost_equal(x_val, y_val, epsilon=1.0e-08):
    """Return True if two float values are close within tolerance.

    Parameters
    ----------
    x_val = (float) one value for comparison (reference)
    y_val = (float) second value for comparison
    epsilon = (float) allowed difference

    Return
    ------
    result = (boolean) status if within tolerance

    Note
    ----
    1. Default tolerance is 1.0E-08.
    2. Values can be negative or positive.
    """
    result = math.isclose(x_val, y_val, abs_tol=epsilon)
    return result


def compute_transition(fault_controls):
    """Compute the depth & temperature at CO2 phase transition.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    fault_controls = (dict) with updated control

    Notes
    -----
    1. Depths are positive values.
    2. Cutoff_depth = depth where CO2 phase transition to gas occurs.
    3. Assume supercritical pressure is primary for finding transition.
    """
    # Check aquifer pressure to see if a transition occurs.
    if (fault_controls['aquifer_pressure'] >= fit.supercrit_pressure() and
            fault_controls['aquifer_temperature']
            >= fit.supercrit_temperature()):

        # Aquifer depth is deep enough for supercritical CO2.
        fault_controls['cutoff_depth'] = fault_controls['aquifer_depth']
        fault_controls['cutoff_pressure'] = fault_controls['aquifer_pressure']
        fault_controls['cutoff_temperature'] = \
            fault_controls['aquifer_temperature']
        fault_controls['cutoff_type'] = 'Full Extent of Fault was used.'

    else:
        # Find depth for supercritical pressure along pressure gradient.
        #  -- Use line equation to find.
        p_slope, p_beta = bcs.find_line(fault_controls['aquifer_pressure'],
                                        fault_controls['aquifer_depth'],
                                        fault_controls['field_pressure'],
                                        fault_controls['inject_depth'])
        super_depth = p_slope * fit.supercrit_pressure() + p_beta

        # Find temperature at this depth from temperature gradient.
        z_slope, z_beta = bcs.find_line(fault_controls['aquifer_depth'],
                                        fault_controls['aquifer_temperature'],
                                        fault_controls['inject_depth'],
                                        fault_controls['inject_temperature'])
        rev_temp = z_slope * super_depth + z_beta

        # Check if temperature > supercritical temperature.
        if rev_temp > fit.supercrit_temperature():
            # If greater, OK - pressure was determinant.
            fault_controls['cutoff_depth'] = super_depth
            fault_controls['cutoff_pressure'] = fit.supercrit_pressure()
            fault_controls['cutoff_temperature'] = rev_temp
            fault_controls['cutoff_type'] = 'Pressure Cutoff was Used.'

        else:
            # Find intersection of Triple Line and pressure-temperature.
            new_temperature, new_pressure = \
                bcs.find_cross(fault_controls)

            # Compute depth from prior terms.
            final_depth = p_slope * new_pressure + p_beta

            # Set fault_controls parameters.
            fault_controls['cutoff_depth'] = final_depth
            fault_controls['cutoff_pressure'] = new_pressure
            fault_controls['cutoff_temperature'] = new_temperature
            fault_controls['cutoff_type'] = 'CO2 Triple Point Line was Used.'

    return fault_controls


def compute_sgr_influence(fault_controls):
    """Compute the effects of SGR on entry pressure & permeability.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    fault_controls = (dict) with updated control

    Notes
    -----
    1. Only for normal faulting.
    2. Simple models; assumes shale layers have significant clay content.
    3. Assume 20% SGR cutoff on effects.
    4. SGR varies from 0.0 to 100.0 (percent).
    5. References:
        - Yielding, G.; Freeman, B.; Needham, D.T. (1997)
        - Manzocchi, T.; Walsh, J. J.; Nell, P.; Yielding, G. (1999)
        https://www.researchgate.net/publication/237231297
        _Fault_transmissibility_multipliers_for_flow_simulation_models
        - Benguigui, A.; Yin, Z.; MacBeth, C. (2013)
        https://doi.org/10.1088/1742-2132/11/2/025006
        - Bourg, I. C.; Ajo-Franklin, J. B. (2017)
               https://pubs.acs.org/doi/10.1021/acs.accounts.7b00261
    """
    # Define terms for clarity.
    sgr = fault_controls['sgr']

    # Set corrections if SGR is in range
    if SGR_MINIMUM <= sgr <= SGR_MAXIMUM:
        # compute new entry pressure due to SGR (assigned to each plate).
        bot = pow(10, SGR_PRESS)
        top = pow(10, (SGR_SLOPE * sgr))
        press_factor = (top / bot)

        # Use log relationship to find correction factor on permeability.
        val = sgr / SGR_MINIMUM
        log_val = math.log10(val)
        log_val *= SGR_CONSTANT
        new_perm = pow(10, log_val)
    else:
        new_perm = 1.0
        press_factor = 1.0

    fault_controls['sgr_press_correction'] = press_factor
    fault_controls['sgr_perm_correction'] = new_perm

    return fault_controls


def define_fracto(fault_controls, param_bounds):
    """Provide the setup fault input values.

    Parameters
    ----------
    fault_controls = (dict) input parameters
    fault= (class) list of segments
    param_bounds = (dict) parameter bounds (dictionary)

    Returns
    -------
    fault_controls = (dict) revised
    """
    # Define log-normal parameters to compute apertures.
    if fault_controls['aperture_approach']:
        # No variability - data from file.
        fault_controls['aperture_mu'] = 0.0
        fault_controls['aperture_scale'] = 0.0
    else:
        # Setup parameters for variable aperture.
        cent, scale = \
            bcs.convert_lognorm_terms(fault_controls['aperture_mean'],
                                      fault_controls['aperture_std'])
        fault_controls['aperture_mu'] = cent
        fault_controls['aperture_scale'] = scale

    # Setup kappa for strike if needed.
    if fault_controls['strike_approach'] and \
            fault_controls['strike_dev'] > V_SMALL_ZERO:

        fault_controls['strike_disperse'] = \
            bcs.convert_kappa(fault_controls['strike_dev'])

    # Compute transition depth.
    fault_controls = compute_transition(fault_controls)

    # Compute effects of SGR.
    fault_controls = compute_sgr_influence(fault_controls)

    # Input apertures from file - if desired.
    fault_controls = fio.input_apertures(fault_controls, param_bounds)

    return fault_controls


def project_to_subsurface(fault_controls):
    """Project fault start point on surface to subsurface.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    x_new = x coordinate of point in subsurface
    y_new = y coordinate of point in subsurface

    Notes
    -----
    "strike" may vary each simulation.
    """
    # Define variables and convert to radians.
    x_surface = fault_controls['x_start']
    y_surface = fault_controls['y_start']
    alpha = math.radians(fault_controls['dip'])
    theta = math.radians(fault_controls['strike'])
    depth = fault_controls['inject_depth']

    # Incline length - use complement.
    beta = math.radians(90.0 - fault_controls['dip'])
    if beta < 90.0:
        # Compute inclination.
        incline = depth / math.cos(beta)
    else:
        # Error - division by zero!
        incline = 0.0
        fileop.opx_problem("Dip Angle is Too Large!")

    # Along dip extent - local x coordinate.
    x_prime = incline * math.cos(alpha)

    # Rotate from local coordinates to N/E.
    delta_x = math.cos(theta) * x_prime
    delta_y = -math.sin(theta) * x_prime

    # Compute new coordinates.
    x_new = x_surface + delta_x
    y_new = y_surface + delta_y

    return x_new, y_new


def create_features(fault_controls, fault, x_start, y_start):
    """Create a fault (list of plates) with default values & coordinates.

    Parameters
    ----------
    fault_controls = (dict) fault control values
    fault = (class) list of plates
    x_start= (float) X coordinate of subsurface start point
    y_start= (float) Y coordinate of subsurface start point

    Returns
    -------
    fault = (list) fault segment/plates
    """
    # Define computation parameters.
    n_segs = fault_controls['n_plates']
    long = fault_controls['length'] / n_segs
    x_valu = x_start
    y_valu = y_start

    # Generate new points and new fault plates - save in "fault" list.
    for _ in range(n_segs):
        point_1 = model.Pointe(x_valu, y_valu)
        point_2 = point_1.arthro(fault_controls['strike'], long)

        new_plate = model.Plate(point_1, point_2,
                                fault_controls['strike'],
                                fault_controls['dip'])
        fault.append(new_plate)
        x_valu, y_valu = point_2.get_coords()

    # Define fault end point.
    fault_controls['fault_end'] = model.Pointe(x_valu, y_valu)

    # Debug.
    if ECHO_2:
        print("\n start sim")
        for part in fault:
            part.print_plate(sys.stdout)

    return fault


def stress_factor(fault_controls, old_strike, new_strike):
    """Compute the stress factor ratio for new strike based on old.

    Parameters
    ----------
    fault_controls = (dict) fault control values
    old_strike = (float) original strike of fault
    new_strike = (float) new strike of fault

    Returns
    -------
    stress ratio = (float) ratio of normal stresses to fault
    """
    # Rotate to get stress perpendicular to fault strike.
    # >> Note that is approximate as using horizontal components only!
    old_strike += 90.0
    new_strike += 90.0

    # Get ratio of new to old stress as correction factor.
    major = fault_controls['max_horizontal']
    minor = fault_controls['min_horizontal']
    stress_angle = fault_controls['max_trend']

    radius_old = bcs.radius_ellipse(major, minor, stress_angle, old_strike)
    radius_new = bcs.radius_ellipse(major, minor, stress_angle, new_strike)
    stress_ratio = radius_new / radius_old

    return stress_ratio


def define_aspects(fault_controls, fault):
    """Define parameters for fault plates.

    Parameters
    ----------
    fault_controls = (dict) fault control values
    fault = (class) list of segments/plates
    file_gaps = (array) apertures from file (if desired)

    Returns
    -------
    fault_controls = (dict) fault control values revised
    """
    # Compute correction for stress field.
    normal_factor = stress_factor(fault_controls,
                                  fault_controls['strike_mean'],
                                  fault_controls['strike'])

    # Define terms for clarity.
    nsegs = fault_controls['n_plates']
    gaps = np.empty(nsegs)

    # Define apertures for plates - depending on option selected.
    if fault_controls['aperture_approach']:
        # Take apertures from file values - no correction for stress field.
        gaps = fault_controls['aperture_data']

    elif fault_controls['vary_aperture']:
        # Vary aperture and correct for stress field.
        for i in range(nsegs):
            gaps[i] = \
                bcs.evaluate_lognormal(fault_controls['aperture_mu'],
                                       fault_controls['aperture_scale'],
                                       fault_controls['aperture_min'],
                                       fault_controls['aperture_max'])
            gaps[i] *= normal_factor
    else:
        pass
        # Use single value for all - no correction.
        valu = fault_controls['aperture_mean']
        gaps.fill(valu)

    # -----------
    # Assign an aperture to each fault plate + remaining parameters.
    for index, part in enumerate(fault):

        # Set plate aperture in mm
        part.aperture = gaps[index]

        # Compute Permeability in meters.
        #   cubic law => k [permeability] - in m^2
        corrected = gaps[index] * fit.mm_to_m()
        squared = math.pow(corrected, 2)
        part.perm = squared / 12.0

        # Entry pressure.
        part.entry = fault_controls['entry_pressure']

        # Debug.
        if ECHO:
            # part.print_plate(sys.stdout)
            pass

    return fault


def define_amplius(fault_controls, fault):
    """Complete fault input values and fault plates.

    Parameters
    ----------
    fault_controls = (dict) fault control values
    fault = (list) list of segments/plates

    Returns
    -------
    fault_controls = (revised)
    """
    # Define new strike, if desired.
    if fault_controls['strike_approach'] and \
            fault_controls['strike_dev'] > V_SMALL_ZERO:

        fault_controls['strike'] = \
            bcs.evaluate_orientation(fault_controls['strike_mean'],
                                     fault_controls['strike_disperse'])
    else:
        fault_controls['strike'] = fault_controls['strike_mean']

    # Compute subsurface coordinates.
    x_valu, y_valu = project_to_subsurface(fault_controls)
    fault_controls['fault_start'] = model.Pointe(x_valu, y_valu)

    # Create fault plates.
    fault = create_features(fault_controls, fault, x_valu, y_valu)

    # Assign aperture and other parameters to fault plates
    fault = define_aspects(fault_controls, fault)

    return fault_controls, fault


def fault_length(fault_controls):
    """Compute the travel path length along fault for liquid Darcy Flow.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    fault_controls = (dict) fault control values with updated controls

    Notes
    -----
    1. Assume no tortuosity / channelization on path.
    2. Depths are positive values.
    3. "cutoff depth" = supercritical transition depth for fault.
    """
    # compute length from injection depth to transition depth.
    bottom = fault_controls['inject_depth']
    top = fault_controls['cutoff_depth']
    delta_depth = bottom - top
    theta = fault_controls['dip']
    total_length = 0.0

    # Near-zero length or dip can crash analysis.
    if delta_depth < V_SMALL_DIFF:
        msg = "Input Error: Transition Depth is Near Injection Depth"
        fileop.opx_problem(msg)
    elif theta < V_SMALL_ANGLE:
        msg = ("Input Error: Fault is Near-Horizontal with a Dip of {}".
               format(fault_controls['dip']))
        fileop.opx_problem(msg)
    else:
        total_length = delta_depth / math.sin(math.radians(theta))

    # Set length as new fault_control.
    fault_controls['travel_distance'] = total_length

    return fault_controls


def fault_plane_eq(fault_controls):
    """Compute the vector equation for the normal to a fault.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    plane_new = (array) np_vector for plane with X-Y-Z components.

    Notes
    -----
    1. Direction cosines are converted from East-North-Down (NED)
        Use Right Hand Rule (RHR)
        v_north = north component
        v_east = east component
        v_down = z (down) component
    2. XYZ is a RHR East-North-Down (ENU) system
    3. see: Penn State (2001)
        https://onlinelibrary.wiley.com/doi/pdf/10.1002/9780470099728.app3
        http://www.geo.cornell.edu/geology/faculty/RWA/structure-lab-manual
        /chapter-2.pdf
        http://eqseis.geosc.psu.edu/cammon/HTML/UsingMATLAB/PDF/
        ML1%20FaultNormals.pdf
    """
    # Define angles.
    phi = math.radians(fault_controls['strike'])
    theta = math.radians(fault_controls['dip'])

    # compute direction cosines (Ref #3).
    v_north = -math.sin(phi) * math.sin(theta)
    v_east = math.cos(phi) * math.sin(theta)
    v_down = -math.cos(theta)

    # Convert from NED to ENU (XYZ) RHS coordinate system (Ref. #1, #2).
    vector_x = v_east
    vector_y = v_north
    vector_z = -v_down

    # Compute "D" value using start point (z = 0.0).
    d_value = -(vector_x * fault_controls['x_start']
                + vector_y * fault_controls['y_start']
                + vector_z * 0.0)

    # Establish vector.
    plane_new = np.array([vector_x, vector_y, vector_z, d_value])

    return plane_new


def test_oblique(planar, focus, fault_controls):
    """Check if closest point to well is within fault segment limits.

    Parameters
    ----------
    planar = (array) vector values for plane
    focus = (array) well location coordinates (3D)
    fault_controls = (dict) fault control values

    Returns
    -------
    direct = (bool) flag if short point within fault extent.

    Notes
    -----
    1. Indicator that fault is facing/near well
        https://mathworld.wolfram.com/Point-PlaneDistance.html
    """
    # Define denominator of computation - value not squared!
    direct = False
    denominator = (planar[0] * planar[0]
                   + planar[1] * planar[1]
                   + planar[2] * planar[2])

    # Compute lambda (eta) factor and determine resulting point on plane.
    if denominator > 0.0:
        numerator = -(planar[0] * focus[0]
                      + planar[1] * focus[1]
                      + planar[2] * focus[2]
                      + planar[3])
        eta = numerator / denominator

        # Compute coordinates of near point on plane.
        normal_pt = np.zeros(3)
        for i in range(3):
            normal_pt[i] = focus[i] + eta * planar[i]

        # Define fault start and end points in subsurface.
        pt_start = fault_controls.get('fault_start')
        x_pt1, y_pt1 = pt_start.get_coords()
        pt_end = fault_controls.get('fault_end')
        x_pt2, y_pt2 = pt_end.get_coords()

        # Check if well location is within fault extent (in subsurface)
        # + EXTRA_D as a margin.
        if (min(x_pt1, x_pt2) - EXTRA_D) < normal_pt[0] \
                < (max(x_pt1, x_pt2) + EXTRA_D):
            if (min(y_pt1, y_pt2) - EXTRA_D) < normal_pt[1] \
                    < (max(y_pt1, y_pt2) + EXTRA_D):
                direct = True

        # Debug.
        if ECHO:
            print('  - Normal point = ', end='')
            print(normal_pt)

    return direct


def distance_vector(planar, well_pt):
    """Compute vector for shortest distance from injection well to fault.

    Parameters
    ----------
    planar = (array) vector values for plane (XYZ system)
    depth = (float) depth of injection point (XYZ System)

    Returns
    -------
    distance = (float) shortest distance from well point to plane.
    new_vector = (array) vector from liner to plane (in XYZ)

    Notes
    -----
    1. Right Hand Rule, North-East-Down Convention
        https://mathworld.wolfram.com/Point-PlaneDistance.html
    """
    # Define denominator of distance computation without "d".
    denominator = math.sqrt(planar[0] * planar[0]
                            + planar[1] * planar[1]
                            + planar[2] * planar[2])

    # Check for zero divisor.
    if denominator > 0.0:
        numerator = (planar[0] * well_pt[0]
                     + planar[1] * well_pt[1]
                     + planar[2] * well_pt[2]
                     + planar[3])
        distance = abs(numerator / denominator)

        # Debug.
        if ECHO:
            val = numerator / denominator
            print('\n  - Distance = {:.5e} m'.format(val))

    else:
        distance = 0.00

    # ---------------------
    # Determine the vector from point to plane in XYZ space.
    vector_new = np.zeros(3)
    for i in range(3):
        vector_new[i] = -(planar[i] - well_pt[i])

    # Debug.
    if ECHO:
        print('  - Perpendicular Point Vector = ', end='')
        print(vector_new)

    return distance, vector_new


def check_inclination(fault_controls):
    """Determine the fault inclination wrt to the well and set factor.

    Parameters
    ----------
    fault_controls = (dict) fault control values

    Returns
    -------
    fault_controls = (dict) fault control values with updated control
    """
    # Establish plane and well point in vector notation (NED).
    plane_vector = fault_plane_eq(fault_controls)
    well_pt = [fault_controls.get('inject_x'),
               fault_controls.get('inject_y'),
               -fault_controls.get('inject_depth')]

    # Determine distance and distance vector from well to plane.
    extent, well_vector = distance_vector(plane_vector, well_pt)
    fault_controls['shortest_distance'] = extent

    # Determine angle of plane normal wrt to well distance vector.
    factor, included_angle = bcs.angle_vector(plane_vector, well_vector)
    fault_controls['angle_between_vectors'] = included_angle

    # Determine if shortest distance point is on/near fault plate extent.
    nearby = test_oblique(plane_vector, well_pt, fault_controls)

    # Determine plane inclination condition and effect factor.
    #   Note for factor ~1.0, vector is perpendicular to plane
    if (is_almost_equal(factor, 1.0, V_SMALL_ONE)) and nearby:
        fault_controls['fault_inclined'] = \
            "Fault Normal Is Parallel to Projection Line."
        fault_controls['fault_orient_effect'] = 0.90
    elif factor > 0.0 and nearby:
        fault_controls['fault_inclined'] = \
            "Inclined Away From Injection Point."
        fault_controls['fault_orient_effect'] = 0.90
    elif factor < 0.0 and nearby:
        fault_controls['fault_inclined'] = \
            "Inclined Towards Injection Point."
        fault_controls['fault_orient_effect'] = 1.10
    else:
        fault_controls['fault_orient_effect'] = 1.00
        if not nearby:
            fault_controls['fault_inclined'] = \
                "Inclination is Oblique wrt Well."
        else:
            # Error.
            fault_controls['fault_inclined'] = \
                "Inclination Not Determined!"

    # Debug.
    if ECHO:
        print('  - Inclination Condition: '
              + fault_controls['fault_inclined'])

    return fault_controls


def fault_create(fault_controls, fault):
    """Manage the fault setup/plate creation.

    Parameters
    ----------
    fault_controls = (dict) fault control values
    fault = (class) fault (with segments)

    Returns
    -------
    fault_controls = (dict) fault control values updated values
    fault = (class) fault plates
    """
    # Define faults.
    fault.clear()
    fault_controls, fault = \
        define_amplius(fault_controls, fault)

    # Initialize: Compute travel length along fault.
    fault_controls = fault_length(fault_controls)

    # Determine inclination wrt to injection well.
    fault_controls = check_inclination(fault_controls)

    return fault_controls, fault


#
# -----------------------------------------------------------------------------
# End of module
