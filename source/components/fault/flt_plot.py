#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions to plot fault history data.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_plot

Contents (9)
    round_exp(exp_value)
    setup_time_history_data(simulation)
    simulation_query(max_realizations, start_value=0)
    create_history_query(query_stage, series)
    range_query(max_realizations)
    control_time_series(max_realizations)
    save_figure_to_file()
    plot_time_series(x_data, data_list, data_maximum, start_sim)
    plot_manager(fault_controls)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import math                     # For truncation
import csv                      # For saving a list of lists
import numpy as np              # For array operations
import matplotlib.pyplot as plt   # For plots

import flt_file as fileop       # For paths and error reports
import flt_global as glo        # IO directory and file names


# Plot constants for figures
HEIGHT = 8                      # Figure height (inches)
WIDTH = 10                      # Figure width (inches)
LEGEND_FONT = 16                # Font size for figure title (pt)
TYP_FONT = 12                   # Font size for axes numbers (pt)
RESOLVE = 90                    # Figure display resolution (dpi)

# Plot Titles - Time history series
LEGEND_SERIES = "\n Fault_Flo Results for CO2 Migration through a Fault \n"

# Font definitions
REG_FONT = {'fontname': 'Arial',    # X-axis, Y-axis font
            'color':    'black',
            'size':      TYP_FONT}
LEG_FONT = {'fontname': 'Arial',    # Title font
            'color':    'blue',
            'size':      LEGEND_FONT}

# General Statements
MAJ_IN = "\n  --> "                             # Page + general message start
LEAD_IN = "      --> "                          # General message start
CMD_IN = "      >>> "                           # Command message start
ERR_IN = "      *** "                           # Error message start
START_TXT = LEAD_IN + "START Realization"       # Multi-history plot start
END_TXT = LEAD_IN + "END Realization"           # Multi-history plot end
CAUTION = (CMD_IN + "If Paused, "
           + "Hit Exit at Top-Right of Plot Window to Proceed ...")
WAIT_FOR_IT = CMD_IN + "Please wait for plot..."
AGAIN = "Please Try Again."                     # Simple repeat request
AGAIN2 = "\n" + CMD_IN + AGAIN                  # Repeat request with new line
EMPTY = ("\n" + CMD_IN + "Y Value Essentially Zero For Selected Simulations!!"
         + "\n" + CMD_IN + "- No Plot; Try Again.")

# Controls and warnings
NEGATIVES = ('n', 'N', 'q', 'Q')                # Keys for negative answer
EXIT_COMMAND = ('x', 'X')                       # Keys that will exit
STAR_IN = "\n       ?-> "                       # Question start
CIRC_IN = "      --> "                          # Other question/statement
MORE_IN = "\n    ?-> "                          # Question start + newline
SERIES_START = MORE_IN + "Create a Time-Series Plot? ('Q'=quit): "
SERIES_END = MORE_IN + "Create Another Time-Series Plot? ('Q'=quit): "
V_SMALL_ZERO = 1.0E-06                          # Very small number
SAVE_FIGURE = True                             # Save plots to file


def round_exp(exp_value):
    """Round-up exponential to desired degree.

    Parameters
    ----------
    exp_value = (float) axis value

    Returns
    -------
    target = (float) rounded exponential
    """
    # Trap error if = 0.0.
    if exp_value != 0.0:
        # Make the number a decimal.
        core = abs(exp_value)
        level = math.trunc(math.log10(core))
        base = math.pow(10.0, level)
        root_exp = math.ceil(exp_value / base)

        # Round and then reconstitute the number.
        # adjust_number = round(root_exp, 1)
        target = root_exp * base  # round to zero decimal
    else:
        target = exp_value

    return target


def setup_time_history_data(simulation):
    """Get time history data from file.

    Parameters
    ----------
    simulation = (int) simulation number to plot

    Returns
    -------
    leakage_data = (NumPy array) CO2 data
    time_data = (NumPy array) time values
    """
    # Construct path name to file.
    sim_number = str(simulation)
    file_name = glo.RESULTS_NAME + sim_number
    subdirectory_path, destination = fileop.get_path_name(glo.OUTPUT_DIR,
                                                          file_name,
                                                          glo.EXTENSION_CSV)
    try:
        with open(destination, 'r') as csvfile:
            csv_reader = csv.reader(csvfile)

            # Skip 2 header lines.
            header = next(csv_reader, None)
            next(csv_reader)

            # Do not read file without a header / empty file.
            if header is None:
                # No data.
                fileop.data_error("Data Error - Array Header is None! "
                                  + "-> No data found in file.",
                                  subdirectory_path, file_name)
            else:
                # Create data array - iterate over each row after the header.
                data_list = []
                for row in csv_reader:
                    data_list.append(row)

        # Convert list to NumPy array.
        npa = np.asarray(data_list, dtype=np.float64)

        # Slice array for data.
        time_data = npa[:, 1]
        leakage_data = npa[:, 2]

    except OSError as err:
        fileop.io_snag(err, subdirectory_path, file_name)
        time_data = np.array([])  # for inspection
        leakage_data = np.array([])   # for inspection

    return leakage_data, time_data


def simulation_query(max_realizations, start_value=0):
    """Get the simulation number to plot.

    Parameters
    ----------
    max_realizations = (int) maximum number of realizations
    start_value = (int) minimum number of realizations

    Returns
    -------
    simulation = (int) simulation number to plot - integer
    """
    # Default values.
    repeat_loop = True
    response = False
    realization = -1

    # Loop to get an appropriate response.
    while repeat_loop:
        try:
            # Get input with prompt.
            code = input(STAR_IN + "Enter Realization Number "
                         + "to Plot ('Q'=quit): ")
            # Check if user wishes to quit.
            if code in NEGATIVES or code in EXIT_COMMAND:
                print(CIRC_IN + "Exiting Plot Option. \n")
                repeat_loop = False
                break
        except ValueError:
            # Parse fails.
            print(ERR_IN + "Invalid Number.  " + AGAIN2)
            continue

        # Check if "entered" is a number and then within correct range.
        try:
            entered = int(code)
            if entered > max_realizations:
                print(ERR_IN + "You typed = {}".format(entered))
                print(ERR_IN + "This Number Exceeds the Maximum "
                      + " of {}! ".format(max_realizations), end='')
                print(AGAIN)
            elif entered <= 0:
                print(ERR_IN + "You typed = {}".format(entered))
                print(ERR_IN + "The Input is Equal to, or Less than Zero! ",
                      end='')
                print(AGAIN)
            elif entered < start_value:
                print(ERR_IN + "You typed = {}".format(entered))
                print(ERR_IN + "The Input is Less Than Starting Value" +
                      " of {}! ".format(start_value), end='')
                print(AGAIN)
            else:
                # Input OK!
                # print(CIRC_IN + "Simulation Selected = {}".format(entered))
                realization = entered
                response = True
                repeat_loop = False
                break
        except ValueError:
            print(ERR_IN + "You typed = {}".format(code))
            print(ERR_IN + "This is Not a Number!! ", end='')
            print(AGAIN)
            continue

    # end while

    return response, realization


def create_history_query(query_stage):
    """Check if user wants to plot a time series.

    Parameters
    ----------
    query_stage = (int) question queue
        0 = first time
        1 = plot again?

    Returns
    -------
    reply = answer plot question
    """
    # Change message depending on place in query queue.
    if query_stage == 0:
        code = input(SERIES_START)
    else:
        code = input(SERIES_END)

    # Get response.
    if (code in NEGATIVES) or (code in EXIT_COMMAND):
        reply = False
    else:
        reply = True

    return reply


def range_query(max_realizations):
    """Get simulation number to plot.

    Parameters
    ----------
    max_realizations = (int) maximum number of realizations

    Returns
    -------
    response = to plot or not - Boolean
    min_simulation = minimum simulation to plot - integer
    max_simulation = maximum simulation to plot - integer
    """
    # Default values.
    end_run = 0

    # Get start simulation for start of run with prompt.
    print(START_TXT, end='')

    # Get simulation number.
    starter = 0
    response, start_run = simulation_query(max_realizations, starter)

    # If OK, get end of range of simulations.
    if response:
        print(END_TXT, end="")
        starter = start_run
        response, end_run = simulation_query(max_realizations, starter)

    return response, start_run, end_run


def control_time_series(max_realizations):
    """Provide overall control to plot a series of simulation from output.

    Parameters
    ----------
    max_realizations = (int) upper limit of simulations from analysis

    Returns
    -------
    N/A
    """
    # Loop until finished plotting.
    while True:
        # Query to get simulation numbers (start and end).
        response, start_run, end_run = range_query(max_realizations)

        # Plot - if user desires a plot.
        if response:
            # Get data and store NumPy arrays in list.
            #   -- adjust counter to catch last run.
            data_list = []
            time_data = []
            final_max = 0.0
            for realization in range(start_run, (end_run + 1)):
                leakage_data, time_data = setup_time_history_data(realization)
                data_list.append(leakage_data)
                instance_max = leakage_data.max()
                if instance_max > final_max:
                    final_max = instance_max

            # Plot data arrays on same plot diagram - if data > 0.
            if len(data_list) > 0:
                plot_time_series(time_data, data_list, final_max, start_run)
            else:
                print(EMPTY)

            # Ask about another plot; exit if not.
            plot_again = create_history_query(1)
            if not plot_again:
                break
        else:
            break
    # end while loop

    # return None


def save_figure_to_file():
    """Save figure of results to output file.

    Parameters
    ----------
    N/A

    Returns
    -------
    N/A
    """
    # Create filename and then save plot to file.
    file_name = "Fault_Flo-plot_" + str(glo.PLOT_NO)
    _, destination = fileop.get_path_name(glo.OUTPUT_DIR,
                                          file_name,
                                          glo.EXTENSION_PNG)
    plt.savefig(destination)

    # Increase figure number for next plot
    glo.PLOT_NO += 1

    # return None


def plot_time_series(x_data, data_list, data_maximum, start_sim):
    """Plot a series of leakage time histories (results).

    Parameters
    ----------
    x_data = (array) time data - assumed same for all
    data_list = (list of arrays) a list of NumPy arrays of leakage
    data_maximum = (float) maximum of <all> y data
    start_sim = (int) number of first simulation for plotting

    Returns
    -------
    N/A

    Notes
    -----
    1. Uses matplotlib functions to plot realizations.
    2. Uses LaTex to format text.
    """
    # Establish window size for plotting in inches.
    plt.figure(figsize=(WIDTH, HEIGHT), dpi=RESOLVE,
               num='Fault_Flux Simulations')

    # Get exponent for y-axis - to normalize data.
    y_max = round_exp(data_maximum)
    cite = math.trunc(math.log10(abs(y_max)))
    y_max /= pow(10.0, cite)

    # Plot each "normalized" data set as line with label.
    for sim, results_array in enumerate(data_list):
        describe = "Simulation #" + str(sim + start_sim)
        y_data = results_array / pow(10.0, cite)
        plt.plot(x_data, y_data, linestyle='solid', linewidth=1.0,
                 label=describe)

    # Set axes limits to limit white space along axes.
    x_min_plot = math.floor(np.min(x_data))
    x_max_plot = math.ceil(np.max(x_data))
    plt.xlim(x_min_plot, x_max_plot)
    plt.ylim(0.0, y_max)

    # Hide axis title.
    pivot = plt.gca()
    pivot.yaxis.offsetText.set_visible(False)

    # Construct plot title and provide axis labels and figure grid.
    new_label = r'Leakage ($\times$10$^{%d}$ tonnes)' % cite
    plt.title(LEGEND_SERIES, fontdict=LEG_FONT)
    plt.xlabel('Time (years)', fontdict=REG_FONT)
    plt.ylabel(new_label, fontdict=REG_FONT)
    plt.grid(which='major', linewidth='0.5')

    # Plot key of some sort.
    if len(data_list) <= 25:
        # Plot key at upper left - if enough space -  say 25 lines.
        plt.legend(loc=2, shadow=True, fancybox=True)
    else:
        # If large number of lines, plot number of lines in top left corner.
        new_label = r'Number of Simulations = %d' % len(data_list)
        plt.text(150, 600, new_label, ha='left', va='center',
                 transform=None, fontdict=REG_FONT)

    # Option to save plot to file or show on console.
    if SAVE_FIGURE:
        # Save to output file.
        save_figure_to_file()
    else:
        # Else show figure on console.
        #   Note: WINDOW BUG: interactive window and plot window
        #                     are not be active together.
        print(CAUTION)
        plt.show()
        # plt.waitforbuttonpress(0) # this will wait for indefinite time

    # return None


def plot_manager(alive, fault_controls):
    """Control plots produced by program.

    Parameters
    ----------
    alive = (bool) status of program; alive = True if stand-alone
    fault_controls = (dict) dictionary of fault controls

    Returns
    -------
    N/A
    """
    # Check if in stand-alone operation mode.
    if alive:
        # Print header, if plotting is desired.
        if fault_controls['plot_time_history']:
            print(MAJ_IN + "PLOT OPTIONS.", end='')

            # Inquire on plotting; limit selection of plot numbers in query.
            max_realizations = (fault_controls['realizations'])
            initial_query = 0
            response = create_history_query(initial_query)

            if response:
                control_time_series(max_realizations)
    # return None


#
# -----------------------------------------------------------------------------
# End of module
