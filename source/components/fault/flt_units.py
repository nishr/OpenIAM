#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module contains functions to provide conversion factors / standard values.

Author: Ernest N. Lindner
Date: 04/21/2019

Module Name
    flt_units

Contents (27)
    euler_constant()
    hydrate_pressure()
    hydrate_temperature()
    triple_pressure()
    triple_temperature()
    supercrit_pressure()
    supercrit_temperature()
    mm_to_m()
    m_to_mm()
    yrs_to_seconds()
    yrs_to_days()
    seconds_to_yrs()
    days_to_yrs()
    gravity()
    pascals()
    megapascals()
    nacl_molar_mass()
    co2_molar_mass()
    ppm_convert()
    kilo_to_tonne()
    kilogram_to_gram()
    microd_to_metersq()
    metersq_to_microd()
    feet_to_meters()
    earth_pressure(depth)
    brine_pressure(depth)
    geothermal_temperature(depth)

Copyright(c) 2021 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""


def euler_constant():
    """Provide Euler's constant used in Theis solution.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) Euler–Mascheroni constant (50 places)

    Notes
    -----
    From https://en.wikipedia.org/wiki/Euler%E2%80%93Mascheroni_constant
    """
    value = 0.577215664901532860606512090082402431004215933593992
    return value


def hydrate_pressure():
    """Provide the pressure limit for P Quadruple Point - hydrate point.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in Pa

    Notes
    -----
    SLOAN, E. D.; KOH, C.; SUM, A. K., (2011)
        Natural gas hydrates in flow assurance.
        Gulf Professional Pub./Elsevier.
        (see Wikipedia, on carbon dioxide clathrate)
        = 4.499 MPa
    """
    value = 4499000.0  # Pa
    return value


def hydrate_temperature():
    """Provide temperature limit for P Quadruple Point - hydrate point.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in degrees celsius

    Notes
    -----
    SLOAN, E. D.; KOH, C.; SUM, A. K., (2011)
        Natural gas hydrates in flow assurance.
        Gulf Professional Pub./Elsevier.
        (see Wikipedia, on carbon dioxide clathrate)
        = 283.0 K
    """
    value = 9.85  # oC
    return value


def triple_pressure():
    """Provide pressure limit for CO2 triple point.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in Pa

    Notes
    -----
    From Span and Wagner (1994)
        www.nist.gov/system/files/documents/srd/jpcrd516.pdf
        = 0.51795 MPa
    """
    value = 517950.0  # Pa
    return value


def triple_temperature():
    """Provide temperature limit for CO2 triple point.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in degrees celsius

    Notes
    -----
    From Span and Wagner (1994)
        www.nist.gov/system/files/documents/srd/jpcrd516.pdf
        = 216.592 K
    """
    value = -56.558  # oC
    return value


def supercrit_pressure():
    """Provide pressure limit for supercritcal CO2.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in Pa

    Notes
    -----
    From Wikipedia:
        https://en.wikipedia.org/wiki/Supercritical_carbon_dioxide
    """
    value = 7377300.0  # Pa
    return value


def supercrit_temperature():
    """Provide temperature limit for supercritical CO2.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in degrees celsius

    Notes
    -----
    NIST:
        https://webbook.nist.gov/cgi/cbook.cgi?ID=C124389&Mask=4
    """
    value = 31.03  # oC
    return value


def mm_to_m():
    """Provide conversion factor for mm to meters.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in m
    """
    value = 1.000E-03  # m
    return value


def m_to_mm():
    """Provide conversion factor for meters to mm.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in mm
    """
    value = 1.000E+03  # mm
    return value


def yrs_to_seconds():
    """Provide conversion factor for years to seconds.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in seconds (for Julian calendar)

    Notes
    -----
    1. Year = 365.25 days => 3.155 760 E+07 sec.
    2. Day value from Wikipedia.
    3. Computed in Excel.
    """
    value = 3.155760E+07    # in seconds
    return value


def yrs_to_days():
    """Provide conversion factor for years to days.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in seconds (Julian calendar)

    Notes
    -----
    1. Year = 365.25 days.
    2. Day value from Wikipedia.
    """
    value = 3.6525E+02    # in days
    return value


def seconds_to_yrs():
    """Provide conversion factor for seconds to years.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in yrs (Julian calendar)

    Notes
    -----
    1. Numbers:
        > Year = 365.25 days => 3.155 760 E+07 sec.
        > 1/year = 3.168 808 781 402 89 E-08.
    2.  Day value from Wikipedia.
    3. Computed in Excel.
    """
    value = 3.16880878140289E-08   # in years
    return value


def days_to_yrs():
    """Provide conversion factor for days to years.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in yrs (Julian calendar)

    Notes
    -----
    1. Numbers:
        > Year = 365.25 days.
        > 1/year = 2.7378508 E-03.
    2. Day value from Wikipedia.
    3. Computed in Excel.
    """
    value = 2.7378507871321E-03  # in years
    return value


def gravity():
    """Provide standard gravity - in metric terms.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) standard acceleration (g) due to gravity (m/sec2)

    Notes
    -----
    Value from Wikipedia.
    """
    value = 9.80665  # m/s2
    return value


def pascals():
    """Provide conversion factor for MPa to Pa.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in pascals (Pa)
    """
    value = 1.000000E+06  # Pa
    return value


def megapascals():
    """Provide conversion factor for Pa to MPa.

    Parameters
    ----------
    None (Pa)

    Returns
    -------
    value = (float) in megapascals (MPa)
    """
    value = 1.000000E-06  # MPa
    return value


def nacl_molar_mass():
    """Provide NaCl molar mass.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) molar mass NaCl (g/mol)

    Notes
    -----
    From: https://www.webqc.org/molecular-weight-of-NaCl.html
    """
    value = 58.4428
    return value


def co2_molar_mass():
    """Provide CO2 molar mass.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) molar mass CO2 (g/mol)

    Notes
    -----
    From https://sciencetrends.com/molar-mass-of-co2-carbon-dioxide/
    """
    value = 44.00964
    return value


def ppm_convert():
    """Provide conversion factor for ppm to decimal.

    Parameters
    ----------
    None (ppm)

    Returns
    -------
    value = (float) unitless
    """
    value = 1.000000E-06
    return value


def kilo_to_tonne():
    """Provide conversion factor for kg to metric tonnes.

    Parameters
    ----------
    None (Kg)

    Returns
    -------
    value = (float) in metric tonne
    """
    value = 1.000000E-03  # tonnes
    return value


def kilogram_to_gram():
    """Provide conversion factor for kg to g.

    Parameters
    ----------
    None (Kg)

    Returns
    -------
    value = (float) in grams
    """
    value = 1.000000E+03  # g
    return value


def microd_to_metersq():
    """Provide conversion factor for microdarcys to m2.

    Parameters
    ----------
    None (microdarcy)

    Returns
    -------
    value = (float) in m2

    Notes
    -----
    From Cardarelli, F. (1997)
        Scientific Unit Conversion: A Practical Guide to Metrication.
    """
    value = 9.86923266716E-19    # m2
    return value


def metersq_to_microd():
    """Provide conversion factor for m2 to microdarcys.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in microdarcys

    Notes
    -----
    From https://en.wikipedia.org/wiki/Darcy_(unit)#Conversions
    """
    value = 1.01325E+18     # microdarcy
    return value


def feet_to_m():
    """Provide conversion factor for feet to meters.

    Parameters
    ----------
    N/A

    Returns
    -------
    value = (float) in meters

    Notes
    -----
    From https://en.wikipedia.org/wiki/Foot_(unit)
    """
    value = 0.3048     # m
    return value


def earth_pressure(depth):
    """Compute lithostatic pressure at a depth.

    Parameters
    ----------
    depth = (float) depth in meters

    Returns
    -------
    value = (float) pressure value in MPa

    Note
    ----
    1. Gradient = Total subsurface mass gradient, kPa/m
    2. https://www.sciencedirect.com/topics/engineering/lithostatic-pressure
    """
    lithostatic_gradient = 24.0  # kPa/m
    value = depth * lithostatic_gradient / 1000.0  # MPa

    return value


def brine_pressure(depth):
    """Compute brine / hydrostatic pressure at a depth.

    Parameters
    ----------
    depth = (float) depth in meters

    Returns
    -------
    value = (float) pressure value in MPa

    Note
    ----
    1. Gradient = Saline water gradient, ~10.53 kPa/m
    2. Salinity is about 80,000 ppm of dissolved solids at 25OC
    3. URL: http://petromodel.ifz.ru/fileadmin/user_upload/docs/aspirantura
       /asp_library/PETROPHYSICS__Second_Edition_.pdf
    """
    hydrostatic_gradient = 10.53  # kPa/m
    value = depth * hydrostatic_gradient / 1000.0  # MPa

    return value


def geothermal_temperature(depth):
    """Compute geothermal temperature at a depth.

    Parameters
    ----------
    depth = (float) depth in meters

    Returns
    -------
    value = (float) temperature (oC)

    Note
    ----
    1. Geothermal gradient ~25.0 oC/km
    2. Linear equation includes surface temperature, assumed = 20.0 oC
    3. URL: https://www.sciencedirect.com/topics/earth-and-planetary-sciences/
            geothermal-gradient
    """
    surface_temperature = 20.0  # or 68 oF
    thermal_gradient = 25.0  # kPa/m
    value = depth * thermal_gradient / 1000.0  # oC
    value += surface_temperature

    return value


#
# -----------------------------------------------------------------------------
# End of module
