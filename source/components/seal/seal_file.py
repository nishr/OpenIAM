"""
PYTHON MODULE:
    seal_file.py
PURPOSE:
    Functions for reading, writing and checking and opening YAML file.
REVISION HISTORY:
    2019-05-06  Started work on this module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (15):
    check_python()
    check_file_floats(data_array, data_type, data_limits)
    check_file_values(data_array, data_type, data_limits)
    opx_problem(msg, err)
    io_snag(err, subdirectory, file_selected)
    io_yml(err, subdirectory, file_selected)
    data_error(subdirectory, file_selected)
    show_warranty()
    acquire_yaml_file(yaml_file_selected)
    acquire_data_array(new_sub, file_selected, extent)
    clean_output()
    get_path_name(subdirectory, file_selected, extension)
    find_local_path(file_selected)
    end_message(alone)
    terminate_code()
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import os                       # For paths
import sys                      # For program exit
import textwrap                 # For warranty header statement
import logging                  # For reporting errors
import numpy as np              # for array
import yaml                     # For structured control file input

# I/O constants
OUTDIR_NAME = "seal_output"     # Subdirectory to be cleaned / output
EXTENSION_CSV = ".csv"          # Result files to be deleted
EXTENSION_NPY = ".npy"          # Detailed data files to be deleted
EXTENSION_TXT = ".txt"

# Other constants
TEXT_WIDTH = 60                 # width of text on console for warranty
LEAD_IN = "\n  --> "            # Message start
ERR_IN = "\n    > "             # Secondary Error messages start
FINAL = "  >>> "                # Last line start
ECHO = False                    # Control to print for debugging
PYTHON_MAJOR = 3                # Python major version of code
PYTHON_MINOR = 6                # Python minor version of code

# Startup Text - 3 groups of text
INFORM_0 = """
The software is provided "As Is", without warranty of any kind, express
or implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement. In no event shall
the authors or copyright holders be liable for any claim, damages or other
liability, whether in an action of contract, tort or otherwise, arising
from, out of or in connection with the software or the use or other dealings
in the software.
"""
INFORM_1 = """
This software development was funded by the Department of Energy, National
Energy Technology Laboratory, an agency of the United States Government,
through a support contract with Battelle Memorial Institute as part of the
Leidos Research Support Team (LRST). Neither the United States Government
nor any agency thereof, nor any of their employees, nor LRST nor any of
their employees, makes any warranty, express or implied, or assumes any
legal liability or responsibility for the accuracy, completeness, or
usefulness of any information, product, software or process disclosed,
or represents that its use would not infringe privately owned rights.
"""
INFORM_2 = """
This software and its documentation were completed as part of National
Risk Assessment Partnership project. Support for this project came from
the U.S. Department of Energy's (DOE) Office of Fossil Energy's
Crosscutting Research program. The technical effort was performed in
support of the DOE National Energy Technology Laboratory's ongoing
research by the Leidos Research Support Team (LRST) under the Research
Support Services contract 89243318CFE000003.
"""
HEADER_0 = (" "*28) + "Warranty"
HEADER_1 = (" "*27) + "Disclaimer"
HEADER_2 = (" "*25) + "Acknowledgment"

HEADER_COPY = "    Copyright(c)2019-2020 by Ernest N. Lindner"
GAP = "  "
LINER = ("-"*60)



def check_python():
    """ Check that user is using Python 3.6.
    Input Variables:
        None
    Return:
        None
    """
    # Get python version of system.
    logging.info('Checking Python Version')
    py_major, py_minor = sys.version_info[0:2]

    # Check if OK - they must use Version 3.6.
    # -> New versions are assumed backwards compatible
    if (py_major != PYTHON_MAJOR) or (py_minor < PYTHON_MINOR):
        msg = ('Python 3.6 is required for this Code!' + ERR_IN
               + 'Version Python {maj}.{mnr} was Detected'.
               format(maj=py_major, mnr=py_minor))
        logging.error(msg)
        terminate_code()

    # return None


def check_file_floats(data_array, data_type, data_bounds):
    """ Check float data from seal file if it is within assumed bounds.
    Input Variables:
        data_array = data to check (NumPy array)
        data_type = data description (str)
        data_bounds = minimum/maximum bounds (dict)
    Return:
        stats = error flag
            =0 no error
            =1 error in data
    """
    # Define error flag.
    stats = 0

    # Check values to be inside min/max.
    for key in data_array:
        if (key < data_bounds[0]) or (key > data_bounds[1]):
            msg = ((data_type + ' from file is outside of defined ' +
                    'bounds with value of {}!').format(key))
            logging.error(msg)
            stats = -1

    return stats


def check_file_values(data_array, data_type, data_bounds):
    """ Check integer data from seal file if it has defined values.
    Input Variables:
        data_array = data to check (NumPy array)
        data_type = data description (str)
        data_bounds = minimum/maximum bounds (dict)
    Return:
        stats = error flag
            =0 no error
            =1 error in data
    """
    # Define error flag.
    stats = 0

    # Check values to either equal to min or max.
    for key in data_array:
        if (int(key) != data_bounds[0]) and (int(key) != data_bounds[1]):
            msg = ((data_type + ' from file is incorrect ' +
                    'with a value of {}!').format(key))
            logging.error(msg)
            stats = -1

    return stats


def opx_problem(reason, err=''):
    """ For operation errors, provides error message and stops.
    Input Variables:
        reason = function error string
        err = OS error message
    Return:
        None
    """
    # Print error message.
    logging.error(reason)

    # Print details only if desired.
    if err != '' and ECHO:
        msg = "OS Report: \n" + ERR_IN + "{0}".format(err)
        logging.error(msg)

    terminate_code()

    # return None


def io_snag(err, subdirectory, file_selected):
    """ For file IO errors, provides short io error message and stops.
    Input Variables:
        err = OS error message
        subdirectory = directory where code was looking
        file_selected = input file
    Return:
        None
    """
    # Show multi-line detailed message with location.
    msg = "OS Error - File Not Found/Accessed!"
    msg += ERR_IN + "Looking for file: '" + file_selected + "'"
    msg += ERR_IN + "Looking in directory: \n" + ERR_IN + subdirectory
    msg += ERR_IN + "Please correct issue and retry."
    logging.error(msg)

    # Print specific details only if desired.
    if err != '' and ECHO:
        msg = "OS Report: \n" + ERR_IN + "{0}".format(err)
        logging.error(msg)

    terminate_code()

    # return None


def io_yml(err):
    """ For file error in YAML file, provides short error message and stops.
    Input Variables:
        err = OS error message
    Return:
        None
    """
    # Show multi-line detailed message if position available.
    if hasattr(err, 'problem_mark'):
        msg = ("Problem While Parsing YAML Control File.")
        mark = err.problem_mark
        msg += (ERR_IN + "Problem location in file: "
                + "line = {0}, column = {1}.".
                format(mark.line+1, mark.column+1))
        msg += (ERR_IN + "Cause: " + str(err.problem) + ' '
                + str(err.context))
        msg += ERR_IN + "Please correct control file and retry."
        logging.error(msg)

    # Otherwise show general message.
    else:
        msg = "Problem While Parsing YAML Control File."
        msg += ERR_IN + "Undefined parse error in file."
        msg += ERR_IN + "Error Report:  {0}".format(err)
        msg += ERR_IN + "Please correct control file and retry."
        logging.error(msg)

    terminate_code()

    # return None


def data_error(reason, subdirectory, file_selected):
    """ For missing or wrong data, provides an error message.
    Input Variables:
        reason = description of issue
        subdirectory = directory where code was looking
        file_selected = input file
    Return:
        None
    """
    # Show multi-line detailed message with location.
    msg = reason
    msg += ERR_IN + "Looking in file: '" + file_selected + "'"
    msg += ERR_IN + "In directory: " + ERR_IN + subdirectory
    msg += ERR_IN + "Please correct issue and retry."
    logging.error(msg)

    terminate_code()

    # return None


def show_warranty():
    """ Write warranty, acknowledgment and disclaimer to console.
    Input Variables:
        None
    Returns:
        None
    """
    # Define lists of text to print and wrapper.
    headers = [HEADER_0, HEADER_1, HEADER_2]
    informers = [INFORM_0, INFORM_1, INFORM_2]
    wrapper = textwrap.TextWrapper(width=TEXT_WIDTH,
                                   fix_sentence_endings=True)

    # Print separator at start.
    print("\n" + GAP + LINER)

    # Print three groups of text.
    for indx in range(3):
        title = headers[indx]
        body = informers[indx]

        # Print header of text.
        print("\n", title, "\n")

        # Clean statement as shown - remove leading space and newlines.
        cleaned = body.replace("\n", " ")
        cleaned = cleaned.strip()

        # Setup text at specified width and print each line.
        word_list = wrapper.wrap(text=cleaned)
        for element in word_list:
            print(GAP + element)

    # Add separator at end of text groups.
    print("\n" + GAP + LINER)

    # Show copyright.
    print(GAP + HEADER_COPY)

    # Print separator at end.
    print(GAP + LINER)

    # Return None


def acquire_yaml_file(yaml_file_selected):
    """ Open text file with YAML options and read data.
    Input Variables:
        yaml_filename = YAML file for seal operations
    Returns:
        docs = YAML file
    """
    # Get full path for startup control file.
    subdirectory = ""
    subdirectory_path, destination = \
        get_path_name(subdirectory, yaml_file_selected, "yaml")

    # Check Open file.
    try:
        stream = open(destination, "r")
    except OSError as err:
        io_snag(err, subdirectory_path, yaml_file_selected)

    # Check get file data and parse.
    try:
        docs = yaml.safe_load(stream)
    except yaml.YAMLError as err:
        io_yml(err)

    return docs


def acquire_data_array(new_sub, file_selected, extent):
    """ Read data from a *.csv file into a NumPy array.
    Input Variables:
        new_sub = subdirectory name where file resides
        file_select = name of selected file
        extent = number of elements expected in array
    Returns:
        out_array = a (flat / 1D) NumPy array
    Note:
        Data file assumes 2 header lines and is 2D!
        Extension assumed to be *.txt
    """
    # Open the current directory and source file for input operations.
    sub_path, source_name = get_path_name(new_sub, file_selected,
                                          EXTENSION_TXT)
    try:
        # Read data as a NumPy array, excluding first two (2) header lines.
        out_array = np.genfromtxt(source_name, delimiter=",",
                                  autostrip=True, skip_header=2)
    except OSError as err:
        # File not found or accessible.
        io_snag(err, sub_path, file_selected)
    except ValueError:
        # Value error.
        data_error("Value Error -> Check number of columns in file!",
                   sub_path, file_selected)
    except EOFError:
        # End of File (EOF) interrupt or no data.
        data_error("EOF Error -> Check Data in File!",
                   sub_path, file_selected)

    # Double check array if present.
    if out_array.size == 0:
        # No data.
        data_error("Data Error - Array size = 0! "
                   + "-> No data found in file.", sub_path, file_selected)

    # Double check if input array has the correct size.
    if extent != out_array.size:
        # Too little data.
        if extent > out_array.size:
            data_error("Data Error - "
                       + "Array size in file is too small!",
                       sub_path, file_selected)
        else:
            data_error("Data Error - "
                       + "Array size in file is too large!",
                       sub_path, file_selected)

    # If no error, flatten array from 2D to 1D - using "C" method.
    out_array = out_array.flatten('C')

    return out_array


def clean_output():
    """  Deletes any *.csv file in destination directory.
    Input Variables:
        None
    Returns:
        None
    """
    # Construct full path from script location to directory file.
    # directory_path = os.getcwd()       # execution directory.
    directory_path = os.path.dirname(os.path.abspath(__file__))
    subdirectory_path = os.path.join(directory_path, OUTDIR_NAME)

    # Create list of all files in output directory.
    old_file_list = os.listdir(subdirectory_path)

    # Delete all *.csv files in output directory.
    for file_name in old_file_list:
        old_file_path = os.path.join(subdirectory_path, file_name)
        if (file_name.endswith(EXTENSION_CSV) or
                file_name.endswith(EXTENSION_NPY)):
            try:
                os.remove(old_file_path)
            except OSError as detail:
                msg = ("File Error While Deleting File" +
                       "- Is File Open?" + LEAD_IN + "FILE: {}".
                       format(file_name))
                opx_problem(msg, detail)

    # return None


def get_path_name(subdirectory, file_selected, extension):
    """ Provides file path.
    Input Variables:
        subdirectory = directory name where code is looking
        file_selected = file name of object to be saved
        file extension for file
    Return:
      subdirectory_path = path to subdirectory (only)
      destination = full path to file
    Note: Assumes destination directory is a subdirectory of executable
    """
    # Define subdirectory path from script file, if defined.
    # directory_path = os.getcwd() # working directory.
    directory_path = os.path.dirname(os.path.abspath(__file__))
    if subdirectory != "":
        subdirectory_path = os.path.join(directory_path, subdirectory)
    else:
        subdirectory_path = directory_path

    # Ensure that extension is in file name.
    if not file_selected.endswith(extension):
        file_selected = file_selected + extension

    # Define destination path with file name.
    destination = os.path.join(subdirectory_path, file_selected)

    return (subdirectory_path, destination)


def find_local_path(file_selected):
    """ Provides file path for a file in source directory.
    Input Variables:
        file_selected
    Return:
      destination = full path to file
    """
    # Define directory path from script file, if defined.
    directory_path = os.path.dirname(os.path.abspath(__file__))

    # Define destination path with file name.
    destination = os.path.join(directory_path, file_selected)

    return destination


def end_message(alone):
    """ Echo message to console on closing program.
    Input Variables:
        ALONE = control parameter to show results to console
    Returns:
        None
    """
    if alone:
        print(LEAD_IN + "SEAL FLUX FINISHED.")
        print(FINAL, end='')  # + pause message

    # return None


def terminate_code():
    """ Echo message on termination of program due to error + stop.
    Input Variables:
        None
    Returns:
        None
    """
    print(LEAD_IN + "TERMINATING PROGRAM DUE TO ERROR.", file=sys.stdout)
    print(FINAL, end='', file=sys.stdout)
    sys.exit(-1)

    # return None


#
##-----------------------------------------------------------------------------
## End of module
