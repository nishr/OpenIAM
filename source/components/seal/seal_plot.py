"""
PYTHON MODULE:
    seal_plot.py
PURPOSE:
    Functions to plot data.
REVISION HISTORY:
    2019-07-17  Created module from seal_display.py.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (20):
    construct_coordinate_data(rows, cols, grid)
    construct_permeability_data(rows, cols, grid)
    test_contour(z_data)
    set_arrays(cols, rows)
    round_exp(exp_value)
    setup_time_history_data(simulation)
    setup_co2_seepage_data(simulation):
    simulation_query(max_realizations, start_value=0)
    create_history_query(query_stage, series)
    create_contour_query(trial)
    range_query(max_realizations)
    control_time_history(max_realizations)
    control_time_series(max_realizations)
    control_perm_contour(x_data, y_data, z_data)
    control_co2_contour(max_realizations, x_data, y_data)
    plot_contour(fig_legend, bar_title, x_data, y_data, z_data)
    plot_bar_chart(rows, cols, z_values)
    plot_time_history(realization, x_data, y_data)
    plot_time_series(x_data, data_list, data_maximum, start_sim)
    plot_manager(seal_controls)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import math                                     # For truncation
import csv                                      # For saving a list of lists
import copy                                     # For array adjustment
import numpy as np                              # For array operations
import matplotlib.pyplot as plt                 # For plots
import matplotlib.cm as cm                      # For color map
from mpl_toolkits.mplot3d import Axes3D         # NOQA: F401 unused import

import seal_file as sfile                       # For paths and error reports

# Constants for file handling
OUT_DIRECTORY = "seal_output"                   # Output directory
TYPE_CSV = ".csv"                               # File Type for  output
TYPE_NPY = ".npy"                               # Compressed file type
SEAL_SIM_NAME = "Seal_Results-Simulation_No._"  # Time history name prefix
CO2_FILE_NAME = "CO2_Grid_Data_for_Sim_"        # CO2 flux name prefix

# Controls and warnings
NEGATIVES = ('n', 'N', 'q', 'Q')                # Keys for negative answer
EXIT_COMMAND = ('x', 'X')                       # Keys that will exit

# General Statements
MAJ_IN = "\n  --> "                             # Page + general message start
LEAD_IN = "      --> "                          # General message start
CMD_IN = "      >>> "                           # Command message start
ERR_IN = "      *** "                           # Error message start
START_TXT = LEAD_IN + "START Realization"       # Multi-history plot
END_TXT = LEAD_IN + "END Realization"           # Multi-history plot
REDO_PLOT = CMD_IN + "Data is non-differentiated! Contour Plot Not Shown!"
CAUTION = CMD_IN + "Hit Exit at Top-Right of Plot Window to Proceed ..."
WAIT_FOR_IT = CMD_IN + "Please wait for plot..."
AGAIN = "\n" + CMD_IN + "Please Try Again."     # Simple repeat request

# Query statements for plotting
CIRC_IN = "      ?-> "
MORE_IN = "\n      ?-> "
FIRST_TRIAL = CIRC_IN + "Create a Single Time-Series Plot? ('Q'=quit): "
SECOND_TRIAL = MORE_IN + "Create Another Single Time Plot? ('Q'=quit): "
SERIES_START = CIRC_IN + "Create a Multiple Time-Series Plot? ('Q'=quit): "
SERIES_END = MORE_IN + "Create Another Multiple Series Plot? ('Q'=quit): "
PLOT_FIRST = CIRC_IN + "Create a CO2 Contour Plot? ('Q'=quit): "
PLOT_SECOND = MORE_IN + "Create Another CO2 Contour Plot? ('Q'=quit): "

# Plot Titles - Time history single & series
LEGEND_MAIN = 'Total CO2 Migration for Realization # '
LEGEND_SERIES = "Total CO2 Migration for a Series of Realizations "
TIME_LABEL = "Time (years)"

# Plot constants for Figures / Contours
HEIGHT = 8                  # Figure height (inches)
WIDTH = 10                  # Figure width (inches)
LEGEND_FONT = 16            # Font size for figure title (pt)
TYP_FONT = 12               # Font size for axes numbers (pt)
N_DIVISIONS = 10            # Subdivisions for contouring
PD_LINEWIDTH = 1            # line width for contour
RESOLVE = 90                # Figure display resolution (dpi)
COLOR_BAR = 'jet'           # Color palette for fill in contouring
C_LINES = 'black'           # Lines color for contours
SHIFT = 20                  # Distance to shift y-label on color bar
BAR_SHIFT_X = 0.75          # Shift of 3D bars along X axis
BAR_SHIFT_Y = 0.50          # Shift of 3D bars along Y axis

# Font definitions
REG_FONT = {'fontname': 'Arial',    # X-axis, Y-axis text
            'color':    'black',
            'size':      TYP_FONT,
           }
TITLE_FONT = {'fontname': 'Arial',
              'color':    'black',
              'size':      LEGEND_FONT,
              'fontweight': 'bold'
             }
LEG_FONT = {'fontname': 'Arial',
            'color':    'blue',
            'size':      LEGEND_FONT,
           }
BAR_FONT = {'fontname': 'Arial',
            'color':    'black',
            'size':      TYP_FONT,
           }



def construct_coordinate_data(rows, cols, grid):
    """ Structure x/y coordinate data into NumPy arrays.
    Input Variables:
        rows = rows in plot
        cols = columns in plot
        grid = a collection of cells
    Returns:
        x_data, y_data = coordinate arrays for cell centers
    """
    # Establish x/y data for contour plots.
    x_data = np.zeros((rows, cols))
    y_data = np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            indx = j + (i * cols)
            x_data[i, j] = grid[indx].x_center
            y_data[i, j] = grid[indx].y_center

    return x_data, y_data


def construct_permeability_data(rows, cols, grid):
    """ Structure permeability data into NumPy array.
    Input Variables:
        rows = rows in plot
        cols = columns in plot
        grid = a collection of cells
    Returns:
        z_data = NumPy array - data to plot
    """
    # Create permeability data for plot.
    z_data = np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            indx = j + (i * cols)
            # report only permeability for active cells.
            if grid[indx].cell.status > 0:
                z_data[i, j] = grid[indx].permeability
            else:
                z_data[i, j] = 0.0

    return z_data


def test_contour(z_data):
    """ Examine data to prevent poor contour plot.
    Input Variables:
        z_data = NumPy array - data to plot
    Returns:
        answer = (Boolean) True = data OK
    """
    # Examine if spread in data - check min/max.
    answer = True
    z_min = np.min(z_data)
    z_max = np.max(z_data)
    if z_max == z_min:
        # No contour possible!
        print(" >>> Uniform Value Across Grid of " + str(z_max)
              + ".")
        print(REDO_PLOT)
        answer = False

    return answer


def set_arrays(cols, rows):
    """ Setup coordinates for 3D bar graph.
    Input Variables:
        rows = rows in plot
        cols = columns in plot
    Returns:
        x_loca = flat array of x coordinates
        y_loca = flat array of y coordinates
    """
    # create axes.
    x_indices = np.arange(cols)
    y_indices = np.arange(rows)

    # Create coordinate data for plot - shift for better image.
    x_coord, y_coord = np.meshgrid(BAR_SHIFT_X + x_indices,
                                   BAR_SHIFT_Y + y_indices)

    # fatten to 1D arrays.
    x_loca = x_coord.ravel()
    y_loca = y_coord.ravel()

    return (x_loca, y_loca)


def round_exp(exp_value):
    """ Round-up exponential to desired degree.
    Input Variables:
        exp_value = general float value
    Returns:
        target = rounded exponential
    """
    # Trap error if = 0.0.
    if exp_value != 0.0:
        # Make the number a decimal.
        core = abs(exp_value)
        level = math.trunc(math.log10(core))
        base = math.pow(10.0, level)
        root_exp = math.ceil(exp_value / base)

        # Round and then reconstitute the number.
        # adjust_number = round(root_exp, 1)
        target = root_exp * base  # round to zero decimal
    else:
        target = exp_value

    return target


def setup_time_history_data(simulation):
    """ Get time history data from file.
    Input Variables:
        simulation = simulation number to plot - integer
    Returns:
        leakage_data = NumPy array of CO2 data
        time_data = NumPy array of time values
    """
    # Construct path name to file.
    sim_number = str(simulation)
    file_name = SEAL_SIM_NAME + sim_number
    subdirectory_path, destination = sfile.get_path_name(OUT_DIRECTORY,
                                                         file_name, TYPE_CSV)
    try:
        with open(destination, 'r') as csvfile:
            csv_reader = csv.reader(csvfile)

            # Skip 2 header lines.
            header = next(csv_reader, None)
            next(csv_reader)

            # Do not read file without a header / empty file.
            if header is None:
                # No data.
                sfile.data_error("Data Error - Array Header is None! "
                                 + "-> No data found in file.",
                                 subdirectory_path, file_name)
            else:
                # Create data array - iterate over each row after the header.
                data_list = []
                for row in csv_reader:
                    data_list.append(row)

            ## DEBUG: Print rows in list
            #for row in data_list:
            #    print(', '.join(row))

        # Convert list to NumPy array.
        npa = np.asarray(data_list, dtype=np.float)

        # Slice array for data.
        time_data = npa[:, 1]
        leakage_data = npa[:, 2]

    except OSError as err:
        sfile.io_snag(err, subdirectory_path, file_name)

    return (leakage_data, time_data)


def setup_co2_seepage_data(simulation):
    """ Get co2 seepage data from NPY file.
    Input Variables:
        simulation = simulation number to plot - integer
    Returns:
        leakage_data = NumPy array of CO2 data
    """
    # Construct path name to file.
    sim_number = str(simulation)
    file_name = CO2_FILE_NAME + sim_number
    subdirectory_path, destination = sfile.get_path_name(OUT_DIRECTORY,
                                                         file_name, TYPE_NPY)
    try:
        # Load NumPy file and return.
        leakage_data = np.load(destination)

    except OSError as err:
        sfile.io_snag(err, subdirectory_path, file_name)

    return leakage_data


def simulation_query(max_realizations, start_value=0):
    """ Get simulation number to plot.
    Input Variables:
        max_realizations = maximum number of realizations
        start_value = minimum number of realizations
    Returns:
        simulation = simulation number to plot - integer
    """
    # Default values.
    repeat_loop = True
    response = False
    realization = -1

    # Loop to get an appropriate response.
    while repeat_loop:
        try:
            # Get input with prompt.
            code = input(CIRC_IN + "Enter Realization Number "
                         + "to Plot ('Q'=quit): ")

            # Check if user wishes to quit.
            if code in NEGATIVES:
                print(CIRC_IN + "Exiting Plot Option. \n")
                repeat_loop = False
                break
        except ValueError:
            # Parse fails.
            print(ERR_IN + "Invalid Number.  " +  AGAIN)
            continue

        # Check if "entered" is within correct range.
        entered = int(code)
        if entered > max_realizations:
            print(ERR_IN + "You typed = {}".format(entered))
            print(ERR_IN + "This Number Exceeds the Maximum "
                  + " of {}!".format(max_realizations), end='')
            print(AGAIN)
        elif entered < 0:
            print(ERR_IN + "You typed = {}".format(entered))
            print(ERR_IN + "The Input is Less than Zero!", end='')
            print(AGAIN)
        elif entered < start_value:
            print(ERR_IN + "You typed = {}".format(entered))
            print(ERR_IN + "The Input is Less Than Starting Value" +
                  " of {}!".format(start_value), end='')
            print(AGAIN)
        else:
            print(CIRC_IN + "Simulation Selected = {}".format(entered))
            realization = entered
            response = True
            repeat_loop = False
            break
    # end while

    return (response, realization)


def create_history_query(query_stage, series):
    """ Check if user wants to plot a time series.
    Input Variables:
        query_stage = question queue
                0 = first time
                1 = plot again?
        Series = Type of plot
                True = series of time plot
                False = single time history
    Returns:
        reply = answer plot question
    """
    # Change message depending on place in query queue.
    if series:
        if query_stage == 0:
            code = input(SERIES_START)
        else:
            code = input(SERIES_END)
    else:
        if query_stage == 0:
            code = input(FIRST_TRIAL)
        else:
            code = input(SECOND_TRIAL)

    # Get response.
    if (code in NEGATIVES) or (code in EXIT_COMMAND):
        reply = False
    else:
        reply = True

    return reply


def create_contour_query(trial):
    """ Check if user wants to plot a time series.
    Input Variables:
        trial = question queue
                0 = first time
                1 = plot again (more plots?)
    Returns:
        reply = answer plot question
    """
    # Ask question based on sequence.
    if trial == 0:
        code = input(PLOT_FIRST)
    else:
        code = input(PLOT_SECOND)

    # Set reply from question - default to yes.
    if (code in NEGATIVES) or (code in EXIT_COMMAND):
        reply = False
    else:
        reply = True

    return reply


def range_query(max_realizations):
    """ Get simulation number to plot.
    Input Variables:
        max_realizations = maximum number of realizations
    Returns:
        response = to plot or not - Boolean
        min_simulation = minimum simulation to plot - integer
        max_simulation = maximum simulation to plot - integer
    """
    # Default values.
    response = False
    start_run = 0
    end_run = 0

    # Get start simulation for start of run with prompt.
    print(START_TXT + "\n", end="")

    # Get simulation number.
    starter = 0
    response, start_run = simulation_query(max_realizations, starter)

    # If OK, get end of range of simulations.
    if response:
        print(END_TXT + "\n", end="")
        starter = start_run
        response, end_run = simulation_query(max_realizations, starter)

    return (response, start_run, end_run)


def control_time_history(max_realizations):
    """ Overall control to plot a simulation from output.
    Input Variables:
        (None)
    Returns:
        (None)
    """
    # Loop until finished plotting.
    while True:
        # Query for simulation number.
        response, realization = simulation_query(max_realizations, 0)

        # Plot - if user still desires a plot.
        if response:
            # Get data & plot series.
            leakage_data, time_data = \
                setup_time_history_data(realization)
            plot_time_history(realization, time_data, leakage_data)

            # Ask about another plot; exit if not.
            plot_again = create_history_query(1, False)
            if not plot_again:
                break
        else:
            break
    # end while

    # return None


def control_time_series(max_realizations):
    """ Overall control to plot a series of simulation from output.
    Input Variables:
        max_realizations = upper limit of simulations from analysis
    Returns:
        (None)
    """
    # Loop until finished plotting.
    while True:
        # Query to get simulation numbers (start and end).
        response, start_run, end_run = range_query(max_realizations)

        # Plot - if user desires a plot.
        if response:

            # Get data and store NumPy arrays in list.
            #   -- adjust counter to catch last run.
            data_list = []
            final_max = 0.0
            for realization in range(start_run, (end_run + 1)):
                leakage_data, time_data = setup_time_history_data(realization)
                data_list.append(leakage_data)
                instance_max = np.amax(leakage_data)
                if instance_max > final_max:
                    final_max = instance_max

            # Plot data arrays on same plot diagram.
            plot_time_series(time_data, data_list, final_max, start_run)

            # Ask about another plot; exit if not.
            plot_again = create_history_query(1, True)
            if not plot_again:
                break
        else:
            break
    # end while

    # return None


def control_perm_contour(x_data, y_data, z_data):
    """ Control plot of permeability contour figure.
    Input Variables:
        fig_legend = legend of plot
        bar_title = title of scale bar
        x_data, y_data = arrays of coordinate values
        z_data = NumPy array - data to plot
    Returns:
        None
    """
    # Examine if spread in data - if OK then plot.
    variable = test_contour(z_data)
    if variable:
        print(WAIT_FOR_IT)
        fig_legend = "Seal Permeability"
        bar_title = "Permeability in mD"
        plot_contour(fig_legend, bar_title, x_data, y_data, z_data)

    # return None


def control_co2_contour(max_realizations, x_data, y_data):
    """ Overall control to plot a simulation from output.
    Input Variables:
        x_data, y_data = arrays of coordinate values
    Returns:
        (None)
    """
    # Loop until finished plotting.
    while True:
        # Query for simulation number.
        response, simulation = simulation_query(max_realizations, 0)

        # Plot - if user still desires a plot.
        if response:
            # Get data.
            leakage_data = setup_co2_seepage_data(simulation)

            # Create contour plot - if data is variable!
            variable = test_contour(leakage_data)
            if variable:
                print(WAIT_FOR_IT)
                fig_legend = "Seal Seepage"
                bar_title = "Seepage in tonnes"
                plot_contour(fig_legend, bar_title, x_data, y_data,
                             leakage_data)

            # Ask about another plot; exit if not.
            plot_again = create_contour_query(1)
            if not plot_again:
                break
        else:
            break
    # end while

    # return None


def plot_contour(fig_legend, bar_title, x_data, y_data, z_data):
    """ Contour data using matplotlib functions.
    Input Variables:
        fig_legend = legend of plot
        bar_title = title of scale bar
        x_data, y_data = NumPy arrays of coordinate values
        z_data = NumPy array - data to plot
    Returns:
        None
    """
    # Define window size for plot (inches).
    plt.figure(figsize=(WIDTH, HEIGHT), dpi=RESOLVE,
               num='Seal_Flux Contour Plot')

    # Get exponent value for z data. - Don't change source!
    z_max = np.max(z_data)
    cite = math.trunc(math.log10(abs(z_max)))
    zed_data = copy.deepcopy(z_data)
    zed_data /= pow(10.0, cite)

    # Plot contours for both line and fill controls.
    contour_x = plt.contour(x_data, y_data, zed_data, N_DIVISIONS,
                            colors=C_LINES, linewidths=PD_LINEWIDTH)
    contour_x = plt.contourf(x_data, y_data, zed_data, N_DIVISIONS,
                             cmap=COLOR_BAR)

    # Draw legend and axis labels - X/Y = areal basis.
    plt.title(fig_legend, fontdict=TITLE_FONT)
    plt.xlabel('X Axis (m)', fontdict=REG_FONT)
    plt.ylabel('Y Axis (m)', fontdict=REG_FONT)

    # Format and plot color bar.
    clb = plt.colorbar(contour_x, shrink=0.8, extend='both',
                       orientation='vertical')
    clb.ax.set_ylabel(bar_title, BAR_FONT, rotation=270,
                      labelpad=SHIFT)

    # Clear and reset "title" of color bar for clarity.
    #   - Use LaTex format $__$ to get superscript for format.
    #   - Used old string format option due to issues with {}.
    clb.ax.yaxis.get_offset_text().set_visible(False)
    show_units = r'$\times$10$^{%d}$'%(cite)           # <== LaTex
    clb.ax.text(-0.25, 1, show_units, va='bottom', ha='left')

    # Show figure; then pause and close.
    print(CAUTION)
    plt.show()

    # return None


def plot_bar_chart(rows, cols, z_values):
    """ Plot 3D Bar chart.
    Input Variables:
       rows = rows in plot
       cols = columns in plot
       z_values = z data in plot - 1D array
    Returns:
       --
    """
    # Notify user to wait.
    print(WAIT_FOR_IT)

    # setup the figure and axes.
    fig = plt.figure(figsize=(WIDTH, HEIGHT), dpi=RESOLVE,
                     num='Seal_Flux Bar Chart')
    ax1 = fig.add_subplot(111, projection='3d')

    # Create coordinate data for plot - shift for better image.
    x_loca, y_loca = set_arrays(cols, rows)

    # Setup for plot.
    width = 1.0
    depth = 1.0
    base = np.zeros_like(z_values)

    # Normalize the colors based on Z value.
    # norm = plt.Normalize(z_values.min(), z_values.max())
    # colors = cm.rainbow(norm(z_values))

    # Define a range of colors
    cmap = cm.get_cmap('Reds')
    max_height = np.max(z_values)
    min_height = np.min(z_values)
    colors = [cmap((k-min_height)/max_height) for k in z_values]

    # Plot data.
    ax1.bar3d(x_loca, y_loca, base, width, depth, z_values, shade=True,
              color=colors)

    # Draw legend and axis labels.
    ax1.set_title('Seal Permeability', loc='left', fontdict=TITLE_FONT)
    ax1.set_xlabel('Column Number', fontdict=REG_FONT)
    ax1.set_ylabel('Row Number', fontdict=REG_FONT)
    ax1.set_zlabel('Permeability in mD', fontdict=REG_FONT)

    # Rotate view.
    # for angle in range(0, 360):
    #     real_angle = 5 * angle + 240
    #     ax1.view_init(30, real_angle)
    #     plt.draw()
    #     plt.pause(.0001)

    print(CAUTION)
    plt.show()

    # return None


def plot_time_history(realization, x_data, y_data):
    """ Plot a leakage time history (results).
    Input Variables:
        realization = realization number selected for plot
        x_data = x-axis data --> time
        y_data = y-axis data --> leakage
    Returns:
        None
    Notes:
        Uses matplotlib functions to plot single realization
        Uses LaTex to format text
    """
    # Define legend text with Realization number.
    fig_legend = (LEGEND_MAIN + str(realization))

   # Establish window size for plotting in inches.
    plt.figure(figsize=(WIDTH, HEIGHT), dpi=RESOLVE,
               num='Seal_Flux History')

    # Get exponent for y-axis and normalize data - keep source!
    y_min = 0.0
    y_max = round_exp(np.max(y_data))
    cite = math.trunc(math.log10(abs(y_max)))
    yed_data = copy.deepcopy(y_data)
    yed_data /= pow(10.0, cite)
    y_max /= pow(10.0, cite)

    # Plot x/y data.
    plt.plot(x_data, yed_data, linestyle='solid', color='blue',
             linewidth=1.5, marker='o', markersize=6)

    # Set axes limits to limit white space along axes.
    x_min_plot = math.floor(np.min(x_data))
    x_max_plot = math.ceil(np.max(x_data))
    plt.xlim(x_min_plot, x_max_plot)
    plt.ylim(y_min, y_max)

    # Get exponent value for y data and place in axis title.
    pivot = plt.gca()
    pivot.yaxis.offsetText.set_visible(False)

    # Construct plot title and provide axis labels and grid.
    new_label = r'Leakage ($\times$10$^{%d}$ tonnes)'%(cite)
    plt.title(fig_legend, fontdict=LEG_FONT)
    plt.xlabel(TIME_LABEL, fontdict=REG_FONT)
    plt.ylabel(new_label, fontdict=REG_FONT)
    plt.grid(which='major', linewidth='0.5')

    # Show figure; then pause and close.
    #   Note: WINDOW BUG: interactive window and plot window
    #                     are not be active together.
    print(CAUTION)
    # plt.ion()       # Allow interactive mode for follow-up questions
    plt.show()
    # plt.waitforbuttonpress(0) # this will wait for indefinite time

    # return None


def plot_time_series(x_data, data_list, data_maximum, start_sim):
    """ Plot a series of leakage time histories (results).
    Input Variables:
        x_data = x-axis data --> time data - assumed same for all
        data_list = a list of NumPy arrays of leakage
        data_maximum = maximum of <all> y data
        start_sim = number of first simulation for plotting
    Returns:
        None
    Notes:
        Uses matplotlib functions to plot single realization
        Uses LaTex to format text
    """
   # Establish window size for plotting in inches.
    plt.figure(figsize=(WIDTH, HEIGHT), dpi=RESOLVE,
               num='Seal_Flux Simulations')

    # Get exponent for y-axis - to normalize data.
    y_max = round_exp(data_maximum)
    cite = math.trunc(math.log10(abs(y_max)))
    y_max /= pow(10.0, cite)

    # Plot each "normalized" data set as line with label.
    for sim, results_array in enumerate(data_list):
        describe = "Simulation #" + str(sim + start_sim)
        y_data = results_array / pow(10.0, cite)
        plt.plot(x_data, y_data, linestyle='solid', linewidth=1.0,
                 label=describe)

    # Set axes limits to limit white space along axes.
    x_min_plot = math.floor(np.min(x_data))
    x_max_plot = math.ceil(np.max(x_data))
    plt.xlim(x_min_plot, x_max_plot)
    plt.ylim(0.0, y_max)

    # Hide axis title.
    pivot = plt.gca()
    pivot.yaxis.offsetText.set_visible(False)

    # Construct plot title and provide axis labels and grid.
    new_label = r'Leakage ($\times$10$^{%d}$ tonnes)'%(cite)
    plt.title(LEGEND_SERIES, fontdict=LEG_FONT)
    plt.xlabel('Time (years)', fontdict=REG_FONT)
    plt.ylabel(new_label, fontdict=REG_FONT)
    plt.grid(which='major', linewidth='0.5')

    # Plot key at upper left.
    plt.legend(loc=2, shadow=True, fancybox=True)

    # Show figure; then pause and close.
    #   Note: WINDOW BUG: interactive window and plot window
    #                     are not be active together.
    print(CAUTION)
    # plt.ion()       # Allow interactive mode for follow-up questions
    plt.show()
    # plt.waitforbuttonpress(0) # this will wait for indefinite time

    # return None


def plot_manager(alive, seal_controls, grid, rows, cols):
    """ Controls plots produced by program.
    Input Variables:
        alive = status of program; alive = True if stand-alone
        seal_controls = dictionary of seal controls
        grid = collection of cells
        rows = number of rows of cells
        cols = number of columns of cells
    Returns:
        None
    """
    # Check if in stand-alone operation mode.
    if alive:
        # Print header, if plotting desired.
        check_plot = [seal_controls['plot_permeability'],
                      seal_controls['plot_time_history'],
                      seal_controls['plot_co2_contour']]
        if True in check_plot:
            print(MAJ_IN + "SELECTED SEAL PLOT OPTIONS.")

        # Limit selection of plot numbers in query.
        max_plots = (seal_controls['realizations'] - 1)

        # -----------------------------------------------------
        ## Permeability plot option.
        if seal_controls['plot_permeability']:

            # Get coordinate data and permeabilities (if alive)
            x_data, y_data = construct_coordinate_data(rows, cols, grid)
            z_data = construct_permeability_data(rows, cols, grid)

            # Print wait, then plot contour of last permeability.
            print(MAJ_IN + "SEAL PERMEABILITY CONTOUR PLOT.")
            control_perm_contour(x_data, y_data, z_data)

            # Plot 3-D Bar Chart of last permeability.
            print(MAJ_IN + "SEAL PERMEABILITY 3D BAR PLOT.")
            z_values = z_data.ravel() # 1D array needed
            plot_bar_chart(rows, cols, z_values)

        # -----------------------------------------------------
        # Time history plot options.
        if seal_controls['plot_time_history']:
            print(MAJ_IN + "SEAL PLOT ONE TIME-HISTORY.")

            # Plot single time-history, if desired.
            initial_query = 0
            response = create_history_query(initial_query, False)
            if response:
                control_time_history(max_plots)

            # Plot a series of time-histories, if desired.
            print(MAJ_IN + "SEAL MULTIPLE TIME-SERIES PLOTS.")
            response = create_history_query(initial_query, True)
            if response:
                control_time_series(max_plots)

        # -----------------------------------------------------
        # CO2 contour plot options.
        if seal_controls['plot_co2_contour']:
            print(MAJ_IN + "SEAL CO2 FLUX CONTOUR PLOT.")

            # Plot co2 contour of a simulation, if desired.
            initial_query = 0
            response = create_contour_query(initial_query)
            if response:
                x_data, y_data = construct_coordinate_data(rows, cols, grid)
                control_co2_contour(max_plots, x_data, y_data)

    # return None


#
##-----------------------------------------------------------------------------
## End of module
