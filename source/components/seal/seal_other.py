"""
PYTHON MODULE:
    seal_other.py
PURPOSE:
    Functions to compute seal thickness, layout, etc.
REVISION HISTORY:
    2019-04-24  Started work on this module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (13):
    thickness_variability(seal_controls, rows, cols)
    filter_thickness_data(rough_array, rows, cols)
    create_time_values(seal_controls)
    establish_simulation_list(sim_step)
    update_simulation_list(sim_step, period, co2_results, brine_results,
                           sim_listing)
    refresh_data_arrays(seal_controls, sim_step, grid, press_top)
    create_sim_storage(rows, cols)
    create_reservoir_storage(rows, cols)
    clear_cycle_storage(co2_flow, brine_flow, one_dimension)
    clear_storage(co2_flow, brine_flow, simulation_list)
    print_data_arrays(grid_table, num_x_cells, num_y_cells, select, outlook)
    print_tabular_data(title, data_list, cols)
    debug_check(checkup, grid, cols, rows)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import sys                              # For standard output
import random                           # For variability in thickness values
from scipy.stats import truncnorm       # For stochastic thickness generation
import scipy.ndimage as ndimage         # For smoothing thickness data
import numpy as np                      # For arrays & input

import seal_file as sfile               # For file operations
import seal_units as sunit              # For unit conversion

# Constants for Reservoir input - *.txt files & debugging
SUBDIR_RESERVOIR = "reservoir_input"    # Injection directory
TIME_STEP_NAME = "time_points.txt"      # Time steps file name
EXTENSION_TXT = ".txt"                  # Extension
SUBDIR_OUT = "seal_output"              # Output directory
DEBUG_FILE = "debug_results"            # Summary file name

# Other operations constants
MUSE = 1.0                              # Level of Gaussian smoothing
CONFIG = 11.3                           # Output format for debugging

# Debug constants
# Note: For large grids, set DEBUG_TO_FILE to "True" as console cannot process
#       larger files and print to console shows only portion of arrays.
DEBUG_TO_FILE = True                    # Control to print Debug to file
OPTIONS = ["PERMEABILITY", "THICKNESS", "INFLUENCE", "AREA", "DEPTH",
           "STATUS", "X-COORDINATES", "Y-COORDINATES", "ENTRY PRESSURE",
           "HISTORY"]     # Debugging printout headers



def thickness_variability(seal_controls, rows, cols):
    """ Compute an array of thickness using a truncated normal distribution.
    Input Variables:
        seal_controls = input dictionary
        rows = number of rows in return array
        cols = number of columns in return array
    Return:
        out_array = thickness array (1D)
    """
    # Define variables and computation array.
    mean_val = seal_controls['thickness_ave']
    sigma = seal_controls['thickness_std']
    min_val = seal_controls['thickness_min']
    max_val = seal_controls['thickness_max']
    big_array = np.zeros((rows, cols))      # 2D array
    out_array = np.zeros((rows * cols))     # 1D (flat) array

    # If variability is small, do not vary thickness!
    if sigma > 0.01:
        # Define limits for truncnorm.
        lower_bound = (min_val - mean_val) / sigma
        upper_bound = (max_val - mean_val) / sigma

        # Create Seed.
        random.seed()  # no repeatability

        # compute array from random distribution.
        big_array = truncnorm.rvs(lower_bound, upper_bound,
                                  loc=mean_val, scale=sigma,
                                  size=[rows, cols])
        # Smooth data in array.
        big_array = filter_thickness_data(big_array, rows, cols, min_val)

    else:
        # For low variability, set all to mean & no smoothing.
        big_array.fill(mean_val)

    # Flatten array from 2D to 1D - using "C" method.
    out_array = big_array.flatten('C')

    # Return flat (1D) array.
    return out_array


def filter_thickness_data(rough_array, rows, cols, minimum):
    """ <Smooth> the random independent values of thickness.
   Input Variables:
        rough_array = independent random thickness array values
        rows = number of rows in return array
        cols = number of columns in return array
        minimum = minimum thickness
    Return:
        smooth_array = smoothed array
    Note:
        No check on maximum as smoothing reduces peak values.
    """
    # define output array and standard deviation of current array.
    smooth_array = np.zeros((rows, cols))
    old_dev = np.std(rough_array)

    # Define gauss filter parameters - isotropic assumption.
    sigma_y = MUSE
    sigma_x = MUSE
    sigma = [sigma_y, sigma_x]

    # Apply filter – “reflect” values at boundary edge.
    smooth_array = ndimage.filters.gaussian_filter(rough_array, sigma,
                                                   mode='reflect')

    # Determine stochastic variables of new array.
    center = np.average(smooth_array)
    new_dev = np.std(smooth_array)

    # Restore standard deviation of sample to create smooth array.
    #   -> Do not change for low deviation values.
    if new_dev > 0.001:
        ratio_dev = old_dev / new_dev
        for i in range(rows):
            for j in range(cols):
                current = smooth_array[i, j]
                smooth_array[i, j] = ((current - center) * ratio_dev
                                      + center)

    # Ensure that all values are greater than the minimum.
    smooth_array[smooth_array < minimum] = minimum

    return smooth_array


def create_time_values(seal_controls):
    """ create a set of time steps for the analysis.
   Input Variables:
        seal_controls = dictionary of seal control values
    Return:
        time_steps = NumPy array of float values (in yrs!)
    Note: Time steps include starting time of zero!
    """
    # Define major variables (in years)!
    numbr_steps = seal_controls['time_points']
    time_steps = np.zeros(seal_controls['time_points'])

    # Examine file input options.
    if seal_controls['time_input']:
        # Open the current directory and source file for input operations.
        sub_path, source_name = sfile.get_path_name(SUBDIR_RESERVOIR,
                                                    TIME_STEP_NAME,
                                                    EXTENSION_TXT)
        try:
            # Read data as a NumPy array, excluding first header line.
            time_steps = np.genfromtxt(source_name, delimiter=",",
                                       autostrip=True, skip_header=1)
        except OSError as err:
            sfile.io_snag(err, sub_path, source_name)

        # Check for error in number of steps.
        if len(time_steps) != numbr_steps:
            msg1 = "Failure in Time Series - Wrong Number of Points!"
            sfile.opx_problem(msg1)

        # Correct data if in days.
        # time_steps *= sunit.days_to_yrs()

    else:
        # Define variables.
        start = seal_controls['start_time']
        end = seal_controls['end_time']

        # Create a series of uniform time steps.
        interval = (end - start) / (numbr_steps - 1)
        for i in range(0, numbr_steps):
            time_steps[i] = (i * interval)

    return time_steps


def establish_simulation_list(sim_step):
    """ Create a list for accumulating results of a simulation.
   Input Variables:
        sim_step = simulation/realization step number
    Return:
        sim_listing = with header information
    """
    # Define header for list.
    sim_listing = []

    # Append first null step.
    new_list = [sim_step, "0.00", "0.00", "0.00"]
    sim_listing.append(new_list)

    # Return new list.
    return  sim_listing


def update_simulation_list(sim_step, period, co2_results, brine_results,
                           sim_listing):
    """ Append data to list of results from a simulation.
   Input Variables:
        sim_step = simulation/realization number
        period = time value of step
        co2_results = current CO2 cumulative flow
        brine_results = current brine cumulative flow
        sim_listing = accumulation list of results for simulation
    Return:
        sim_listing = accumulation list of results for simulation
    """
    # Define data.
    track = str(sim_step)
    clock = str(period)
    co2_value = str(co2_results)
    brine_value = str(brine_results)

    # Create new list and append.
    new_list = [track, clock, co2_value, brine_value]
    sim_listing.append(new_list)

    # Return new list.
    return  sim_listing


def refresh_data_arrays(seal_controls, sim_step, grid, press_top):
    """ Refresh thickness and pressure arrays as needed.
   Input Variables:
        seal_controls = dictionary of seal control values
        sim_step = simulation/realization number
        grid = collection of cells
        press_top = pressure at top of cells
    Return:
        press_top = (original or updated)
    Note:
        -- Do not update on last simulation step - set Boolean.
        -- Do not adjust thickness, if it is from file.
        -- Pressure not changed, if thickness not changed.
    """
    # Set criterion for last step.
    step_allow = (sim_step < (seal_controls['realizations'] - 1))

    # Re-evaluate thickness if not from file and not on last step.
    if not seal_controls['thickness_approach'] and step_allow:
        rows = seal_controls['rows']
        cols = seal_controls['cols']

        # Get new stochastic thickness.
        new_thickness_array = np.zeros((rows * cols))
        new_thickness_array = \
            thickness_variability(seal_controls, rows, cols)

        # Re-evaluate pressure.
        if not seal_controls['upper_approach']:
            #  Update thickness and adjust pressure - if press. not from file.
            for numbr, cell in enumerate(grid):

                # Update cell thickness - save old value, set new.
                old_thickness = cell.thickness
                cell.thickness = new_thickness_array[numbr]

                # Compute changes relative to original cell data.
                depth_change = old_thickness - cell.thickness
                pressure_change = (cell.brineDensity * depth_change
                                   * sunit.gravity())

                # Update both pressure and current depth of cell.
                press_top[numbr] += pressure_change
                cell.depth += depth_change

        else:
            # Pressure from file - update thickness values only.
            for numbr, cell in enumerate(grid):
                cell.thickness = new_thickness_array[numbr]

    # else: No thickness or pressure change

    return press_top


def create_sim_storage(rows, cols):
    """ Create NumPy arrays storage arrays for loops.
   Input Variables:
        rows = number of rows in storage
        cols = number of columns in storage
    Return:
        sim_co2_flow = CO2 flow for simulation
        sim_brine_flow = brine flow for simulation
        co2_flow_step = CO2 flow for time step
        brine_flow_step = brine flow for time step

    """
    # Create 2D storage arrays for a realization.
    sim_co2_flow = np.zeros((rows, cols))
    sim_brine_flow = np.zeros((rows, cols))

    # Create 1D storage arrays for each computation of delta time.
    co2_flow_step = np.zeros((rows * cols))
    brine_flow_step = np.zeros((rows * cols))

    return (sim_co2_flow, sim_brine_flow, co2_flow_step,
            brine_flow_step)


def create_reservoir_storage(rows, cols):
    """ Create NumPy arrays for input from reservoir.
   Input Variables:
        rows = number of rows in storage
        cols = number of columns in storage
    Return:
        base_co2_saturation = base CO2 saturation from input
        base_co2_pressure = base CO2 pressure from input
    """
    # Create reservoir arrays.
    base_co2_saturation = np.zeros((rows, cols))
    base_co2_pressure = np.zeros((rows, cols))

    return (base_co2_saturation, base_co2_pressure)


def clear_cycle_storage(co2_flow, brine_flow, one_dimension):
    """ Set arrays to 1D and clear arrays for new loop.
   Input Variables:
        co2_flow = CO2 flow - NumPy array
        brine_flow = brine flow - NumPy array
        one_dimension = rows * cols
    Return:
        co2_flow = zeroed
        brine_flow = zeroed
    """
    brine_flow.shape = (one_dimension)
    co2_flow.shape = (one_dimension)
    co2_flow.fill(0.0)
    brine_flow.fill(0.0)

    return (co2_flow, brine_flow)


def clear_storage(co2_flow, brine_flow, simulation_list):
    """ Clear storage arrays for loop.
   Input Variables:
        co2_flow = CO2 flow - NumPy array
        brine_flow = brine flow - NumPy array
        simulation_list = data list
    Return:
        co2_flow = zeroed
        brine_flow = zeroed
        simulation_list = clear
    """
    co2_flow.fill(0.0)
    brine_flow.fill(0.0)
    simulation_list.clear()

    return (co2_flow, brine_flow, simulation_list)


def print_data_arrays(grid_table, rows, cols, print_list, outlook):
    """ Print data values in column/row format.
    Input Variables:
        grid_table = list of cells
        rows = number of rows in return array
        cols = number of columns in return array
        print_list  = list of selections to print:
            = 0: permeability
            = 1: thickness
            = 2: influence
            = 3: area
            = 4: depth
            = 5: status
            = 6: x-coordinate
            = 7: y-coordinate
            = 8: entry pressure
            = 9: history
        outlook = file destination
    Returns:
        None
    Note:
        Change "n_vals" for fewer columns in a line.
        Permeability set later than other input!
    """
    # If list exists, print each item in list.
    if print_list:
        for select in print_list:
            # Print header and setup for printout.
            print("\n  " + OPTIONS[select], file=outlook)
            n_vals = cols - 1   # => number of columns to print

            #  Print table in row/column format.
            for i in range(rows):  # y dimension
                print("   ", end="", file=outlook)
                for j in range(cols):  # x dimension
                    stat_cell = j + (i * cols)

                    # Select print results as defined.
                    valu = grid_table[stat_cell].get_value(select)

                    # print result on line.
                    if select == 5:    # Status values are integers
                        print(" {:2d}".format(valu), end="", file=outlook)
                    else:
                        print(" {:7.3e}".format(valu), end="", file=outlook)

                    # check for new row.
                    if j % n_vals == 0 and j > 0:
                        print("", file=outlook)
                        print("", end="", file=outlook)

    # return None


def print_tabular_data(title, data_list, cols):
    """ Print data values in column/row format.
    Input Variables:
        title = description of data
        data_list = list of values
        cols = number of columns for printing
    Returns:
        None
    Note:
        Spacing => CONFIG = 11.3 (see above).
        See response at:
        https://stackoverflow.com/questions/9535954/printing-lists-as-tabular-data
    """
    # Compute the number of rows and the "remainder" columns on last line.
    n_rows, remainder = divmod(len(data_list), cols)

    # Setup format and controls.
    pattern = '{{:{}e}}'.format(CONFIG)
    typ_line = '\n'.join(pattern * cols for _ in range(n_rows))
    last_line = pattern * remainder

    # Print data with formatting.
    print("  " + title)
    print(typ_line.format(*data_list))
    print(last_line.format(*data_list[n_rows*cols:]))

    # return None


def debug_check(checkup, grid, rows, cols):
    """ Print out arrays for debugging/checking.
    Input Variables:
        checkup = flag to print list for debugging (set in seal_flux)
        seal_params = input parameters of seal component (dictionary)
        rows = rows of cells (Y cells)
        cols = columns of cells (X cells)
    Return:
        None
    Note:
        Printout per code in "print_list."
        -> Code: perm.=0; thickness=1; influence=2; area=3; depth=4;
                 status=5; x-coordinates=6; y-coordinates=7;
                 entry pressure=8; history=9
        -> Permeability not input before computation loop start!
        See seal_model function: "get_value" for correlation in code.
    """
    # Print cell data, if desired.
    if checkup:
        # Define lists/parameters to be printed
        print_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]  # See "seal_model"

        # Print to file, if desired.
        if DEBUG_TO_FILE:
            # For file output, get path.
            sub_path, destination = sfile.get_path_name(SUBDIR_OUT, DEBUG_FILE,
                                                        EXTENSION_TXT)

            # Write information to file, "outlook".
            try:
                with open(destination, 'w') as outlook:
                    # Print selected arrays.
                    print_data_arrays(grid, rows, cols, print_list, outlook)
            except OSError as err:
                sfile.io_snag(err, sub_path, DEBUG_FILE)

        else:
            # Print to console.
            outlook = sys.stdout
            print_data_arrays(grid, rows, cols, print_list, outlook)

        # Clean for other uses.
        print_list.clear()

    ## Print properties of a cell.
    # marker = 12
    # grid[marker].print_cell()  # ==> Commented out

    # return None


#
##-----------------------------------------------------------------------------
## End of module
