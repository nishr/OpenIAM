"""
PYTHON MODULE:
    seal_intro.py
PURPOSE:
    Functions for getting YAML file data and checking/sorting.
REVISION HISTORY:
    2020-01-14  Created module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (17):
    convert_permeability(docs, seal_controls)
    convert_two_phase(docs, seal_controls)
    convert_time_params(docs, seal_controls)
    convert_thickness(docs, seal_controls)
    convert_yaml_parameters(docs, seal_controls)
    acquire_seal_parameters(yaml_file_selected, seal_controls)
    check_blanks(seal_params)
    define_let_limits(param_bounds)
    define_input_limits()
    check_input_parameters(seal_params, param_bounds)
    check_input_functions(key, val, stats, error_msg)
    cross_check_input(seal_params)
    preliminaries(alone, yaml_file)
    input_coordinates(grid, param_bounds)
    convert_lognorm_terms(mean_val, std_dev)
    create_layout(seal_controls, grid)
    define_finish(seal_controls, grid, param_bounds)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import math                     # For Tanh + other functions
import logging                  # For reporting errors
import time                     # For calculating runtime

import seal_file as sfile       # For file operations
import seal_model as mod        # For Class definitions
import seal_display as dis      # Save summary data and create plots

# I/O constants
SUBDIR_NAME = "seal_input"      # seal input subdirectory
X_COORD_NAME = "seal_X_coord.txt"   # File name for x-coordinates
Y_COORD_NAME = "seal_Y_coord.txt"   # File name for y-coordinates

# Other constants
CODE_VERSION = "1.4"            # Seal Flux version
PY_VERSION = "3.7"              # Python version used for this code
ECHO = False                    # Control to print for debugging



def convert_permeability(docs, seal_controls):
    """ Convert YAML permeability parameters into seal controls.
    Input Variables:
        docs = YAML array input
        seal_controls = control parameter dictionary
    Returns:
        seal_controls = control parameters (modified)
    """
    # Set permeability parameters if permeability <not> from file.
    if not seal_controls['perm_input_approach']:
        seal_controls['perm_mean'] = docs['Permeability']['avePermeability']
        seal_controls['perm_std'] = docs['Permeability']['stdDevPermeability']
        seal_controls['perm_min'] = docs['Permeability']['minPermeability']
        seal_controls['perm_max'] = docs['Permeability']['maxPermeability']
        seal_controls['heterogeneity_approach'] = \
            docs['Permeability']['heterogeneityApproach']
        seal_controls['perm_heter_factor'] = docs['Permeability']['heterFactor']

        # Internal control on stochastic permeability - False = no variability.
        if ((seal_controls['perm_std'] < 0.0001) or
                (seal_controls['perm_input_approach'])):
            seal_controls['vary_perm_choice'] = False
        else:
            seal_controls['vary_perm_choice'] = True
    else:
        # For file input, heterogeneity is not considered.
        seal_controls['heterogeneity_approach'] = False

    return seal_controls


def convert_two_phase(docs, seal_controls):
    """ Convert YAML two_phase parameters into seal controls.
    Input Variables:
        docs = YAML array input
        seal_controls = control parameter dictionary
    Returns:
        seal_controls = control parameters (modified)
    """
    # Set two_phase parameters depending on model.
    if 'BC' in seal_controls['relative_model']:
        # BrooksCoreyModel:
        #   -> Avoid using lambda in Python source -> use zeta.
        seal_controls['zeta'] = docs['BrooksCoreyModel']['lambda']

    else:
        # Set LETModel parameters.
        seal_controls['l_wetting'] = docs['LETModel']['wetting1']
        seal_controls['e_wetting'] = docs['LETModel']['wetting2']
        seal_controls['t_wetting'] = docs['LETModel']['wetting3']
        seal_controls['l_nonwet'] = docs['LETModel']['nonwet1']
        seal_controls['e_nonwet'] = docs['LETModel']['nonwet2']
        seal_controls['t_nonwet'] = docs['LETModel']['nonwet3']

        # Set LET CapillaryPressure:
        seal_controls['l_capillary'] = docs['LETCapillaryModel']['capillary1']
        seal_controls['e_capillary'] = docs['LETCapillaryModel']['capillary2']
        seal_controls['t_capillary'] = docs['LETCapillaryModel']['capillary3']
        seal_controls['max_capillary'] = \
            docs['LETCapillaryModel']['maxCapillary']

    return seal_controls


def convert_time_params(docs, seal_controls):
    """ Convert YAML model parameters into seal controls.
    Input Variables:
        docs = YAML array input
        seal_controls = control parameter dictionary
    Returns:
        seal_controls = control parameters (modified)
    """
    # Determine model type.
    seal_controls['model'] = docs['TimeModel']['influenceModel']

    # Set model parameters if model =1 or =2.
    if seal_controls['model'] > 0:
        seal_controls['rate_effect'] = docs['TimeModel']['rateEffect']
        seal_controls['total_effect'] = docs['TimeModel']['totalEffect']
        seal_controls['reactivity'] = docs['TimeModel']['reactivity']
        seal_controls['clay_content'] = docs['TimeModel']['clayContent']
        seal_controls['carbonate_content'] = \
            docs['TimeModel']['carbonateContent']
        seal_controls['clay_type'] = docs['TimeModel']['clayType']

    return seal_controls


def convert_thickness(docs, seal_controls):
    """ Convert YAML thickness parameters into seal controls.
    Input Variables:
        docs = YAML array input
        seal_controls = control parameter dictionary
    Returns:
        seal_controls = control parameters (modified)
    """
    # Set thickness parameters if thickness <not> from file.
    if not seal_controls['thickness_approach']:
        seal_controls['thickness_ave'] = docs['Thickness']['aveThickness']
        seal_controls['thickness_std'] = docs['Thickness']['stdDevThickness']
        seal_controls['thickness_min'] = docs['Thickness']['minThickness']
        seal_controls['thickness_max'] = docs['Thickness']['maxThickness']

    return seal_controls


def convert_yaml_parameters(docs, seal_controls):
    """ Convert YAML parameters into seal controls (1 of 2).
    Input Variables:
        docs = YAML array input
        seal_controls = control parameter dictionary
    Returns:
        stream = YAML file
    Note:
        Parameters that not needed are not set and therefore are not checked.
    """
    # ModelParams:
    seal_controls['title'] = docs['ModelParams']['runTitle']
    seal_controls['start_time'] = docs['ModelParams']['startTime']
    seal_controls['end_time'] = docs['ModelParams']['endTime']
    seal_controls['time_points'] = int(docs['ModelParams']['timePoints'])
    seal_controls['realizations'] = int(docs['ModelParams']['realizations'])
    seal_controls['time_input'] = docs['ModelParams']['timeInput']

    # Description:
    seal_controls['rows'] = int(docs['Description']['gridRows'])
    seal_controls['cols'] = int(docs['Description']['gridColumns'])
    seal_controls['num_cells'] = int(docs['Description']['numCells'])
    seal_controls['cell_height'] = docs['Description']['cellHeight']
    seal_controls['cell_width'] = docs['Description']['cellWidth']
    seal_controls['area'] = docs['Description']['area']
    seal_controls['salinity'] = docs['Description']['salinity']
    seal_controls['temperature'] = docs['Description']['aveTemperature']
    seal_controls['base_depth'] = docs['Description']['aveBaseDepth']
    seal_controls['base_pressure'] = docs['Description']['aveBasePressure']

    # Conditions-reference:
    seal_controls['ref_depth'] = docs['Conditions']['staticDepth']
    seal_controls['ref_pressure'] = docs['Conditions']['staticPressure']

    # Conditions-fluid properties:
    seal_controls['co2_density'] = docs['Conditions']['CO2Density']
    seal_controls['co2_viscosity'] = docs['Conditions']['CO2Viscosity']
    seal_controls['brine_density'] = docs['Conditions']['brineDensity']
    seal_controls['brine_viscosity'] = docs['Conditions']['brineViscosity']
    seal_controls['co2_solubility'] = docs['Conditions']['CO2Solubility']

    # Controls:
    seal_controls['depth_approach'] = docs['Controls']['depthApproach']
    seal_controls['layout_approach'] = docs['Controls']['layoutApproach']
    seal_controls['area_approach'] = docs['Controls']['areaApproach']
    seal_controls['upper_approach'] = docs['Controls']['upperBoundApproach']
    seal_controls['active_approach'] = docs['Controls']['activeCellsApproach']
    seal_controls['entry_approach'] = docs['Controls']['entryApproach']
    seal_controls['perm_input_approach'] = docs['Controls']['permApproach']
    seal_controls['thickness_approach'] = \
        docs['Controls']['thicknessApproach']
    seal_controls['correlate_entry_approach'] = \
        docs['Controls']['correlateApproach']
    seal_controls['interpolate_approach'] = \
        docs['Controls']['interpolateApproach']
    seal_controls['initialize'] = docs['Controls']['initializeApproach']

   # Permeability variability parameters:
    convert_permeability(docs, seal_controls)  # Optional parameters.

    # RelativeFlowLimits:
    seal_controls['resid_brine'] = \
        docs['RelativeFlowLimits']['brineResSaturation']
    seal_controls['resid_co2'] = \
        docs['RelativeFlowLimits']['CO2ResSaturation']
    seal_controls['relative_model'] = \
        docs['RelativeFlowLimits']['relativeModel']
    seal_controls['perm_ratio'] = \
        docs['RelativeFlowLimits']['permRatio']

    # CapillaryPressure:
    seal_controls['entry_pressure'] = \
        docs['CapillaryPressure']['entryPressure']

    # Two-phase model controls, depending on model selection:
    convert_two_phase(docs, seal_controls)  # Optional parameters.

    # TimeModel controls, if model > 0:
    convert_time_params(docs, seal_controls) # Optional parameters.

    # Thickness variability parameters:
    convert_thickness(docs, seal_controls) # Optional parameters.

    # SealPlots:
    seal_controls['plot_permeability'] = docs['SealPlots']['permeabilityPlot']
    seal_controls['plot_time_history'] = docs['SealPlots']['timeSeriesPlot']
    seal_controls['plot_co2_contour'] = docs['SealPlots']['CO2ContourPlot']

    return seal_controls


def acquire_seal_parameters(yaml_file_selected, seal_controls):
    """ Obtain seal parameters from a formatted YAML file.
    Input Variables:
        yaml_file_selected = file name of YAML file in same directory
        seal_controls = dictionary of seal controls (empty)
    Returns:
        seal_controls = dictionary of seal parameters - with values
    """
    ## Get YAML data from file.
    docs = sfile.acquire_yaml_file(yaml_file_selected)

    # Load parameters into seal_controls dictionary.
    try:
        convert_yaml_parameters(docs, seal_controls)
    except KeyError as errd:
        cause = errd.args[0]
        msg = ("Parameter <{}> is missing from YAML file!".format(cause))
        sfile.opx_problem(msg  + "\n      " +
                          "- KeyError in defining Seal_Controls Dictionary!")

    return seal_controls


def check_blanks(seal_params):
    """ Check whether if any input parameter is blank (= None).
    Input Variables:
        seal_params = input parameters of seal component model (dictionary)
    Return:
        None
    Note:
        > "logging" module still uses string format - added statements
            to format string messages prior to logging.

    Error messages -> explicitly format string outside of <logging>
                       as <logging> still uses string format!
    """
    # Define text and error flag.
    txt_blank = '> Parameter <{}> is None!'
    err_flag = 0

    # Check all values for None.
    for key, val in seal_params.items():
        if val is None:
            msg = txt_blank.format(key)
            logging.error(msg)
            err_flag = -1

    # Report error if found. Missing data is Fatal!
    if err_flag == -1:
        sfile.opx_problem("Input Parameter(s) are Missing!")

    # return None


def define_let_limits(param_bounds):
    """ Define numeric limits/ranges of LET model parameters.
    Input Variables:
        pars_bounds = dictionary with min/max indices
    Return:
        param_bounds = updated dictionary
    """
    # Two-phase model parameters for L-E-T model.
    param_bounds['l_wetting'] = [0.5, 5.0]
    param_bounds['e_wetting'] = [0.1, 30.0]
    param_bounds['t_wetting'] = [0.0, 3.0]
    param_bounds['l_nonwet'] = [0.5, 5.0]
    param_bounds['e_nonwet'] = [0.1, 30.0]
    param_bounds['t_nonwet'] = [0.0, 3.0]

    # Other parameters for two-phase L-E-T model.
    param_bounds['l_capillary'] = [0.01, 5.0]
    param_bounds['e_capillary'] = [0.01, 30.0]
    param_bounds['t_capillary'] = [0.01, 3.0]
    param_bounds['max_capillary'] = [1.0E+02, 2.0E+08]

    return param_bounds


def define_input_limits():
    """ Define numeric limits/ranges of seal input parameters.
    Input Variables:
        None
    Return:
        param_bounds = dictionary with min/max indices
    """
    # Define dictionary of input bounds.
    param_bounds = dict()

    # Controls (sec).
    param_bounds['start_time'] = [0.0, 5.0E+3]
    param_bounds['end_time'] = [1.0E+1, 1.0E+5]
    param_bounds['time_points'] = [1, 50]
    param_bounds['realizations'] = [1, 10000]

    # Permeability (in microdarcys!).
    param_bounds['perm_mean'] = [1.0E-04, 1.0E+02]
    param_bounds['perm_std'] = [0.0, 1.0E+01]
    param_bounds['perm_min'] = [1.0E-06, 1.0E+01]
    param_bounds['perm_max'] = [1.0E-03, 1.0E+08]  # max = 100 darcys
    param_bounds['perm_heter_factor'] = [1.0E-02, 1.0E+02]

    # Geometry (m/m2/Pa).
    param_bounds['rows'] = [1, 1000]
    param_bounds['cols'] = [1, 1000]
    param_bounds['num_cells'] = [1, 1000000]
    param_bounds['cell_height'] = [1.0, 1.0E+04]
    param_bounds['cell_width'] = [1.0, 1.0E+04]
    param_bounds['area'] = [1.0, 2.6E+05]
    param_bounds['ref_depth'] = [0.8E+02, 9.5E+03]
    param_bounds['ref_pressure'] = [1.0E+06, 6.0E+07]
    param_bounds['base_depth'] = [0.8E+02, 9.5E+03]
    param_bounds['base_pressure'] = [1.0E+06, 6.0E+07]

    # Fluid parameters.
    param_bounds['temperature'] = [3.1E+01, 1.8E+02]
    param_bounds['salinity'] = [0.0, 8.0E+04]
    param_bounds['brine_density'] = [8.8E+02, 1.08E+03]
    param_bounds['brine_viscosity'] = [1.5E-04, 1.6E-03]
    param_bounds['co2_density'] = [9.3E+01, 1.05E+03]
    param_bounds['co2_viscosity'] = [1.8E-05, 1.4E-04]
    param_bounds['co2_solubility'] = [0.0, 2.0]

    # Relative permeability limits.
    param_bounds['resid_brine'] = [0.01, 0.35]
    param_bounds['resid_co2'] = [0.00, 0.35]
    param_bounds['perm_ratio'] = [0.00, 1.5]
    param_bounds['entry_pressure'] = [1.0E+02, 2.0E+06]

    # Two-phase model parameters for BC model.
    param_bounds['zeta'] = [0.00, 5.0]

    # Time-model parameters.
    param_bounds['influence'] = [0, 1.0]
    param_bounds['total_effect'] = [0.01, 200.0]
    param_bounds['rate_effect'] = [0.01, 0.65]
    param_bounds['reactivity'] = [0.0, 10.0]
    param_bounds['clay_content'] = [0.0, 100.0]
    param_bounds['carbonate_content'] = [0.0, 100.0]

    # Thickness.
    param_bounds['thickness_ave'] = [10.0, 1000.0]
    param_bounds['thickness_std'] = [0.00, 500.0]
    param_bounds['thickness_min'] = [5.0, 100.0]
    param_bounds['thickness_max'] = [10.0, 1000.0]

    # Define LET Parameters.
    param_bounds = define_let_limits(param_bounds)

    # Repository Input.
    param_bounds['reservoir_pressure'] = [1.0E+00, 1.0E+08]
    param_bounds['reservoir_saturation'] = [0.0, 1.0]
    param_bounds['coordinates'] = [-1.0E+05, 1.0E+05]

    return param_bounds


def check_input_parameters(seal_params, param_bounds):
    """ Check whether input parameters fall within specified limits.
    Input Variables:
        seal_params = input parameters of seal component model (dictionary)
        param_bounds = bounds on seal parameters (w. 2 values)
    Return:
        stats = error code, default = 0, if warning = -1
    Note:
        > "logging" module still uses string format - added statements
            to format string messages prior to logging.
        > Some parameters are checked using a defined list.
    """
    # Define parameter for checking.
    stats = 0

    # Combine True/False parameters into single list.
    true_false_parameters = ['approach', 'initialize', 'plot',
                             'time_input', 'choice']
    truth_list = [True, False]

    # Combine function types into a single list.
    function_parameters = ['clay_type', 'model', 'relative_model', 'title']

    # Error messages -> explicitly format string outside of <logging>
    #                   as <logging> still uses string format!
    txt_lost = '> Parameter <{}> not found!'
    txt_wrong = ('Parameter <{}> is not defined correctly '
                 + 'and has a value of {}!')
    txt_outside = ('> Parameter <{}> is outside of the recommended '
                   + 'bounds with a value of {}!')
    txt_not_seal = ('> Parameter <{}> not recognized as a '
                    + 'Seal input parameter.')

    # Check if values are within defined range.
    for key, val in seal_params.items():

        # Check title.
        if 'title' in key:
            if val == '':
                msg = txt_lost.format(key)
                logging.error(msg)
                stats = -1

        # Check functions lists have correct input.
        elif key in function_parameters:
            stats = check_input_functions(key, val, stats, txt_wrong)

        # Check True/False controls.
        elif [ele for ele in true_false_parameters if ele in key]:
            if val not in truth_list:
                msg = txt_wrong.format(key, val)
                logging.error(msg)
                stats = -1

        # Check numeric values to be within bounds.
        elif key in param_bounds:
            if ((val < param_bounds[key][0])
                    or (val > param_bounds[key][1])):
                msg = txt_outside.format(key, val)
                logging.error(msg)
                stats = -1

        else:
            # Missing parameter in seal_controls.
            msg = txt_not_seal.format(key)
            logging.error(msg)
            stats = -1

    return stats


def check_input_functions(key, val, stats, error_msg):
    """ Check whether input parameters have specified function values.
    Input Variables:
        key = input parameter title from dictionary
        val = item in parameter list
        stats = current error status
        error_msg = error message to output if error
    Return:
        function_stats = error code, if warning set to (-1)
    """
    # Define parameter lists for checking.
    clay_list = ["smectite", "illite", "chlorite"]
    model_list = [0, 1, 2]
    relative_list = ["LET", "BC"]

    # Check depending on list type.
    # -----------------------------
    function_stats = stats

    # Clay types.
    if key == 'clay_type':
        if val not in clay_list:
            msg = error_msg.format(key, val)
            logging.error(msg)
            function_stats = -1

    # Model types.
    elif key == 'model':
        if val not in model_list:
            msg = error_msg.format(key, val)
            logging.error(msg)
            function_stats = -1

    # Relative permeability types.
    else:
        if key == 'relative_model':
            if val not in relative_list:
                msg = error_msg.format(key, val)
                logging.error(msg)
                function_stats = -1

    return function_stats


def cross_check_input(seal_params):
    """ Check if input is numerically logical for permeability and
        thickness.
    Input Variables:
        seal_params = input parameters of seal component (dictionary)
    Return:
        stats = error code, default = 0, if warning = -1
    Note:
        "logging" module still uses string format -
            format string messages prior to calling <logging>.
    """
    # Define message strings.
    txt_cross = ('> Minimum {} exceeds maximum {}! ')
    txt_outside = ('> Mean {} is outside min./max. bounds! ')

    # Set default return.
    stats = 0

    # Run check only if no file input for permeability.
    if not seal_params['perm_input_approach']:

        # Run checks on min and max values and mean.
        if seal_params['perm_min'] > seal_params['perm_max']:
            causitive = "permeability"
            msg = txt_cross.format(causitive, causitive)
            logging.error(msg)
            stats = -1
        if (seal_params['perm_mean'] > seal_params['perm_max'] or
                seal_params['perm_mean'] < seal_params['perm_min']):
            causitive = "permeability"
            msg = txt_outside.format(causitive)
            logging.error(msg)
            stats = -1

    # Run check only if no file input for thickness.
    if not seal_params['thickness_approach']:

        # Run checks on thickness values.
        if seal_params['thickness_min'] > seal_params['thickness_max']:
            causitive = "thickness"
            msg = txt_cross.format(causitive, causitive)
            logging.error(msg)
            stats = -1
        if (seal_params['thickness_ave'] > seal_params['thickness_max'] or
                seal_params['thickness_ave'] < seal_params['thickness_min']):
            causitive = "thickness"
            msg = txt_outside.format(causitive)
            logging.error(msg)
            stats = -1

    return stats


def preliminaries(alone, yaml_file):
    """ Open control file and check data.
    Input Variables:
      alone = status of code operation; True = if stand-alone
      yaml_file = name of input file
    Return:
      seal_controls = input parameters (dictionary)
      param_bounds = parameter bounds (dictionary)
    """
    # Start clock.
    timer = time.monotonic()

    # Show header statements on console.
    if alone:
        print("\n  *** SEAL FLUX MODEL - Version: " + CODE_VERSION +
              " ***" + "          Python" + PY_VERSION)
        sfile.show_warranty()
        dis.echo_status(alone, 0)

    #Check Python
    sfile.check_python()

    # Start-up - input control parameters from text file.
    seal_controls = {}
    seal_controls = acquire_seal_parameters(yaml_file, seal_controls)

    if alone:
        dis.echo_status(alone, 1)

    # Wipe output directory to remove old output files.
    sfile.clean_output()

    # Check for any missing data.
    check_blanks(seal_controls)

    # Identify input bounds and check values.
    param_bounds = dict()
    param_bounds = define_input_limits()
    status = check_input_parameters(seal_controls, param_bounds)
    status2 = cross_check_input(seal_controls)

    # Consider all warnings fatal! - and exit program.
    if status < 0 or status2 < 0:
        sfile.opx_problem("Input Parameter(s) Outside of Range or Wrong!")

    # Save code start in seal_controls for use later.
    seal_controls['code_start'] = timer

    return seal_controls, param_bounds


def input_coordinates(seal_controls, grid, param_bounds):
    """ Input x and y coordinate data from file.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = list of cells
        param_bounds = parameter bounds (dictionary)
    Return:
        Nonecell.set_coord
    """
    # Check if input is desired.
    if seal_controls['layout_approach']:

        # Write status of input process.
        if ECHO:
            print("   >> Input from files: '" + X_COORD_NAME + "' and '"
                  + Y_COORD_NAME + "' ")

        # Get data arrays - ".txt" added by acquire_data_array.
        x_data_array = sfile.acquire_data_array(SUBDIR_NAME, X_COORD_NAME,
                                                len(grid))
        y_data_array = sfile.acquire_data_array(SUBDIR_NAME, Y_COORD_NAME,
                                                len(grid))

        # Check if numeric values are within typical limits.
        stat1 = 0
        stat2 = 0
        val_limits = param_bounds['coordinates']
        stat1 = sfile.check_file_floats(x_data_array, "X-Coordinate value",
                                        val_limits)
        stat2 = sfile.check_file_floats(y_data_array, "Y-Coordinate value",
                                        val_limits)
        if (stat1 < 0) or (stat2 < 0):
            sfile.opx_problem("File value(s) outside defined bounds " +
                              "caused program to exit!")

        # Update cell data.
        for numbr, cell in enumerate(grid):
            cell.set_coord(x_data_array[numbr], y_data_array[numbr])

    # return None


def convert_lognorm_terms(mean_val, std_dev):
    """ Convert mean/std_dev into log-normal parameters for NumPy.
    Input Variables:
        mean_val = mean permeability in microdarcys
        std_dev = std deviation of permeability
    Return:
        log_mu = log-normal mean
        sigma = log-normal variance
    Note:
        see https://en.wikipedia.org/wiki/Log-normal_distribution
    """
    # define common terms.
    variance = math.pow(std_dev, 2)
    mean_2 = math.pow(mean_val, 2)
    inside = 1.0 + (variance / mean_2)

    # Compute location - mean.
    term = math.sqrt(inside)
    log_mu = math.log(mean_val / term)

    # Compute sigma - variance.
    term = math.log(inside)
    sigma = math.sqrt(term)

    return log_mu, sigma


def create_layout(seal_controls, grid):
    """ create grid (list of cells) with default values & coordinates.
   Input Variables:
        seal_controls = dictionary of seal control values
        grid = list of cells (empty)
    Return:
        grid = with new cells - center coordinates
    Note: For simple setup, rectangular shape is assumed;
          centers are  1/2 of relevant dimension
    """
    # Define variables for clarity.
    rows = seal_controls['rows']
    cols = seal_controls['cols']
    high = seal_controls['cell_height']
    wide = seal_controls['cell_width']

    # Create a grid of uniform cells with defined center offsets.
    #    NOTE: Y-coordinates are from top! ((data read in from top-down).
    upper = (high * rows) - (high * 0.5)
    side_shift = (wide * 0.5)
    for i in range(rows):
        for j in range(cols):
            y_center = upper - (i * high)
            x_center = (j * wide) + side_shift
            grid.append(mod.Cell(x_center, y_center))

    # Return cells with average coordinate values.
    return grid


def define_finish(seal_controls, grid, param_bounds):
    """ Finish input operations by defining values.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = list of cells
        param_bounds = parameter bounds (dictionary)
    Return:
        seal_controls = (revised)
    """
    # Create a grid of cells.
    create_layout(seal_controls, grid)

    # Assign class variables to Cell Class.
    mod.Cell.assign_controls(seal_controls)

    # Define coordinates of grid from file if desired.
    input_coordinates(seal_controls, grid, param_bounds)

    # Define log-normal parameters to compute total permeabilities for grid.
    if not seal_controls['perm_input_approach']:
        seal_controls['perm_location'], seal_controls['perm_scale'] = \
            convert_lognorm_terms(seal_controls['perm_mean'],
                                  seal_controls['perm_std'])
    else:
        seal_controls['perm_location'] = 0.0
        seal_controls['perm_scale'] = 0.0

    # Define time-dependent permeability model and create list for results.
    mod.Cell.influenceModel = seal_controls['model']

    # Define variables for main line..
    rows = seal_controls['rows']
    cols = seal_controls['cols']

    # Set control for reading permeabilities from file.
    #  - Note:  In reading permeabilities from file, only one set of data is
    #           expected; therefore, the following control limits reading of
    #           input file only once.
    seal_controls['read_perm'] = True

    return (grid, rows, cols, seal_controls)


#
##-----------------------------------------------------------------------------
## End of module
