"""
PYTHON MODULE:
    seal_upload.py
PURPOSE:
    Functions to define data from files or using default parameters.
REVISION HISTORY:
    2020-01-14  Started work on this module (Created from other modules).
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (11):
    define_file_limits(param_bounds)
    define_influence_factor(seal_controls, grid, file_limits)
    define_thickness(seal_controls, grid, file_limits)
    define_active(seal_controls, grid, file_limits)
    define_area(seal_controls, grid, file_limits)
    define_depth(seal_controls, grid, file_limits)
    define_entry(seal_controls, grid, file_limits)
    input_various_data(input_controls, grid, param_bounds)
    define_top_press_array(seal_controls, grid, press_top, param_bounds)
    get_data_lines(destination, file_location, rows, cols, skip_lines)
    input_reservoir_data(sequence, rows, cols, param_bounds)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
from itertools import repeat        # For line input
import numpy as np                  # For arrays & input

import seal_file as sfile           # For file operations
import seal_other as sot            # Functions for thickness and depth

# Directory and filenames for seal file input - *.txt files.
SUBDIR_INPUT = "seal_input"         # Defined seal input subdirectory
EXTENSION_TXT = ".txt"              # Extension for all seal input files
THICKNESS_NAME = "seal_thickness.txt"   # Thickness input file
BASE_NAME = "seal_base_depth.txt"       # Depth input file
ACTIVE_NAME = "seal_active.txt"         # Status input file
AREA_NAME = "seal_area.txt"             # Area input file
INFLUENCE_NAME = "seal_influence.txt"   # Inflience input file
PRESS_NAME = "seal_top_pressure.txt"    # Top boundary pressure input
ENTRY_NAME = "seal_entry_pressure.txt"  # Threshold pressure input file

# Directory and filenames for reservoir input - *.txt files.
SUBDIR_RESERVOIR = "reservoir_input"        # reservoir input subdirectory
SATURATION_NAME = "lookup_reservoir_co2_saturation.txt"  # Base saturation
PRESSURE_NAME = "lookup_reservoir_co2_pressure.txt"      # Base pressure

# Other constants
ECHO = False                        # DEBUG printout - status of file input



def define_file_limits(param_bounds):
    """ Define numeric ranges of file parameters.
    Input Variables:
        param_bounds = min/max bounds for Yaml file (dict)
    Return:
        param_bounds2 = min/max values for file input (dict)
    Note:
        Function modifies prior dictionary bounds for use with files.
    """
    # Define dictionary of input bounds.
    param_bounds2 = dict()

    # Define influence factor limits.
    param_bounds2['influence'] = [0.0, 1.0]

    # Define area limits.
    param_bounds2['area'] = [param_bounds['area'][0],
                             param_bounds['area'][1]]

    # Define thickness limits.
    param_bounds2['thickness'] = [param_bounds['thickness_min'][0],
                                  param_bounds['thickness_max'][1]]

    # Define active cell limits.
    param_bounds2['status'] = [0, 1]

    # Define entry pressure limits.
    param_bounds2['entry'] = [param_bounds['entry_pressure'][0],
                              param_bounds['entry_pressure'][1]]

    # Define depth to top of cell limits.
    param_bounds2['top_depth'] = [param_bounds['ref_depth'][0],
                                  param_bounds['ref_depth'][1]]

    return param_bounds2


def define_influence_factor(seal_controls, grid, file_limits):
    """ Define influence model values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = list of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        stats = error flag
    """
    if seal_controls['initialize']:
        # Echo operation to user.
        if ECHO:
            print("   >> Input from file: '" + INFLUENCE_NAME + "'")

        # Get data array - ".txt" added by acquire_data_array.
        data_array = sfile.acquire_data_array(SUBDIR_INPUT, INFLUENCE_NAME,
                                              len(grid))

        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['influence']
        stats = sfile.check_file_floats(data_array, "Influence Factor value",
                                        val_limits)

        # Update influence factor for each cell.
        for numbr, cell in enumerate(grid):
            cell.influence = data_array[numbr]

    else:
        # Set default influence factor for each cell.
        stats = 0
        for cell in grid:
            cell.influence = 1.0

    return stats


def define_thickness(seal_controls, grid, file_limits):
    """ Define thickness values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        stats = error flag
    """
    # Define computation array.
    rows = seal_controls['rows']
    cols = seal_controls['cols']
    new_thickness_array = np.zeros((rows, cols))

    if seal_controls['thickness_approach']:
        # echo operation to user.
        if ECHO:
            print("  --> Input from file: '" + THICKNESS_NAME + "'")

        # Define data array from file.
        new_thickness_array = sfile.acquire_data_array(SUBDIR_INPUT,
                                                       THICKNESS_NAME,
                                                       len(grid))

        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['thickness']
        stats = sfile.check_file_floats(new_thickness_array, "Thickness value",
                                        val_limits)
    else:
        # Generate thickness data array based on stochastic values (1D array)
        stats = 0
        new_thickness_array = sot.thickness_variability(seal_controls,
                                                        rows, cols)

    # Set cell thickness value of grid from 1D array.
    for numbr, cell in enumerate(grid):
        cell.thickness = new_thickness_array[numbr]

    return stats


def define_active(seal_controls, grid, file_limits):
    """ Define active status values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        stats = error flag
    """
    if seal_controls['active_approach']:
        # echo operation to user.
        if ECHO:
            print("  --> Input from file: '" + ACTIVE_NAME + "'")

        # Get data array from file.
        data_array = sfile.acquire_data_array(SUBDIR_INPUT, ACTIVE_NAME,
                                              len(grid))

        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['status']
        stats = sfile.check_file_values(data_array, "Cell Status value",
                                        val_limits)

        # Update cell values
        for numbr, cell in enumerate(grid):
            cell.status = int(data_array[numbr])

    else:
        # Set cells active if not input from file.
        for cell in grid:
            cell.status = int(1)
        stats = 0

    return stats


def define_area(seal_controls, grid, file_limits):
    """ Define area values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        stats = error flag
    """
    if seal_controls['area_approach']:
        # echo operation to user.
        if ECHO:
            print("  --> Input from file: '" + AREA_NAME + "'")

        # Get data array from file.
        data_array = sfile.acquire_data_array(SUBDIR_INPUT, AREA_NAME,
                                              len(grid))

        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['area']
        stats = sfile.check_file_floats(data_array, "Area value",
                                        val_limits)

        # Update cell values.
        for numbr, cell in enumerate(grid):
            cell.area = data_array[numbr]

    else:
        # Set cells to default area parameter.
        stats = 0
        for cell in grid:
            cell.area = seal_controls['area']

    return stats


def define_depth(seal_controls, grid, file_limits):
    """ Define depth values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        stats = error flag
    """
    if seal_controls['depth_approach']:
        # echo operation to user.
        if ECHO:
            print("  --> Input from file: '" + BASE_NAME + "'")

        # Get data array from file.
        data_array = sfile.acquire_data_array(SUBDIR_INPUT, BASE_NAME,
                                              len(grid))

        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['top_depth']
        stats = sfile.check_file_floats(data_array, "Depth value",
                                        val_limits)

        # Update cell values.
        for numbr, cell in enumerate(grid):
            cell.set_depth(data_array[numbr])

    else:
        # Set cells to default using base depth parameter for calculation.
        stats = 0
        for cell in grid:
            if cell.status > 0:
                cell.set_depth(seal_controls['base_depth'])

    return stats


def define_entry(seal_controls, grid, file_limits):
    """ Define threshold pressure values for each cell.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        file_limits = minimum / maximum bounds (dict)
    Return:
        None
    """
    if seal_controls['entry_approach']:
        # Get data array from file.
        if ECHO:
            print("  --> Input from file: '" + ENTRY_NAME + "'")
        data_array = sfile.acquire_data_array(SUBDIR_INPUT, ENTRY_NAME,
                                              len(grid))


        # Check if numeric values are within limits.
        stats = 0
        val_limits = file_limits['entry']
        stats = sfile.check_file_floats(data_array, "Entry pressure value",
                                        val_limits)

        # Update cell values.
        for numbr, cell in enumerate(grid):
            cell.entryPressure = data_array[numbr]

    else:
        # Set cells to default entry pressure parameter.
        stats = 0
        for cell in grid:
            cell.entryPressure = seal_controls['entry_pressure']

    return stats


def input_various_data(seal_controls, grid, param_bounds):
    """ Control file to get input vales for various data types.
    Input Variables:
        seal_controls = seal controls (dictionary)
        grid = list of cells
        param_bounds = seal parameter bounds (dictionary)
    Return:
        None
    Note:
        input_controls: index for cell aspects - all are *.txt files
    """
    # Set error flag and define limits.
    err_flag = 0
    file_limits = dict()
    file_limits = define_file_limits(param_bounds)

    # 1. Initialize influence factor, as desired.
    result = define_influence_factor(seal_controls, grid, file_limits)
    err_flag += result

    # 2. Define thickness of grid.
    result = define_thickness(seal_controls, grid, file_limits)
    err_flag += result

    # 3. Define active cells in grid; default is active.
    result = define_active(seal_controls, grid, file_limits)
    err_flag += result

    # 4. Define area of grid.
    result = define_area(seal_controls, grid, file_limits)
    err_flag += result

    # 5. Define depth to top of cell.
    result = define_depth(seal_controls, grid, file_limits)
    err_flag += result

    # 6. Define entry pressure of grid.
    result = define_entry(seal_controls, grid, file_limits)
    err_flag += result

    # Report error if found. Data outside bounds is deemed fatal!
    if err_flag < 0:
        sfile.opx_problem("File value(s) outside defined bounds " +
                          "caused program to exit!")

    return grid


def define_top_press_array(seal_controls, grid, dimension, param_bounds):
    """ Define top pressure values.
    Input Variables:
        seal_controls = input parameters (dictionary)
        grid = collection of cells
        dimension = size of 1D press_top
    Return:
        press_top = NumPy array
    """
    press_top = np.zeros((dimension))

    # Get data depending on user choice.
    if seal_controls['upper_approach']:
        # Input top pressures from file.
        if ECHO:
            print("  --> Input from file: '" + PRESS_NAME + "'")
        press_top = sfile.acquire_data_array(SUBDIR_INPUT, PRESS_NAME,
                                             len(grid))

        # Check if numeric values are within typical pressure bounds.
        # - Use base pressure as representative.
        stats = 0
        val_limits = param_bounds['base_pressure']
        stats = sfile.check_file_floats(press_top, "Top pressure value",
                                        val_limits)

        if stats < 0:
            sfile.opx_problem("File value(s) outside defined bounds " +
                              "caused program to exit!")

    else:
        # Compute static pressure from reference depth and reference pressure.
        for numbr, cell in enumerate(grid):
            press_top[numbr] = \
                cell.compute_static_pressure(seal_controls['ref_pressure'],
                                             seal_controls['ref_depth'])

    return press_top


def get_data_lines(destination, file_location, rows, cols, skip_lines):
    """ Get block of data from repository file.
    Input Variables:
        destination = path to input file
        file_location = list of subdirectory & file
            =0 -> subdirectory
            =1 -> filename
        rows = rows
        cols = columns
        skip_lines  = present position count
    Return:
        new_array = NumPy array of values in row/col format
    """
    # Define list.
    data_list = []

    try:
        # Read data after skipping previous data.
        with open(destination) as line_input:
            # Skip to right position.
            for _ in repeat(None, skip_lines):
                lined = line_input.readline()

            # Read rows of data.
            for _ in repeat(None, rows):
                lined = line_input.readline()
                interim = lined.strip().split(',')
                divided = interim[0:cols]
                data_list.append(divided)

    except OSError as err:
        sfile.io_snag(err, file_location[0], file_location[1])

    # Convert to array - possible error in reading.
    try:
        new_array = np.asarray(data_list, dtype=np.float64)
    except ValueError:
        # Value error found - exit.
        sfile.data_error("Value Error! -> Check number of columns in file!",
                         file_location[0], file_location[1])

    # Error check on shape.
    if new_array.shape != (rows, cols):
        msg1 = "Failure in Reservoir Data Input,"
        msg1 += "- Wrong Number of Rows/Columns!"
        sfile.opx_problem(msg1)

    return new_array


def input_reservoir_data(sequence, rows, cols, param_bounds):
    """ Input data from reservoir files at a time point.
    Input Variables:
        sequence = current time point index (starts at 1)
        rows = rows of data
        cols = columns of data
        param_bounds = data bounds (dictionary)
    Return:
        pressure = base CO2 pressure - as NumPy array
        saturation = base CO2 saturation - as NumPy array
    Note:
        Files assumed to have extension *.txt,
        and are comma delimited, with NO header.
    Assumption:
        Pressure and Saturation at start of time step are assumed to be
        constant for entire period - no averaging employed in this version.
    """
    # Setup data arrays.
    saturation = np.zeros((rows, cols))
    pressure = np.zeros((rows, cols))
    skip_lines = (sequence - 1) * rows  # sequence starts at 1

    # Saturation - Open data array and read lines until start.
    if ECHO:
        print("  --> Input from file: '" + SATURATION_NAME + "'")
    sub_path, destination = \
       sfile.get_path_name(SUBDIR_RESERVOIR, SATURATION_NAME, EXTENSION_TXT)
    location = [sub_path, SATURATION_NAME]
    saturation = get_data_lines(destination, location, rows, cols, skip_lines)

    # Reservoir - Open data array and read lines until start.
    if ECHO:
        print("  --> Input from file: '" + PRESSURE_NAME + "'")
    sub_path, destination = \
       sfile.get_path_name(SUBDIR_RESERVOIR, PRESSURE_NAME, EXTENSION_TXT)
    location = [sub_path, PRESSURE_NAME]
    pressure = get_data_lines(destination, location, rows, cols, skip_lines)

    # Flatten arrays for use in computations.
    pressure.shape = (rows*cols)
    saturation.shape = (rows*cols)

    # Check that the 2 data arrays are within limits.
    err_flag = 0
    val_limits = param_bounds['reservoir_pressure']
    stats = sfile.check_file_floats(pressure, "Reservoir pressure value",
                                    val_limits)
    err_flag += stats

    val_limits = param_bounds['reservoir_saturation']
    stats = sfile.check_file_floats(saturation, "Reservoir saturation value",
                                    val_limits)
    err_flag += stats

    # If error, halt program.
    if err_flag < 0:
        sfile.opx_problem("File value(s) outside defined bounds " +
                          "caused program to exit!")

    return (pressure, saturation)


#
##-----------------------------------------------------------------------------
## End of module
