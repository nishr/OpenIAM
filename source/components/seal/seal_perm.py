"""
PYTHON MODULE:
    seal_perm.py
PURPOSE:
    Functions to compute seal permeability.
REVISION HISTORY:
    2019-04-22  Started work on this module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (15):
    evaluate_carbonate(react, carbonate)
    evaluate_clay(react, clay_type, clay_content)
    model_z_analysis(final_effect, delay_term, history)
    mineral_factor(reactivity, velocity, clay_content, clay_type, carbonate)
    compute_effective_saturation(co2_saturation, brine_residual, co2_residual)
    compute_capillary_pressure(zeta, beta, normal_saturation,
        bubbling_pressure)
    compute_brine_pressure(base_co2_pressure, capillary_pressure)
    brine_relative_permeability(rel_model, effective_saturation, seal_controls
    co2_relative_permeability(rel_model, effective_saturation, seal_controls)
    compute_current_permeability(rel_factor, total_permeability)
    define_permeability(grid, param_bounds)
    obtain_permeability(mid, scale, seal_controls, grid, param_bounds)
    evaluate_perm_variability(mean_val, std_dev, minimal, maximal)
    evaluate_areal_heter(grid_work, seal_controls)
    correlate_entry_pressure(seal_controls, grid)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import random                       # For variability in values
import math                         # For Tanh + other functions
from datetime import datetime       # For random seed
import numpy as np                  # Random seed & log-normal

import seal_other as sot            # Functions for thickness and depth
import seal_file as sfile           # Input/output related operations
import seal_units as sunit          # For unit conversion

# Seal model parameters
AXIS_SHIFT = 2.5                    # <S> - shift on x-axis for time model
HIGH_CLAY_SWELL = 1.0               # Model #2 - high swell degree
MID_CLAY_SWELL = 0.3                # Model #2 - med. swell degree
LOW_CLAY_SWELL = 0.0                # Model #2 - low. swell degree
V_CRITICAL = 0.5                    # Transition velocity (m/sec)
CARB_MINIMUM = 30.0                 # Minimum carb. content for reaction
CLAY_MINIMUM = 30.0                 # Minimum clay content for swell
CARB_CRITICAL = CARB_MINIMUM        # Critical carb. content for cementation
MAX_POSITIVE_CHANGE = 1.5           # Maximum + change due to carbonate
MAX_NEGATIVE_CHANGE = 0.9           # Maximum - change due to carbonate
MAX_NEGATIVE_SWELL = 0.99           # Maximum - change due to clay

# Permeability and heterogeneity controls
PERCENT = 8.0                       # Max. % cells for areal heterogeneity
MIN_SATURATION = 0.001              # Numerical minimum limit
MAX_SATURATION = 0.999              # Numerical maximum limit
V_SMALL_VARIANCE = 1.0e-05          # Small variability of log normal

# Input permeability data
SUBDIR_NAME = "seal_input"          # Seal input subdirectory
PERM_NAME = "seal_permeability.txt" # Permeability input file name

# Other constants
BOZO = False                        # Control to print permeability each loop



def evaluate_carbonate(react, carbonate):
    """ Compute carbonate factor for model #2.
    Input Variables:
        react = reactivity parameter (0 to 10)
        carbonate = carbonate content as %
    Return:
        change = change factor on permeability for carbonate case
    """
    # Compute carbonate factor.
    change = (react / 10.0) * (carbonate / 100.0)

    return change


def evaluate_clay(react, clay_type, clay_content):
    """ Compute clay factor for model #2.
    Input Variables:
        react = reactivity parameter (0 to 10)
        clay_type = type of clay
            smectite = high
            illite = medium
            chlorite = low
        clay_content = clay content as %
    Return:
        change = change factor on permeability for clay case
    """
     # Change based on degree from on clay type.
    if clay_type == "smectite":
        clay_factor = HIGH_CLAY_SWELL
    elif clay_type == "illite":
        clay_factor = MID_CLAY_SWELL
    else:
        clay_factor = LOW_CLAY_SWELL  # default = chlorite

    # Compute permeability change.
    change = clay_factor * (react / 10.0) * (clay_content / 100.0)
    return change


def model_z_analysis(final_effect, delay_term, history):
    """ Construct logic to use time for influence factor.
    Input Variables:
        final effect = final ratio for change in permeability
        time_term = time to see effect in time-line
        history = cumulative time of alteration (yrs)
    Return:
        Factor = Influence factor
    """
    # Compute equation terms.
    beta = math.tanh(AXIS_SHIFT)                 # curve shift
    tau = (1.0 - final_effect) / (1.0 + beta)   # overall factor

    # Compute terms based on total flow time in cell.
    a_term = math.tanh(delay_term * history - AXIS_SHIFT)
    t_term = tau * (a_term + beta)

    # Compute influence as function of 1.0.
    factor = (1.0 - t_term)
    return factor


def mineral_factor(velocity, reactivity, clay_content, clay_type,
                   carbonate):
    """ Construct logic to use Model #2 for influence factor computation.
    Input Variables:
        velocity = CO2 flow velocity (m/s)
        reactivity = reactivity factor (1 to 10)
        clay_content = clay content (%)
        clay_type = clay type name
        carbonate = carbonate content (%)
    Return:
        model_effect = Influence factor due to mineralogy
    """
    # Check if sufficient clay or carbonate content for reaction.
    if (carbonate >= CARB_MINIMUM) or (clay_content >= CLAY_MINIMUM):

        # - Check carbonate - if total carbonate content > 30%,
        #   ((carbonate model will dominate even if high clay)).
        if carbonate >= CLAY_MINIMUM:
            # Compute extent of change based on carbonate.
            change = evaluate_carbonate(reactivity, carbonate)

            # Determine if erosion or accumulation in this time step,
            # as a function of velocity.
            if velocity >= V_CRITICAL:
                model_effect = MAX_POSITIVE_CHANGE * change
            else:
                model_effect = 1.0 - (change * MAX_NEGATIVE_CHANGE)
        else:
            # Compute extent change based on clay model.
            change = evaluate_clay(reactivity, clay_type, clay_content)

            # Determine closure in this time step from swell;
            # - velocity has no effect.
            model_effect = 1.0 - (change * MAX_NEGATIVE_SWELL)
    else:
        # No change in influence.
        model_effect = 1.0

    return model_effect


def compute_effective_saturation(co2_saturation, brine_residual, co2_residual):
    """ Compute the effective saturation value for two-phase flow.
    Input Variables:
        co2_saturation = current CO2 saturation (decimal)
        brine_residual = brine residual saturation (decimal)
        co2_residual = co2 residual saturation (decimal)
    Returns:
        effective_saturation => normalized wet (brine) saturation
    """
    # Compute current brine saturation.
    wet_saturation = 1.0 - co2_saturation

    #  Compute effective saturation within residual limits.
    if wet_saturation <= brine_residual:
        effective_saturation = 0.0
    elif wet_saturation > 1.0 - co2_residual:
        effective_saturation = 1.0
    else:
        effective_saturation = (wet_saturation - brine_residual) \
                                / (1.0 - brine_residual - co2_residual)
    return effective_saturation


def compute_brine_pressure(base_co2_pressure, capillary_pressure):
    """ Compute the base brine pressures on seal horizon.
    Input Variables:
        base_co2_pressure = co2 pressure at base from reservoir data
        capillary_preesure = capillary pressure
    Returns:
        base_pressure = brine pressure at base of seal (MPa)
    """
    # Compute brine pressure from co2 pressure.
    base_pressure = base_co2_pressure - capillary_pressure

    return base_pressure


def brine_relative_permeability(rel_model, effective_saturation,
                                seal_controls):
    """ Compute the wetting relative permeabilities.
    Input Variables:
        rel_model = relative permeability model (str)  either"LET" or "BC"
        effective_saturation = effective saturation
        seal_controls =  dictionary of seal parameters
    Returns:
        wet_perm = wetting relative permeability (brine)
    Note:
        See Equation B-8 in manual for Brooks-Corey model.
        See Equation B-16 in manual for LET model.
    """
    # Define relative permeability based on model type.

    if 'LET' in rel_model:
        # Define wetting relative permeability using LET model.
        term_1 = pow(effective_saturation, seal_controls['l_wetting'])
        non_sat = 1.0 - effective_saturation
        term_2 = (seal_controls['e_wetting'] *
                  pow(non_sat, seal_controls['t_wetting']))
        wet_perm = term_1 / (term_1 + term_2)
    else:
        # Define nonwetting permeability using B_C, with zeta = lambda.
        term_1 = (2.0 + 3.0 * seal_controls['zeta']) / seal_controls['zeta']
        wet_perm = pow(effective_saturation, term_1)

    return wet_perm


def co2_relative_permeability(rel_model, effective_saturation, seal_controls):
    """ Compute the nonwetting relative permeability.
    Input Variables:
        rel_model = relative permeability model (str)  either"LET" or "BC"
        effective_saturation = effective wetting saturation
        seal_controls =  dictionary of seal parameters
    Returns:
        nonwet_perm = nonwetting relative permeability (co2)
    Note:
        See Equation B-9 in manual for Brooks-Corey model.
        See Equation B-17 in manual for LET model.
    """
    # Define nonwetting saturation and ratio.
    non_saturation = 1.0 - effective_saturation
    p_ratio = seal_controls['perm_ratio']

    # Define relative permeability based on model type.

    if 'LET' in rel_model:
        # Define nonwetting permeability using LET model.
        term_1 = pow(non_saturation, seal_controls['l_nonwet'])
        term_2 = (seal_controls['e_nonwet'] *
                  pow(effective_saturation, seal_controls['t_nonwet']))
        fcap = term_1 / (term_1 + term_2)
        nonwet_perm = p_ratio * fcap
    else:
        # Define nonwetting permeability using B_C, with zeta = lambda.
        term_1 = pow(non_saturation, 2.0)
        term_2 = (2.0 + seal_controls['zeta']) / seal_controls['zeta']
        term_3 = pow(effective_saturation, term_2)
        nonwet_perm = p_ratio * term_1 * (1.0 - term_3)

    return nonwet_perm


def compute_capillary_pressure(rel_model, normal_saturation, seal_controls,
                               entry_pressure):
    """ Compute the capillary pressure based on effective saturation.
    Input Variables:
        rel_model = relative permeability model = BC/LET
        normal_saturation = normalized saturation between residual
               values = effective wetting saturation
        seal_controls = dictionary of seal parameters
        entry_pressure = limit pressure (Pa) for capillary pressure
    Returns:
        capillary pressure (Pa)
    Note:
        See Equation B-15a/b in manual for Brooks-Corey model.
        See Equations B-18 and B-19 in manual for LET model.
    """
    # Define nonwetting saturation.
    non_saturation = 1.0 - normal_saturation

    # Define capillary pressure based on model type.

    if 'LET' in rel_model:
        # Compute based on LET capillary terms.
        term_1 = pow(non_saturation, seal_controls['l_capillary'])
        term_2 = (seal_controls['e_capillary'] *
                  pow((normal_saturation), seal_controls['t_capillary']))
        fcap = term_1 / (term_1 + term_2)
        change = seal_controls['max_capillary'] - entry_pressure

        capillary_pressure = fcap * change + entry_pressure
    else:
        # Compute based on modified Brooks-Corey model.
        exponent = (1.0 / seal_controls['zeta'])
        if normal_saturation >= MAX_SATURATION:
            divisor = 1.0
        elif normal_saturation <= MIN_SATURATION:
            divisor = pow(MIN_SATURATION, exponent)
        else:
            divisor = pow(normal_saturation, exponent)

        capillary_pressure = (entry_pressure / divisor)

    return capillary_pressure  # in Pascals


def compute_current_permeability(rel_factor, affect, total_permeability):
    """ Compute the current permeability of cell.
    Input Variables:
        rel_factor = relative permeability of cell (decimal)
        affect = influence factor on permeability
        total permeability of cell (mD)
    Returns:
        effective_permeability = permeability at base of seal (m2)
    Notes:
        input in microdarcy but output is converted to m2!
    """
    # Compute brine pressure from co2 pressure.
    effective_permeability = (rel_factor * affect * total_permeability)
    effective_permeability *= sunit.microd_to_metersq()

    return effective_permeability


def define_permeability(grid, param_bounds):
    """ Define area values.
    Input Variables:
        grid = list of cells
        param_bounds = input bounds (dictionary)
    Return:
        Data from file - 1D NumPy array
    """
    # Get data array from file.
    data_array = sfile.acquire_data_array(SUBDIR_NAME, PERM_NAME, len(grid))

    # Check data to be within bounds.
    val_limits = [param_bounds['perm_min'][0],
                  param_bounds['perm_max'][1]]
    stats = sfile.check_file_floats(data_array, "Permeability input value",
                                    val_limits)
    if stats < 0:
        sfile.opx_problem("File value(s) outside defined bounds " +
                          "caused program to exit!")

    return data_array


def obtain_permeability(seal_controls, grid, param_bounds):
    """ Define vertical permeability as variable or from file
    Input Variables:
        seal_controls = control parameter dictionary
        grid = list of cells
        param_bounds = input bounds (dictionary)
    Return:
        interim = total permeability
    Note:
        > Input values are in microdarcys.
        > mid = location of log-distribution
        > scale = scale of log-distribution
    """
    # Get data from file, if desired.
    if seal_controls['perm_input_approach']:
        # Read file but only once.
        if seal_controls['read_perm']:
            data_array = define_permeability(grid, param_bounds)
            for numbr, cell in enumerate(grid):
                cell.permeability = data_array[numbr]
            seal_controls['read_perm'] = False  # No additional reads
    else:
        # Set values for use in loop.
        mid = seal_controls['perm_location']
        scale = seal_controls['perm_scale']

        # Define permeability value for every cell in grid.
        for cell in grid:

            if seal_controls['vary_perm_choice']:
                # For variable permeability, evaluate random value.
                perm_value = \
                    evaluate_perm_variability(mid, scale,
                                              seal_controls['perm_min'],
                                              seal_controls['perm_max'])
            else:
                # For Uniform permeability, set to mean.
                perm_value = seal_controls['perm_mean']

            # Then set cell value.
            cell.permeability = perm_value

    # Debugging check - printout permeability for each simulation!
    if BOZO:
        # Create list of permeability data.
        print_list = []
        for cell in grid:
            print_list.append(cell.permeability)

        # Print data in tabular format.
        title = "PERMEABILITY"
        sot.print_tabular_data(title, print_list, seal_controls['cols'])

    # return None


def evaluate_perm_variability(mu_d, perm_scale, minimal, maximal):
    """ Define vertical permeability for each cell.
    Input Variables:
        mu_d = location of log-distribution
        perm_scale = scale of log-distribution
        minimum = minimum vertical permeability in μD
        maximum = maximum vertical permeability in μD
    Return:
        new_value = total permeability
    Note:
        > Permeability is random - using log-normal distribution.
        > Values are censored to be within the defined min. and max.
        > Input values are in microdarcys.
    """
    # Define values and arrays.
    random.seed(datetime.now())

    # Use random distribution to get values + censor values.
    while True:
        new_value = np.random.lognormal(mean=mu_d, sigma=perm_scale,
                                        size=None)
        if minimal <= new_value <= maximal:
            break

    return new_value


def evaluate_areal_heter(grid_work, total_cells, seal_controls):
    """  Generate random zones across seal grid of higher permeability.
    Input Variables:
        grid = list of cells
        heter_limit = total number of cells in grid
        perm_increase = factor increase in permeability of zone
    Returns:
        None
    """
    # Only implement if desired and if variability is used.
    if (seal_controls['heterogeneity_approach'] and
            seal_controls['perm_input_approach']):

        # Define number of cells for inclusion.
        heter_limit = int(PERCENT * total_cells / 100.0)

        # Create list of cell index order and randomly shuffle.
        extent = len(grid_work)
        index_set = list(range(0, extent))
        random.shuffle(index_set)       # Eliminates duplicates!

        # Loop over grid until maximum number of random cells is adjusted.
        heter_count = 0
        for element in range(extent):
            heter_index = index_set[element]
            if grid_work[heter_index].status > 0:
                # Break loop if at limit
                heter_count += 1
                if heter_count > heter_limit:
                    break

                # increase permeability of cell if still in loop.
                cell_perm = (grid_work[heter_index].permeability
                             * seal_controls['perm_heter_factor'])
                grid_work[heter_index].permeability = cell_perm

    # return None


def correlate_entry_pressure(seal_controls, grid):
    """ Correlate threshold value of cell with permeability.
    Input Variables:
        seal_controls = control parameter dictionary
        grid = list of cells
    Return:
        None
    """
    # Change entry pressure only if desired and permeability is
    #    <not> from file.
    if seal_controls['correlate_entry_approach'] and \
        not seal_controls['entry_approach']:

        # Define parameters for clarity.
        ref_pressure = seal_controls['entry_pressure']
        ref_permeability = seal_controls['perm_mean']

        # Define new threshold pressure for each cell.
        for cell in grid:
            ratio = math.sqrt(ref_permeability / cell.permeability)
            cell.entryPressure = ref_pressure * ratio

    # return None


#
##-----------------------------------------------------------------------------
## End of module
