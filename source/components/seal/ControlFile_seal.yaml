---
# Seal_Model Input - Example  #1
#   This example Yaml file illustrates input for seal model which is part
#   of the NRAP-Open-IAM.
#   The setup file uses variable descriptions of thickness and permeability
#   but does not use file input. (92 lines)
# Last Modified: 05 March 2020
# Author: E.N. Lindner
#------------------------------------------------------------------------------
ModelParams:
    runTitle: Seal_Model Input - Example No. 1
    startTime: 0.0              # years
    endTime: 200.0              # years
    timePoints: 33              # Including start point
    realizations: 1             # Simulation cycles
    timeInput: Yes              # Yes/No; Yes = Time steps from File
Description:
    gridRows: 6                 # Y-axis divisions
    gridColumns: 12             # X-axis divisions
    numCells: 72                # !!! total number of cells including non-active
    cellHeight: 100.00          # m; Ave. ea. cell dimension N-S (Y-axis)
    cellWidth: 100.00           # m; Ave. ea. cell dimension E-W (X-axis)
    area: 100.0                 # m2
    salinity: 15000.0           # ppm
    aveTemperature: 50.0        # oC
    aveBaseDepth: 1130          # !!! m; Ave. seal base depth, positive below grade
    aveBasePressure: 3.20e+7    # Pa; Ave. at seal base
Conditions:
    staticDepth:  1000.0        # m;  Reference Depth, positive below grade
    staticPressure: 1.00e+7     # Pa; Reference Pressure - at Reference Depth
    CO2Density: 597.8           # !!! kg/m^3; Ignored if Interpolate = yes
    CO2Viscosity: 4.52e-5       # !!! Pa*s;   Ignored if Interpolate = yes
    brineDensity: 1004.00       # kg/m^3; Ignored if Interpolate = yes
    brineViscosity: 5.634e-4    # Pa*s;   Ignored if Interpolate = yes
    CO2Solubility: 0.0          # !!! kg/kg ; Ignored if Interpolate = yes
Controls:
    depthApproach: No           # Yes/No; Yes = Depths to cell base from file
    layoutApproach: No          # Yes/No; Yes = Coordinates from file
    areaApproach: No            # Yes/No: Yes = Area from file
    upperBoundApproach: No      # Yes/No; Yes = Upper-bound pressure from file
    activeCellsApproach: No     # Yes/No; Yes = Status from file
    entryApproach: No           # Yes/No; Yes = Entry pressure from file
    permApproach: No            # Yes/No; Yes = Permeability from file
    thicknessApproach: No       # Yes/No; Yes = Input from File
    correlateApproach: No       # Yes/No; Yes = Correlate entry press. w. perm.
    interpolateApproach: No     # Yes/No; Yes = Interpolate fluid constants
    initializeApproach: No      # Yes/No; Yes = initialize Model from file
Permeability:
    avePermeability: 2.50E-2    # !!! microdarcys
    stdDevPermeability: 0.0     # !!! microdarcys
    minPermeability: 1.0E-5     # !!! microdarcys
    maxPermeability: 1.0        # !!! microdarcys
    heterogeneityApproach: No   # Yes/No; Yes = Include heterogeneity
    heterFactor: 0.5            # !!!
RelativeFlowLimits:
    brineResSaturation: 0.15    # !!! Residual wetting
    CO2ResSaturation: 0.00      # !!! Residual nonwetting; typically = 0.0
    relativeModel: LET          # Model type; options: BC, LET
    permRatio: 0.6              # Ratio of nonwetting/wetting
CapillaryPressure:
    entryPressure: 5000.0       # !!! Pa - Default threshold value
BrooksCoreyModel:
    lambda:  2.5                # Used only if relativeModel = BC
LETModel:
    wetting1: 1.0               # Used only if relativeModel = LET
    wetting2: 10.0              # Used only if relativeModel = LET
    wetting3: 1.25              # Used only if relativeModel = LET
    nonwet1: 1.05               # Used only if relativeModel = LET
    nonwet2: 3.50               # Used only if relativeModel = LET
    nonwet3: 1.25               # Used only if relativeModel = LET
LETCapillaryModel:
    capillary1: 0.20            # Used only if relativeModel = LET
    capillary2: 2.80            # Used only if relativeModel = LET
    capillary3: 0.43            # Used only if relativeModel = LET
    maxCapillary: 1.0E+07       # Pa; Used only if relativeModel = LET
TimeModel:
    influenceModel: 0           # Options: 0/1/2
    influence: 1.0
    rateEffect: 0.1             # Time model -> Rate of effect
    totalEffect: 0.1            # Time model -> Total effect as ratio
    reactivity: 8.0             # Value 1 to 10
    clayContent: 60.0           # percent
    carbonateContent: 8.0       # percent
    clayType: smectite          # Clay type; types: smectite/illite/chlorite
Thickness:
    aveThickness: 100.0         # m
    stdDevThickness: 20.00      # m
    minThickness: 75.0          # !!! m
    maxThickness: 125.0         # !!! m
SealPlots:
    permeabilityPlot: No        # Yes/No; Yes = plot permeabilities
    timeSeriesPlot: Yes         # Yes/No; Yes = plot time history
    CO2ContourPlot: No          # !!! Yes/No; Yes = contour CO2 release
### End

