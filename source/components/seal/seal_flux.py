"""
PYTHON MODULE:
    seal_flux.py
PURPOSE:
    Main Module - Code performs a seal model ROM analysis for the NRAP.
REVISION HISTORY:
    2019-04-16  Started work on module, named "main_line.py"
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE:
    main()
-------------------------------------------------------------------------------
                            Warranty Disclaimer
  This software is provided for use as-is and no representations about the
  suitability of this software is made. It is provided without warranty.

  This software development was funded by the Department of Energy, National
  Energy Technology Laboratory, an agency of the United States Government,
  through a support contract with Battelle Memorial Institute as part of the
  Leidos Research Support Team (LRST). Neither the United States Government
  nor any agency thereof, nor any of their employees, nor LRST nor any of
  their employees, makes any warranty, express or implied, or assumes any
  legal liability or responsibility for the accuracy, completeness, or
  usefulness of any information, product, software or process disclosed,
  or represents that its use would not infringe privately owned rights.
-------------------------------------------------------------------------------
                            Acknowledgment
  This software and its documentation were completed as part of National
  Risk Assessment Partnership project. Support for this project came from
  the U.S. Department of Energy's (DOE) Office of Fossil Energy's
  Crosscutting Research program. The technical effort was performed in
  support of the DOE National Energy Technology Laboratory's ongoing
  research by the Leidos Research Support Team (LRST) under the Research
  Support Services contract 89243318CFE000003.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import logging
import seal_intro as intro          # Gets YAML data and checks input
import seal_display as dis          # Save summary data and create plots
import seal_file as sfile           # Input/output related operations
import seal_fluids as fluid         # Functions for density & viscosity
import seal_model as mod            # Functions for Class definitions
import seal_other as sot            # Functions for thickness and depth
import seal_perm as perm            # Functions for permeability definition
import seal_plot as splot           # Plot results
import seal_upload as sup           # Input file data

# Input control file name
YAML_FILE = "ControlFile_seal.yaml" # Input file name in source directory

# Operation controls and debugging - also controlled by RUNNING
DETAIL_PERM = False         # Detail - Save permeability matrix to file
DETAIL_STEP = False         # Detail - Save results of time step data to file
ECHO = False                # Detail - Print progress banner of each time step
BOZO = False                # DEBUG - Print data arrays and cell value example

RUNNING = (__name__ == "__main__")



def main():
    """ Stand-alone operation of code.
    Input Variables:
        None
    Return:
        None
    """

    #--------------------------------------------------------------------------
    # A. SETUP
    #--------------------------------------------------------------------------
    # Read control File.
    seal_controls, param_bounds = intro.preliminaries(RUNNING, YAML_FILE)

    # Assign variables to class and complete other input tasks.
    grid = []    # Assume 1D array to assign later in row-column format
    grid, rows, cols, seal_controls = intro.define_finish(seal_controls, grid,
                                                          param_bounds)

    #--------------------------------------------------------------------------
    # B. INITIALIZATION
    #--------------------------------------------------------------------------
    dis.echo_status(RUNNING, 2)
    sim_flux = []

    # Input data from major files, as desired with error check.
    grid = sup.input_various_data(seal_controls, grid, param_bounds)

    # Define pressure at top of cells in either of two ways.
    # --> Static pressure requires depth to top of cell.
    # press_top is array of size (rows*cols)
    press_top = sup.define_top_press_array(
        seal_controls, grid, (cols * rows), param_bounds)

    # Interpolate fluid properties, if desired:
    # update seal_controls and grid cells properties
    fluid.manage_interpolation(RUNNING, seal_controls, press_top,
                               param_bounds)

    # Detailed DEBUG here, if BOZO = True: print arrays.
    sot.debug_check(BOZO, grid, rows, cols)

    #--------------------------------------------------------------------------
    # C. COMPUTATION LOOPS
    #--------------------------------------------------------------------------
    dis.echo_status(RUNNING, 3)

    # Establish time step array.
    time_period = sot.create_time_values(seal_controls)  # In years!!

    # Create storage arrays for time steps within a realization.
    # sim_co2_flow, sim_brine_flow are zero matrices of size (rows, cols)
    # co2_flow_step, brine_flow_step are zero arrays of size (rows*cols)
    sim_co2_flow, sim_brine_flow, co2_flow_step, brine_flow_step = \
        sot.create_sim_storage(rows, cols)

    # Create reservoir input arrays.
    # base_co2_saturation, base_co2_pressure are matrices of size (rows, cols)
    base_co2_saturation, base_co2_pressure = \
        sot.create_reservoir_storage(rows, cols)

    # REALIZATION LOOP <<<<<<<<
    for sim_step in range(seal_controls['realizations']):

        # Echo status to console.
        dis.echo_sim_step(RUNNING, sim_step)

        # Compute permeability and threshold values for every cell.
        perm.obtain_permeability(seal_controls, grid, param_bounds)
        perm.correlate_entry_pressure(seal_controls, grid)

        # Define permeability heterogeneities in grid, if desired.
        perm.evaluate_areal_heter(grid, rows * cols, seal_controls)

        # Establish realization list for output.
        simulation_list = sot.establish_simulation_list(sim_step)

        # START TIME STEP LOOP <<<<<<<<
        for chrono in range(1, seal_controls['time_points']):

            # Values computed at end of first step -> step = #1!
            current = time_period[chrono]

            # Provide header, if showing time steps.
            dis.echo_time_step(ECHO, RUNNING, current)

            # Input reservoir pressure and saturation arrays from files.
            # base_co2_pressure, base_co2_saturation are arrays of size (rows*cols)
            base_co2_pressure, base_co2_saturation = \
                 sup.input_reservoir_data(chrono, rows, cols, param_bounds)

            # Compute Darcy flow for each cell over one increment.
            for cell_number in range(rows * cols):

                # Get reservoir data for this cell.
                co2_base_pressure = base_co2_pressure[cell_number]
                co2_base_saturation = base_co2_saturation[cell_number]

                # Compute flow rates for each cell - vol./sec.
                # NOTE: Top saturation not used at this version;
                #   bottom saturations are assumed to be steady-state value.
                # NOTE: Dissolved CO2 is included in CO2 flow calculation as
                #       fluids despite assuming immiscible flow as theory.

                flow_rate_co2, flow_rate_brine = \
                    grid[cell_number].compute_flow(co2_base_pressure,
                                                   co2_base_saturation,
                                                   press_top[cell_number],
                                                   seal_controls)

                # Update history of cell.
                grid[cell_number].track_history(co2_base_pressure,
                                                flow_rate_co2, current)

                # Update influence factors for cell.
                grid[cell_number].compute_model_influence(flow_rate_co2)

                # Accumulate vol. flows of each cell for this time step.
                brine_flow_step[cell_number] = flow_rate_brine
                co2_flow_step[cell_number] = flow_rate_co2

            # Convert flows into mass flows in tonnes for interval.
            # Here, co2_flow_step, brine_flow_step become matrices of size (rows, cols)
            co2_flow_step, brine_flow_step = \
                mod.convert_flows(co2_flow_step, brine_flow_step, current,
                                  time_period[chrono - 1], cols)

            # Sum time step flows with prior flows for cumulative flows.
            # sim_co2_flow and sim_brine_flow are arrays of size (rows, cols)
            sim_co2_flow += co2_flow_step
            sim_brine_flow += brine_flow_step

            # Write 2D data to a file for <current> time step, if debug.
            if DETAIL_STEP:
                dis.cache_step_results(sim_step, current,
                                       seal_controls["title"],
                                       sim_co2_flow, sim_brine_flow)

            # if at last time step - save results for contour plot.
            if chrono == (seal_controls['time_points'] - 1):
                dis.cache_grid_results(sim_step, sim_co2_flow, sim_brine_flow,
                                       RUNNING, seal_controls)

            # Sum flows over grid for single total flow for time step.
            total_co2, total_brine = dis.aggregate_results(sim_co2_flow,
                                                           sim_brine_flow)

            # Store current results in a list of total simulation results.
            simulation_list = \
                sot.update_simulation_list(sim_step, time_period[chrono],
                                           total_co2, total_brine,
                                           simulation_list)

            # Flatten and zero-out calculation arrays for next cycle.
            co2_flow_step, brine_flow_step = \
                sot.clear_cycle_storage(co2_flow_step, brine_flow_step,
                                        (cols * rows))

            # END TIME STEP LOOP <<<<<<<<

        # Write the list of results for this simulation.
        dis.cache_sim_results(sim_step, simulation_list,
                              seal_controls['title'])

        # Write permeability data for simulation, if debugging.
        if DETAIL_PERM:
            dis.cache_perm(sim_step, grid, seal_controls)

        # Save simulation CO2 flux results to a list.
        sim_flux.append(total_co2)

        # Zero-out computation arrays for next simulation loop.
        sim_co2_flow, sim_brine_flow, simulation_list = \
            sot.clear_storage(sim_co2_flow, sim_brine_flow, simulation_list)

        # Compute new depth, thickness and pressure values for next loop.
        press_top = \
            sot.refresh_data_arrays(seal_controls, sim_step, grid, press_top)

    # END OF REALIZATION LOOP  <<<<<<<

    #--------------------------------------------------------------------------
    # D. AT END: WRITE STATUS & SUMMARY FILE.
    #--------------------------------------------------------------------------
    # Write run time, if stand-alone.
    dis.closeout(RUNNING, seal_controls)

    # Write dictionary values to summary file.
    dis.write_summary(seal_controls, sim_flux)
    dis.echo_status(RUNNING, 4)

    # Show plots, if desired.
    splot.plot_manager(RUNNING, seal_controls, grid, rows, cols)

    # Print Exit statement.
    sfile.end_message(RUNNING)

#------------------------------------------------------------------------------
# Start MAIN Line of Code.
#------------------------------------------------------------------------------
if __name__ == "__main__":
    # Setup logging
    logging.basicConfig(level=logging.DEBUG)

    # Run script
    main()


#
##-----------------------------------------------------------------------------
## End of module
#--------1---------2---------3---------4---------5---------6---------7---------8
