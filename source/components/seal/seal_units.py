"""
PYTHON MODULE:
   seal_units.py
PURPOSE:
    Functions to provide conversion factors / standard values.
REVISION HISTORY:
    2019-04-16  Finalized module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (14):
    yrs_to_seconds()
    yrs_to_days()
    seconds_to_yrs()
    days_to_yrs()
    gravity()
    pascals()
    megapascals()
    nacl_molar_mass()
    co2_molar_mass():
    ppm_convert()
    kilo_to_tonne()
    kilogram_to_gram()
    microd_to_metersq()
    metersq_to_microd()
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""



def yrs_to_seconds():
    """ Conversion factor for years to seconds.
    Input Variables:
        None (years)
    Returns:
        value = in seconds (for Julian calendar)
    Notes:
        > Year = 365.25 days => 3.155 760 E+07 sec
        > Day value from Wikipedia
        > Computed in Excel
    """
    value = 3.155760E+07    # in seconds
    return value


def yrs_to_days():
    """ Conversion factor for years to days
    Input Variables:
        None (years)
    Returns:
        value = in seconds (Julian calendar)
    Notes:
        > Year = 365.25 days
        > Day value from Wikipedia
    """
    value = 3.6525E+02    # in days
    return value


def seconds_to_yrs():
    """ Conversion factor for seconds to years.
    Input Variables:
        None (seconds)
    Returns:
        value = in yrs (Julian calendar)
    Notes:
        > Year = 365.25 days => 3.155 760 E+07 sec
        > 1/year = 3.168 808 781 402 89 E-08
        > Day value from Wikipedia
        > Computed in Excel
    """
    value = 3.16880878140289E-08   # in years
    return value


def days_to_yrs():
    """ Conversion factor for days to years.
    Input Variables:
        None (days)
    Returns:
        value = in yrs (Julian calendar)
    Notes:
        > Year = 365.25 days
        > 1/year = 2.7378508
        > Day value from Wikipedia
        > Computed in Excel
    """
    value = 2.7378507871321E-03  # in years
    return value


def gravity():
    """ Provides standard gravity - in metric terms.
    Input Variables:
        None
    Returns:
        Standard acceleration (g) due to gravity (m/sec2)
    Notes
        > Value from Wikipedia
    """
    value = 9.80665  # m/s2
    return value


def pascals():
    """ Conversion factor for MPa to Pa.
    Input Variables:
        None
    Returns:
        value = in pascals (Pa)
    """
    value = 1.000000E+06  # Pa
    return value


def megapascals():
    """ Conversion factor for Pa to MPa.
    Input Variables:
        None (Pa)
    Returns:
        value = in megapascals (MPa)
    """
    value = 1.000000E-06  # MPa
    return value


def nacl_molar_mass():
    """ Provides NaCl molar mass.
    Input Variables:
        None
    Returns:
        value = molar mass NaCl (g/mol)
    Notes:
        > From: https://www.webqc.org/molecular-weight-of-NaCl.html
    """
    value = 58.4428
    return value


def co2_molar_mass():
    """ Provides CO2 molar mass.
    Input Variables:
        None
    Returns:
        value = molar mass CO2 (g/mol)
    Notes:
        > From  https://sciencetrends.com/molar-mass-of-co2-carbon-dioxide/
    """
    value = 44.00964
    return value


def ppm_convert():
    """ Conversion factor for ppm to float.
    Input Variables:
        None (ppm)
    Returns:
        value = Ratio as a float
    """
    value = 1.000000E-06
    return value


def kilo_to_tonne():
    """ Conversion factor for kg to metric tonnes.
    Input Variables:
        None (Kg)
    Returns:
        value = in metric tonne
    """
    value = 1.000000E-03  # tonnes
    return value


def kilogram_to_gram():
    """ Conversion factor for kg to g.
    Input Variables:
        None (Kg)
    Returns:
        value = in grams
    """
    value = 1.000000E+03  # g
    return value


def microd_to_metersq():
    """ Conversion factor for microdarcys to m2.
    Input Variables:
        None (microdarcy)
    Returns:
        value = in a m2
    Notes:
        > From Cardarelli, F. (1997)
            Scientific Unit Conversion: A Practical Guide to Metrication.
    """
    value = 9.86923266716E-19    # m2
    return value


def metersq_to_microd():
    """ Conversion factor for m2 to microdarcys.
    Input Variables:
        None (meters^2)
    Returns:
        value = in microdarcys
    Notes:
        > From https://en.wikipedia.org/wiki/Darcy_(unit)#Conversions
    """
    value = 1.01325E+18     # microdarcy
    return value


#
##-----------------------------------------------------------------------------
## End of module
