"""
PYTHON MODULE:
   seal_fluids.py
PURPOSE:
    Functions to control the interpolation of density and viscosity.
REVISION HISTORY:
    2019-05-10  Created module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (13):
    conduct_2d_interp(vect_x, vect_y, interp_table, pressure, temperature)
    debug_shape(method, dotted, temperature, pressure, salinity)
    linear_interpolate(target, x1, x2, y1, y2)
    salinity_locate(target,vector)
    convert_salinity(salinity)
    define_lookup_array(option)
    co2_properties(pressure, temperature)
    brine_density_property(pressure, temperature, salinity=0.0)
    brine_viscosity_property(pressure, temperature, salinity=0.0)
    co2_solubility(pressure, temperature, salinity=0.0)
    reset_fluid_properties(seal_controls, property_list)
    interpolate_fluid_properties(seal_controls, pressure)
    manage_interpolation(alive, seal_controls, press_top, param_bounds)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import numpy as np
from scipy import interpolate

import data_arrays as sdata             # Data arrays indices
import seal_units as sunit              # For unit conversion
import seal_file as sfile               # Error in file operations.
import seal_model as mod                # Functions for Class definitions
import seal_upload as sup               # input file data

# Data file names
CO2_DENSITY_FILE = "data_co2_density.npy"          # CO2 density table
CO2_VISCOSITY_FILE = "data_co2_viscosity.npy"      # CO2 visocity table
BRINE_DENSITY_FILE = "data_brine_density.npy"      # Brine density table
BRINE_VISCOSITY_FILE = "data_brine_viscosity.npy"  # Brine visocsity table
CO2_SOLUBILITY_FILE = "data_co2_solubility.npy"    # CO2 solubility table

# Other constants
ECHO = False                            # DEBUG Print variables



def conduct_2d_interp(vect_x, vect_y, interp_table, pressure, temperature):
    """
    Purpose: Perform 2D interpolation on a density/viscosity table
             to get a value for a given temperature and pressure.
    Input Variables:
        vectx = vector of x values for table
        vecty = vector of y values for table
        interp_table2d = 2D table of values
        pressure = pressure (MPa) at point
        temperature = temperature(oC) at point
    Return:
        value = density or viscosity at given temperature and pressure
    """
    # Define vectors and array for interpolation.
    x_values = vect_x
    y_values = vect_y
    z_values = interp_table

    # Use SciPy function interp2d to obtain density value.
    condition = interpolate.interp2d(x_values, y_values, z_values,
                                     kind='cubic')
    value = float(condition(pressure, temperature))

    # Function completed.
    return value


def debug_shape(bmethod, dotted, temperature, pressure, salinity):
    """
    Purpose: Print shape features for debug.
    Input Variables:
        bmethod = interpolation method
        dotted = array position
        temperature = temperature (oC)
        pressure = reference pressure (Pa)
        salinity = salinity (ppm)
    Return:
        None
    """
    # Convert with units.
    print("   ** Interpolation Type: {0}".format(bmethod))
    print("   ** Salinity Index: {0}".format(dotted))

    temp_val = str(temperature.shape).replace(',', '')
    print("   ** Shape of temperature vector: {}".format(temp_val))

    temp_val = str(pressure.shape).replace(',', '')
    print("   ** Shape of pressure vector {}".format(temp_val))

    temp_val = str(salinity.shape).replace(',', '')
    print("   ** Shape of salinity array {}".format(temp_val))

    # return None


def linear_interpolate(target, x_pt1, x_pt2, y_pt1, y_pt2):
    """
    Purpose: Perform linear 1D interpolation of value from series.
    Input Variables:
            target = current salinity (x-value)
            x_pt1  = x-value (salinity) at point 1
            x_pt2  = x-value (salinity) at point 2
            y_pt1  = function(density or viscosity) at point 1
            y_pt2  = function(density or viscosity) at point 2
    Return:
        value = interpolated value
    Note:
        See Wikipedia for equation.
    """
    # Define values for computation.
    del_x = x_pt2 - x_pt1
    del_y = y_pt2 - y_pt1

    # Return if denominator is OK.
    if del_x != 0.0:
        value = y_pt1 + ((del_y/del_x) * (target - x_pt1))
    else:
        msg1 = "Attempt to linear interpolate failed "
        msg1 += "during input of linear interpolate."
        sfile.opx_problem(msg1)

    # return value.
    return float(value)


def salinity_locate(target, vector):
    """
    Purpose: Search array to find appropriate location in array for
             specified salinity to conduct interpolation.
    Input Variables:
        target = specified salinity
        vector = vector of salinity values controlling interpolation
    Return:
        answer = index in vector where value is == or <= than target
        analysis = type of analysis to be conducted
                2 = 2D cubic interpolation at constant salinity
                3 = 3D cubic/linear interpolations at different salinities
    """
    # Set search values.
    min_index = 0
    max_index = vector.size - 1
    min_of_array = vector[min_index]
    max_of_array = vector[max_index]
    analysis = 0

    # Check if target is equal to any control points; use 2D analysis.
    if target in vector:
        answer = np.nonzero(vector == target)[0][0] # zeros > tuple of indices
        analysis = 2
    # Check if target is less than interpolation range.
    elif target <= min_of_array:
        answer = min_index   # -> use lowest salinity array.
        analysis = 2
    # Check if target is greater than interpolation range.
    elif target >= max_of_array:
        answer = max_index   # -> use highest salinity array.
        analysis = 2
    # Otherwise conduct basic search for values in-between controls points.
    else:
        for trak in range(0, max_index-1):
            if vector[trak] < target <= vector[trak+1]:
                answer = trak
                analysis = 3

    # return values
    return (answer, analysis)


def convert_salinity(salinity):
    """
    Purpose: Convert salinity in ppm to Molal for brine viscosity.
    Input Variables:
        salinity = NaCl in ppm
    Return:
        molal =  = NaCl in Molal (mol/kg)
    Note:
        molar mass is in [g/kg]
        Check: 100000 ppm =. 0.1 weight% converts to 1.9 m in NACl
        http://www2.ucdsb.on.ca/tiss/stretton/CHEM4/APSolutions.html
    """
    concentration = salinity * sunit.ppm_convert()     # weight ratio
    solvent = (1 - concentration)                   # solvent alone
    molal = salinity / (sunit.nacl_molar_mass() * sunit.kilogram_to_gram())
    molal /= solvent

    # finished
    return molal


def define_lookup_array(option):
    """
    Purpose: Get full path to the lookup file and load data.
    Input Variables:
        option = lookup file name
    Return:
        NumPy array = density or viscosity array
    """
    # Get file path
    file_path = sfile.find_local_path(option)

    # Load NumPy file and return
    lookup_array = np.load(file_path)

    return lookup_array


def co2_properties(pressure, temperature):
    """
    Purpose: Establish density and viscosity parameters for CO2
             using 2D lookup tables.
    Input Variables:
        pressure = pressure of CO2 (MPa)
        temperature = temperature of CO2 (oC)
    Return:
        z_density = CO2 density (kg/m3)
        viscosity = CO2 viscosity (Pa-s)
    Reference:
        1. CO2 Density from NIST REFPROP software, based on
          Span & Wagner (1996). Range 0.1 to 60 MPa and 1 to 180 oC.
        2. CO2 viscosity from NIST REFPROP software, based on
          Fenghour et al (1998).  Range 0.1 to 60 MPa and 1 to 180 oC.
    """
    # Load density and viscosity LUTs
    co2_density_lut = define_lookup_array(CO2_DENSITY_FILE)
    co2_viscosity_lut = define_lookup_array(CO2_VISCOSITY_FILE)

    # Use 2D cubic interpolation to estimate values from table
    z_density = conduct_2d_interp(sdata.CO2_DENSITY_PRESSURE,
                                  sdata.CO2_DENSITY_TEMP,
                                  co2_density_lut,
                                  pressure, temperature)

    # NOTE:
    #   CO2_VISCOSITY_PRESSURE = CO2_DENSITY_PRESSURE
    #   CO2_VISCOSITY_TEMP = CO2_DENSITY_TEMP
    z_viscosity = conduct_2d_interp(sdata.CO2_DENSITY_PRESSURE,
                                    sdata.CO2_DENSITY_TEMP,
                                    co2_viscosity_lut,
                                    pressure, temperature)
    return (z_density, z_viscosity)


def brine_density_property(pressure, temperature, salinity=0.0):
    """
    Purpose: Estimate brine density at specific temperature, pressure
             and salinity using a 3D lookup table.
    Input Variables:
        pressure = pressure of brine (MPa)
        temperature = temperature of brine (oC)
        salinity = NaCl in solution (ppm)
    Return:
        z_density = brine density (kg/m3)
    Reference:
        Temperature/pressure/salinity-dependent values based on
        Sun et al (2008) + density of pure H2O provided by
        NIST (2010) (Tables from Sun provided by J Morrell;
        NIST data based on Wagner & Pruss [1995]).
        EOS valid to 100 MPa, 374 oC and salinity of
        ~80,000 mg/kg (ratio of mass of solute / mass of solution,
        or ppm).
    """
    # Identify interpolation method & index in 3D array.
    #   -> It is a function of (salinity, pressure, temperature)
    #   -> method = 2 for 2D interpolation - cubic
    #   -> method = 3 for 3D interpolation - cubic + linear (salinity)
    #   -> dot = position in array
    dot, method = salinity_locate(salinity, sdata.BRINE_DENSITY_SALINITY)

    # Catch error, if it occurs.
    if method not in (2, 3):
        sfile.opx_problem("Failure in interpolation during brine density "
                          + "calculation.")

    ## Debug print.
    if ECHO:
        print("\n  For Brine Density")
        debug_shape(method, dot,
                    sdata.BRINE_DENSITY_TEMP,
                    sdata.BRINE_DENSITY_PRESSURE,
                    sdata.BRINE_DENSITY_SALINITY)
        print("   ** Salinity in ppm: = {0}".format(salinity))

    # STEP #1: 2D Interpolation  ----------------------------------------------

    # Establish variables and arrays for operation.
    i_rows = len(sdata.BRINE_DENSITY_PRESSURE)
    i_cols = len(sdata.BRINE_DENSITY_TEMP)

    # Load density LUT
    brine_density_lut = define_lookup_array(BRINE_DENSITY_FILE)

    # Establish 2D subarray for density at low index salinity.
    density_low = np.zeros((i_cols, i_rows))
    density_low = brine_density_lut[dot, :, :]

    # Conduct 2D interpolation with low array
    z_density = conduct_2d_interp(sdata.BRINE_DENSITY_PRESSURE,
                                  sdata.BRINE_DENSITY_TEMP,
                                  density_low, pressure, temperature)

    # STEP #2: 3D Interpolation  ----------------------------------------------

    # Perform 3d interpolation across salinities, if required (method = 3).
    if method == 3:

        # Establish subarray for density at high index salinity.
        density_high = np.zeros((i_cols, i_rows))
        density_high = brine_density_lut[(dot + 1), :, :]

        # Conduct 2D interpolation with low array.
        z_density_up = conduct_2d_interp(sdata.BRINE_DENSITY_PRESSURE,
                                         sdata.BRINE_DENSITY_TEMP,
                                         density_high, pressure, temperature)

        # Conduct linear interpolations between values.
        low_salinity = sdata.BRINE_DENSITY_SALINITY[dot]
        high_salinity = sdata.BRINE_DENSITY_SALINITY[dot + 1]
        z_density = linear_interpolate(salinity,
                                       low_salinity, high_salinity,
                                       z_density, z_density_up)

    return z_density


def brine_viscosity_property(pressure, temperature, salinity=0.0):
    """
    Purpose: Establish viscosity at specific temperature, pressure,
             and salinity using 3D lookup table.
    Input Variables:
        pressure = pressure of brine (MPa)
        temperature = temperature of brine (oC)
        salinity = NaCl in ppm (to be converted to molal)
    Return:
        z_viscosity = brine viscosity (Pa-s)
    Reference:
        Temperature/pressure/salinity-dependent viscosity of a
        saline solution based on Mao and Duan (2009) in terms
        of molality + viscosity of pure H2O provided by
        NIST (2010); NIST data based on Huber et al [2009]
        (Tables from Mao & Duan data provided by J Morrell).
        EOS valid to 100 MPa, 350 oC and salinity to 6 M.
    """
    # Identify interpolation method & index in 3D array
    #   -> It is a function of (salinity, pressure, temperature)
    #   -> method = 2 for 2D interpolation - cubic
    #   -> method = 3 for 3D interpolation - cubic + linear (on saline)
    #   -> dot = position in array

    #  Note: lookup table is in Molal, not ppm!
    sal_molal = convert_salinity(salinity)
    dot, method = salinity_locate(sal_molal, sdata.BRINE_VISCOSITY_SALINITY)

    # Debug.
    if ECHO:
        print("\n  For Brine Viscosity")
        debug_shape(method, dot,
                    sdata.BRINE_VISCOSITY_TEMP,
                    sdata.BRINE_VISCOSITY_PRESSURE,
                    sdata.BRINE_VISCOSITY_SALINITY)
        print("   ** Salinity in Molal: = {0}".format(sal_molal))

    # Catch error, if it occurs.
    if method not in (2, 3):
        sfile.opx_problem("Failure in interpolation during brine viscosity "
                          + "calculation.")

    # STEP #1: 2D Interpolation  ----------------------------------------------

    # Establish variables for operation.
    i_rows = len(sdata.BRINE_VISCOSITY_PRESSURE)
    i_cols = len(sdata.BRINE_VISCOSITY_TEMP)

    # Load viscosity LUT
    brine_viscosity_lut = define_lookup_array(BRINE_VISCOSITY_FILE)

    # Establish subarray for viscosity at low index salinity.
    viscosity_low = np.zeros((i_cols, i_rows))
    viscosity_low = brine_viscosity_lut[dot, :, :]

    # Conduct 2D interpolation with low array.
    z_viscosity = conduct_2d_interp(sdata.BRINE_VISCOSITY_PRESSURE,
                                    sdata.BRINE_VISCOSITY_TEMP,
                                    viscosity_low, pressure, temperature)

    # STEP #2: 3D Interpolation  ----------------------------------------------

    # Perform 3d interpolation across salinities, if required (method = 3).
    if method == 3:

        # Establish subarray for viscosity at high index salinity.
        viscosity_high = np.zeros((i_cols, i_rows))
        viscosity_high = brine_viscosity_lut[(dot + 1), :, :]

        # Conduct 2D interpolation with low array.
        z_viscosity_up = conduct_2d_interp(sdata.BRINE_VISCOSITY_PRESSURE,
                                           sdata.BRINE_VISCOSITY_TEMP,
                                           viscosity_high, pressure,
                                           temperature)

        # Conduct linear interpolations between values.
        low_salinity = sdata.BRINE_VISCOSITY_SALINITY[dot]
        high_salinity = sdata.BRINE_VISCOSITY_SALINITY[dot + 1]
        z_viscosity = linear_interpolate(sal_molal,
                                         low_salinity, high_salinity,
                                         z_viscosity, z_viscosity_up)

    return z_viscosity


def co2_solubility_property(pressure, temperature, salinity=0.0):
    """
    Purpose: Estimate CO2 solubility in brine at temperature, pressure
             and salinity using a 3D lookup table.
    Input Variables:
        pressure = pressure of brine (MPa)
        temperature = temperature of brine (oC)
        salinity = brine (NaCl) in mol
    Return:
        z_solubility = CO2 solubility (mol/kg)
    Reference:
        Temperature/pressure/salinity-dependent values based on
        programming the equation of state correlations as reported
        by Duan et al. (2006) and computing required values by
        Jason Monnell, Research Assistant Professor, University of Pittsburgh.
    """
    # Convert ppm to mol for analysis.
    sal_molal = convert_salinity(salinity)

    # Identify interpolation method & index in 3D array.
    #   -> It is a function of (salinity, pressure, temperature)
    #   -> method = 2 for 2D interpolation - cubic
    #   -> method = 3 for 3D interpolation - cubic + linear (salinity)
    #   -> dot = position in array
    dot, method = salinity_locate(sal_molal, sdata.CO2_SOLUBILITY_SALINITY)

    # Catch error, if it occurs.
    if method not in (2, 3):
        sfile.opx_problem("Failure in interpolation during CO2 solubility "
                          + "calculation.")

    ## Debug print.
    if ECHO:
        print("\n  For CO2 solubility")
        debug_shape(method, dot,
                    sdata.CO2_SOLUBILITY_TEMP,
                    sdata.CO2_SOLUBILITY_PRESSURE,
                    sdata.CO2_SOLUBILITY_SALINITY)
        print("   ** Salinity in mol: = {0}".format(sal_molal))

    # STEP #1: 2D Interpolation  ----------------------------------------------

    # Establish variables and arrays for operation.
    i_rows = len(sdata.CO2_SOLUBILITY_PRESSURE)
    i_cols = len(sdata.CO2_SOLUBILITY_TEMP)

    # Read LUT and create NPY file
    #  co2_solubility_lut = sol_data.CO2_SOLUBILTY_TABLE
    #  target = "data_co2_solubility"
    #  file_path = sfile.find_local_path(target)
    #  np.save(file_path, co2_solubility_lut)

    #  Load solubility LUT
    co2_solubility_lut = define_lookup_array(CO2_SOLUBILITY_FILE)

    # Establish 2D subarray for solubility at low index salinity.
    solubility_low = np.zeros((i_cols, i_rows))
    solubility_low = co2_solubility_lut[dot, :, :]

    # Conduct 2D interpolation with low array
    z_solubility = conduct_2d_interp(sdata.CO2_SOLUBILITY_TEMP,
                                     sdata.CO2_SOLUBILITY_PRESSURE,
                                     solubility_low, temperature, pressure)

    # STEP #2: 3D Interpolation  ----------------------------------------------

    # Perform 3d interpolation across salinities, if required (method = 3).
    if method == 3:

        # Establish subarray for solubility at high index salinity.
        solubility_high = np.zeros((i_cols, i_rows))
        solubility_high = co2_solubility_lut[(dot + 1), :, :]

        # Conduct 2D interpolation with low array.
        z_solubility_up = conduct_2d_interp(sdata.CO2_SOLUBILITY_TEMP,
                                            sdata.CO2_SOLUBILITY_PRESSURE,
                                            solubility_high, temperature,
                                            pressure)

        # Conduct linear interpolations between values.
        low_salinity = sdata.CO2_SOLUBILITY_SALINITY[dot]
        high_salinity = sdata.CO2_SOLUBILITY_SALINITY[dot + 1]
        z_solubility = linear_interpolate(sal_molal,
                                          low_salinity, high_salinity,
                                          z_solubility, z_solubility_up)

    return z_solubility


def reset_fluid_properties(seal_controls, property_list):
    """ Update fluid parameters in Class and in seal_controls.
    Input Variables:
        seal_controls = dictionary of seal controls
        property_list:
            co2_density = new CO2 density
            co2_viscosity = new CO2 viscosity
            brine_density = new brine density
            brine_viscosity = new brine viscosity
            co2solubility = new CO2 solubility
    Return:
        seal_controls
    """
    # Translate list.
    co2_density = property_list[0]
    co2_viscosity = property_list[1]
    brine_density = property_list[2]
    brine_viscosity = property_list[3]
    co2_solubility = property_list[4]

    # Reset Cell Class variables.
    mod.Cell.CO2Density = co2_density
    mod.Cell.CO2Viscosity = co2_viscosity
    mod.Cell.brineDensity = brine_density
    mod.Cell.brineViscosity = brine_viscosity
    mod.Cell.CO2Solubility = co2_solubility

    # Reset seal controls for output.
    seal_controls['co2_density'] = co2_density
    seal_controls['co2_viscosity'] = co2_viscosity
    seal_controls['brine_density'] = brine_density
    seal_controls['brine_viscosity'] = brine_viscosity
    seal_controls['co2_solubility'] = co2_solubility

    return seal_controls


def interpolate_fluid_properties(seal_controls, pressure):
    """
    Purpose: Interpolate to get fluid properties for brine & CO2.
    Input Variables:
        seal_controls = dictionary of seal controls
        pressure = reference pressure (Pa)
    Return:
        seal_controls = (updated) dictionary of seal controls
    """
    # Convert pressure to MPa and set other computation parameters.
    pressure *= sunit.megapascals()
    temperature = seal_controls['temperature']
    salinity = seal_controls['salinity']

    # zero fluid values.
    co2_density = 0.0
    co2_viscosity = 0.0
    brine_density = 0.0
    brine_viscosity = 0.0
    co2_solubility = 0.0

    # Lookup CO2 density, viscosity from lookup tables.
    co2_density, co2_viscosity = co2_properties(pressure, temperature)
    brine_density = brine_density_property(pressure, temperature, salinity)
    brine_viscosity = brine_viscosity_property(pressure, temperature, salinity)
    co2_solubility = co2_solubility_property(pressure, temperature, salinity)

    # Create list for transfer for reset.
    property_list = [co2_density, co2_viscosity, brine_density,
                     brine_viscosity, co2_solubility]

    # Reset controls within model and controls.
    reset_fluid_properties(seal_controls, property_list)

    # Debug print for check.
    if ECHO:
        # Debug - print variables for CO2 and brine
        echo_txt = ("\n   ** CO2 density and viscosity = {0:.3f} kg/m3 "
                    .format(co2_density))
        echo_txt += ("and {0:.5e} Pa-s".format(co2_viscosity))
        print(echo_txt)

        echo_txt = ("   ** brine density and viscosity = {0:.3f} kg/m3 "
                    .format(brine_density))
        echo_txt += ("and {0:.5e} Pa-s".format(brine_viscosity))
        print(echo_txt)

        print("   ** CO2 solubility = {0:.3f} kg/kg".format(co2_solubility))

    return seal_controls


def manage_interpolation(alive, seal_controls, press_top, param_bounds):
    """
    Purpose: Manage interpolation of fluid properties for brine & CO2.
    Input Variables:
        alive = flag on stand-alone operations
        seal_controls = dictionary of seal controls
        press_top = pressure at top of seal (NumPy array)
        param_bounds = parameter bounds (dict)
    Return:
        seal_controls = (updated) dictionary of seal controls
    """
    # Generate new values as desired by user.
    if seal_controls['interpolate_approach']:

        # Echo status to user
        if alive:
            print("\n --> INTERPOLATING FLUID PROPERTIES.")

        # Compute average pressure top and bottom of seal.
        rows = seal_controls['rows']
        cols = seal_controls['cols']
        base_press_array, dummy = \
            sup.input_reservoir_data(1, rows, cols, param_bounds)
        base_pressure_ave = np.average(base_press_array)
        top_pressure_ave = np.average(press_top)
        seal_controls['base_pressure'] = base_pressure_ave

        # Compute average pressure at center of seal for interpolation.
        ave_pressure = (base_pressure_ave + top_pressure_ave) / 2.0

        # Interpolate data @ average pressure of seal center & reset values.
        seal_controls = \
            interpolate_fluid_properties(seal_controls, ave_pressure)

    # return None


#
##-----------------------------------------------------------------------------
## End of module
