"""
PYTHON MODULE:
    seal_display.py
PURPOSE:
    Functions to write data to files and plots.
REVISION HISTORY:
    2019-05-03  Started work on this module.
    2020-05-01  Beta Version 1.4
FUNCTIONS IN MODULE (20):
    aggregate_results(co2_flow, brine_flow)
    save_csv_results(file_name, data_block, data_title)
    save_sim_list(file_name, sim_listing, title)
    save_npy_results(file_name, data_block)
    cache_grid_results(sim, mass_flow_co2, mass_flow_brine,
                       allow, seal_controls)
    cache_step_results(sim, time_value, sim_title, mass_flow_co2,
                       mass_flow_brine)
    cache_sim_results(sim_numbr, sim_listing, entitle)
    cache_perm(sim_numbr, grid, seal_controls)
    write_model_parameters(zum, seal_controls)
    write_description_parameters(zum, seal_controls)
    write_control_values(zum, seal_controls)
    write_permeability(zum, seal_controls)
    write_relative_perm(zum, seal_controls)
    write_reactivity(zum, seal_controls)
    write_co2_results(zum, sim_flux_list)
    write_summary(seal_controls, sim_flux_list)
    closeout(alive, seal_controls)
    echo_start(alive)
    echo_time_step(desired, alive, current)
    echo_sim_step(alive, sim_step)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    Copyright(c) 2019-2020 by Ernest N. Lindner - All Rights Reserved
-------------------------------------------------------------------------------
"""
import time                                 # For calculating time
from datetime import timedelta              # For calculating runtime
from datetime import datetime
import csv                                  # For saving a list of lists
import numpy as np                          # For array operations

import seal_file as sfile                   # For paths and error reports
import seal_units as sunit                  # For unit conversion

# Constants for file handling
OUT_DIRECTORY = "seal_output"               # Output directory
SUMMARY_FILE = "_seal_summary"              # Summary file name
EXTENSION_TXT = ".txt"                      # File Type of summary file
EXTENSION_CSV = ".csv"                      # File Type for output
EXTENSION_NPY = ".npy"                      # File Type for binary file

PERM_NAME = "Total_Permeability-_Simulation_No._"   # Detailed file name
OUTPUT_NAME = "Seal_Results-Simulation_No._"        # Output file name
BRINE_INTRO = "Brine_Grid_Data_for_Sim_"    # Start - file of brine
CO2_INTRO = "CO2_Grid_Data_for_Sim_"        # Start - file of CO2
LEAD_IN = "\n  --> "                        # Message start
INFO_IN = "    > "                          # Other messages start



def aggregate_results(co2_flow, brine_flow):
    """  Compute cumulative flows of cells and for entire grid for time step.
    Input Variables:
        brine_flow_step = NumPy array of brine flow during step
        co2_flow_step = NumPy array of co2 flow during step
    Returns:
        sum_co2_flow = total delta co2 for grid
        sum_brine_flow = total delta brine flow for grid
    """
    # Compute CO2 flow [= time * CO2 rate].
    sum_co2_flow = np.sum(co2_flow)

    # Compute brine flow [= time * brine_rate].
    sum_brine_flow = np.sum(brine_flow)

    return (sum_co2_flow, sum_brine_flow)


def save_csv_results(file_name, data_block, run_title, data_title):
    """ Store seal output in a *.csv file.
    Input Variables:
        file_name = Name of output file (string)
        data_block = NumPy 2D data array
        data_title = header for data file
    Returns:
        NONE
   """
    # Construct full path for file.
    sub_path, destination = sfile.get_path_name(OUT_DIRECTORY, file_name,
                                                EXTENSION_CSV)

    # Write data - with two header lines.
    overall_title = " > Run:  " + run_title + "\n "
    overall_title += data_title
    try:
        np.savetxt(destination, data_block, fmt="%15.5e", delimiter=",",
                   comments=" ", header=overall_title)
    except OSError as err:
        sfile.io_snag(err, sub_path, file_name)

    # return None


def save_sim_list(file_name, sim_listing, title):
    """ Store seal output in a *.csv file.
    Input Variables:
        file_name = Name of output file (string)
        data_block = NumPy 2D data array
        data_title = header for data file
    Returns:
        NONE
    """
    # Construct full path for file.
    sub_path, destination = sfile.get_path_name(OUT_DIRECTORY, file_name,
                                                EXTENSION_CSV)

    # Define header strings.
    overall_title = "  Run:  " + title
    fieldnames = [" Sim. No.", " Time (yrs)", " CO2 Flow (tonnes)",
                  " Brine Flow (tonnes)"]

    # Write data with 2 header lines.
    try:
        with open(destination, "w", newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow([overall_title])
            writer.writerow(fieldnames)
            for line in sim_listing:
                writer.writerow(line)
    except OSError as err:
        sfile.io_snag(err, sub_path, file_name)

    # return None


def save_npy_results(file_name, data_block):
    """ Store seal output in a NumPy file.
    Input Variables:
        file_name = Name of output file (string)
        data_block = NumPy 2D data array
    Returns:
        NONE
   """
    # Construct full path for file.
    sub_path, destination = sfile.get_path_name(OUT_DIRECTORY, file_name,
                                                EXTENSION_NPY)

    # Write data to path.
    try:
        np.save(destination, data_block)
    except OSError as err:
        sfile.io_snag(err, sub_path, file_name)

    # return None


def cache_grid_results(simulation, mass_flow_co2, mass_flow_brine,
                       allow, seal_controls):
    """ Store NumPy file of grid results from a simulation.
    Input Variables:
        simulation = realization number
        mass_flow_co2 = 2D NumPy array of co2 mass for realization
        mass_flow_brine = 2D NumPy array of brine mass for realization
        allow = stand-alone operation
        seal_controls = dictionary of seal parameters
    Returns:
        NONE
    Note:
        Option to create brine flux file is disabled.
    """
    # Conduct operation only if stand-alone, and contour is desired.
    if allow:
        if seal_controls['plot_co2_contour']:

            # Construct file names.
            insert = str(simulation)

            output1 = CO2_INTRO + insert + EXTENSION_NPY
            output2 = BRINE_INTRO + insert + EXTENSION_NPY

            # --> Store time-variant data for each cell for time X.
            save_npy_results(output1, mass_flow_co2)
            save_npy_results(output2, mass_flow_brine)

    # return None


def cache_step_results(simulation, time_value, sim_title, mass_flow_co2,
                       mass_flow_brine):
    """ Develop file names and control data storage of a time-step.
    Input Variables:
        simulation = realization number
        time_value = time value of step (float)
        sim_title = title of simulation
        mass_flow_co2 = 2D NumPy array of oc2mass for realization
        mass_flow_brine = 2D NumPy array of brine mass for realization
    Returns:
        NONE
    """
    # Construct file names.
    insert = str(simulation) + "_at_Time_" + str(time_value)
    output1 = BRINE_INTRO + insert + EXTENSION_CSV
    output2 = CO2_INTRO + insert + EXTENSION_CSV

    # --> Store time-variant data for each cell for time X.
    title = " CO2 Flow Through Seal (tonnes)"
    save_csv_results(output2, mass_flow_co2, sim_title, title)

    title = " Brine Flow Through Seal (tonnes)"
    save_csv_results(output1, mass_flow_brine, sim_title, title)

    # return None


def cache_sim_results(sim_numbr, sim_listing, entitle):
    """ Develop file names and control the data storage of a simulation.
    Input Variables:
        sim_numbr = realization number
        sim_listing = list of lists of results for a realization
        entitle = title for run
    Returns:
        NONE
    """
    # Construct unique file name for saving a realization.
    current_sim = OUTPUT_NAME + str(sim_numbr)
    output_name = current_sim + EXTENSION_CSV

    # Save List of lists.
    save_sim_list(output_name, sim_listing, entitle)

    # return None


def cache_perm(sim_numbr, grid, seal_controls):
    """ Develop file name and stores permeability values for simulation.
    Input Variables:
        sim_numbr = realization number
        grid = list of cells
        seal_controls = dictionary of seal parameters
    Returns:
        NONE
    """
    # set variables for clarity.
    entitle = seal_controls['title']
    rows = seal_controls['rows']
    cols = seal_controls['cols']

    # Construct unique file name for saving data.
    current_data = PERM_NAME + str(sim_numbr)
    output_name = current_data + EXTENSION_CSV

    # Create perm array from grid results.
    perm_array = np.zeros((rows * cols))    # 1D (flat) array
    for numbr, cell in enumerate(grid):
        perm_array[numbr] = cell.permeability
    perm_array.shape = (rows, cols)         # form to 2D array

    # Create header lines and save results to file - with average.
    perm_ave = np.average(perm_array)
    perm_txt = "{:.2f}".format(perm_ave)
    data_name = "Total Permeability Data (mD) for Each Cell"
    data_name += "   -  Average = " + perm_txt
    save_csv_results(output_name, perm_array, entitle, data_name)

# return None


def write_model_parameters(zum, seal_controls):
    """ Print time model info to file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # Print time stamp for printout.
    print("\n  Seal Flux Summary", file=zum)
    now = datetime.now()
    clump = "  --> Simulation Printout Date: %a %d %b %Y - Time: %H:%M"
    stamp = now.strftime(clump)
    print(stamp, file=zum)

    # Print run details.
    print("\n  > Model Parameters", file=zum)

    print("  --> Analysis Title = {:<}".
          format(seal_controls.get('title')), file=zum)
    print("  --> Start Time of Analysis     = {:<.2f} years".
          format(seal_controls.get('start_time')), file=zum)
    print("  --> End Time of Analysis       = {:<.2f} years".
          format(seal_controls.get('end_time')), file=zum)
    print("  --> Time Steps for Run         = {:<5}".
          format(seal_controls.get('time_points')), file=zum)
    print("  --> Number of Realizations     = {:<5}".
          format(seal_controls.get('realizations')), file=zum)

    if seal_controls.get('time_input'):
        print("  --> < Time Steps Input From File >", file=zum)
    else:
        print("  --> < Uniform Time Steps Assumed >", file=zum)

    # return None


def write_description_parameters(zum, seal_controls):
    """ Print description data to file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    print("\n  > Seal Description and Conditions", file=zum)

    # Print seal descriptors.
    print("  --> Rows in Seal Grid          = {:<5}".
          format(seal_controls.get('rows')), file=zum)
    print("  --> Columns in Seal Grid       = {:<5}".
          format(seal_controls.get('cols')), file=zum)
    print("  --> Average Cell Height        = {:.0f} m".
          format(seal_controls.get('cell_height')), file=zum)
    print("  --> Average Cell Width         = {:.0f} m".
          format(seal_controls.get('cell_width')), file=zum)
    print("  --> Reference Temperature      = {:.0f} oC".
          format(seal_controls.get('temperature')), file=zum)
    print("  --> Reference Salinity         = {:.0f} ppm".
          format(seal_controls.get('salinity')), file=zum)
    print("  --> Reference Depth            = {:.0f} m".
          format(seal_controls.get('ref_depth')), file=zum)

    valu = seal_controls.get('ref_pressure') * sunit.megapascals()
    print("  --> Reference Pressure         = {:.4e} MPa".
          format(valu), file=zum)

    print("  --> Depth to Seal Base         = {:.0f} m".
          format(seal_controls.get('base_depth')), file=zum)

    valu = seal_controls.get('base_pressure') * sunit.megapascals()
    print("  --> Ave. Seal Base Pressure    = {:.4e} MPa".
          format(valu), file=zum)

    # ----------------------------------------
    print("\n  > Fluid Parameters", file=zum)

    # Interpolation Control.
    if seal_controls.get('interpolate_approach'):
        print("  --> < Fluid Properties Interpolated >", file=zum)
    else:
        print("  --> < Fluid Properties Used from Input Values >", file=zum)

    # Print calculated fluid parameters.
    print("  --> CO2 Density                = {:.0f} kg/m3".
          format(seal_controls.get('co2_density')), file=zum)
    print("  --> CO2 Viscosity              = {:.4e} Pa-s".
          format(seal_controls.get('co2_viscosity')), file=zum)
    print("  --> Brine Density              = {:.0f} kg/m3".
          format(seal_controls.get('brine_density')), file=zum)
    print("  --> Brine Viscosity            = {:.4e} Pa-s".
          format(seal_controls.get('brine_viscosity')), file=zum)
    print("  --> CO2 Solubility             = {:.4e} kg/kg".
          format(seal_controls.get('co2_solubility')), file=zum)

    # return None


def write_control_values(zum, seal_controls):
    """ Print controls (boolean parameters) to file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    print("\n  > Seal Approach Parameters", file=zum)

    # Depth Control.
    if seal_controls.get('depth_approach'):
        print("  --> Depth Option: Depth to Repository from File", file=zum)
    else:
        print("  --> Depth Option: All Cells use Repository Depth as Basis",
              file=zum)

    # Area Control.
    if seal_controls.get('area_approach'):
        print("  --> Area Option: Areas Input from File.", file=zum)
    else:
        print("  --> Area Option: All Cells have a Uniform Area = {} m2.".
              format(seal_controls.get('area')), file=zum)

    # Layout Control.
    if seal_controls.get('layout_approach'):
        print("  --> Layout Option: Coordinates Input from File.", file=zum)
    else:
        print("  --> Layout Option: Simple Grid Layout Assumed.", file=zum)

    # Boundary Control.
    if seal_controls.get('upper_approach'):
        print("  --> Top Boundary Option: Input Boundary Conditions "
              + "from File.", file=zum)
    else:
        print("  --> Top Boundary Option: Used Static Pressure for Analysis.",
              file=zum)

    # Active Control.
    if seal_controls.get('active_approach'):
        print("  --> Active Cell Option: Input from File.", file=zum)
    else:
        print("  --> Active Cell Option: All Cells are Active.", file=zum)

    # Influence Control.
    if seal_controls.get('initialize'):
        print("  --> Influence Array Initialized at Start From File.",
              file=zum)
    else:
        print("  --> Influence Array Not Initialized.", file=zum)

    # return None


def write_permeability(zum, seal_controls):
    """ Print permeability data to file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # print permeability variability parameters.
    if seal_controls.get('perm_input_approach'):
        print("  --> Permeability Option: Permeabilities Input from File",
              file=zum)
    elif not seal_controls.get('vary_perm_choice'):
        print("\n  > Uniform Permeability - Set to Mean Value", file=zum)
        print("  --> Mean Total Permeability    = {:<.3f} microdarcy".
              format(seal_controls.get('perm_mean')), file=zum)
    else:
        print("    --> Computed with Censored Log-normal Distribution",
              file=zum)
        print("  --> Mean Total Permeability    = {:<.3f} microdarcy".
              format(seal_controls.get('perm_mean')), file=zum)
        print("  --> Std. Dev. Permeability     = {:<.4f} microdarcy".
              format(seal_controls.get('perm_std')), file=zum)
        print("  --> Minimum Total Permeability = {:.3f} microdarcy".
              format(seal_controls.get('perm_min')), file=zum)
        print("  --> Maximum Total Permeability = {:.3f} microdarcy".
              format(seal_controls.get('perm_max')), file=zum)

    # Heterogeneity response.
    if (seal_controls.get('perm_input_approach') and
            seal_controls.get('heterogeneity_approach')):
        print("  --> Heterogeneity Option: Included.", file=zum)
        print("  --> Heterogeneity Factor       = {:.3f}".
              format(seal_controls.get('perm_heter_factor')), file=zum)
    else:
        print("  --> Heterogeneity Option: NOT Included.", file=zum)

    # return None


def write_relative_perm(zum, seal_controls):
    """ Print relative permeability controls to a file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # print relative permeability parameters.
    print("\n  > Relative Permeability Parameters", file=zum)

    # Model type.
    m_type = seal_controls.get('relative_model')
    if "LET" in m_type:
        print("  --> Relative Permeability = L-E-T Model",
              file=zum)
    else:
        print("  --> Relative Permeability ="  +
              "Brooks-Corey Model", file=zum)

    # Print limits on Relative Permeability Model.
    print("  --> Residual Brine Saturation  = {:.2f}".
          format(seal_controls.get('resid_brine')), file=zum)
    print("  --> Residual CO2 Saturation    = {:.2f}".
          format(seal_controls.get('resid_co2')), file=zum)
    print("  --> Nonwetting/Wetting Ratio   = {:.2f}".
          format(seal_controls.get('perm_ratio')), file=zum)

    # LET & BC relative permeability parameters.
    if 'LET' in seal_controls.get('relative_model'):
        print("  --> 'L' Wetting Parameter      = {:.2f}".
              format(seal_controls.get('l_wetting')), file=zum)
        print("  --> 'E' Wetting Parameter      = {:.2f}".
              format(seal_controls.get('e_wetting')), file=zum)
        print("  --> 'T' Wetting Parameter      = {:.2f}".
              format(seal_controls.get('t_wetting')), file=zum)
        print("  --> 'L' Nonwetting Parameter   = {:.2f}".
              format(seal_controls.get('l_nonwet')), file=zum)
        print("  --> 'E' Nonwetting Parameter   = {:.2f}".
              format(seal_controls.get('e_nonwet')), file=zum)
        print("  --> 'T' Nonwetting Parameter   = {:.2f}".
              format(seal_controls.get('t_nonwet')), file=zum)
    else:
        print("  --> Lambda Parameter for BC    = {:.2f}".
              format(seal_controls.get('zeta')), file=zum)

    # ----------------------------------------
    print("\n  > Capillary Pressure Parameters", file=zum)
    # Capillary pressure parameters - in <MPa>.
    cap_pressure = seal_controls.get('entry_pressure')
    cap_pressure *= sunit.megapascals()   # Convert values in Pa to MPa
    print("  --> Ave. Threshold Pressure    = {:.4e} MPa".
          format(cap_pressure), file=zum)

    # --> LET model - capillary parameters.
    if 'LET' in seal_controls.get('relative_model'):
        print("  --> 'L' Capillary Parameter    = {:.2f}".
              format(seal_controls.get('l_capillary')), file=zum)
        print("  --> 'E' Capillary Parameter    = {:.2f}".
              format(seal_controls.get('e_capillary')), file=zum)
        print("  --> 'T' Capillary Parameter    = {:.2f}".
              format(seal_controls.get('t_capillary')), file=zum)

        max_press = seal_controls.get('max_capillary')
        max_press *= sunit.megapascals()   # Convert values to <MPa>
        print("  --> Maximum Capillary Pressure  = {:.4e} MPa".
              format(max_press), file=zum)

    # return None


def write_reactivity(zum, seal_controls):
    """ Print reactivity parameters to file named "zum."
    Input Variables:
        zum = file label
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # Print reactivity and thickness parameters.
    print("\n  > Reactivity & Alteration Parameters", file=zum)
    time_dependent_model = seal_controls.get('model')
    print("  --> Time-Dependent Model       = {:1d}".
          format(time_dependent_model), file=zum)

    # Material parameters - if model > 0.
    if time_dependent_model > 0:
        print("  --> Time Rate of Change        = {:.4f}".
              format(seal_controls.get('rate_effect')), file=zum)
        print("  --> Total Effect of Change     = {:.4f}".
              format(seal_controls.get('total_effect')), file=zum)
        print("  --> Seal Reactivity            = {:.2f}".
              format(seal_controls.get('reactivity')), file=zum)
        print("  --> Total Clay Content         = {:.2f}%".
              format(seal_controls.get('clay_content')), file=zum)
        print("  --> Total Carbonate            = {:.2f}%".
              format(seal_controls.get('carbonate_content')), file=zum)
        print("  --> Clay Type                  = {:<}".
              format(seal_controls.get('clay_type')), file=zum)

   # Thickness variables - tailor response to selection.
    print("\n  > Seal Thickness", file=zum)
    if seal_controls.get('thickness_approach'):
        print("  --> Thickness Option: Thickness Values from File", file=zum)
    else:
        print("  --> Thickness Option: Compute Thickness", file=zum)
        print("    --> Method: Truncated Normal Distribution", file=zum)
        print("  --> Mean Thickness             = {:.1f} m".
              format(seal_controls.get('thickness_ave')), file=zum)
        print("  --> Thickness Std. Dev.        = {:.1f} m".
              format(seal_controls.get('thickness_std')), file=zum)
        print("  --> Minimum Thickness          = {:.1f} m".
              format(seal_controls.get('thickness_min')), file=zum)
        print("  --> Maximum Thickness          = {:.1f} m".
              format(seal_controls.get('thickness_max')), file=zum)

    # return None


def write_co2_results(zum, sim_flux_list):
    """ Compute and print results of simulations to file "zum."
    Input Variables:
        zum = file label
        sim_flux_list = list of CO2 results at the end of simulations
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # Convert list to array.
    simulation_number = len(sim_flux_list)
    flux_data = np.asarray(sim_flux_list)

    # compute statistics of the array.
    ave_flux_data = np.mean(flux_data)
    std_dev = np.std(flux_data)
    min_flux_data = np.amin(flux_data)
    max_flux_data = np.amax(flux_data)
    argument_min = np.argmin(flux_data)
    argument_max = np.argmax(flux_data)

    # print separator.
    print("\n  ", end='', file=zum)
    print("*" * 60, file=zum)

    # print statistics.
    print("\n  > Summary of Results", file=zum)
    print("  --> Number of Simulations               = {:,}".
          format(simulation_number), file=zum)
    print("  --> Average CO2 Flux of All Simulations = {:.3E} tonne".
          format(ave_flux_data), file=zum)
    print("  --> Standard Deviation of CO2 flux =      {:.3E} tonne".
          format(std_dev), file=zum)
    print("  --> Minimum CO2 Flux of All Simulations = {:<.3E} tonne".
          format(min_flux_data), file=zum)
    print("       - at Index =      {:}".format(argument_min),
          file=zum)
    print("  --> Maximum CO2 Flux of All Simulations = {:.3E} tonne".
          format(max_flux_data), file=zum)
    print("       - at Index =      {:}".format(argument_max),
          file=zum)

    # return None


def write_summary(seal_controls, sim_flux_list):
    """  Control the printout of summary data to file.
    Input Variables:
        seal_controls = dictionary of seal parameters
        list of CO2 flux at end of simulations
    Returns:
        None
    Note:
        SUMMARY_FILE = Defined destination file name
    """
    # Construct full path to results directory and summary file.
    #  -- Check for extension "txt".
    sub_path, destination = sfile.get_path_name(OUT_DIRECTORY, SUMMARY_FILE,
                                                EXTENSION_TXT)
    # Write  information to file on seal run.
    try:
        with open(destination, 'w') as zum:
            write_model_parameters(zum, seal_controls)
            write_description_parameters(zum, seal_controls)
            write_control_values(zum, seal_controls)
            write_permeability(zum, seal_controls)
            write_relative_perm(zum, seal_controls)
            write_reactivity(zum, seal_controls)
            write_co2_results(zum, sim_flux_list)
    except OSError as err:
        sfile.io_snag(err, sub_path, SUMMARY_FILE)

    # return None


def closeout(alive, seal_controls):
    """  Write time information to console.
    Input Variables:
        alive = flag to indicate stand-alone operation
        seal_controls = dictionary of seal parameters
    Returns:
        None
    """
    # Compute run time and echo status to user.
    if alive:
        # Stop clock on runtime.
        end_code = time.monotonic()

        # Echo end.
        print(LEAD_IN + "COMPLETED ANALYSIS COMPUTATIONS.")

        # Echo execution time.
        start_code = seal_controls['code_start']
        elapsed_time = timedelta(seconds=(end_code - start_code))
        print(INFO_IN + "Analysis Execution Time = {}".format(elapsed_time))

    # return None


def echo_status(alive, phase):
    """  Echo progress to console.
    Input Variables:
        alive = flag to indicate stand-alone operation
        phase = index position in program
    Returns:
        None
    """
    # Echo status to user.
    if alive:
        if phase == 0:
            print(LEAD_IN + "READING CONTROL FILE.")
        elif phase == 1:
            print(LEAD_IN + "DELETING OLD FILES & PERFORMING INPUT CHECK.")
        elif phase == 2:
            print(LEAD_IN + "INITIALIZING PARAMETERS.")
        elif phase == 3:
            print(LEAD_IN + "STARTING SEAL COMPUTATIONS.")
        elif phase == 4:
            print(LEAD_IN + "CREATED SEAL SUMMARY FILE. ")
        else:
            print("\n Code Error in Echo!")

    # return None


def echo_time_step(desired, alive, current):
    """  Echo time step progress to console.
    Input Variables:
        desired = flag to indicate header is to be printed
        alive = stand-alone operation
        current = current time step
    Returns:
        None
    """
    # Echo progress line to user on console.
    if desired and alive:
        print("    -->> Time Step = {}".format(current))

    # return None


def echo_sim_step(alive, sim_step):
    """  Echo simulation progress to console.
    Input Variables:
        alive = stand-alone operation
        sim_step = current simulation step
    Returns:
        None
    """
    # Echo progress line to user on console.
    if alive:
        print("  --> Realization Loop = {}".format(sim_step + 1))

    # return None


#
##-----------------------------------------------------------------------------
## End of module
